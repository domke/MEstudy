 direct: atlas -> {'elpa', 'hiop', 'bohrium', 'nektar', 'ceres-solver', 'gmt', 'casacore', 'cardioid', 'hypre', 'strumpack', 'xhmm', 'moab', 'sirius', 'blaspp', 'openmx', 'openfast', 'lammps', 'cosma', 'hydrogen', 'shtools', 'bart', 'aspa', 'harminv', 'meep', 'opium', 'openmolcas', 'elsd', 'grass', 'py-numpy', 'root', 'trilinos', 'dbcsr', 'kokkos-kernels', 'libcint', 'cminpack', 'phist', 'abinit', 'py-cvxopt', 'atompaw', 'frontistr', 'qmcpack', 'dealii', 'mxnet', 'py-gpaw', 'elmerfem', 'py-scipy', 'pagmo', 'gslib', 'armadillo', 'mumps', 'superlu-mt', 'libflame', 'sundials', 'lazyten', 'elemental', 'qrupdate', 'yambo', 'octopus', 'steps', 'ermod', 'psi4', 'py-theano', 'thornado-mini', 'cantera', 'hpl', 'linsys-v', 'arpack-ng', 'miniqmc', 'mfem', 'librom', 'exasp2', 'gmsh', 'cp2k', 'aoflagger', 'ipopt', 'latte', 'superlu-dist', 'petsc', 'wannier90', 'py-torch', 'amp', 'ghost', 'coevp', 'scs', 'planck-likelihood', 'cbench', 'elsdc', 'ntpoly', 'suite-sparse', 'opencv', 'butterflypack', 'sw4lite', 'plumed', 'dakota', 'siesta', 'portage', 'kaldi', 'nekcem', 'globalarrays', 'draco', 'elk', 'qbox', 'tasmanian', 'netlib-scalapack', 'chombo', 'bml', 'netlib-lapack', 'csdp', 'clapack', 'plink', 'caffe', 'plasma', 'esmf', 'dhpmm-f', 'coinhsl', 'dsdp', 'jags', 'elsi', 'plink-ng', 'magma', 'octave', 'akantu', 'r', 'nwchem', 'superlu', 'cblas', 'quantum-espresso', 'gsl'}
 check: elpa
. direct: elpa -> {'cp2k', 'elsi', 'quantum-espresso', 'sirius'}
. check: cp2k
.. direct: cp2k -> set()
. check: elsi
.. direct: elsi -> set()
. check: quantum-espresso
.. direct: quantum-espresso -> set()
. check: sirius
.. direct: sirius -> {'cp2k'}
.. check: cp2k
 check: hiop
. direct: hiop -> set()
 check: bohrium
. direct: bohrium -> set()
 check: nektar
. direct: nektar -> set()
 check: ceres-solver
. direct: ceres-solver -> {'votca-xtp'}
. check: votca-xtp
.. direct: votca-xtp -> set()
 check: gmt
. direct: gmt -> {'hc', 'fstrack', 'gmtsar'}
. check: hc
.. direct: hc -> {'citcoms'}
.. check: citcoms
... direct: citcoms -> set()
. check: fstrack
.. direct: fstrack -> set()
. check: gmtsar
.. direct: gmtsar -> set()
 check: casacore
. direct: casacore -> {'aoflagger', 'chgcentre', 'dysco'}
. check: aoflagger
.. direct: aoflagger -> {'cotter'}
.. check: cotter
... direct: cotter -> set()
. check: chgcentre
.. direct: chgcentre -> set()
. check: dysco
.. direct: dysco -> set()
 check: cardioid
. direct: cardioid -> set()
 check: hypre
. direct: hypre -> {'quinoa', 'nalu-wind', 'xsdktrilinos', 'trilinos', 'petsc', 'fastmath', 'xsdk', 'ceed', 'mfem', 'elmerfem', 'sundials'}
. check: quinoa
.. direct: quinoa -> set()
. check: nalu-wind
.. direct: nalu-wind -> set()
. check: xsdktrilinos
.. direct: xsdktrilinos -> set()
. check: trilinos
.. direct: trilinos -> {'amp', 'quinoa', 'frontistr', 'nalu', 'nalu-wind', 'omega-h', 'xsdktrilinos', 'camellia', 'albany', 'percept', 'fastmath', 'fenics', 'petsc', 'xsdk', 'dealii', 'phist', 'elmerfem', 'sundials'}
.. check: amp
... direct: amp -> set()
.. check: quinoa
.. check: frontistr
... direct: frontistr -> set()
.. check: nalu
... direct: nalu -> set()
.. check: nalu-wind
.. check: omega-h
... direct: omega-h -> {'xsdk'}
... check: xsdk
.... direct: xsdk -> {'xsdk-examples'}
.... check: xsdk-examples
..... direct: xsdk-examples -> set()
.. check: xsdktrilinos
.. check: camellia
... direct: camellia -> set()
.. check: albany
... direct: albany -> set()
.. check: percept
... direct: percept -> set()
.. check: fastmath
... direct: fastmath -> set()
.. check: fenics
... direct: fenics -> set()
.. check: petsc
... direct: petsc -> {'gmsh', 'slepc', 'fenics', 'dealii', 'amp', 'xsdktrilinos', 'alquimia', 'fastmath', 'pism', 'xsdk', 'libmesh', 'sundials', 'steps', 'elsi', 'precice', 'ceed', 'akantu', 'mofem-cephas', 'hpgmg', 'py-petsc4py', 'phist', 'mfem', 'pflotran'}
... check: gmsh
.... direct: gmsh -> {'dealii', 'omega-h'}
.... check: dealii
..... direct: dealii -> {'xsdk', 'aspect', 'dftfe'}
..... check: xsdk
..... check: aspect
...... direct: aspect -> set()
..... check: dftfe
...... direct: dftfe -> set()
.... check: omega-h
... check: slepc
.... direct: slepc -> {'py-slepc4py', 'libmesh', 'mofem-cephas', 'elsi', 'fenics', 'xsdk', 'dealii', 'gmsh'}
.... check: py-slepc4py
..... direct: py-slepc4py -> set()
.... check: libmesh
..... direct: libmesh -> set()
.... check: mofem-cephas
..... direct: mofem-cephas -> {'mofem-minimal-surface-equation', 'mofem-fracture-module', 'mofem-users-modules'}
..... check: mofem-minimal-surface-equation
...... direct: mofem-minimal-surface-equation -> set()
..... check: mofem-fracture-module
...... direct: mofem-fracture-module -> set()
..... check: mofem-users-modules
...... direct: mofem-users-modules -> {'mofem-minimal-surface-equation', 'mofem-fracture-module'}
...... check: mofem-minimal-surface-equation
...... check: mofem-fracture-module
.... check: elsi
.... check: fenics
.... check: xsdk
.... check: dealii
.... check: gmsh
... check: fenics
... check: dealii
... check: amp
... check: xsdktrilinos
... check: alquimia
.... direct: alquimia -> {'xsdk'}
.... check: xsdk
... check: fastmath
... check: pism
.... direct: pism -> set()
... check: xsdk
... check: libmesh
... check: sundials
.... direct: sundials -> {'amrex', 'cantera', 'fastmath', 'xsdk', 'dealii', 'mfem'}
.... check: amrex
..... direct: amrex -> {'fastmath', 'xsdk'}
..... check: fastmath
..... check: xsdk
.... check: cantera
..... direct: cantera -> set()
.... check: fastmath
.... check: xsdk
.... check: dealii
.... check: mfem
..... direct: mfem -> {'axom', 'laghos', 'glvis', 'remhos', 'ascent', 'xsdk', 'ceed', 'cardioid'}
..... check: axom
...... direct: axom -> set()
..... check: laghos
...... direct: laghos -> {'ceed', 'ecp-proxy-apps'}
...... check: ceed
....... direct: ceed -> set()
...... check: ecp-proxy-apps
....... direct: ecp-proxy-apps -> set()
..... check: glvis
...... direct: glvis -> set()
..... check: remhos
...... direct: remhos -> {'ceed'}
...... check: ceed
..... check: ascent
...... direct: ascent -> {'ecp-viz-sdk', 'warpx'}
...... check: ecp-viz-sdk
....... direct: ecp-viz-sdk -> set()
...... check: warpx
....... direct: warpx -> set()
..... check: xsdk
..... check: ceed
..... check: cardioid
... check: steps
.... direct: steps -> set()
... check: elsi
... check: precice
.... direct: precice -> {'of-precice', 'xsdk'}
.... check: of-precice
..... direct: of-precice -> set()
.... check: xsdk
... check: ceed
... check: akantu
.... direct: akantu -> set()
... check: mofem-cephas
... check: hpgmg
.... direct: hpgmg -> {'ceed'}
.... check: ceed
... check: py-petsc4py
.... direct: py-petsc4py -> {'xsdk', 'py-libensemble', 'py-slepc4py', 'py-sfepy'}
.... check: xsdk
.... check: py-libensemble
..... direct: py-libensemble -> {'xsdk'}
..... check: xsdk
.... check: py-slepc4py
.... check: py-sfepy
..... direct: py-sfepy -> set()
... check: phist
.... direct: phist -> {'xsdk'}
.... check: xsdk
... check: mfem
... check: pflotran
.... direct: pflotran -> {'xsdk', 'alquimia'}
.... check: xsdk
.... check: alquimia
.. check: xsdk
.. check: dealii
.. check: phist
.. check: elmerfem
... direct: elmerfem -> set()
.. check: sundials
. check: petsc
. check: fastmath
. check: xsdk
. check: ceed
. check: mfem
. check: elmerfem
. check: sundials
 check: strumpack
. direct: strumpack -> {'mfem', 'xsdk'}
. check: mfem
. check: xsdk
 check: xhmm
. direct: xhmm -> set()
 check: moab
. direct: moab -> {'mofem-cephas', 'meshkit', 'camellia'}
. check: mofem-cephas
. check: meshkit
.. direct: meshkit -> set()
. check: camellia
 check: sirius
 check: blaspp
. direct: blaspp -> {'lapackpp', 'warpx'}
. check: lapackpp
.. direct: lapackpp -> {'warpx'}
.. check: warpx
. check: warpx
 check: openmx
. direct: openmx -> set()
 check: openfast
. direct: openfast -> {'nalu-wind'}
. check: nalu-wind
 check: lammps
. direct: lammps -> {'parsplice'}
. check: parsplice
.. direct: parsplice -> set()
 check: cosma
. direct: cosma -> {'cp2k'}
. check: cp2k
 check: hydrogen
. direct: hydrogen -> {'lbann'}
. check: lbann
.. direct: lbann -> set()
 check: shtools
. direct: shtools -> set()
 check: bart
. direct: bart -> set()
 check: aspa
. direct: aspa -> set()
 check: harminv
. direct: harminv -> {'meep'}
. check: meep
.. direct: meep -> {'py-python-meep'}
.. check: py-python-meep
... direct: py-python-meep -> set()
 check: meep
 check: opium
. direct: opium -> set()
 check: openmolcas
. direct: openmolcas -> set()
 check: elsd
. direct: elsd -> set()
 check: grass
. direct: grass -> {'qgis'}
. check: qgis
.. direct: qgis -> set()
 check: py-numpy
. direct: py-numpy -> {'py-cogent', 'py-basemap', 'py-bokeh', 'py-cartopy', 'bohrium', 'nlopt', 'py-espressopp', 'py-methylcode', 'py-torchvision', 'py-nc-time-axis', 'py-numexpr', 'py-brian', 'py-picrust', 'minigan', 'py-pywcs', 'py-wcsaxes', 'casacore', 'py-pyface', 'py-tables', 'simulationio', 'py-multiqc', 'nrm', 'sparta', 'py-pynio', 'py-statsmodels', 'py-pyquaternion', 'py-snuggs', 'sirius', 'py-cftime', 'ibmisc', 'xtensor-python', 'py-yahmm', 'py-astropy', 'py-mo-pack', 'py-onnx', 'sensei', 'memsurfer', 'py-illumina-utils', 'py-yt', 'py-openpmd-validator', 'bart', 'py-pymc3', 'py-adios', 'py-arviz', 'ascent', 'py-dlcpar', 'py-matplotlib', 'py-rnacocktail', 'sz', 'nest', 'py-fparser', 'py-thinc', 'partitionfinder', 'trinity', 'py-stratify', 'xcfun', 'py-syned', 'root', 'py-scikit-learn', 'trilinos', 'libcint', 'py-petsc4py', 'lbann', 'py-ecos', 'py-jpype1', 'py-librosa', 'adios2', 'gdal', 'py-lazyarray', 'py-zarr', 'py-edffile', 'py-wxpython', 'py-traitsui', 'py-pypar', 'py-pygdal', 'py-scikit-optimize', 'py-osqp', 'py-gluoncv', 'py-vcf-kit', 'qmcpack', 'arrow', 'plplot', 'mxnet', 'py-biopandas', 'py-gpaw', 'py-pymatgen', 'py-phonopy', 'py-scipy', 'py-h5py', 'eccodes', 'py-pygpu', 'py-umi-tools', 'openimageio', 'ont-albacore', 'py-espresso', 'trilinos-catalyst-ioss-adapter', 'py-soundfile', 'vigra', 'py-dask', 'py-spefile', 'py-goatools', 'py-macs2', 'py-projectq', 'py-jplephem', 'py-merlin', 'py-numba', 'py-guiqwt', 'hoomd-blue', 'py-scs', 'py-pipits', 'py-resampy', 'py-periodictable', 'py-hatchet', 'py-pycbc', 'py-pyfftw', 'py-pyfasta', 'py-biom-format', 'psi4', 'py-bottleneck', 'py-fastcluster', 'py-faststructure', 'py-petastorm', 'openpmd-api', 'py-weave', 'py-patsy', 'py-theano', 'cantera', 'asdf-cxx', 'py-localcider', 'py-python-meep', 'py-torchsummary', 'py-transformers', 'conduit', 'find-circ', 'py-emcee', 'py-spatialist', 'cp2k', 'py-cinema-lib', 'py-rseqc', 'py-seaborn', 'py-pywavelets', 'py-chainer', 'py-wradlib', 'py-misopy', 'py-crossmap', 'py-symfit', 'py-svgpathtools', 'py-torch', 'py-psyclone', 'py-pybedtools', 'py-spglib', 'py-mayavi', 'py-libensemble', 'py-neo', 'py-cyvcf2', 'py-numexpr3', 'py-pomegranate', 'py-crispresso', 'opencv', 'py-abipy', 'py-elephant', 'py-pyrad', 'py-pyugrid', 'py-scientificpython', 'py-colormath', 'py-pandas', 'py-griddataformats', 'py-iminuit', 'py-pycuda', 'py-ase', 'py-srsly', 'py-rasterio', 'py-cdat-lite', 'py-tifffile', 'boost', 'py-nestle', 'py-wxmplot', 'sgpp', 'mrtrix3', 'py-cnvkit', 'py-dgl', 'py-checkm-genome', 'py-opentuner', 'py-gensim', 'py-mg-rast-tools', 'py-biopython', 'geopm', 'py-lmfit', 'py-tomopy', 'py-weblogo', 'py-asdf', 'py-numcodecs', 'cmor', 'py-xarray', 'tasmanian', 'timemory', 'mefit', 'py-fits-tools', 'py-pyfits', 'py-sfepy', 'openmm', 'py-hpcbench', 'py-kmodes', 'py-opppy', 'py-mdanalysis', 'py-gnuplot', 'fenics', 'py-pytools', 'py-scikit-image', 'py-tensorboardx', 'py-pybigwig', 'cosmomc', 'caffe', 'grib-api', 'flann', 'py-tensorboard', 'py-spacy', 'gdl', 'py-modred', 'catalyst', 'py-pyarrow', 'pism', 'py-colorpy', 'redundans', 'rmats', 'py-tensorflow', 'py-blis', 'py-deeptools', 'py-blosc', 'py-dxchange', 'py-pymol', 'py-pynn', 'py-shapely', 'py-sncosmo', 'py-quantities', 'py-tuiview', 'py-bx-python', 'magics', 'py-thirdorder', 'py-imageio', 'py-mmcv', 'precice', 'py-cf-units', 'py-horovod', 'akantu', 'py-brian2', 'py-pyrosar', 'hic-pro', 'paraview', 'py-htseq', 'py-mlxtend', 'py-ont-fast5-api', 'py-cclib', 'py-torchtext', 'py-h5sh', 'py-pythonqwt', 'py-lscsoft-glue', 'py-opt-einsum', 'py-cvxpy', 'py-wub', 'py-netcdf4'}
. check: py-cogent
.. direct: py-cogent -> {'py-picrust'}
.. check: py-picrust
... direct: py-picrust -> set()
. check: py-basemap
.. direct: py-basemap -> set()
. check: py-bokeh
.. direct: py-bokeh -> {'py-dask'}
.. check: py-dask
... direct: py-dask -> {'py-scikit-image'}
... check: py-scikit-image
.... direct: py-scikit-image -> {'py-astropy', 'py-cinema-lib', 'py-tomopy'}
.... check: py-astropy
..... direct: py-astropy -> {'py-dxchange', 'py-sncosmo', 'py-yt', 'py-pycbc', 'py-pywcs', 'py-fits-tools', 'py-wcsaxes'}
..... check: py-dxchange
...... direct: py-dxchange -> {'py-tomopy'}
...... check: py-tomopy
....... direct: py-tomopy -> set()
..... check: py-sncosmo
...... direct: py-sncosmo -> set()
..... check: py-yt
...... direct: py-yt -> set()
..... check: py-pycbc
...... direct: py-pycbc -> set()
..... check: py-pywcs
...... direct: py-pywcs -> set()
..... check: py-fits-tools
...... direct: py-fits-tools -> set()
..... check: py-wcsaxes
...... direct: py-wcsaxes -> set()
.... check: py-cinema-lib
..... direct: py-cinema-lib -> set()
.... check: py-tomopy
. check: py-cartopy
.. direct: py-cartopy -> set()
. check: bohrium
. check: nlopt
.. direct: nlopt -> {'py-libensemble', 'r-nloptr', 'pktools'}
.. check: py-libensemble
.. check: r-nloptr
... direct: r-nloptr -> {'r-corhmm', 'r-lme4', 'r-ptw'}
... check: r-corhmm
.... direct: r-corhmm -> set()
... check: r-lme4
.... direct: r-lme4 -> {'r-pbkrtest', 'r-car', 'r-jomo'}
.... check: r-pbkrtest
..... direct: r-pbkrtest -> {'r-car'}
..... check: r-car
...... direct: r-car -> {'r-aer', 'r-caret', 'r-factominer'}
...... check: r-aer
....... direct: r-aer -> set()
...... check: r-caret
....... direct: r-caret -> {'r-seurat', 'r-adabag'}
....... check: r-seurat
........ direct: r-seurat -> set()
....... check: r-adabag
........ direct: r-adabag -> {'r-rminer'}
........ check: r-rminer
......... direct: r-rminer -> {'r-condop'}
......... check: r-condop
.......... direct: r-condop -> set()
...... check: r-factominer
....... direct: r-factominer -> {'r-factoextra'}
....... check: r-factoextra
........ direct: r-factoextra -> set()
.... check: r-car
.... check: r-jomo
..... direct: r-jomo -> {'r-mitml'}
..... check: r-mitml
...... direct: r-mitml -> {'r-mice'}
...... check: r-mice
....... direct: r-mice -> set()
... check: r-ptw
.... direct: r-ptw -> {'r-alsace'}
.... check: r-alsace
..... direct: r-alsace -> set()
.. check: pktools
... direct: pktools -> set()
. check: py-espressopp
.. direct: py-espressopp -> set()
. check: py-methylcode
.. direct: py-methylcode -> set()
. check: py-torchvision
.. direct: py-torchvision -> {'minigan', 'py-horovod'}
.. check: minigan
... direct: minigan -> set()
.. check: py-horovod
... direct: py-horovod -> {'minigan'}
... check: minigan
. check: py-nc-time-axis
.. direct: py-nc-time-axis -> set()
. check: py-numexpr
.. direct: py-numexpr -> {'py-pandas', 'py-tables'}
.. check: py-pandas
... direct: py-pandas -> {'py-seaborn', 'py-biopandas', 'cosmomc', 'py-pybedtools', 'py-umi-tools', 'py-geeup', 'py-pyani', 'py-pyarrow', 'py-crispresso', 'py-abipy', 'py-dask', 'py-elephant', 'py-goatools', 'py-statsmodels', 'py-astropy', 'portcullis', 'py-pipits', 'py-pymc3', 'py-hatchet', 'py-arviz', 'py-cnvkit', 'py-biom-format', 'py-petastorm', 'py-pauvre', 'py-mlxtend', 'partitionfinder', 'geopm', 'py-scikit-learn', 'py-xarray', 'lbann', 'py-geopandas', 'py-rnacocktail', 'py-wub'}
... check: py-seaborn
.... direct: py-seaborn -> {'py-mdanalysis', 'py-pyani', 'py-crispresso', 'py-abipy', 'py-wub'}
.... check: py-mdanalysis
..... direct: py-mdanalysis -> {'candle-benchmarks'}
..... check: candle-benchmarks
...... direct: candle-benchmarks -> {'ecp-proxy-apps'}
...... check: ecp-proxy-apps
.... check: py-pyani
..... direct: py-pyani -> set()
.... check: py-crispresso
..... direct: py-crispresso -> set()
.... check: py-abipy
..... direct: py-abipy -> set()
.... check: py-wub
..... direct: py-wub -> set()
... check: py-biopandas
.... direct: py-biopandas -> set()
... check: cosmomc
.... direct: cosmomc -> set()
... check: py-pybedtools
.... direct: py-pybedtools -> {'py-metasv', 'py-rnacocktail'}
.... check: py-metasv
..... direct: py-metasv -> set()
.... check: py-rnacocktail
..... direct: py-rnacocktail -> set()
... check: py-umi-tools
.... direct: py-umi-tools -> set()
... check: py-geeup
.... direct: py-geeup -> set()
... check: py-pyani
... check: py-pyarrow
.... direct: py-pyarrow -> {'py-pandas', 'py-horovod', 'py-petastorm'}
.... check: py-pandas
.... check: py-horovod
.... check: py-petastorm
..... direct: py-petastorm -> {'py-horovod'}
..... check: py-horovod
... check: py-crispresso
... check: py-abipy
... check: py-dask
... check: py-elephant
.... direct: py-elephant -> set()
... check: py-goatools
.... direct: py-goatools -> set()
... check: py-statsmodels
.... direct: py-statsmodels -> {'py-wub', 'py-goatools'}
.... check: py-wub
.... check: py-goatools
... check: py-astropy
... check: portcullis
.... direct: portcullis -> set()
... check: py-pipits
.... direct: py-pipits -> set()
... check: py-pymc3
.... direct: py-pymc3 -> set()
... check: py-hatchet
.... direct: py-hatchet -> set()
... check: py-arviz
.... direct: py-arviz -> {'py-pymc3'}
.... check: py-pymc3
... check: py-cnvkit
.... direct: py-cnvkit -> set()
... check: py-biom-format
.... direct: py-biom-format -> {'py-pipits', 'py-picrust'}
.... check: py-pipits
.... check: py-picrust
... check: py-petastorm
... check: py-pauvre
.... direct: py-pauvre -> set()
... check: py-mlxtend
.... direct: py-mlxtend -> set()
... check: partitionfinder
.... direct: partitionfinder -> set()
... check: geopm
.... direct: geopm -> set()
... check: py-scikit-learn
.... direct: py-scikit-learn -> {'py-kmodes', 'candle-benchmarks', 'partitionfinder', 'py-mlxtend', 'tppred', 'py-scikit-optimize', 'py-librosa', 'py-ytopt'}
.... check: py-kmodes
..... direct: py-kmodes -> set()
.... check: candle-benchmarks
.... check: partitionfinder
.... check: py-mlxtend
.... check: tppred
..... direct: tppred -> set()
.... check: py-scikit-optimize
..... direct: py-scikit-optimize -> {'py-ytopt'}
..... check: py-ytopt
...... direct: py-ytopt -> set()
.... check: py-librosa
..... direct: py-librosa -> set()
.... check: py-ytopt
... check: py-xarray
.... direct: py-xarray -> {'py-arviz', 'py-wradlib'}
.... check: py-arviz
.... check: py-wradlib
..... direct: py-wradlib -> set()
... check: lbann
... check: py-geopandas
.... direct: py-geopandas -> set()
... check: py-rnacocktail
... check: py-wub
.. check: py-tables
... direct: py-tables -> {'geopm', 'partitionfinder', 'py-sfepy'}
... check: geopm
... check: partitionfinder
... check: py-sfepy
. check: py-brian
.. direct: py-brian -> set()
. check: py-picrust
. check: minigan
. check: py-pywcs
. check: py-wcsaxes
. check: casacore
. check: py-pyface
.. direct: py-pyface -> {'py-traitsui', 'py-mayavi'}
.. check: py-traitsui
... direct: py-traitsui -> {'py-mayavi', 'py-apptools'}
... check: py-mayavi
.... direct: py-mayavi -> set()
... check: py-apptools
.... direct: py-apptools -> {'py-envisage', 'py-mayavi'}
.... check: py-envisage
..... direct: py-envisage -> {'py-mayavi'}
..... check: py-mayavi
.... check: py-mayavi
.. check: py-mayavi
. check: py-tables
. check: simulationio
.. direct: simulationio -> set()
. check: py-multiqc
.. direct: py-multiqc -> set()
. check: nrm
.. direct: nrm -> set()
. check: sparta
.. direct: sparta -> set()
. check: py-pynio
.. direct: py-pynio -> set()
. check: py-statsmodels
. check: py-pyquaternion
.. direct: py-pyquaternion -> set()
. check: py-snuggs
.. direct: py-snuggs -> {'py-rasterio'}
.. check: py-rasterio
... direct: py-rasterio -> set()
. check: sirius
. check: py-cftime
.. direct: py-cftime -> {'py-cf-units', 'py-netcdf4', 'py-nc-time-axis'}
.. check: py-cf-units
... direct: py-cf-units -> set()
.. check: py-netcdf4
... direct: py-netcdf4 -> {'py-dxchange', 'py-mdanalysis', 'py-cdo', 'py-wradlib', 'py-pyugrid', 'py-abipy', 'py-arviz'}
... check: py-dxchange
... check: py-mdanalysis
... check: py-cdo
.... direct: py-cdo -> set()
... check: py-wradlib
... check: py-pyugrid
.... direct: py-pyugrid -> set()
... check: py-abipy
... check: py-arviz
.. check: py-nc-time-axis
. check: ibmisc
.. direct: ibmisc -> set()
. check: xtensor-python
.. direct: xtensor-python -> set()
. check: py-yahmm
.. direct: py-yahmm -> {'py-vcf-kit'}
.. check: py-vcf-kit
... direct: py-vcf-kit -> set()
. check: py-astropy
. check: py-mo-pack
.. direct: py-mo-pack -> set()
. check: py-onnx
.. direct: py-onnx -> {'lbann'}
.. check: lbann
. check: sensei
.. direct: sensei -> set()
. check: memsurfer
.. direct: memsurfer -> set()
. check: py-illumina-utils
.. direct: py-illumina-utils -> set()
. check: py-yt
. check: py-openpmd-validator
.. direct: py-openpmd-validator -> set()
. check: bart
. check: py-pymc3
. check: py-adios
.. direct: py-adios -> set()
. check: py-arviz
. check: ascent
. check: py-dlcpar
.. direct: py-dlcpar -> {'orthofinder'}
.. check: orthofinder
... direct: orthofinder -> {'orthofiller'}
... check: orthofiller
.... direct: orthofiller -> set()
. check: py-matplotlib
.. direct: py-matplotlib -> {'py-cogent', 'py-seaborn', 'py-opppy', 'py-basemap', 'py-mdanalysis', 'py-cartopy', 'py-espressopp', 'py-scikit-optimize', 'py-vcf-kit', 'py-gluoncv', 'qmcpack', 'py-wradlib', 'py-nc-time-axis', 'py-misopy', 'py-scikit-image', 'py-brian', 'cosmomc', 'minigan', 'py-wcsaxes', 'py-pymatgen', 'py-phonopy', 'py-multiqc', 'candle-benchmarks', 'py-umi-tools', 'py-pyani', 'pism', 'py-abipy', 'py-colorpy', 'py-crispresso', 'py-pyugrid', 'py-statsmodels', 'bcftools', 'py-deeptools', 'py-descartes', 'py-iminuit', 'py-sncosmo', 'py-mpld3', 'py-ase', 'py-astropy', 'py-yahmm', 'py-illumina-utils', 'py-yt', 'prmon', 'bart', 'py-wxmplot', 'py-hatchet', 'py-arviz', 'py-pycbc', 'py-cnvkit', 'py-checkm-genome', 'py-pauvre', 'py-quast', 'paraview', 'py-mlxtend', 'py-htseq', 'geopm', 'py-python-meep', 'py-localcider', 'lbann', 'py-geopandas', 'py-spatialist', 'timemory', 'py-pydv', 'py-wub', 'py-sfepy'}
.. check: py-cogent
.. check: py-seaborn
.. check: py-opppy
... direct: py-opppy -> set()
.. check: py-basemap
.. check: py-mdanalysis
.. check: py-cartopy
.. check: py-espressopp
.. check: py-scikit-optimize
.. check: py-vcf-kit
.. check: py-gluoncv
... direct: py-gluoncv -> set()
.. check: qmcpack
... direct: qmcpack -> set()
.. check: py-wradlib
.. check: py-nc-time-axis
.. check: py-misopy
... direct: py-misopy -> set()
.. check: py-scikit-image
.. check: py-brian
.. check: cosmomc
.. check: minigan
.. check: py-wcsaxes
.. check: py-pymatgen
... direct: py-pymatgen -> {'py-abipy'}
... check: py-abipy
.. check: py-phonopy
... direct: py-phonopy -> set()
.. check: py-multiqc
.. check: candle-benchmarks
.. check: py-umi-tools
.. check: py-pyani
.. check: pism
.. check: py-abipy
.. check: py-colorpy
... direct: py-colorpy -> set()
.. check: py-crispresso
.. check: py-pyugrid
.. check: py-statsmodels
.. check: bcftools
... direct: bcftools -> {'pgdspider', 'py-pysam', 'delly2', 'py-vcf-kit', 'phyluce', 'augustus'}
... check: pgdspider
.... direct: pgdspider -> set()
... check: py-pysam
.... direct: py-pysam -> {'py-metasv', 'hic-pro', 'py-biomine', 'py-htseq', 'py-pybedtools', 'py-tetoolkit', 'py-whatshap', 'py-umi-tools', 'bsseeker2', 'py-breakseq2', 'py-wub', 'py-misopy', 'find-circ', 'py-rnacocktail', 'py-cnvkit', 'py-deeptools', 'py-crossmap', 'py-atropos', 'py-rseqc', 'py-checkm-genome'}
.... check: py-metasv
.... check: hic-pro
..... direct: hic-pro -> set()
.... check: py-biomine
..... direct: py-biomine -> set()
.... check: py-htseq
..... direct: py-htseq -> {'mefit', 'kaiju'}
..... check: mefit
...... direct: mefit -> set()
..... check: kaiju
...... direct: kaiju -> set()
.... check: py-pybedtools
.... check: py-tetoolkit
..... direct: py-tetoolkit -> set()
.... check: py-whatshap
..... direct: py-whatshap -> set()
.... check: py-umi-tools
.... check: bsseeker2
..... direct: bsseeker2 -> set()
.... check: py-breakseq2
..... direct: py-breakseq2 -> set()
.... check: py-wub
.... check: py-misopy
.... check: find-circ
..... direct: find-circ -> set()
.... check: py-rnacocktail
.... check: py-cnvkit
.... check: py-deeptools
..... direct: py-deeptools -> set()
.... check: py-crossmap
..... direct: py-crossmap -> set()
.... check: py-atropos
..... direct: py-atropos -> set()
.... check: py-rseqc
..... direct: py-rseqc -> set()
.... check: py-checkm-genome
..... direct: py-checkm-genome -> set()
... check: delly2
.... direct: delly2 -> set()
... check: py-vcf-kit
... check: phyluce
.... direct: phyluce -> set()
... check: augustus
.... direct: augustus -> {'busco', 'orthofiller', 'braker', 'maker'}
.... check: busco
..... direct: busco -> set()
.... check: orthofiller
.... check: braker
..... direct: braker -> set()
.... check: maker
..... direct: maker -> set()
.. check: py-deeptools
.. check: py-descartes
... direct: py-descartes -> {'py-geopandas'}
... check: py-geopandas
.. check: py-iminuit
... direct: py-iminuit -> {'py-sncosmo'}
... check: py-sncosmo
.. check: py-sncosmo
.. check: py-mpld3
... direct: py-mpld3 -> {'py-pycbc'}
... check: py-pycbc
.. check: py-ase
... direct: py-ase -> {'py-gpaw'}
... check: py-gpaw
.... direct: py-gpaw -> set()
.. check: py-astropy
.. check: py-yahmm
.. check: py-illumina-utils
.. check: py-yt
.. check: prmon
... direct: prmon -> set()
.. check: bart
.. check: py-wxmplot
... direct: py-wxmplot -> {'py-abipy'}
... check: py-abipy
.. check: py-hatchet
.. check: py-arviz
.. check: py-pycbc
.. check: py-cnvkit
.. check: py-checkm-genome
.. check: py-pauvre
.. check: py-quast
... direct: py-quast -> set()
.. check: paraview
... direct: paraview -> {'openfoam', 'sensei', 'of-catalyst', 'trilinos-catalyst-ioss-adapter', 'foam-extend', 'ecp-viz-sdk'}
... check: openfoam
.... direct: openfoam -> {'of-precice', 'of-catalyst'}
.... check: of-precice
.... check: of-catalyst
..... direct: of-catalyst -> set()
... check: sensei
... check: of-catalyst
... check: trilinos-catalyst-ioss-adapter
.... direct: trilinos-catalyst-ioss-adapter -> {'nalu-wind'}
.... check: nalu-wind
... check: foam-extend
.... direct: foam-extend -> set()
... check: ecp-viz-sdk
.. check: py-mlxtend
.. check: py-htseq
.. check: geopm
.. check: py-python-meep
.. check: py-localcider
... direct: py-localcider -> set()
.. check: lbann
.. check: py-geopandas
.. check: py-spatialist
... direct: py-spatialist -> {'py-pyrosar'}
... check: py-pyrosar
.... direct: py-pyrosar -> set()
.. check: timemory
... direct: timemory -> set()
.. check: py-pydv
... direct: py-pydv -> set()
.. check: py-wub
.. check: py-sfepy
. check: py-rnacocktail
. check: sz
.. direct: sz -> {'adios', 'ecp-viz-sdk', 'adios2'}
.. check: adios
... direct: adios -> {'openpmd-api', 'sensei', 'conduit', 'py-adios', 'ascent', 'savanna'}
... check: openpmd-api
.... direct: openpmd-api -> {'warpx'}
.... check: warpx
... check: sensei
... check: conduit
.... direct: conduit -> {'mfem', 'axom', 'ascent', 'lbann'}
.... check: mfem
.... check: axom
.... check: ascent
.... check: lbann
... check: py-adios
... check: ascent
... check: savanna
.... direct: savanna -> set()
.. check: ecp-viz-sdk
.. check: adios2
... direct: adios2 -> {'openfoam', 'openpmd-api', 'seacas', 'ecp-io-sdk', 'trilinos', 'visit', 'tau'}
... check: openfoam
... check: openpmd-api
... check: seacas
.... direct: seacas -> set()
... check: ecp-io-sdk
.... direct: ecp-io-sdk -> set()
... check: trilinos
... check: visit
.... direct: visit -> {'ecp-viz-sdk', 'nektools', 'damaris', 'sensei'}
.... check: ecp-viz-sdk
.... check: nektools
..... direct: nektools -> {'ceed'}
..... check: ceed
.... check: damaris
..... direct: damaris -> set()
.... check: sensei
... check: tau
.... direct: tau -> {'timemory', 'savanna'}
.... check: timemory
.... check: savanna
. check: nest
.. direct: nest -> set()
. check: py-fparser
.. direct: py-fparser -> {'py-psyclone'}
.. check: py-psyclone
... direct: py-psyclone -> set()
. check: py-thinc
.. direct: py-thinc -> {'py-spacy'}
.. check: py-spacy
... direct: py-spacy -> {'py-spacy-models-en-core-web-sm'}
... check: py-spacy-models-en-core-web-sm
.... direct: py-spacy-models-en-core-web-sm -> set()
. check: partitionfinder
. check: trinity
.. direct: trinity -> {'trinotate', 'phyluce'}
.. check: trinotate
... direct: trinotate -> set()
.. check: phyluce
. check: py-stratify
.. direct: py-stratify -> set()
. check: xcfun
.. direct: xcfun -> set()
. check: py-syned
.. direct: py-syned -> set()
. check: root
.. direct: root -> {'hepmc', 'cnvnator', 'dd4hep', 'relax', 'acts', 'gaudi', 'podio', 'vecgeom', 'delphes'}
.. check: hepmc
... direct: hepmc -> {'relax', 'acts'}
... check: relax
.... direct: relax -> {'gaudi'}
.... check: gaudi
..... direct: gaudi -> set()
... check: acts
.... direct: acts -> set()
.. check: cnvnator
... direct: cnvnator -> set()
.. check: dd4hep
... direct: dd4hep -> {'acts'}
... check: acts
.. check: relax
.. check: acts
.. check: gaudi
.. check: podio
... direct: podio -> set()
.. check: vecgeom
... direct: vecgeom -> {'geant4'}
... check: geant4
.... direct: geant4 -> {'vecgeom', 'dd4hep', 'acts'}
.... check: vecgeom
.... check: dd4hep
.... check: acts
.. check: delphes
... direct: delphes -> set()
. check: py-scikit-learn
. check: trilinos
. check: libcint
.. direct: libcint -> set()
. check: py-petsc4py
. check: lbann
. check: py-ecos
.. direct: py-ecos -> {'py-cvxpy'}
.. check: py-cvxpy
... direct: py-cvxpy -> set()
. check: py-jpype1
.. direct: py-jpype1 -> set()
. check: py-librosa
. check: adios2
. check: gdal
.. direct: gdal -> {'py-cartopy', 'py-pygdal', 'r-rgdal', 'saga-gis', 'r-sf', 'mapserver', 'gmt', 'py-wradlib', 'r-gdalutils', 'liblas', 'py-pynio', 'qgis', 'ncl', 'sumo', 'py-tuiview', 'pktools', 'py-fiona', 'py-rasterio', 'py-pyrosar', 'grass', 'mapnik', 'gplates'}
.. check: py-cartopy
.. check: py-pygdal
... direct: py-pygdal -> set()
.. check: r-rgdal
... direct: r-rgdal -> {'r-gdalutils', 'r-bfastspatial', 'r-tigris'}
... check: r-gdalutils
.... direct: r-gdalutils -> {'r-bfastspatial'}
.... check: r-bfastspatial
..... direct: r-bfastspatial -> set()
... check: r-bfastspatial
... check: r-tigris
.... direct: r-tigris -> {'r-tidycensus'}
.... check: r-tidycensus
..... direct: r-tidycensus -> set()
.. check: saga-gis
... direct: saga-gis -> set()
.. check: r-sf
... direct: r-sf -> {'r-cdcfluview', 'r-tigris', 'r-spdep', 'r-exactextractr', 'r-spatialeco', 'r-tidycensus'}
... check: r-cdcfluview
.... direct: r-cdcfluview -> set()
... check: r-tigris
... check: r-spdep
.... direct: r-spdep -> {'r-adegenet', 'r-spatialreg', 'r-gwmodel', 'r-spatialeco'}
.... check: r-adegenet
..... direct: r-adegenet -> set()
.... check: r-spatialreg
..... direct: r-spatialreg -> {'r-gwmodel'}
..... check: r-gwmodel
...... direct: r-gwmodel -> set()
.... check: r-gwmodel
.... check: r-spatialeco
..... direct: r-spatialeco -> set()
... check: r-exactextractr
.... direct: r-exactextractr -> {'r-spatialeco'}
.... check: r-spatialeco
... check: r-spatialeco
... check: r-tidycensus
.. check: mapserver
... direct: mapserver -> set()
.. check: gmt
.. check: py-wradlib
.. check: r-gdalutils
.. check: liblas
... direct: liblas -> {'pktools', 'grass'}
... check: pktools
... check: grass
.. check: py-pynio
.. check: qgis
.. check: ncl
... direct: ncl -> set()
.. check: sumo
... direct: sumo -> set()
.. check: py-tuiview
... direct: py-tuiview -> set()
.. check: pktools
.. check: py-fiona
... direct: py-fiona -> {'py-geopandas'}
... check: py-geopandas
.. check: py-rasterio
.. check: py-pyrosar
.. check: grass
.. check: mapnik
... direct: mapnik -> {'py-python-mapnik'}
... check: py-python-mapnik
.... direct: py-python-mapnik -> set()
.. check: gplates
... direct: gplates -> set()
. check: py-lazyarray
.. direct: py-lazyarray -> {'py-pynn'}
.. check: py-pynn
... direct: py-pynn -> set()
. check: py-zarr
.. direct: py-zarr -> set()
. check: py-edffile
.. direct: py-edffile -> {'py-dxchange'}
.. check: py-dxchange
. check: py-wxpython
.. direct: py-wxpython -> {'py-traitsui', 'grass', 'py-wxmplot', 'py-abipy', 'py-matplotlib', 'py-pyface'}
.. check: py-traitsui
.. check: grass
.. check: py-wxmplot
.. check: py-abipy
.. check: py-matplotlib
.. check: py-pyface
. check: py-traitsui
. check: py-pypar
.. direct: py-pypar -> set()
. check: py-pygdal
. check: py-scikit-optimize
. check: py-osqp
.. direct: py-osqp -> {'py-cvxpy'}
.. check: py-cvxpy
. check: py-gluoncv
. check: py-vcf-kit
. check: qmcpack
. check: arrow
.. direct: arrow -> {'parquet-cpp', 'py-pyarrow'}
.. check: parquet-cpp
... direct: parquet-cpp -> set()
.. check: py-pyarrow
. check: plplot
.. direct: plplot -> {'gdl'}
.. check: gdl
... direct: gdl -> set()
. check: mxnet
.. direct: mxnet -> {'py-dgl', 'py-horovod'}
.. check: py-dgl
... direct: py-dgl -> set()
.. check: py-horovod
. check: py-biopandas
. check: py-gpaw
. check: py-pymatgen
. check: py-phonopy
. check: py-scipy
.. direct: py-scipy -> {'py-kmodes', 'py-opppy', 'py-mdanalysis', 'py-seaborn', 'py-cartopy', 'py-torchvision', 'py-scikit-optimize', 'py-osqp', 'py-gluoncv', 'py-vcf-kit', 'py-wradlib', 'py-misopy', 'py-scikit-image', 'py-brian', 'py-gpaw', 'cosmomc', 'py-symfit', 'py-pymatgen', 'py-phonopy', 'pagmo', 'py-cdo', 'nrm', 'py-libensemble', 'py-umi-tools', 'py-pomegranate', 'py-pyani', 'sparta', 'py-pydv', 'py-abipy', 'py-elephant', 'py-pyrad', 'py-goatools', 'py-pyugrid', 'py-statsmodels', 'py-deeptools', 'py-spykeutils', 'py-tensorflow', 'sirius', 'py-iminuit', 'py-dxchange', 'py-sncosmo', 'py-ase', 'py-astropy', 'py-projectq', 'py-scs', 'py-yahmm', 'py-guiqwt', 'py-yt', 'py-resampy', 'py-thirdorder', 'py-nestle', 'py-pymc3', 'py-arviz', 'py-pycbc', 'py-cnvkit', 'sgpp', 'py-dgl', 'py-biom-format', 'py-checkm-genome', 'akantu', 'orthofiller', 'py-gensim', 'py-mg-rast-tools', 'hic-pro', 'py-mlxtend', 'partitionfinder', 'py-patsy', 'py-lmfit', 'py-theano', 'py-tomopy', 'cantera', 'py-python-meep', 'py-localcider', 'py-scikit-learn', 'py-syned', 'py-librosa', 'py-ecos', 'py-fits-tools', 'py-cvxpy', 'py-sfepy'}
.. check: py-kmodes
.. check: py-opppy
.. check: py-mdanalysis
.. check: py-seaborn
.. check: py-cartopy
.. check: py-torchvision
.. check: py-scikit-optimize
.. check: py-osqp
.. check: py-gluoncv
.. check: py-vcf-kit
.. check: py-wradlib
.. check: py-misopy
.. check: py-scikit-image
.. check: py-brian
.. check: py-gpaw
.. check: cosmomc
.. check: py-symfit
... direct: py-symfit -> set()
.. check: py-pymatgen
.. check: py-phonopy
.. check: pagmo
... direct: pagmo -> set()
.. check: py-cdo
.. check: nrm
.. check: py-libensemble
.. check: py-umi-tools
.. check: py-pomegranate
... direct: py-pomegranate -> {'py-cnvkit'}
... check: py-cnvkit
.. check: py-pyani
.. check: sparta
.. check: py-pydv
.. check: py-abipy
.. check: py-elephant
.. check: py-pyrad
... direct: py-pyrad -> set()
.. check: py-goatools
.. check: py-pyugrid
.. check: py-statsmodels
.. check: py-deeptools
.. check: py-spykeutils
... direct: py-spykeutils -> set()
.. check: py-tensorflow
... direct: py-tensorflow -> {'py-dgl', 'py-horovod', 'py-tfdlpack', 'py-tensorflow-estimator'}
... check: py-dgl
... check: py-horovod
... check: py-tfdlpack
.... direct: py-tfdlpack -> {'py-dgl'}
.... check: py-dgl
... check: py-tensorflow-estimator
.... direct: py-tensorflow-estimator -> {'py-tensorboard'}
.... check: py-tensorboard
..... direct: py-tensorboard -> set()
.. check: sirius
.. check: py-iminuit
.. check: py-dxchange
.. check: py-sncosmo
.. check: py-ase
.. check: py-astropy
.. check: py-projectq
... direct: py-projectq -> set()
.. check: py-scs
... direct: py-scs -> {'py-cvxpy'}
... check: py-cvxpy
.. check: py-yahmm
.. check: py-guiqwt
... direct: py-guiqwt -> set()
.. check: py-yt
.. check: py-resampy
... direct: py-resampy -> {'py-librosa'}
... check: py-librosa
.. check: py-thirdorder
... direct: py-thirdorder -> set()
.. check: py-nestle
... direct: py-nestle -> {'py-sncosmo'}
... check: py-sncosmo
.. check: py-pymc3
.. check: py-arviz
.. check: py-pycbc
.. check: py-cnvkit
.. check: sgpp
... direct: sgpp -> set()
.. check: py-dgl
.. check: py-biom-format
.. check: py-checkm-genome
.. check: akantu
.. check: orthofiller
.. check: py-gensim
... direct: py-gensim -> set()
.. check: py-mg-rast-tools
... direct: py-mg-rast-tools -> set()
.. check: hic-pro
.. check: py-mlxtend
.. check: partitionfinder
.. check: py-patsy
... direct: py-patsy -> {'py-statsmodels', 'py-pymc3'}
... check: py-statsmodels
... check: py-pymc3
.. check: py-lmfit
... direct: py-lmfit -> set()
.. check: py-theano
... direct: py-theano -> {'candle-benchmarks', 'py-keras', 'py-pymc3'}
... check: candle-benchmarks
... check: py-keras
.... direct: py-keras -> {'candle-benchmarks', 'py-horovod'}
.... check: candle-benchmarks
.... check: py-horovod
... check: py-pymc3
.. check: py-tomopy
.. check: cantera
.. check: py-python-meep
.. check: py-localcider
.. check: py-scikit-learn
.. check: py-syned
.. check: py-librosa
.. check: py-ecos
.. check: py-fits-tools
.. check: py-cvxpy
.. check: py-sfepy
. check: py-h5py
.. direct: py-h5py -> {'py-dxchange', 'py-astropy', 'candle-benchmarks', 'py-ont-fast5-api', 'sirius', 'py-dxfile', 'py-tomopy', 'py-h5sh', 'py-yt', 'ont-albacore', 'py-h5glance', 'py-openpmd-validator', 'py-pymc3', 'py-guidata', 'py-wradlib', 'py-pycbc', 'py-horovod', 'py-tensorflow', 'py-biom-format', 'simulationio'}
.. check: py-dxchange
.. check: py-astropy
.. check: candle-benchmarks
.. check: py-ont-fast5-api
... direct: py-ont-fast5-api -> {'ont-albacore'}
... check: ont-albacore
.... direct: ont-albacore -> set()
.. check: sirius
.. check: py-dxfile
... direct: py-dxfile -> {'py-dxchange'}
... check: py-dxchange
.. check: py-tomopy
.. check: py-h5sh
... direct: py-h5sh -> set()
.. check: py-yt
.. check: ont-albacore
.. check: py-h5glance
... direct: py-h5glance -> set()
.. check: py-openpmd-validator
.. check: py-pymc3
.. check: py-guidata
... direct: py-guidata -> {'py-guiqwt'}
... check: py-guiqwt
.. check: py-wradlib
.. check: py-pycbc
.. check: py-horovod
.. check: py-tensorflow
.. check: py-biom-format
.. check: simulationio
. check: eccodes
.. direct: eccodes -> {'cdo', 'magics', 'libemos'}
.. check: cdo
... direct: cdo -> {'py-cdo'}
... check: py-cdo
.. check: magics
... direct: magics -> {'cdo'}
... check: cdo
.. check: libemos
... direct: libemos -> {'magics'}
... check: magics
. check: py-pygpu
.. direct: py-pygpu -> {'py-theano'}
.. check: py-theano
. check: py-umi-tools
. check: openimageio
.. direct: openimageio -> set()
. check: ont-albacore
. check: py-espresso
.. direct: py-espresso -> set()
. check: trilinos-catalyst-ioss-adapter
. check: py-soundfile
.. direct: py-soundfile -> {'py-librosa'}
.. check: py-librosa
. check: vigra
.. direct: vigra -> set()
. check: py-dask
. check: py-spefile
.. direct: py-spefile -> {'py-dxchange'}
.. check: py-dxchange
. check: py-goatools
. check: py-macs2
.. direct: py-macs2 -> set()
. check: py-projectq
. check: py-jplephem
.. direct: py-jplephem -> {'py-astropy'}
.. check: py-astropy
. check: py-merlin
.. direct: py-merlin -> set()
. check: py-numba
.. direct: py-numba -> {'py-resampy', 'py-librosa'}
.. check: py-resampy
.. check: py-librosa
. check: py-guiqwt
. check: hoomd-blue
.. direct: hoomd-blue -> set()
. check: py-scs
. check: py-pipits
. check: py-resampy
. check: py-periodictable
.. direct: py-periodictable -> set()
. check: py-hatchet
. check: py-pycbc
. check: py-pyfftw
.. direct: py-pyfftw -> {'py-tomopy'}
.. check: py-tomopy
. check: py-pyfasta
.. direct: py-pyfasta -> {'py-methylcode'}
.. check: py-methylcode
. check: py-biom-format
. check: psi4
.. direct: psi4 -> set()
. check: py-bottleneck
.. direct: py-bottleneck -> {'py-astropy', 'py-pandas'}
.. check: py-astropy
.. check: py-pandas
. check: py-fastcluster
.. direct: py-fastcluster -> set()
. check: py-faststructure
.. direct: py-faststructure -> set()
. check: py-petastorm
. check: openpmd-api
. check: py-weave
.. direct: py-weave -> {'py-pycbc'}
.. check: py-pycbc
. check: py-patsy
. check: py-theano
. check: cantera
. check: asdf-cxx
.. direct: asdf-cxx -> set()
. check: py-localcider
. check: py-python-meep
. check: py-torchsummary
.. direct: py-torchsummary -> set()
. check: py-transformers
.. direct: py-transformers -> set()
. check: conduit
. check: find-circ
. check: py-emcee
.. direct: py-emcee -> {'py-sncosmo', 'py-pycbc'}
.. check: py-sncosmo
.. check: py-pycbc
. check: py-spatialist
. check: cp2k
. check: py-cinema-lib
. check: py-rseqc
. check: py-seaborn
. check: py-pywavelets
.. direct: py-pywavelets -> {'py-scikit-image', 'py-tomopy'}
.. check: py-scikit-image
.. check: py-tomopy
. check: py-chainer
.. direct: py-chainer -> set()
. check: py-wradlib
. check: py-misopy
. check: py-crossmap
. check: py-symfit
. check: py-svgpathtools
.. direct: py-svgpathtools -> set()
. check: py-torch
.. direct: py-torch -> {'py-torchtext', 'py-torchvision', 'py-torchsummary', 'py-torchaudio', 'py-torch-nvidia-apex', 'minigan', 'py-dgl', 'py-horovod'}
.. check: py-torchtext
... direct: py-torchtext -> set()
.. check: py-torchvision
.. check: py-torchsummary
.. check: py-torchaudio
... direct: py-torchaudio -> set()
.. check: py-torch-nvidia-apex
... direct: py-torch-nvidia-apex -> set()
.. check: minigan
.. check: py-dgl
.. check: py-horovod
. check: py-psyclone
. check: py-pybedtools
. check: py-spglib
.. direct: py-spglib -> {'py-abipy', 'py-pymatgen'}
.. check: py-abipy
.. check: py-pymatgen
. check: py-mayavi
. check: py-libensemble
. check: py-neo
.. direct: py-neo -> {'py-spykeutils', 'py-pynn', 'py-elephant'}
.. check: py-spykeutils
.. check: py-pynn
.. check: py-elephant
. check: py-cyvcf2
.. direct: py-cyvcf2 -> {'py-vcf-kit'}
.. check: py-vcf-kit
. check: py-numexpr3
.. direct: py-numexpr3 -> set()
. check: py-pomegranate
. check: py-crispresso
. check: opencv
.. direct: opencv -> {'candle-benchmarks', 'bohrium', 'saga-gis', 'py-mmcv', 'mxnet', 'lbann', 'py-cinema-lib', 'cntk', 'caffe', 'py-torch'}
.. check: candle-benchmarks
.. check: bohrium
.. check: saga-gis
.. check: py-mmcv
... direct: py-mmcv -> set()
.. check: mxnet
.. check: lbann
.. check: py-cinema-lib
.. check: cntk
... direct: cntk -> set()
.. check: caffe
... direct: caffe -> set()
.. check: py-torch
. check: py-abipy
. check: py-elephant
. check: py-pyrad
. check: py-pyugrid
. check: py-scientificpython
.. direct: py-scientificpython -> set()
. check: py-colormath
.. direct: py-colormath -> {'py-spectra'}
.. check: py-spectra
... direct: py-spectra -> {'py-multiqc'}
... check: py-multiqc
. check: py-pandas
. check: py-griddataformats
.. direct: py-griddataformats -> {'py-mdanalysis'}
.. check: py-mdanalysis
. check: py-iminuit
. check: py-pycuda
.. direct: py-pycuda -> set()
. check: py-ase
. check: py-srsly
.. direct: py-srsly -> {'py-thinc', 'py-spacy'}
.. check: py-thinc
.. check: py-spacy
. check: py-rasterio
. check: py-cdat-lite
.. direct: py-cdat-lite -> set()
. check: py-tifffile
.. direct: py-tifffile -> {'py-dxchange'}
.. check: py-dxchange
. check: boost
.. direct: boost -> {'lordec', 'bohrium', 'py-espressopp', 'bridger', 'nektar', 'salmon', 'xssp', 'mysql', 'casacore', 'quinoa', 'mariadb', 'iq-tree', 'thrift', 'folly', 'revbayes', 'cleverleaf', 'gource', 'ibmisc', 'cbtf-argonavis-gui', 'dd4hep', 'muster', 'paraver', 'abyss', 'helics', 'modern-wheel', 'faodel', 'isaac', 'sympol', 'mapnik', 'mofem-cephas', 'templight-tools', 'hipsycl', 'trilinos', 'dysco', 'sfcgal', 'yaml-cpp', 'snap-korf', 'metall', 'flux-sched', 'isaac-server', 'stat', 'symengine', 'mira', 'autodock-vina', 'qmcpack', 'arrow', 'flecsale', 'paradiseo', 'dealii', 'polymake', 'xdmf3', 'simgrid', 'casper', 'pagmo', 'nix', 'openimageio', 'liblas', 'py-espresso', 'meraculous', 'libkml', 'source-highlight', 'vigra', 'hisea', 'bcl2fastq2', 'vtk', 'flecsph', 'libmesh', 'highfive', 'seqan', 'clfft', 'dimemas', 'sailfish', 'launchmon', 'channelflow', 'psi4', 'ecflow', 'augustus', 'denovogear', 'cbtf-krell', 'cantera', 'votca-tools', 'openbabel', 'ns-3-dev', 'cbtf', 'gplates', 'aoflagger', 'cntk', 'erne', 'librom', 'range-v3', 'samrai', 'chill', 'imp', 'mothur', 'kea', 'amp', 'metabat', 'shark', 'py-python-mapnik', 'r-phantompeakqualtools', 'aspcud', 'parsplice', 'masurca', 'py-pycuda', 'dakota', 'tfel', 'portage', 'branson', 'percept', 'spot', 'fairlogger', 'sgpp', 'tophat', 'mgis', 'py-quast', 'delly2', 'fsl', 'automaded', 'ethminer', 'gaudi', 'mercury', 'assimp', 'adol-c', 'rose', 'votca-csg-tutorials', 'fenics', 'caffe', 'gearshifft', 'flann', 'hyperscan', 'blasr', 'votca-csgapps', 'piranha', 'valgrind', 'geant4', 'mallocmc', 'flecsi', 'openspeedshop-utils', 'apex', 'libfive', 'scallop', 'heaptrack', 'cpprestsdk', 'mrnet', 'openspeedshop', 'express', 'pktools', 'parquet-cpp', 'xios', 'cbtf-argonavis', 'votca-csg', 'magics', 'manta', 'precice', 'wt', 'veloc', 'libint', 'akantu', 'ncbi-toolkit', 'pbbam', 'cgal', 'openfoam', 'dyninst', 'strelka', 'hpx', 'biobloom', 'acts', 'extrae', 'hpctoolkit', 'multiverso', 'jali', 'damaris', 'adept-utils'}
.. check: lordec
... direct: lordec -> {'halc'}
... check: halc
.... direct: halc -> set()
.. check: bohrium
.. check: py-espressopp
.. check: bridger
... direct: bridger -> set()
.. check: nektar
.. check: salmon
... direct: salmon -> {'trinity'}
... check: trinity
.. check: xssp
... direct: xssp -> set()
.. check: mysql
... direct: mysql -> {'py-mysqlclient', 'root', 'py-mysqldb1'}
... check: py-mysqlclient
.... direct: py-mysqlclient -> {'py-sqlalchemy'}
.... check: py-sqlalchemy
..... direct: py-sqlalchemy -> {'py-cogent', 'py-geoalchemy2', 'py-agate-sql', 'py-alembic', 'py-csvkit', 'py-jupyterhub', 'py-opentuner'}
..... check: py-cogent
..... check: py-geoalchemy2
...... direct: py-geoalchemy2 -> set()
..... check: py-agate-sql
...... direct: py-agate-sql -> {'py-csvkit'}
...... check: py-csvkit
....... direct: py-csvkit -> set()
..... check: py-alembic
...... direct: py-alembic -> {'py-jupyterhub'}
...... check: py-jupyterhub
....... direct: py-jupyterhub -> set()
..... check: py-csvkit
..... check: py-jupyterhub
..... check: py-opentuner
...... direct: py-opentuner -> set()
... check: root
... check: py-mysqldb1
.... direct: py-mysqldb1 -> set()
.. check: casacore
.. check: quinoa
.. check: mariadb
... direct: mariadb -> {'perl-dbd-mysql', 'grass', 'r-rmariadb', 'root', 'wt', 'r-rmysql', 'kentutils', 'orthomcl', 'slurm'}
... check: perl-dbd-mysql
.... direct: perl-dbd-mysql -> {'biopieces'}
.... check: biopieces
..... direct: biopieces -> set()
... check: grass
... check: r-rmariadb
.... direct: r-rmariadb -> set()
... check: root
... check: wt
.... direct: wt -> set()
... check: r-rmysql
.... direct: r-rmysql -> {'r-taxizedb', 'r-genomicfeatures'}
.... check: r-taxizedb
..... direct: r-taxizedb -> {'r-phylostratr'}
..... check: r-phylostratr
...... direct: r-phylostratr -> set()
.... check: r-genomicfeatures
..... direct: r-genomicfeatures -> {'r-organismdbi', 'r-ensembldb', 'r-lumi', 'r-txdb-hsapiens-ucsc-hg18-knowngene', 'r-txdb-hsapiens-ucsc-hg19-knowngene', 'r-genelendatabase', 'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-bumphunter', 'r-fdb-infiniummethylation-hg19', 'r-gviz', 'r-fdb-infiniummethylation-hg18', 'r-allelicimbalance', 'r-biovizbase'}
..... check: r-organismdbi
...... direct: r-organismdbi -> {'r-ggbio'}
...... check: r-ggbio
....... direct: r-ggbio -> {'r-reportingtools', 'r-somaticsignatures'}
....... check: r-reportingtools
........ direct: r-reportingtools -> {'r-affycoretools'}
........ check: r-affycoretools
......... direct: r-affycoretools -> {'r-agimicrorna'}
......... check: r-agimicrorna
.......... direct: r-agimicrorna -> set()
....... check: r-somaticsignatures
........ direct: r-somaticsignatures -> {'r-yapsa'}
........ check: r-yapsa
......... direct: r-yapsa -> set()
..... check: r-ensembldb
...... direct: r-ensembldb -> {'r-ggbio', 'r-biovizbase'}
...... check: r-ggbio
...... check: r-biovizbase
....... direct: r-biovizbase -> {'r-ggbio', 'r-gviz'}
....... check: r-ggbio
....... check: r-gviz
........ direct: r-gviz -> {'r-allelicimbalance'}
........ check: r-allelicimbalance
......... direct: r-allelicimbalance -> set()
..... check: r-lumi
...... direct: r-lumi -> {'r-watermelon'}
...... check: r-watermelon
....... direct: r-watermelon -> set()
..... check: r-txdb-hsapiens-ucsc-hg18-knowngene
...... direct: r-txdb-hsapiens-ucsc-hg18-knowngene -> {'r-fdb-infiniummethylation-hg18'}
...... check: r-fdb-infiniummethylation-hg18
....... direct: r-fdb-infiniummethylation-hg18 -> set()
..... check: r-txdb-hsapiens-ucsc-hg19-knowngene
...... direct: r-txdb-hsapiens-ucsc-hg19-knowngene -> {'r-fdb-infiniummethylation-hg19'}
...... check: r-fdb-infiniummethylation-hg19
....... direct: r-fdb-infiniummethylation-hg19 -> {'r-methylumi'}
....... check: r-methylumi
........ direct: r-methylumi -> {'r-lumi', 'r-watermelon'}
........ check: r-lumi
........ check: r-watermelon
..... check: r-genelendatabase
...... direct: r-genelendatabase -> {'r-goseq'}
...... check: r-goseq
....... direct: r-goseq -> {'trinity'}
....... check: trinity
..... check: r-variantannotation
...... direct: r-variantannotation -> {'r-yapsa', 'r-ggbio', 'r-allelicimbalance', 'r-somaticsignatures', 'r-ampliqueso', 'r-biovizbase'}
...... check: r-yapsa
...... check: r-ggbio
...... check: r-allelicimbalance
...... check: r-somaticsignatures
...... check: r-ampliqueso
....... direct: r-ampliqueso -> set()
...... check: r-biovizbase
..... check: r-ggbio
..... check: r-alpine
...... direct: r-alpine -> set()
..... check: r-bumphunter
...... direct: r-bumphunter -> {'r-minfi'}
...... check: r-minfi
....... direct: r-minfi -> {'r-quantro', 'r-illuminahumanmethylation450kanno-ilmn12-hg19', 'r-methylumi'}
....... check: r-quantro
........ direct: r-quantro -> {'r-yarn'}
........ check: r-yarn
......... direct: r-yarn -> set()
....... check: r-illuminahumanmethylation450kanno-ilmn12-hg19
........ direct: r-illuminahumanmethylation450kanno-ilmn12-hg19 -> {'r-watermelon'}
........ check: r-watermelon
....... check: r-methylumi
..... check: r-fdb-infiniummethylation-hg19
..... check: r-gviz
..... check: r-fdb-infiniummethylation-hg18
..... check: r-allelicimbalance
..... check: r-biovizbase
... check: kentutils
.... direct: kentutils -> set()
... check: orthomcl
.... direct: orthomcl -> set()
... check: slurm
.... direct: slurm -> {'openmpi', 'mpich', 'charmpp', 'mvapich2', 'libyogrt'}
.... check: openmpi
..... direct: openmpi -> {'powerapi', 'pennant', 'mesquite', 'modylas', 'citcoms', 'strumpack', 'warpx', 'pruners-ninja', 'mpi-bash', 'libsharp', 'hydrogen', 'cgns', 'abyss', 'py-adios', 'exodusii', 'nest', 'faodel', 'mofem-cephas', 'openfoam-org', 'hpgmg', 'trilinos', 'libcircle', 'stat', 'py-pypar', 'revocap-refiner', 'kahip', 'parmetis', 'mitos', 'paradiseo', 'dealii', 'mpifileutils', 'elmerfem', 'libmesh', 'ppopen-appl-bem-at', 'heffte', 'qmd-progress', 'octopus', 'eztrace', 'kripke', 'fftw', 'hpl', 'linsys-v', 'libvdwxc', 'exasp2', 'samrai', 'latte', 'charmpp', 'parallel-netcdf', 'phasta', 'py-torch', 'scr', 'orca', 'cosp2', 'hpx5', 'scorec-core', 'sw4lite', 'siesta', 'rempi', 'exampm', 'MeshSimAdvanced', 'boost', 'minismac2d', 'draco', 'icet', 'ppopen-appl-fdm', 'libsplash', 'scotch', 'xsbench', 'adios', 'phylobayesmpi', 'arborx', 'cosmomc', 'dihydrogen', 'tioga', 'catalyst', 'bertini', 'revocap-coupler', 'valgrind', 'ember', 'alquimia', 'picsar', 'flecsi', 'pism', 'py-tensorflow', 'mrbayes', 'ppopen-math-vis', 'pmlib', 'ross', 'cram', 'veloc', 'akantu', 'openfoam', 'hpx', 'minivite', 'funhpc', 'grackle', 'hpctoolkit', 'damaris', 'adept-utils', 'elpa', 'py-espressopp', 'intel-mpi-benchmarks', 'cloverleaf3d', 'ebms', 'xbraid', 'panda', 'sirius', 'ampliconnoise', 'netcdf-fortran', 'bookleaf-cpp', 'muster', 'albany', 'aluminum', 'aspa', 'mrcpp', 'ascent', 'minighost', 'kokkos-nvcc-wrapper', 'dbcsr', 'converge', 'vtk-m', 'phist', 'qmcpack', 'minitri', 'lulesh', 'minife', 'pagmo', 'opencoarrays', 'gslib', 'megadock', 'vtk', 'flecsph', 'hpccg', 'elemental', 'everytrace-example', 'lwgrp', 'adlbx', 'pbmpi', 'yambo', 'hoomd-blue', 'molcas', 'tau', 'vpfft', 'openstf', 'dftfe', 'boxlib', 'mpileaks', 'mfem', 'sosflow', 'minimd', 'spfft', 'ppopen-appl-fem', 'wannier90', 'amp', 'ior', 'cbench', 'ntpoly', 'ppopen-appl-fdm-at', 'sst-macro', 'dataspaces', 'parsplice', 'lwm2', 'camellia', 'mpe2', 'macsio', 'upcxx', 'ppopen-appl-bem', 'scorep', 'cabana', 'scalasca', 'globalarrays', 'simul', 'qbox', 'mercury', 'med', 'bml', 'nccl-tests', 'fenics', 'amber', 'liggghts', 'hmmer', 'hdf5', 'athena', 'precice', 'slate', 'quicksilver', 'exabayes', 'extrae', 'ray', 'meshkit', 'cardioid', 'hypre', 'eq-r', 'regcm', 'axom', 'seacas', 'ccs-qcd', 'meep', 'raft', 'omega-h', 'textparser', 'adiak', 'lbann', 'callpath', 'adios2', 'amgx', 'minixyce', 'py-gpaw', 'osu-micro-benchmarks', 'nalu-wind', 'py-h5py', 'ppopen-appl-amr-fdm', 'py-espresso', 'fast-global-file-status', 'sundials', 'simplemoc', 'steps', 'shuffile', 'channelflow', 'ffr', 'cbtf-krell', 'tracer', 'netcdf-c', 'thornado-mini', 'gmsh', 'cntk', 'erne', 'meme', 'py-mpi4py', 'amg2013', 'ppopen-appl-dem-util', 'mpip', 'relion', 'infernal', 'camx', 'vampirtrace', 'opencv', 'chatterbug', 'sgpp', 'nekcem', 'hwloc', 'cloverleaf', 'papyrus', 'geopm', 'elk', 'dtcmp', 'swiftsim', 'tasmanian', 'timemory', 'redset', 'snap', 'nalu', 'nek5000', 'h5cpp', 'flann', 'mpix-launch-swift', 'ompss', 'er', 'miniamr', 'p3dfft3', 'tealeaf', 'elsi', 'damselfly', 'py-horovod', 'zoltan', 'sst-core', 'mdtest', 'visit', 'henson', 'caliper', 'foam-extend', 'pflotran', 'jali', 'nekbone', 'hiop', 'migrate', 'nektar', 'picsarlite', 'libnbc', 'libhio', 'netgauge', 'iq-tree', 'pfft', 'revbayes', 'moab', 'openfdtd', 'openmx', 'openfast', 'lammps', 'cosma', 'amrex', 'ppopen-math-mp', 'libquo', 'helics', 'swfft', 'amrvis', 'gromacs', 'maker', 'isaac', 'h5hut', 'raxml', 'vpic', 'filo', 'p4est', 'abinit', 'frontistr', 'rankstr', 'npb', 'gasnet', 'diy', 'netgen', 'pidx', 'xdmf3', 'parmgridgen', 'mumps', 'shengbte', 'everytrace', 'mstk', 'kvtree', 'turbine', 'ppopen-appl-fvm', 'pnmpi', 'openpmd-api', 'amg', 'clamr', 'py-python-meep', 'conduit', 'spath', 'arpack-ng', 'miniqmc', 'cp2k', 'librom', 'asagi', 'superlu-dist', 'petsc', 'eem', 'ghost', 'coevp', 'py-libensemble', 'codes', 'pumi', 'neuron', 'butterflypack', 'darshan-runtime', 'tycho2', 'plumed', 'dakota', 'portage', 'comd', 'branson', 'r-rmpi', 'unifyfs', 'minigmg', 'automaded', 'typhon', 'h5part', 'netlib-scalapack', 'chombo', 'vtk-h', 'silo', 'xsdktrilinos', 'esmf', 'fastmath', 'pfunit', 'savanna', 'xios', 'graph500', 'cgm', 'paraview', 'examinimd', 'nwchem', 'quantum-espresso', 'typhonio', 'multiverso'}
..... check: powerapi
...... direct: powerapi -> set()
..... check: pennant
...... direct: pennant -> set()
..... check: mesquite
...... direct: mesquite -> set()
..... check: modylas
...... direct: modylas -> set()
..... check: citcoms
..... check: strumpack
..... check: warpx
..... check: pruners-ninja
...... direct: pruners-ninja -> set()
..... check: mpi-bash
...... direct: mpi-bash -> set()
..... check: libsharp
...... direct: libsharp -> {'healpix-cxx'}
...... check: healpix-cxx
....... direct: healpix-cxx -> set()
..... check: hydrogen
..... check: cgns
...... direct: cgns -> {'trilinos', 'seacas'}
...... check: trilinos
...... check: seacas
..... check: abyss
...... direct: abyss -> {'transabyss', 'phyluce'}
...... check: transabyss
....... direct: transabyss -> set()
...... check: phyluce
..... check: py-adios
..... check: exodusii
...... direct: exodusii -> {'flecsale', 'mstk', 'macsio', 'jali'}
...... check: flecsale
....... direct: flecsale -> set()
...... check: mstk
....... direct: mstk -> {'jali'}
....... check: jali
........ direct: jali -> set()
...... check: macsio
....... direct: macsio -> {'ecp-proxy-apps'}
....... check: ecp-proxy-apps
...... check: jali
..... check: nest
..... check: faodel
...... direct: faodel -> {'ecp-io-sdk'}
...... check: ecp-io-sdk
..... check: mofem-cephas
..... check: openfoam-org
...... direct: openfoam-org -> set()
..... check: hpgmg
..... check: trilinos
..... check: libcircle
...... direct: libcircle -> {'mpifileutils', 'mpi-bash'}
...... check: mpifileutils
....... direct: mpifileutils -> set()
...... check: mpi-bash
..... check: stat
...... direct: stat -> set()
..... check: py-pypar
..... check: revocap-refiner
...... direct: revocap-refiner -> {'frontistr'}
...... check: frontistr
..... check: kahip
...... direct: kahip -> {'openfoam'}
...... check: openfoam
..... check: parmetis
...... direct: parmetis -> {'seacas', 'octopus', 'mofem-cephas', 'mumps', 'bookleaf-cpp', 'pexsi', 'branson', 'draco', 'strumpack', 'petsc', 'fenics', 'flecsi', 'moab', 'superlu-dist', 'trilinos', 'foam-extend', 'phist', 'zoltan'}
...... check: seacas
...... check: octopus
....... direct: octopus -> set()
...... check: mofem-cephas
...... check: mumps
....... direct: mumps -> {'frontistr', 'ipopt', 'trilinos', 'petsc', 'elmerfem', 'akantu'}
....... check: frontistr
....... check: ipopt
........ direct: ipopt -> {'pagmo'}
........ check: pagmo
....... check: trilinos
....... check: petsc
....... check: elmerfem
....... check: akantu
...... check: bookleaf-cpp
....... direct: bookleaf-cpp -> set()
...... check: pexsi
....... direct: pexsi -> {'cp2k'}
....... check: cp2k
...... check: branson
....... direct: branson -> set()
...... check: draco
....... direct: draco -> set()
...... check: strumpack
...... check: petsc
...... check: fenics
...... check: flecsi
....... direct: flecsi -> {'flecsale', 'flecsph'}
....... check: flecsale
....... check: flecsph
........ direct: flecsph -> set()
...... check: moab
...... check: superlu-dist
....... direct: superlu-dist -> {'hypre', 'pexsi', 'elsi', 'draco', 'trilinos', 'petsc', 'fastmath', 'xsdk', 'mfem', 'sundials'}
....... check: hypre
....... check: pexsi
....... check: elsi
....... check: draco
....... check: trilinos
....... check: petsc
....... check: fastmath
....... check: xsdk
....... check: mfem
....... check: sundials
...... check: trilinos
...... check: foam-extend
...... check: phist
...... check: zoltan
....... direct: zoltan -> {'ghost', 'sst-core', 'openfoam', 'scorec-core', 'fastmath', 'moab', 'mstk', 'pumi', 'elmerfem', 'jali'}
....... check: ghost
........ direct: ghost -> {'phist'}
........ check: phist
....... check: sst-core
........ direct: sst-core -> {'sst-elements', 'sst-macro'}
........ check: sst-elements
......... direct: sst-elements -> set()
........ check: sst-macro
......... direct: sst-macro -> {'sst-transports'}
......... check: sst-transports
.......... direct: sst-transports -> set()
....... check: openfoam
....... check: scorec-core
........ direct: scorec-core -> set()
....... check: fastmath
....... check: moab
....... check: mstk
....... check: pumi
........ direct: pumi -> {'ceed', 'mfem', 'xsdk', 'fastmath'}
........ check: ceed
........ check: mfem
........ check: xsdk
........ check: fastmath
....... check: elmerfem
....... check: jali
..... check: mitos
...... direct: mitos -> set()
..... check: paradiseo
...... direct: paradiseo -> set()
..... check: dealii
..... check: mpifileutils
..... check: elmerfem
..... check: libmesh
..... check: ppopen-appl-bem-at
...... direct: ppopen-appl-bem-at -> set()
..... check: heffte
...... direct: heffte -> set()
..... check: qmd-progress
...... direct: qmd-progress -> {'latte'}
...... check: latte
....... direct: latte -> {'lammps'}
....... check: lammps
..... check: octopus
..... check: eztrace
...... direct: eztrace -> set()
..... check: kripke
...... direct: kripke -> set()
..... check: fftw
...... direct: fftw -> {'openmm', 'cdo', 'r-diversitree', 'ssht', 'libvips', 'py-espressopp', 'nektar', 'qmcpack', 'picsarlite', 'gmt', 'petsc', 'r-imager', 'spfft', 'py-gpaw', 'casacore', 'gearshifft', 'relion', 'namd', 'gdl', 'nalu-wind', 'pfft', 'cbench', 'picsar', 'megadock', 'py-espresso', 'warpx', 'pism', 'vigra', 'sirius', 'openmx', 'p3dfft3', 'lammps', 'heffte', 'accfft', 'unblur', 'yambo', 'octopus', 'shtools', 'cistem', 'ermod', 'bart', 'channelflow', 'libemos', 'athena', 'swfft', 'mrtrix3', 'r-fftwtools', 'py-pyfftw', 'raft', 'octave', 'swftools', 'gromacs', 'nfft', 'vpfft', 'openfoam', 'grass', 'ctffind', 'root', 'votca-tools', 'quantum-espresso', 'tinker', 'elk', 'qbox', 'libvdwxc', 'cp2k', 'abinit', 'aoflagger', 'py-cvxopt'}
...... check: openmm
....... direct: openmm -> set()
...... check: cdo
...... check: r-diversitree
....... direct: r-diversitree -> set()
...... check: ssht
....... direct: ssht -> set()
...... check: libvips
....... direct: libvips -> set()
...... check: py-espressopp
...... check: nektar
...... check: qmcpack
...... check: picsarlite
....... direct: picsarlite -> {'ecp-proxy-apps'}
....... check: ecp-proxy-apps
...... check: gmt
...... check: petsc
...... check: r-imager
....... direct: r-imager -> set()
...... check: spfft
....... direct: spfft -> {'sirius'}
....... check: sirius
...... check: py-gpaw
...... check: casacore
...... check: gearshifft
....... direct: gearshifft -> set()
...... check: relion
....... direct: relion -> set()
...... check: namd
....... direct: namd -> set()
...... check: gdl
...... check: nalu-wind
...... check: pfft
....... direct: pfft -> {'pnfft', 'libvdwxc'}
....... check: pnfft
........ direct: pnfft -> set()
....... check: libvdwxc
........ direct: libvdwxc -> {'sirius', 'py-gpaw'}
........ check: sirius
........ check: py-gpaw
...... check: cbench
....... direct: cbench -> set()
...... check: picsar
....... direct: picsar -> set()
...... check: megadock
....... direct: megadock -> set()
...... check: py-espresso
...... check: warpx
...... check: pism
...... check: vigra
...... check: sirius
...... check: openmx
...... check: p3dfft3
....... direct: p3dfft3 -> set()
...... check: lammps
...... check: heffte
...... check: accfft
....... direct: accfft -> set()
...... check: unblur
....... direct: unblur -> set()
...... check: yambo
....... direct: yambo -> set()
...... check: octopus
...... check: shtools
...... check: cistem
....... direct: cistem -> set()
...... check: ermod
....... direct: ermod -> set()
...... check: bart
...... check: channelflow
....... direct: channelflow -> set()
...... check: libemos
...... check: athena
....... direct: athena -> set()
...... check: swfft
....... direct: swfft -> {'ecp-proxy-apps'}
....... check: ecp-proxy-apps
...... check: mrtrix3
....... direct: mrtrix3 -> set()
...... check: r-fftwtools
....... direct: r-fftwtools -> set()
...... check: py-pyfftw
...... check: raft
....... direct: raft -> set()
...... check: octave
....... direct: octave -> {'nlopt', 'sbml', 'h5utils', 'octave-optim', 'octave-splines', 'octave-struct'}
....... check: nlopt
....... check: sbml
........ direct: sbml -> set()
....... check: h5utils
........ direct: h5utils -> set()
....... check: octave-optim
........ direct: octave-optim -> set()
....... check: octave-splines
........ direct: octave-splines -> set()
....... check: octave-struct
........ direct: octave-struct -> {'octave-optim'}
........ check: octave-optim
...... check: swftools
....... direct: swftools -> {'r-animation'}
....... check: r-animation
........ direct: r-animation -> {'r-phytools'}
........ check: r-phytools
......... direct: r-phytools -> {'r-convevol', 'r-paleotree'}
......... check: r-convevol
.......... direct: r-convevol -> set()
......... check: r-paleotree
.......... direct: r-paleotree -> set()
...... check: gromacs
....... direct: gromacs -> {'votca-csg'}
....... check: votca-csg
........ direct: votca-csg -> {'votca-ctp', 'votca-csg-tutorials', 'votca-xtp', 'votca-csgapps'}
........ check: votca-ctp
......... direct: votca-ctp -> set()
........ check: votca-csg-tutorials
......... direct: votca-csg-tutorials -> set()
........ check: votca-xtp
........ check: votca-csgapps
......... direct: votca-csgapps -> set()
...... check: nfft
....... direct: nfft -> set()
...... check: vpfft
....... direct: vpfft -> set()
...... check: openfoam
...... check: grass
...... check: ctffind
....... direct: ctffind -> set()
...... check: root
...... check: votca-tools
....... direct: votca-tools -> {'votca-csg', 'votca-ctp', 'votca-xtp'}
....... check: votca-csg
....... check: votca-ctp
....... check: votca-xtp
...... check: quantum-espresso
...... check: tinker
....... direct: tinker -> set()
...... check: elk
....... direct: elk -> set()
...... check: qbox
....... direct: qbox -> set()
...... check: libvdwxc
...... check: cp2k
...... check: abinit
....... direct: abinit -> set()
...... check: aoflagger
...... check: py-cvxopt
....... direct: py-cvxopt -> set()
..... check: hpl
...... direct: hpl -> set()
..... check: linsys-v
...... direct: linsys-v -> set()
..... check: libvdwxc
..... check: exasp2
...... direct: exasp2 -> set()
..... check: samrai
...... direct: samrai -> {'cleverleaf'}
...... check: cleverleaf
....... direct: cleverleaf -> set()
..... check: latte
..... check: charmpp
...... direct: charmpp -> {'quinoa', 'namd'}
...... check: quinoa
...... check: namd
..... check: parallel-netcdf
...... direct: parallel-netcdf -> {'accfft', 'ior', 'netcdf-c', 'esmf', 'ecp-io-sdk', 'trilinos', 'moab'}
...... check: accfft
...... check: ior
....... direct: ior -> set()
...... check: netcdf-c
....... direct: netcdf-c -> {'nco', 'adios', 'cdo', 'asagi', 'gmt', 'dealii', 'netcdf-cxx', 'ioapi', 'grib-api', 'gdl', 'eccodes', 'fpocket', 'hc', 'esmf', 'regcm', 'py-pynio', 'qgis', 'trilinos-catalyst-ioss-adapter', 'pism', 'moab', 'vtk', 'ncl', 'lammps', 'siesta', 'seacas', 'yambo', 'netcdf-fortran', 'py-cdat-lite', 'xios', 'channelflow', 'fstrack', 'ncview', 'sz', 'exodusii', 'ferret', 'paraview', 'grass', 'nccmp', 'r-ncdf4', 'trilinos', 'cmor', 'mfem', 'gdal', 'netcdf-cxx4', 'py-netcdf4'}
....... check: nco
........ direct: nco -> set()
....... check: adios
....... check: cdo
....... check: asagi
........ direct: asagi -> set()
....... check: gmt
....... check: dealii
....... check: netcdf-cxx
........ direct: netcdf-cxx -> {'magics', 'dealii', 'vtk'}
........ check: magics
........ check: dealii
........ check: vtk
......... direct: vtk -> {'openfoam', 'sensei', 'py-mayavi', 'memsurfer', 'opencascade', 'visit', 'fenics', 'liggghts', 'opencv', 'libmesh'}
......... check: openfoam
......... check: sensei
......... check: py-mayavi
......... check: memsurfer
......... check: opencascade
.......... direct: opencascade -> {'gmsh'}
.......... check: gmsh
......... check: visit
......... check: fenics
......... check: liggghts
.......... direct: liggghts -> set()
......... check: opencv
......... check: libmesh
....... check: ioapi
........ direct: ioapi -> set()
....... check: grib-api
........ direct: grib-api -> {'cdo', 'magics', 'libemos'}
........ check: cdo
........ check: magics
........ check: libemos
....... check: gdl
....... check: eccodes
....... check: fpocket
........ direct: fpocket -> set()
....... check: hc
....... check: esmf
........ direct: esmf -> {'ncl'}
........ check: ncl
....... check: regcm
........ direct: regcm -> set()
....... check: py-pynio
....... check: qgis
....... check: trilinos-catalyst-ioss-adapter
....... check: pism
....... check: moab
....... check: vtk
....... check: ncl
....... check: lammps
....... check: siesta
........ direct: siesta -> set()
....... check: seacas
....... check: yambo
....... check: netcdf-fortran
........ direct: netcdf-fortran -> {'siesta', 'yambo', 'octopus', 'xios', 'esmf', 'regcm', 'amber', 'etsf-io', 'elmerfem', 'abinit', 'ferret', 'ioapi'}
........ check: siesta
........ check: yambo
........ check: octopus
........ check: xios
......... direct: xios -> set()
........ check: esmf
........ check: regcm
........ check: amber
......... direct: amber -> set()
........ check: etsf-io
......... direct: etsf-io -> set()
........ check: elmerfem
........ check: abinit
........ check: ferret
......... direct: ferret -> set()
........ check: ioapi
....... check: py-cdat-lite
....... check: xios
....... check: channelflow
....... check: fstrack
....... check: ncview
........ direct: ncview -> set()
....... check: sz
....... check: exodusii
....... check: ferret
....... check: paraview
....... check: grass
....... check: nccmp
........ direct: nccmp -> set()
....... check: r-ncdf4
........ direct: r-ncdf4 -> {'r-mzr'}
........ check: r-mzr
......... direct: r-mzr -> {'r-msnbase'}
......... check: r-msnbase
.......... direct: r-msnbase -> set()
....... check: trilinos
....... check: cmor
........ direct: cmor -> set()
....... check: mfem
....... check: gdal
....... check: netcdf-cxx4
........ direct: netcdf-cxx4 -> {'ibmisc'}
........ check: ibmisc
....... check: py-netcdf4
...... check: esmf
...... check: ecp-io-sdk
...... check: trilinos
...... check: moab
..... check: phasta
...... direct: phasta -> {'fastmath'}
...... check: fastmath
..... check: py-torch
..... check: scr
...... direct: scr -> {'macsio', 'axom'}
...... check: macsio
...... check: axom
..... check: orca
...... direct: orca -> set()
..... check: cosp2
...... direct: cosp2 -> set()
..... check: hpx5
...... direct: hpx5 -> set()
..... check: scorec-core
..... check: sw4lite
...... direct: sw4lite -> {'ecp-proxy-apps'}
...... check: ecp-proxy-apps
..... check: siesta
..... check: rempi
...... direct: rempi -> set()
..... check: exampm
...... direct: exampm -> set()
..... check: MeshSimAdvanced
...... direct: MeshSimAdvanced -> set()
..... check: boost
..... check: minismac2d
...... direct: minismac2d -> set()
..... check: draco
..... check: icet
...... direct: icet -> {'isaac'}
...... check: isaac
....... direct: isaac -> set()
..... check: ppopen-appl-fdm
...... direct: ppopen-appl-fdm -> set()
..... check: libsplash
...... direct: libsplash -> set()
..... check: scotch
...... direct: scotch -> {'ghost', 'openfoam', 'mumps', 'nektar', 'openfoam-org', 'strumpack', 'fenics', 'mmg', 'foam-extend', 'akantu'}
...... check: ghost
...... check: openfoam
...... check: mumps
...... check: nektar
...... check: openfoam-org
...... check: strumpack
...... check: fenics
...... check: mmg
....... direct: mmg -> set()
...... check: foam-extend
...... check: akantu
..... check: xsbench
...... direct: xsbench -> {'ecp-proxy-apps'}
...... check: ecp-proxy-apps
..... check: adios
..... check: phylobayesmpi
...... direct: phylobayesmpi -> set()
..... check: arborx
...... direct: arborx -> set()
..... check: cosmomc
..... check: dihydrogen
...... direct: dihydrogen -> set()
..... check: tioga
...... direct: tioga -> {'nalu-wind', 'nalu'}
...... check: nalu-wind
...... check: nalu
..... check: catalyst
...... direct: catalyst -> {'ecp-viz-sdk', 'damaris', 'of-catalyst'}
...... check: ecp-viz-sdk
...... check: damaris
...... check: of-catalyst
..... check: bertini
...... direct: bertini -> set()
..... check: revocap-coupler
...... direct: revocap-coupler -> {'frontistr'}
...... check: frontistr
..... check: valgrind
...... direct: valgrind -> {'libpsl', 'openmpi', 'hpx', 'petsc', 'wget', 'lz4', 'flux-core', 'argobots'}
...... check: libpsl
....... direct: libpsl -> {'wget'}
....... check: wget
........ direct: wget -> {'icedtea', 'kraken2'}
........ check: icedtea
......... direct: icedtea -> {'pasta', 'mapserver', 'ignite', 'rocketmq', 'astral', 'kinesis', 'stc', 'hadoop', 'libbeagle', 'pgdspider', 'aperture-photometry', 'qorts', 'thrift', 'trimmomatic', 'opentsdb', 'interproscan', 'picard', 'sbt', 'jafka', 'figtree', 'genomefinisher', 'openmpi', 'graphviz', 'drill', 'trinity', 'rdptools', 'igv', 'py-jpype1', 'gdal', 'mrbench', 'hbase', 'jmol', 'ant', 'grnboost', 'geode', 'cosbench', 'plplot', 'pilon', 'canal', 'flink', 'maven', 'antlr', 'pagit', 'sbml', 'jackcess', 'gradle', 'hdf', 'mixcr', 'nextflow', 'py-pipits', 'sqoop', 'vardictjava', 'bbmap', 'cromwell', 'jblob', 'flume', 'lucene', 'r-xlconnect', 'accumulo', 'bref3', 'py-crispresso', 'opencv', 'igvtools', 'beast1', 'commons-logging', 'sumo', 'r-xlsx', 'graylog2-server', 'tfel', 'ucx', 'fastqc', 'r-xlconnectjars', 'commons-lang', 'storm', 'minced', 'fseq', 'py-quast', 'kafka', 'spark', 'prism', 'rdp-classifier', 'tajo', 'commons-lang3', 'beagle', 'beast-tracer', 'claw', 'r-seurat', 'rose', 'gatk', 'hpcviewer', 'tassel', 'cromwell-womtool', 'varscan', 'blast2go', 'haploview', 'scala', 'beast2', 'cassandra', 'octave', 'r', 'r-rjava', 'testdfsio', 'elasticsearch'}
......... check: pasta
.......... direct: pasta -> set()
......... check: mapserver
......... check: ignite
.......... direct: ignite -> set()
......... check: rocketmq
.......... direct: rocketmq -> set()
......... check: astral
.......... direct: astral -> set()
......... check: kinesis
.......... direct: kinesis -> set()
......... check: stc
.......... direct: stc -> {'savanna', 'mpix-launch-swift'}
.......... check: savanna
.......... check: mpix-launch-swift
........... direct: mpix-launch-swift -> {'savanna'}
........... check: savanna
......... check: hadoop
.......... direct: hadoop -> {'testdfsio', 'hive', 'spark'}
.......... check: testdfsio
........... direct: testdfsio -> set()
.......... check: hive
........... direct: hive -> set()
.......... check: spark
........... direct: spark -> {'grnboost'}
........... check: grnboost
............ direct: grnboost -> set()
......... check: libbeagle
.......... direct: libbeagle -> {'mrbayes', 'beast1'}
.......... check: mrbayes
........... direct: mrbayes -> set()
.......... check: beast1
........... direct: beast1 -> set()
......... check: pgdspider
......... check: aperture-photometry
.......... direct: aperture-photometry -> set()
......... check: qorts
.......... direct: qorts -> set()
......... check: thrift
.......... direct: thrift -> {'arrow', 'parquet-cpp'}
.......... check: arrow
.......... check: parquet-cpp
......... check: trimmomatic
.......... direct: trimmomatic -> set()
......... check: opentsdb
.......... direct: opentsdb -> set()
......... check: interproscan
.......... direct: interproscan -> {'maker'}
.......... check: maker
......... check: picard
.......... direct: picard -> {'phyluce'}
.......... check: phyluce
......... check: sbt
.......... direct: sbt -> {'grnboost'}
.......... check: grnboost
......... check: jafka
.......... direct: jafka -> set()
......... check: figtree
.......... direct: figtree -> {'treesub'}
.......... check: treesub
........... direct: treesub -> set()
......... check: genomefinisher
.......... direct: genomefinisher -> set()
......... check: openmpi
......... check: graphviz
.......... direct: graphviz -> {'cbtf-argonavis-gui', 'maloc', 'ladot', 'axom', 'r-rgraphviz', 'graphicsmagick', 'doxygen', 'qtgraph', 'stat', 'uriparser', 'comd', 'opensubdiv', 'root', 'flecsi', 'dealii', 'py-pyside2', 'librom', 'py-pydot'}
.......... check: cbtf-argonavis-gui
........... direct: cbtf-argonavis-gui -> set()
.......... check: maloc
........... direct: maloc -> set()
.......... check: ladot
........... direct: ladot -> set()
.......... check: axom
.......... check: r-rgraphviz
........... direct: r-rgraphviz -> {'r-pathview', 'r-gostats'}
........... check: r-pathview
............ direct: r-pathview -> set()
........... check: r-gostats
............ direct: r-gostats -> {'r-affycoretools', 'r-reportingtools'}
............ check: r-affycoretools
............ check: r-reportingtools
.......... check: graphicsmagick
........... direct: graphicsmagick -> {'gdl', 'gmt'}
........... check: gdl
........... check: gmt
.......... check: doxygen
........... direct: doxygen -> {'openmm', 'py-espressopp', 'revocap-refiner', 'paradiseo', 'dealii', 'mysql', 'py-breathe', 'maloc', 'glfw', 'mmg', 'vigra', 'uriparser', 'ibmisc', 'axom', 'hoomd-blue', 'netcdf-fortran', 'clingo', 'nest', 'pnmpi', 'pbbam', 'geopm', 'opensubdiv', 'wireshark', 'conduit', 'gaudi', 'uvw', 'librom', 'netcdf-cxx4', 'range-v3'}
........... check: openmm
........... check: py-espressopp
........... check: revocap-refiner
........... check: paradiseo
........... check: dealii
........... check: mysql
........... check: py-breathe
............ direct: py-breathe -> {'lbann'}
............ check: lbann
........... check: maloc
........... check: glfw
............ direct: glfw -> {'opensubdiv'}
............ check: opensubdiv
............. direct: opensubdiv -> set()
........... check: mmg
........... check: vigra
........... check: uriparser
............ direct: uriparser -> {'libkml'}
............ check: libkml
............. direct: libkml -> {'gdal'}
............. check: gdal
........... check: ibmisc
........... check: axom
........... check: hoomd-blue
........... check: netcdf-fortran
........... check: clingo
............ direct: clingo -> {'aspcud'}
............ check: aspcud
............. direct: aspcud -> set()
........... check: nest
........... check: pnmpi
............ direct: pnmpi -> set()
........... check: pbbam
............ direct: pbbam -> {'blasr', 'blasr-libcpp'}
............ check: blasr
............. direct: blasr -> {'pbsuite', 'halc'}
............. check: pbsuite
.............. direct: pbsuite -> set()
............. check: halc
............ check: blasr-libcpp
............. direct: blasr-libcpp -> {'blasr'}
............. check: blasr
........... check: geopm
........... check: opensubdiv
........... check: wireshark
............ direct: wireshark -> set()
........... check: conduit
........... check: gaudi
........... check: uvw
............ direct: uvw -> set()
........... check: librom
............ direct: librom -> set()
........... check: netcdf-cxx4
........... check: range-v3
............ direct: range-v3 -> {'gaudi'}
............ check: gaudi
.......... check: qtgraph
........... direct: qtgraph -> {'cbtf-argonavis-gui'}
........... check: cbtf-argonavis-gui
.......... check: stat
.......... check: uriparser
.......... check: comd
........... direct: comd -> {'ecp-proxy-apps'}
........... check: ecp-proxy-apps
.......... check: opensubdiv
.......... check: root
.......... check: flecsi
.......... check: dealii
.......... check: py-pyside2
........... direct: py-pyside2 -> {'py-qtpy'}
........... check: py-qtpy
............ direct: py-qtpy -> {'py-qtawesome', 'py-spyder'}
............ check: py-qtawesome
............. direct: py-qtawesome -> {'py-spyder'}
............. check: py-spyder
.............. direct: py-spyder -> {'py-guidata'}
.............. check: py-guidata
............ check: py-spyder
.......... check: librom
.......... check: py-pydot
........... direct: py-pydot -> {'py-hatchet', 'py-goatools'}
........... check: py-hatchet
........... check: py-goatools
......... check: drill
.......... direct: drill -> set()
......... check: trinity
......... check: rdptools
.......... direct: rdptools -> {'py-pipits'}
.......... check: py-pipits
......... check: igv
.......... direct: igv -> set()
......... check: py-jpype1
......... check: gdal
......... check: mrbench
.......... direct: mrbench -> set()
......... check: hbase
.......... direct: hbase -> set()
......... check: jmol
.......... direct: jmol -> set()
......... check: ant
.......... direct: ant -> {'beast-tracer', 'claw', 'rdptools', 'opencv', 'treesub', 'stc'}
.......... check: beast-tracer
........... direct: beast-tracer -> set()
.......... check: claw
........... direct: claw -> set()
.......... check: rdptools
.......... check: opencv
.......... check: treesub
.......... check: stc
......... check: grnboost
......... check: geode
.......... direct: geode -> set()
......... check: cosbench
.......... direct: cosbench -> set()
......... check: plplot
......... check: pilon
.......... direct: pilon -> {'py-unicycler'}
.......... check: py-unicycler
........... direct: py-unicycler -> set()
......... check: canal
.......... direct: canal -> set()
......... check: flink
.......... direct: flink -> set()
......... check: maven
.......... direct: maven -> {'mrbench', 'testdfsio', 'ucx', 'neo4j', 'sqoop', 'kinesis', 'canal', 'interproscan', 'accumulo', 'zookeeper-benchmark'}
.......... check: mrbench
.......... check: testdfsio
.......... check: ucx
........... direct: ucx -> {'openmpi', 'mpich', 'libfabric', 'charmpp'}
........... check: openmpi
........... check: mpich
............ direct: mpich -> {'powerapi', 'pennant', 'mesquite', 'modylas', 'citcoms', 'strumpack', 'warpx', 'pruners-ninja', 'mpi-bash', 'libsharp', 'hydrogen', 'cgns', 'abyss', 'py-adios', 'exodusii', 'nest', 'faodel', 'mofem-cephas', 'openfoam-org', 'hpgmg', 'trilinos', 'libcircle', 'stat', 'py-pypar', 'revocap-refiner', 'kahip', 'parmetis', 'mitos', 'paradiseo', 'dealii', 'mpifileutils', 'elmerfem', 'libmesh', 'ppopen-appl-bem-at', 'heffte', 'qmd-progress', 'octopus', 'eztrace', 'kripke', 'fftw', 'hpl', 'linsys-v', 'libvdwxc', 'exasp2', 'samrai', 'latte', 'charmpp', 'parallel-netcdf', 'phasta', 'py-torch', 'scr', 'cosp2', 'hpx5', 'scorec-core', 'sw4lite', 'siesta', 'rempi', 'exampm', 'MeshSimAdvanced', 'boost', 'minismac2d', 'draco', 'icet', 'ppopen-appl-fdm', 'kallisto', 'libsplash', 'scotch', 'xsbench', 'adios', 'phylobayesmpi', 'arborx', 'cosmomc', 'dihydrogen', 'tioga', 'catalyst', 'bertini', 'revocap-coupler', 'valgrind', 'ember', 'alquimia', 'picsar', 'flecsi', 'pism', 'py-tensorflow', 'mrbayes', 'ppopen-math-vis', 'pmlib', 'ross', 'cram', 'veloc', 'akantu', 'openfoam', 'hpx', 'minivite', 'funhpc', 'grackle', 'hpctoolkit', 'damaris', 'adept-utils', 'elpa', 'py-espressopp', 'intel-mpi-benchmarks', 'cloverleaf3d', 'ebms', 'xbraid', 'panda', 'sirius', 'ampliconnoise', 'netcdf-fortran', 'bookleaf-cpp', 'muster', 'albany', 'aluminum', 'aspa', 'mrcpp', 'ascent', 'minighost', 'kokkos-nvcc-wrapper', 'dbcsr', 'converge', 'vtk-m', 'phist', 'qmcpack', 'minitri', 'lulesh', 'minife', 'pagmo', 'opencoarrays', 'gslib', 'megadock', 'vtk', 'flecsph', 'hpccg', 'elemental', 'lwgrp', 'adlbx', 'pbmpi', 'yambo', 'hoomd-blue', 'tau', 'vpfft', 'openstf', 'dftfe', 'boxlib', 'mpileaks', 'mfem', 'sosflow', 'minimd', 'spfft', 'ppopen-appl-fem', 'wannier90', 'amp', 'ior', 'cbench', 'ntpoly', 'ppopen-appl-fdm-at', 'sst-macro', 'dataspaces', 'parsplice', 'lwm2', 'camellia', 'mpe2', 'macsio', 'upcxx', 'ppopen-appl-bem', 'scorep', 'cabana', 'scalasca', 'globalarrays', 'simul', 'qbox', 'mercury', 'med', 'bml', 'nccl-tests', 'fenics', 'amber', 'liggghts', 'hmmer', 'hdf5', 'athena', 'precice', 'slate', 'quicksilver', 'exabayes', 'extrae', 'flux-core', 'ray', 'meshkit', 'cardioid', 'hypre', 'eq-r', 'regcm', 'axom', 'seacas', 'ccs-qcd', 'meep', 'raft', 'omega-h', 'libnrm', 'textparser', 'adiak', 'lbann', 'callpath', 'adios2', 'amgx', 'minixyce', 'py-gpaw', 'osu-micro-benchmarks', 'nalu-wind', 'py-h5py', 'ppopen-appl-amr-fdm', 'py-espresso', 'fast-global-file-status', 'sundials', 'simplemoc', 'steps', 'shuffile', 'channelflow', 'ffr', 'cbtf-krell', 'tracer', 'netcdf-c', 'thornado-mini', 'gmsh', 'cntk', 'meme', 'py-mpi4py', 'amg2013', 'ppopen-appl-dem-util', 'mpip', 'relion', 'infernal', 'camx', 'vampirtrace', 'opencv', 'chatterbug', 'sgpp', 'nekcem', 'hwloc', 'cloverleaf', 'papyrus', 'geopm', 'elk', 'dtcmp', 'swiftsim', 'tasmanian', 'timemory', 'redset', 'snap', 'nalu', 'nek5000', 'h5cpp', 'flann', 'mpix-launch-swift', 'ompss', 'er', 'miniamr', 'p3dfft3', 'tealeaf', 'elsi', 'damselfly', 'py-horovod', 'zoltan', 'sst-core', 'mdtest', 'visit', 'henson', 'caliper', 'foam-extend', 'pflotran', 'jali', 'nekbone', 'hiop', 'nektar', 'picsarlite', 'libnbc', 'libhio', 'netgauge', 'iq-tree', 'pfft', 'revbayes', 'moab', 'openfdtd', 'openmx', 'openfast', 'lammps', 'cosma', 'amrex', 'ppopen-math-mp', 'libquo', 'helics', 'swfft', 'amrvis', 'gromacs', 'maker', 'isaac', 'h5hut', 'raxml', 'vpic', 'filo', 'p4est', 'abinit', 'frontistr', 'rankstr', 'npb', 'gasnet', 'diy', 'netgen', 'pidx', 'xdmf3', 'parmgridgen', 'mumps', 'shengbte', 'everytrace', 'mstk', 'kvtree', 'turbine', 'ppopen-appl-fvm', 'pnmpi', 'openpmd-api', 'amg', 'clamr', 'py-python-meep', 'conduit', 'spath', 'arpack-ng', 'miniqmc', 'cp2k', 'librom', 'asagi', 'superlu-dist', 'petsc', 'eem', 'swap-assembler', 'ghost', 'coevp', 'py-libensemble', 'codes', 'pumi', 'neuron', 'butterflypack', 'darshan-runtime', 'tycho2', 'plumed', 'dakota', 'portage', 'comd', 'branson', 'r-rmpi', 'unifyfs', 'minigmg', 'automaded', 'typhon', 'h5part', 'netlib-scalapack', 'chombo', 'vtk-h', 'silo', 'xsdktrilinos', 'esmf', 'fastmath', 'pfunit', 'savanna', 'xios', 'graph500', 'cgm', 'paraview', 'examinimd', 'nwchem', 'quantum-espresso', 'typhonio', 'multiverso'}
............ check: powerapi
............ check: pennant
............ check: mesquite
............ check: modylas
............ check: citcoms
............ check: strumpack
............ check: warpx
............ check: pruners-ninja
............ check: mpi-bash
............ check: libsharp
............ check: hydrogen
............ check: cgns
............ check: abyss
............ check: py-adios
............ check: exodusii
............ check: nest
............ check: faodel
............ check: mofem-cephas
............ check: openfoam-org
............ check: hpgmg
............ check: trilinos
............ check: libcircle
............ check: stat
............ check: py-pypar
............ check: revocap-refiner
............ check: kahip
............ check: parmetis
............ check: mitos
............ check: paradiseo
............ check: dealii
............ check: mpifileutils
............ check: elmerfem
............ check: libmesh
............ check: ppopen-appl-bem-at
............ check: heffte
............ check: qmd-progress
............ check: octopus
............ check: eztrace
............ check: kripke
............ check: fftw
............ check: hpl
............ check: linsys-v
............ check: libvdwxc
............ check: exasp2
............ check: samrai
............ check: latte
............ check: charmpp
............ check: parallel-netcdf
............ check: phasta
............ check: py-torch
............ check: scr
............ check: cosp2
............ check: hpx5
............ check: scorec-core
............ check: sw4lite
............ check: siesta
............ check: rempi
............ check: exampm
............ check: MeshSimAdvanced
............ check: boost
............ check: minismac2d
............ check: draco
............ check: icet
............ check: ppopen-appl-fdm
............ check: kallisto
............. direct: kallisto -> {'trinity'}
............. check: trinity
............ check: libsplash
............ check: scotch
............ check: xsbench
............ check: adios
............ check: phylobayesmpi
............ check: arborx
............ check: cosmomc
............ check: dihydrogen
............ check: tioga
............ check: catalyst
............ check: bertini
............ check: revocap-coupler
............ check: valgrind
............ check: ember
............. direct: ember -> {'ecp-proxy-apps'}
............. check: ecp-proxy-apps
............ check: alquimia
............ check: picsar
............ check: flecsi
............ check: pism
............ check: py-tensorflow
............ check: mrbayes
............ check: ppopen-math-vis
............. direct: ppopen-math-vis -> {'ppopen-appl-fdm'}
............. check: ppopen-appl-fdm
............ check: pmlib
............. direct: pmlib -> set()
............ check: ross
............. direct: ross -> {'codes'}
............. check: codes
.............. direct: codes -> {'tracer'}
.............. check: tracer
............... direct: tracer -> set()
............ check: cram
............. direct: cram -> set()
............ check: veloc
............. direct: veloc -> {'ecp-io-sdk'}
............. check: ecp-io-sdk
............ check: akantu
............ check: openfoam
............ check: hpx
............. direct: hpx -> {'flecsi', 'kokkos'}
............. check: flecsi
............. check: kokkos
.............. direct: kokkos -> {'cabana', 'exampm', 'lammps', 'kokkos-kernels'}
.............. check: cabana
............... direct: cabana -> {'exampm'}
............... check: exampm
.............. check: exampm
.............. check: lammps
.............. check: kokkos-kernels
............... direct: kokkos-kernels -> set()
............ check: minivite
............. direct: minivite -> {'ecp-proxy-apps'}
............. check: ecp-proxy-apps
............ check: funhpc
............. direct: funhpc -> set()
............ check: grackle
............. direct: grackle -> set()
............ check: hpctoolkit
............. direct: hpctoolkit -> set()
............ check: damaris
............ check: adept-utils
............. direct: adept-utils -> {'callpath', 'mpileaks'}
............. check: callpath
.............. direct: callpath -> {'automaded', 'mpileaks'}
.............. check: automaded
............... direct: automaded -> set()
.............. check: mpileaks
............... direct: mpileaks -> set()
............. check: mpileaks
............ check: elpa
............ check: py-espressopp
............ check: intel-mpi-benchmarks
............. direct: intel-mpi-benchmarks -> set()
............ check: cloverleaf3d
............. direct: cloverleaf3d -> set()
............ check: ebms
............. direct: ebms -> set()
............ check: xbraid
............. direct: xbraid -> set()
............ check: panda
............. direct: panda -> set()
............ check: sirius
............ check: ampliconnoise
............. direct: ampliconnoise -> set()
............ check: netcdf-fortran
............ check: bookleaf-cpp
............ check: muster
............. direct: muster -> {'ravel'}
............. check: ravel
.............. direct: ravel -> set()
............ check: albany
............ check: aluminum
............. direct: aluminum -> {'dihydrogen', 'hydrogen', 'lbann'}
............. check: dihydrogen
............. check: hydrogen
............. check: lbann
............ check: aspa
............ check: mrcpp
............. direct: mrcpp -> set()
............ check: ascent
............ check: minighost
............. direct: minighost -> set()
............ check: kokkos-nvcc-wrapper
............. direct: kokkos-nvcc-wrapper -> {'kokkos'}
............. check: kokkos
............ check: dbcsr
............. direct: dbcsr -> set()
............ check: converge
............. direct: converge -> set()
............ check: vtk-m
............. direct: vtk-m -> {'vtk-h', 'ecp-viz-sdk'}
............. check: vtk-h
.............. direct: vtk-h -> {'ascent'}
.............. check: ascent
............. check: ecp-viz-sdk
............ check: phist
............ check: qmcpack
............ check: minitri
............. direct: minitri -> {'ecp-proxy-apps'}
............. check: ecp-proxy-apps
............ check: lulesh
............. direct: lulesh -> set()
............ check: minife
............. direct: minife -> {'ecp-proxy-apps'}
............. check: ecp-proxy-apps
............ check: pagmo
............ check: opencoarrays
............. direct: opencoarrays -> set()
............ check: gslib
............. direct: gslib -> {'ceed', 'mfem'}
............. check: ceed
............. check: mfem
............ check: megadock
............ check: vtk
............ check: flecsph
............ check: hpccg
............. direct: hpccg -> set()
............ check: elemental
............. direct: elemental -> {'lbann'}
............. check: lbann
............ check: lwgrp
............. direct: lwgrp -> {'dtcmp'}
............. check: dtcmp
.............. direct: dtcmp -> {'mpifileutils', 'scr'}
.............. check: mpifileutils
.............. check: scr
............ check: adlbx
............. direct: adlbx -> {'turbine'}
............. check: turbine
.............. direct: turbine -> {'stc'}
.............. check: stc
............ check: pbmpi
............. direct: pbmpi -> set()
............ check: yambo
............ check: hoomd-blue
............ check: tau
............ check: vpfft
............ check: openstf
............. direct: openstf -> set()
............ check: dftfe
............ check: boxlib
............. direct: boxlib -> set()
............ check: mpileaks
............ check: mfem
............ check: sosflow
............. direct: sosflow -> {'caliper'}
............. check: caliper
.............. direct: caliper -> {'flecsi', 'bookleaf-cpp', 'timemory', 'kripke'}
.............. check: flecsi
.............. check: bookleaf-cpp
.............. check: timemory
.............. check: kripke
............ check: minimd
............. direct: minimd -> set()
............ check: spfft
............ check: ppopen-appl-fem
............. direct: ppopen-appl-fem -> set()
............ check: wannier90
............. direct: wannier90 -> {'cp2k'}
............. check: cp2k
............ check: amp
............ check: ior
............ check: cbench
............ check: ntpoly
............. direct: ntpoly -> {'elsi'}
............. check: elsi
............ check: ppopen-appl-fdm-at
............. direct: ppopen-appl-fdm-at -> set()
............ check: sst-macro
............ check: dataspaces
............. direct: dataspaces -> {'adios', 'adios2'}
............. check: adios
............. check: adios2
............ check: parsplice
............ check: lwm2
............. direct: lwm2 -> set()
............ check: camellia
............ check: mpe2
............. direct: mpe2 -> {'clamr'}
............. check: clamr
.............. direct: clamr -> set()
............ check: macsio
............ check: upcxx
............. direct: upcxx -> {'timemory'}
............. check: timemory
............ check: ppopen-appl-bem
............. direct: ppopen-appl-bem -> {'ppopen-appl-bem-at'}
............. check: ppopen-appl-bem-at
............ check: scorep
............. direct: scorep -> {'tau', 'chatterbug'}
............. check: tau
............. check: chatterbug
.............. direct: chatterbug -> set()
............ check: cabana
............ check: scalasca
............. direct: scalasca -> set()
............ check: globalarrays
............. direct: globalarrays -> set()
............ check: simul
............. direct: simul -> set()
............ check: qbox
............ check: mercury
............. direct: mercury -> {'unifyfs', 'margo', 'ecp-io-sdk'}
............. check: unifyfs
.............. direct: unifyfs -> {'ecp-io-sdk'}
.............. check: ecp-io-sdk
............. check: margo
.............. direct: margo -> {'unifyfs'}
.............. check: unifyfs
............. check: ecp-io-sdk
............ check: med
............. direct: med -> {'mofem-cephas'}
............. check: mofem-cephas
............ check: bml
............. direct: bml -> {'qmd-progress', 'exasp2'}
............. check: qmd-progress
............. check: exasp2
............ check: nccl-tests
............. direct: nccl-tests -> set()
............ check: fenics
............ check: amber
............ check: liggghts
............ check: hmmer
............. direct: hmmer -> {'itsx', 'trinotate', 'py-pipits', 'repeatmasker', 'busco', 'barrnap', 'prokka', 'py-checkm-genome', 'orthofiller'}
............. check: itsx
.............. direct: itsx -> {'py-pipits'}
.............. check: py-pipits
............. check: trinotate
............. check: py-pipits
............. check: repeatmasker
.............. direct: repeatmasker -> {'repeatmodeler', 'maker'}
.............. check: repeatmodeler
............... direct: repeatmodeler -> set()
.............. check: maker
............. check: busco
............. check: barrnap
.............. direct: barrnap -> set()
............. check: prokka
.............. direct: prokka -> set()
............. check: py-checkm-genome
............. check: orthofiller
............ check: hdf5
............. direct: hdf5 -> {'nektar', 'saga-gis', 'pacbio-dextractor', 'casacore', 'py-tables', 'simulationio', 'libhio', 'quinoa', 'citcoms', 'regcm', 'py-pynio', 'openmc', 'moab', 'cleverleaf', 'sirius', 'openfast', 'lammps', 'axom', 'sensei', 'memsurfer', 'cgns', 'aspa', 'meep', 'julea', 'raft', 'sz', 'openmolcas', 'faodel', 'h5hut', 'mofem-cephas', 'ecp-io-sdk', 'trilinos', 'adios2', 'gdal', 'abinit', 'qmcpack', 'dealii', 'lulesh', 'xdmf3', 'py-h5py', 'armadillo', 'qgis', 'py-espresso', 'vigra', 'vtk', 'flecsph', 'libmesh', 'highfive', 'matio', 'yambo', 'votca-xtp', 'channelflow', 'molcas', 'turbine', 'openpmd-api', 'netcdf-c', 'thornado-mini', 'conduit', 'gmsh', 'librom', 'cdo', 'samrai', 'r-hdf5r', 'damaris', 'imp', 'petsc', 'hdf5-blosc', 'amp', 'ior', 'opencv', 'camellia', 'macsio', 'unifyfs', 'cmor', 'kallisto', 'rockstar', 'swiftsim', 'libsplash', 'h5part', 'py-sfepy', 'med', 'chombo', 'adios', 'py-mdanalysis', 'h5cpp', 'fenics', 'etsf-io', 'kealib', 'caffe', 'slurm', 'flann', 'hal', 'blasr', 'silo', 'gdl', 'alquimia', 'flecsi', 'ncl', 'xios', 'votca-csg', 'h5utils', 'octave', 'ferret', 'sst-core', 'paraview', 'blasr-libcpp', 'grackle', 'quantum-espresso', 'visit', 'legion', 'typhonio', 'pflotran', 'h5z-zfp', 'py-netcdf4'}
............. check: nektar
............. check: saga-gis
............. check: pacbio-dextractor
.............. direct: pacbio-dextractor -> {'falcon'}
.............. check: falcon
............... direct: falcon -> set()
............. check: casacore
............. check: py-tables
............. check: simulationio
............. check: libhio
.............. direct: libhio -> set()
............. check: quinoa
............. check: citcoms
............. check: regcm
............. check: py-pynio
............. check: openmc
.............. direct: openmc -> set()
............. check: moab
............. check: cleverleaf
............. check: sirius
............. check: openfast
............. check: lammps
............. check: axom
............. check: sensei
............. check: memsurfer
............. check: cgns
............. check: aspa
............. check: meep
............. check: julea
.............. direct: julea -> set()
............. check: raft
............. check: sz
............. check: openmolcas
............. check: faodel
............. check: h5hut
.............. direct: h5hut -> set()
............. check: mofem-cephas
............. check: ecp-io-sdk
............. check: trilinos
............. check: adios2
............. check: gdal
............. check: abinit
............. check: qmcpack
............. check: dealii
............. check: lulesh
............. check: xdmf3
.............. direct: xdmf3 -> set()
............. check: py-h5py
............. check: armadillo
.............. direct: armadillo -> {'gdal', 'pktools', 'lazyten'}
.............. check: gdal
.............. check: pktools
.............. check: lazyten
............... direct: lazyten -> set()
............. check: qgis
............. check: py-espresso
............. check: vigra
............. check: vtk
............. check: flecsph
............. check: libmesh
............. check: highfive
.............. direct: highfive -> set()
............. check: matio
.............. direct: matio -> {'trilinos', 'seacas'}
.............. check: trilinos
.............. check: seacas
............. check: yambo
............. check: votca-xtp
............. check: channelflow
............. check: molcas
.............. direct: molcas -> set()
............. check: turbine
............. check: openpmd-api
............. check: netcdf-c
............. check: thornado-mini
.............. direct: thornado-mini -> {'ecp-proxy-apps'}
.............. check: ecp-proxy-apps
............. check: conduit
............. check: gmsh
............. check: librom
............. check: cdo
............. check: samrai
............. check: r-hdf5r
.............. direct: r-hdf5r -> {'r-seurat'}
.............. check: r-seurat
............. check: damaris
............. check: imp
.............. direct: imp -> set()
............. check: petsc
............. check: hdf5-blosc
.............. direct: hdf5-blosc -> set()
............. check: amp
............. check: ior
............. check: opencv
............. check: camellia
............. check: macsio
............. check: unifyfs
............. check: cmor
............. check: kallisto
............. check: rockstar
.............. direct: rockstar -> {'py-yt'}
.............. check: py-yt
............. check: swiftsim
.............. direct: swiftsim -> set()
............. check: libsplash
............. check: h5part
.............. direct: h5part -> {'quinoa'}
.............. check: quinoa
............. check: py-sfepy
............. check: med
............. check: chombo
.............. direct: chombo -> {'fastmath'}
.............. check: fastmath
............. check: adios
............. check: py-mdanalysis
............. check: h5cpp
.............. direct: h5cpp -> set()
............. check: fenics
............. check: etsf-io
............. check: kealib
.............. direct: kealib -> {'gdal'}
.............. check: gdal
............. check: caffe
............. check: slurm
............. check: flann
.............. direct: flann -> {'coevp'}
.............. check: coevp
............... direct: coevp -> set()
............. check: hal
.............. direct: hal -> set()
............. check: blasr
............. check: silo
.............. direct: silo -> {'amp', 'coevp', 'samrai', 'bookleaf-cpp', 'exampm', 'visit', 'macsio', 'conduit', 'lulesh'}
.............. check: amp
.............. check: coevp
.............. check: samrai
.............. check: bookleaf-cpp
.............. check: exampm
.............. check: visit
.............. check: macsio
.............. check: conduit
.............. check: lulesh
............. check: gdl
............. check: alquimia
............. check: flecsi
............. check: ncl
............. check: xios
............. check: votca-csg
............. check: h5utils
............. check: octave
............. check: ferret
............. check: sst-core
............. check: paraview
............. check: blasr-libcpp
............. check: grackle
............. check: quantum-espresso
............. check: visit
............. check: legion
.............. direct: legion -> {'flecsi'}
.............. check: flecsi
............. check: typhonio
.............. direct: typhonio -> {'macsio'}
.............. check: macsio
............. check: pflotran
............. check: h5z-zfp
.............. direct: h5z-zfp -> set()
............. check: py-netcdf4
............ check: athena
............ check: precice
............ check: slate
............. direct: slate -> set()
............ check: quicksilver
............. direct: quicksilver -> set()
............ check: exabayes
............. direct: exabayes -> set()
............ check: extrae
............. direct: extrae -> {'ompss'}
............. check: ompss
.............. direct: ompss -> set()
............ check: flux-core
............. direct: flux-core -> {'flux-sched'}
............. check: flux-sched
.............. direct: flux-sched -> set()
............ check: ray
............. direct: ray -> {'biopieces'}
............. check: biopieces
............ check: meshkit
............ check: cardioid
............ check: hypre
............ check: eq-r
............. direct: eq-r -> set()
............ check: regcm
............ check: axom
............ check: seacas
............ check: ccs-qcd
............. direct: ccs-qcd -> set()
............ check: meep
............ check: raft
............ check: omega-h
............ check: libnrm
............. direct: libnrm -> set()
............ check: textparser
............. direct: textparser -> set()
............ check: adiak
............. direct: adiak -> {'caliper'}
............. check: caliper
............ check: lbann
............ check: callpath
............ check: adios2
............ check: amgx
............. direct: amgx -> set()
............ check: minixyce
............. direct: minixyce -> set()
............ check: py-gpaw
............ check: osu-micro-benchmarks
............. direct: osu-micro-benchmarks -> set()
............ check: nalu-wind
............ check: py-h5py
............ check: ppopen-appl-amr-fdm
............. direct: ppopen-appl-amr-fdm -> set()
............ check: py-espresso
............ check: fast-global-file-status
............. direct: fast-global-file-status -> {'stat'}
............. check: stat
............ check: sundials
............ check: simplemoc
............. direct: simplemoc -> set()
............ check: steps
............ check: shuffile
............. direct: shuffile -> {'er'}
............. check: er
.............. direct: er -> {'veloc', 'scr'}
.............. check: veloc
.............. check: scr
............ check: channelflow
............ check: ffr
............. direct: ffr -> set()
............ check: cbtf-krell
............. direct: cbtf-krell -> {'cbtf-argonavis-gui', 'openspeedshop', 'cbtf-argonavis', 'openspeedshop-utils', 'cbtf-lanl'}
............. check: cbtf-argonavis-gui
............. check: openspeedshop
.............. direct: openspeedshop -> set()
............. check: cbtf-argonavis
.............. direct: cbtf-argonavis -> {'cbtf-argonavis-gui', 'openspeedshop', 'openspeedshop-utils'}
.............. check: cbtf-argonavis-gui
.............. check: openspeedshop
.............. check: openspeedshop-utils
............... direct: openspeedshop-utils -> {'cbtf-argonavis-gui'}
............... check: cbtf-argonavis-gui
............. check: openspeedshop-utils
............. check: cbtf-lanl
.............. direct: cbtf-lanl -> set()
............ check: tracer
............ check: netcdf-c
............ check: thornado-mini
............ check: gmsh
............ check: cntk
............ check: meme
............. direct: meme -> set()
............ check: py-mpi4py
............. direct: py-mpi4py -> {'py-cogent', 'paraview', 'candle-benchmarks', 'openpmd-api', 'py-h5py', 'sensei', 'catalyst', 'py-espressopp', 'py-libensemble', 'py-adios', 'vtk', 'ascent', 'py-petsc4py', 'sirius', 'adios2'}
............. check: py-cogent
............. check: paraview
............. check: candle-benchmarks
............. check: openpmd-api
............. check: py-h5py
............. check: sensei
............. check: catalyst
............. check: py-espressopp
............. check: py-libensemble
............. check: py-adios
............. check: vtk
............. check: ascent
............. check: py-petsc4py
............. check: sirius
............. check: adios2
............ check: amg2013
............. direct: amg2013 -> set()
............ check: ppopen-appl-dem-util
............. direct: ppopen-appl-dem-util -> set()
............ check: mpip
............. direct: mpip -> set()
............ check: relion
............ check: infernal
............. direct: infernal -> {'trnascan-se'}
............. check: trnascan-se
.............. direct: trnascan-se -> set()
............ check: camx
............. direct: camx -> set()
............ check: vampirtrace
............. direct: vampirtrace -> set()
............ check: opencv
............ check: chatterbug
............ check: sgpp
............ check: nekcem
............. direct: nekcem -> {'ceed'}
............. check: ceed
............ check: hwloc
............. direct: hwloc -> {'prrte', 'powerapi', 'openpbs', 'mitos', 'slurm', 'ghost', 'hpx5', 'ompss', 'pmix', 'pocl', 'qthreads', 'aluminum', 'tau', 'gromacs', 'openmpi', 'kokkos', 'llvm', 'hpx', 'geopm', 'llvm-flang', 'funhpc', 'kokkos-legacy', 'variorum', 'lbann', 'flux-core'}
............. check: prrte
.............. direct: prrte -> set()
............. check: powerapi
............. check: openpbs
.............. direct: openpbs -> {'openmpi'}
.............. check: openmpi
............. check: mitos
............. check: slurm
............. check: ghost
............. check: hpx5
............. check: ompss
............. check: pmix
.............. direct: pmix -> {'prrte', 'slurm', 'mpich'}
.............. check: prrte
.............. check: slurm
.............. check: mpich
............. check: pocl
.............. direct: pocl -> {'clfft', 'grass', 'bohrium', 'onednn', 'hpx5', 'sgpp', 'gdal', 'lammps', 'gearshifft'}
.............. check: clfft
............... direct: clfft -> {'gearshifft'}
............... check: gearshifft
.............. check: grass
.............. check: bohrium
.............. check: onednn
............... direct: onednn -> {'py-torch'}
............... check: py-torch
.............. check: hpx5
.............. check: sgpp
.............. check: gdal
.............. check: lammps
.............. check: gearshifft
............. check: qthreads
.............. direct: qthreads -> {'funhpc', 'minife', 'kokkos-legacy'}
.............. check: funhpc
.............. check: minife
.............. check: kokkos-legacy
............... direct: kokkos-legacy -> {'cabana', 'examinimd', 'arborx', 'kokkos-kernels-legacy', 'miniaero'}
............... check: cabana
............... check: examinimd
................ direct: examinimd -> {'ecp-proxy-apps'}
................ check: ecp-proxy-apps
............... check: arborx
............... check: kokkos-kernels-legacy
................ direct: kokkos-kernels-legacy -> set()
............... check: miniaero
................ direct: miniaero -> set()
............. check: aluminum
............. check: tau
............. check: gromacs
............. check: openmpi
............. check: kokkos
............. check: llvm
.............. direct: llvm -> {'rust-bindgen', 'py-llvmlite', 'sas', 'symengine', 'pocl', 'llvm-openmp-ompt', 'mesa', 'hipsycl', 'f18', 'flecsi', 'ldc-bootstrap', 'oclint', 'cquery', 'iwyu', 'rtags', 'sst-macro', 'py-pyside2', 'octave', 'ldc', 'archer'}
.............. check: rust-bindgen
............... direct: rust-bindgen -> set()
.............. check: py-llvmlite
............... direct: py-llvmlite -> {'py-numba'}
............... check: py-numba
.............. check: sas
............... direct: sas -> set()
.............. check: symengine
............... direct: symengine -> {'dealii', 'py-symengine'}
............... check: dealii
............... check: py-symengine
................ direct: py-symengine -> set()
.............. check: pocl
.............. check: llvm-openmp-ompt
............... direct: llvm-openmp-ompt -> {'cbtf-krell', 'timemory', 'archer'}
............... check: cbtf-krell
............... check: timemory
............... check: archer
................ direct: archer -> set()
.............. check: mesa
............... direct: mesa -> {'fox', 'dislin', 'kitty', 'ftgl', 'qt', 'gl2ps', 'catalyst', 'geant4', 'vtk', 'libepoxy', 'jasper', 'r-rgl', 'opendx', 'glvis', 'glew', 'hwloc', 'paraview', 'grass', 'opensubdiv', 'root', 'ethminer', 'rodinia', 'vesta', 'mesa-glu', 'freeglut'}
............... check: fox
................ direct: fox -> {'sumo'}
................ check: sumo
............... check: dislin
................ direct: dislin -> {'shoremap'}
................ check: shoremap
................. direct: shoremap -> set()
............... check: kitty
................ direct: kitty -> set()
............... check: ftgl
................ direct: ftgl -> {'root'}
................ check: root
............... check: qt
................ direct: qt -> {'cube', 'qwt', 'fenics', 'plplot', 'dealii-parameter-gui', 'py-pyside2', 'cmake', 'py-pyqt4', 'qtkeychain', 'qca', 'silo', 'py-pyside', 'qjson', 'py-spyder', 'texstudio', 'geant4', 'openimageio', 'qscintilla', 'qgis', 'opencv', 'vtk', 'libfive', 'ravel', 'qt-creator', 'cbtf-argonavis-gui', 'memaxes', 'openspeedshop', 'qtgraph', 'magics', 'sqlitebrowser', 'py-shiboken', 'gnuplot', 'mrtrix3', 'octave', 'py-pyqt5', 'ecflow', 'cgal', 'graphviz', 'paraview', 'audacious', 'openscenegraph', 'tulip', 'poppler', 'root', 'draco', 'qwtpolar', 'visit', 'wireshark', 'kdiff3', 'gplates'}
................ check: cube
................. direct: cube -> {'scorep', 'scalasca'}
................. check: scorep
................. check: scalasca
................ check: qwt
................. direct: qwt -> {'gplates', 'visit', 'qgis', 'qwtpolar'}
................. check: gplates
................. check: visit
................. check: qgis
................. check: qwtpolar
.................. direct: qwtpolar -> {'qgis'}
.................. check: qgis
................ check: fenics
................ check: plplot
................ check: dealii-parameter-gui
................. direct: dealii-parameter-gui -> {'aspect'}
................. check: aspect
................ check: py-pyside2
................ check: cmake
................. direct: cmake -> {'metis', 'gmtsar', 'aspect', 'quinoa', 'bolt', 'strumpack', 'gtkorvo-cercs-env', 'numamma', 'templight', 'cbtf-lanl', 'hydrogen', 'cgns', 'dd4hep', 'sqlitebrowser', 'modern-wheel', 'libgit2', 'exodusii', 'nest', 'cppad', 'faodel', 'llvm', 'bamtools', 'sympol', 'libmng', 'mofem-cephas', 'openfoam-org', 'opensubdiv', 'templight-tools', 'hipsycl', 'trilinos', 'dysco', 'mpilander', 'fish', 'mpark-variant', 'mofem-users-modules', 'rdma-core', 'libtree', 'py-osqp', 'arrow', 'parmetis', 'mitos', 'paradiseo', 'dealii', 'json-fortran', 'graphlib', 'plplot', 'mpifileutils', 'elmerfem', 'sst-transports', 'votca-ctp', 'raja', 'span-lite', 'eccodes', 'libical', 'xgboost', 'qgis', 'libkml', 'graphite2', 'nut', 'heffte', 'libibumad', 'qmd-progress', 'seqan', 'protobuf', 'votca-xtp', 'spiral', 'kripke', 'glew', 'yajl', 'sentencepiece', 'denovogear', 'opencascade', 'uvw', 'aoflagger', 'cppzmq', 'latte', 'racon', 'dealii-parameter-gui', 'cotter', 'tethex', 'phasta', 'py-torch', 'libjpeg-turbo', 'scr', 'shark', 'scorec-core', 'py-tfdlpack', 'jsoncpp', 'pocl', 'flibcpp', 'packmol', 'exampm', 'percept', 'f18', 'xsdk-examples', 'bpp-seq', 'tulip', 'iegenlib', 'draco', 'icet', 'ethminer', 'bam-readcount', 'kallisto', 'libaec', 'libsplash', 'assimp', 'pugixml', 'arborx', 'eigen', 'grib-api', 'jchronoss', 'dihydrogen', 'sas', 'tioga', 'brotli', 'catalyst', 'alquimia', 'flecsi', 'pism', 'apex', 'uriparser', 'magics', 'prmon', 'pmlib', 'ross', 'cram', 'axl', 'rapidjson', 'tinyxml', 'veloc', 'wt', 'akantu', 'ompt-openmp', 'openfoam', 'hpx', 'openscenegraph', 'funhpc', 'superlu', 'xeus', 'wireshark', 'jhpcn-df', 'xtl', 'libgpuarray', 'freeglut', 'damaris', 'lordec', 'adept-utils', 'blaze', 'osqp', 'bohrium', 'cpuinfo', 'py-espressopp', 'nlohmann-json', 'mapserver', 'freebayes', 'ftgl', 'podio', 'flang', 'rust', 'mariadb', 'aom', 'intel-tbb', 'relax', 'openmc', 'panda', 'sirius', 'blaspp', 'cbtf-argonavis-gui', 'bookleaf-cpp', 'benchmark', 'brpc', 'albany', 'muster', 'aluminum', 'mrcpp', 'ascent', 'pngwriter', 'cereal', 'catch2', 'breakdancer', 'kokkos-nvcc-wrapper', 'ecp-io-sdk', 'dbcsr', 'nfs-ganesha', 'vtk-m', 'phist', 'yaml-cpp', 'symengine', 'qmcpack', 'iwyu', 'ldc', 'somatic-sniper', 'pythia6', 'pagmo', 'opencoarrays', 'onednn', 'libdivsufsort', 'meraculous', 'trilinos-catalyst-ioss-adapter', 'vtk', 'pgmath', 'flecsph', 'sleef', 'sniffles', 'highfive', 'lazyten', 'elemental', 'everytrace-example', 'mono', 'hoomd-blue', 'libssh2', 'gitconddb', 'satsuma2', 'molcas', 'ecflow', 'dftfe', 'laszip', 'clara', 'boxlib', 'tinker', 'cpu-features', 'virtualgl', 'cbtf', 'sosflow', 'glog', 'imp', 'intel-llvm', 'spfft', 'py-pyside2', 'cnmem', 'ctre', 'mindthegap', 'qtkeychain', 'shiny-server', 'amp', 'ntpoly', 'mmg', 'veccore', 'neovim', 'cuda-memtest', 'parsplice', 'cnpy', 'py-symengine', 'camellia', 'macsio', 'py-dgl', 'cabana', 'doxygen', 'mariadb-c-client', 'gtkorvo-dill', 'kim-api', 'mercury', 'med', 'openmm', 'bml', 'talass', 'claw', 'py-pybind11', 'votca-csg-tutorials', 'fenics', 'gearshifft', 'log4cplus', 'spglib', 'libfive', 'heaptrack', 'sollve', 'memaxes', 'uchardet', 'tinyxml2', 'llvm-openmp-ompt', 'xrootd', 'cbtf-argonavis', 'lemon', 'mad-numdiff', 'oclint', 'manta', 'sph2pipe', 'gmodel', 'precice', 'capstone', 'pbbam', 'strelka', 'dyninst', 'poppler', 'ufo-filters', 'nnvm', 'ants', 'tixi', 'gloo', 'rmlab', 'zfp', 'ray', 'c-ares', 'xtensor', 'libtlx', 'cardioid', 'qca', 'glfw', 'mofem-fracture-module', 'poppler-data', 'double-conversion', 'bpp-phyl', 'krims', 'numap', 'cleverleaf', 'mutationpp', 'ibmisc', 'axom', 'py-onnx', 'ginkgo', 'jasper', 'memsurfer', 'seacas', 'sensei', 'minisign', 'gunrock', 'dcmtk', 'utf8proc', 'raft', 'omega-h', 'root', 'bpp-suite', 'kokkos-kernels', 'libcint', 'textparser', 'adiak', 'cminpack', 'callpath', 'lbann', 'oce', 'optional-lite', 'adios2', 'sfcgal', 'umpire', 'amgx', 'isaac-server', 'mofem-minimal-surface-equation', 'hepmc', 'nalu-wind', 'uqtk', 'armadillo', 'liblas', 'py-espresso', 'sbml', 'vigra', 'libssh', 'sundials', 'c-blosc', 'julia', 'glm', 'string-view-lite', 'clfft', 'tensorflow-serving-client', 'unittest-cpp', 'steps', 'shuffile', 'channelflow', 'openjpeg', 'psi4', 'cbtf-krell', 'spdlog', 'openbabel', 'gmsh', 'range-v3', 'chgcentre', 'ntirpc', 'codec2', 'ldc-bootstrap', 'relion', 'nanoflann', 'jansson', 'opencv', 'vc', 'rr', 'sumo', 'clhep', 'accfft', 'llvm-openmp', 'pegtl', 'libmmtf-cpp', 'fairlogger', 'mgis', 'papyrus', 'perfstubs', 'variorum', 'gaudi', 'tasmanian', 'libffs', 'timemory', 'redset', 'task', 'nalu', 'h5cpp', 'ps-lite', 'umap', 'caffe', 'votca-csgapps', 'flann', 'hyperscan', 'gdl', 'ufo-core', 'mallocmc', 'mongo-cxx-driver', 'er', 'openspeedshop-utils', 'xsimd', 'openspeedshop', 'express', 'fann', 'pktools', 'votca-csg', 'elsi', 'gflags', 'cquery', 'clingo', 'damselfly', 'py-horovod', 'mongo-c-driver', 'kokkos-legacy', 'visit', 'henson', 'caliper', 'foam-extend', 'libnetworkit', 'jali', 'hiop', 'nlopt', 'nektar', 'salmon', 'taskd', 'ceres-solver', 'gmt', 'mysql', 'casacore', 'simulationio', 'exiv2', 'py-pyside', 'iq-tree', 'revbayes', 'sortmerna', 'lapackpp', 'lammps', 'openfast', 'cosma', 'xtensor-python', 'bear', 'ngmlr', 'amrex', 'helics', 'stinger', 'sz', 'openmolcas', 'gromacs', 'isaac', 'libwebsockets', 'llvm-flang', 'vpic', 'xcfun', 'qhull', 'sdl2', 'filo', 'cppgsl', 'unqlite', 'fmt', 'bpp-core', 'metall', 'frontistr', 'mt-metis', 'flatcc', 'rankstr', 'spades', 'flecsale', 'diy', 'pidx', 'xdmf3', 'simgrid', 'qjson', 'sdsl-lite', 'snappy', 'c-blosc2', 'everytrace', 'openimageio', 'dmlc-core', 'grpc', 'rtags', 'swipl', 'bcl2fastq2', 'libpmemobj-cpp', 'googletest', 'sailfish', 'mstk', 'muparser', 'kvtree', 'nsimd', 'pnmpi', 'kokkos', 'openpmd-api', 'clamr', 'asdf-cxx', 'votca-tools', 'conduit', 'spath', 'arpack-ng', 'openkim-models', 'gccxml', 'miniqmc', 'gplates', 'diamond', 'asagi', 'superlu-dist', 'ghost', 'gl2ps', 'suite-sparse', 'pumi', 'ravel', 'butterflypack', 'aspcud', 'dakota', 'tfel', 'portage', 'branson', 'cmocka', 'py-shiboken', 'flatbuffers', 'hyphy', 'cxxopts', 'delphes', 'kcov', 'automaded', 'lighttpd', 'msgpack-c', 'typhon', 'netlib-scalapack', 'leveldb', 'nanopb', 'netlib-lapack', 'vdt', 'mbedtls', 'vtk-h', 'abseil-cpp', 'kealib', 'vecgeom', 'piranha', 'xsdktrilinos', 'geant4', 'plasma', 'py-pyarrow', 'libbson', 'pfunit', 'nanomsg', 'libevpath', 'libspatialindex', 'py-blosc', 'cpprestsdk', 'parquet-cpp', 'libmo-unpack', 'gotcha', 'libemos', 'qnnpack', 'hacckernels', 'magma', 'davix', 'archer', 'cgal', 'paraview', 'gtkorvo-atl', 'of-catalyst', 'uncrustify', 'acts', 'cpp-httplib', 'legion', 'typhonio', 'multiverso', 'ecp-viz-sdk'}
................. check: metis
.................. direct: metis -> {'frontistr', 'gmsh', 'ipopt', 'superlu-dist', 'petsc', 'parmetis', 'dealii', 'netgen', 'ppopen-appl-fem', 'mumps', 'hpx5', 'strumpack', 'flecsi', 'moab', 'suite-sparse', 'elemental', 'qmd-progress', 'seacas', 'octopus', 'branson', 'mstk', 'ffr', 'ppopen-appl-fvm', 'zoltan', 'snbone', 'openfoam', 'openfoam-org', 'draco', 'trilinos', 'swiftsim', 'foam-extend', 'mfem', 'jali'}
.................. check: frontistr
.................. check: gmsh
.................. check: ipopt
.................. check: superlu-dist
.................. check: petsc
.................. check: parmetis
.................. check: dealii
.................. check: netgen
................... direct: netgen -> {'gmsh', 'meshkit'}
................... check: gmsh
................... check: meshkit
.................. check: ppopen-appl-fem
.................. check: mumps
.................. check: hpx5
.................. check: strumpack
.................. check: flecsi
.................. check: moab
.................. check: suite-sparse
................... direct: suite-sparse -> {'trilinos', 'petsc', 'fenics', 'dealii', 'ceed', 'mfem', 'octave', 'sundials', 'py-cvxopt'}
................... check: trilinos
................... check: petsc
................... check: fenics
................... check: dealii
................... check: ceed
................... check: mfem
................... check: octave
................... check: sundials
................... check: py-cvxopt
.................. check: elemental
.................. check: qmd-progress
.................. check: seacas
.................. check: octopus
.................. check: branson
.................. check: mstk
.................. check: ffr
.................. check: ppopen-appl-fvm
................... direct: ppopen-appl-fvm -> set()
.................. check: zoltan
.................. check: snbone
................... direct: snbone -> set()
.................. check: openfoam
.................. check: openfoam-org
.................. check: draco
.................. check: trilinos
.................. check: swiftsim
.................. check: foam-extend
.................. check: mfem
.................. check: jali
................. check: gmtsar
................. check: aspect
................. check: quinoa
................. check: bolt
.................. direct: bolt -> set()
................. check: strumpack
................. check: gtkorvo-cercs-env
.................. direct: gtkorvo-cercs-env -> {'libffs', 'gtkorvo-atl'}
.................. check: libffs
................... direct: libffs -> {'libevpath'}
................... check: libevpath
.................... direct: libevpath -> {'adios', 'sosflow'}
.................... check: adios
.................... check: sosflow
.................. check: gtkorvo-atl
................... direct: gtkorvo-atl -> {'libffs'}
................... check: libffs
................. check: numamma
.................. direct: numamma -> set()
................. check: templight
.................. direct: templight -> set()
................. check: cbtf-lanl
................. check: hydrogen
................. check: cgns
................. check: dd4hep
................. check: sqlitebrowser
.................. direct: sqlitebrowser -> set()
................. check: modern-wheel
.................. direct: modern-wheel -> set()
................. check: libgit2
.................. direct: libgit2 -> {'gitconddb', 'py-pygit2', 'r-git2r', 'rust'}
.................. check: gitconddb
................... direct: gitconddb -> set()
.................. check: py-pygit2
................... direct: py-pygit2 -> set()
.................. check: r-git2r
................... direct: r-git2r -> {'r-usethis', 'r-devtools'}
................... check: r-usethis
.................... direct: r-usethis -> {'r-devtools'}
.................... check: r-devtools
..................... direct: r-devtools -> {'r-irkernel', 'r-pacman'}
..................... check: r-irkernel
...................... direct: r-irkernel -> set()
..................... check: r-pacman
...................... direct: r-pacman -> set()
................... check: r-devtools
.................. check: rust
................... direct: rust -> {'rust-bindgen', 'librsvg', 'py-setuptools-rust', 'py-tokenizers', 'fd-find', 'bat', 'ripgrep', 'exa'}
................... check: rust-bindgen
................... check: librsvg
.................... direct: librsvg -> {'cairo'}
.................... check: cairo
..................... direct: cairo -> {'py-pycairo', 'harfbuzz', 'guacamole-server', 'i3', 'perl-cairo', 'ncl', 'fio', 'gtkplus', 'pdf2svg', 'gnuplot', 'pango', 'py-py2cairo', 'cairomm', 'hwloc', 'r-cairo', 'texlive', 'intel-gpu-tools', 'r', 'librsvg', 'graphviz', 'grass', 'py-pygtk', 'mapnik', 'poppler', 'openbabel', 'vesta', 'gobject-introspection', 'genometools'}
..................... check: py-pycairo
...................... direct: py-pycairo -> {'py-python-mapnik', 'py-xdot', 'py-pygobject'}
...................... check: py-python-mapnik
...................... check: py-xdot
....................... direct: py-xdot -> {'stat'}
....................... check: stat
...................... check: py-pygobject
....................... direct: py-pygobject -> {'libpeas', 'py-xdot', 'py-pygtk', 'py-matplotlib'}
....................... check: libpeas
........................ direct: libpeas -> set()
....................... check: py-xdot
....................... check: py-pygtk
........................ direct: py-pygtk -> {'stat'}
........................ check: stat
....................... check: py-matplotlib
..................... check: harfbuzz
...................... direct: harfbuzz -> {'mapnik', 'kitty', 'pango', 'texlive', 'qt'}
...................... check: mapnik
...................... check: kitty
...................... check: pango
....................... direct: pango -> {'librsvg', 'graphviz', 'imagemagick', 'i3', 'libpeas', 'py-xdot', 'gtkplus', 'magics', 'plplot', 'gnuplot', 'openbabel', 'pangomm', 'wt', 'gtksourceview', 'genometools', 'r'}
....................... check: librsvg
....................... check: graphviz
....................... check: imagemagick
........................ direct: imagemagick -> {'meme', 'r-magick', 'r-animation', 'py-matplotlib', 'py-pillow', 'octave', 'py-wand'}
........................ check: meme
........................ check: r-magick
......................... direct: r-magick -> {'r-animation'}
......................... check: r-animation
........................ check: r-animation
........................ check: py-matplotlib
........................ check: py-pillow
......................... direct: py-pillow -> {'py-wxpython', 'py-openslide-python', 'py-basemap', 'py-bokeh', 'py-cartopy', 'py-guiqwt', 'py-torchvision', 'py-gluoncv', 'py-imageio', 'py-pycbc', 'py-scikit-image', 'py-matplotlib', 'timemory', 'py-pauvre'}
......................... check: py-wxpython
......................... check: py-openslide-python
.......................... direct: py-openslide-python -> set()
......................... check: py-basemap
......................... check: py-bokeh
......................... check: py-cartopy
......................... check: py-guiqwt
......................... check: py-torchvision
......................... check: py-gluoncv
......................... check: py-imageio
.......................... direct: py-imageio -> set()
......................... check: py-pycbc
......................... check: py-scikit-image
......................... check: py-matplotlib
......................... check: timemory
......................... check: py-pauvre
........................ check: octave
........................ check: py-wand
......................... direct: py-wand -> set()
....................... check: i3
........................ direct: i3 -> set()
....................... check: libpeas
....................... check: py-xdot
....................... check: gtkplus
........................ direct: gtkplus -> {'libnotify', 'workrave', 'sublime-text', 'libpeas', 'plplot', 'qt', 'slurm', 'ghostscript', 'gtkmm', 'opencv', 'dia', 'fio', 'wxwidgets', 'grandr', 'py-pygobject', 'gtksourceview', 'icedtea', 'graphviz', 'py-pygtk', 'libcanberra', 'py-xdot', 'emacs', 'vesta'}
........................ check: libnotify
......................... direct: libnotify -> {'boinc-client'}
......................... check: boinc-client
.......................... direct: boinc-client -> set()
........................ check: workrave
......................... direct: workrave -> set()
........................ check: sublime-text
......................... direct: sublime-text -> set()
........................ check: libpeas
........................ check: plplot
........................ check: qt
........................ check: slurm
........................ check: ghostscript
......................... direct: ghostscript -> {'graphviz', 'imagemagick', 'graphicsmagick', 'py-weblogo', 'groff', 'gmt', 'py-matplotlib', 'texlive'}
......................... check: graphviz
......................... check: imagemagick
......................... check: graphicsmagick
......................... check: py-weblogo
.......................... direct: py-weblogo -> set()
......................... check: groff
.......................... direct: groff -> {'man-db'}
.......................... check: man-db
........................... direct: man-db -> set()
......................... check: gmt
......................... check: py-matplotlib
......................... check: texlive
.......................... direct: texlive -> {'flann', 'gl2ps', 'ladot', 'py-espressopp', 'r-animation', 'py-matplotlib', 'pandoc'}
.......................... check: flann
.......................... check: gl2ps
........................... direct: gl2ps -> {'octave', 'root', 'sumo'}
........................... check: octave
........................... check: root
........................... check: sumo
.......................... check: ladot
.......................... check: py-espressopp
.......................... check: r-animation
.......................... check: py-matplotlib
.......................... check: pandoc
........................... direct: pandoc -> {'r-rmarkdown', 'r-rgl', 'r-rstan', 'r-reprex', 'r-rstantools', 'r-stanheaders', 'r-bookdown', 'r-loo'}
........................... check: r-rmarkdown
............................ direct: r-rmarkdown -> {'r-biocstyle', 'r-reprex', 'r-bookdown', 'r-gistr'}
............................ check: r-biocstyle
............................. direct: r-biocstyle -> set()
............................ check: r-reprex
............................. direct: r-reprex -> {'r-tidyverse'}
............................. check: r-tidyverse
.............................. direct: r-tidyverse -> {'trinity'}
.............................. check: trinity
............................ check: r-bookdown
............................. direct: r-bookdown -> {'r-biocstyle'}
............................. check: r-biocstyle
............................ check: r-gistr
............................. direct: r-gistr -> {'r-rbokeh'}
............................. check: r-rbokeh
.............................. direct: r-rbokeh -> set()
........................... check: r-rgl
............................ direct: r-rgl -> {'r-ampliqueso', 'r-geomorph'}
............................ check: r-ampliqueso
............................ check: r-geomorph
............................. direct: r-geomorph -> set()
........................... check: r-rstan
............................ direct: r-rstan -> {'r-construct'}
............................ check: r-construct
............................. direct: r-construct -> set()
........................... check: r-reprex
........................... check: r-rstantools
............................ direct: r-rstantools -> {'r-construct'}
............................ check: r-construct
........................... check: r-stanheaders
............................ direct: r-stanheaders -> {'r-construct', 'r-rstan'}
............................ check: r-construct
............................ check: r-rstan
........................... check: r-bookdown
........................... check: r-loo
............................ direct: r-loo -> {'r-rstan'}
............................ check: r-rstan
........................ check: gtkmm
......................... direct: gtkmm -> {'workrave'}
......................... check: workrave
........................ check: opencv
........................ check: dia
......................... direct: dia -> set()
........................ check: fio
......................... direct: fio -> set()
........................ check: wxwidgets
......................... direct: wxwidgets -> {'py-wxpython', 'gdl', 'grass', 'ctffind', 'saga-gis', 'cistem', 'paraver', 'plplot', 'gnuplot', 'wxpropgrid', 'boinc-client'}
......................... check: py-wxpython
......................... check: gdl
......................... check: grass
......................... check: ctffind
......................... check: saga-gis
......................... check: cistem
......................... check: paraver
.......................... direct: paraver -> set()
......................... check: plplot
......................... check: gnuplot
.......................... direct: gnuplot -> {'py-quast', 'signalp', 'canu', 'mummer', 'meraculous', 'paradiseo', 'octave', 'cohmm', 'ruby-gnuplot'}
.......................... check: py-quast
.......................... check: signalp
........................... direct: signalp -> {'targetp'}
........................... check: targetp
............................ direct: targetp -> set()
.......................... check: canu
........................... direct: canu -> set()
.......................... check: mummer
........................... direct: mummer -> {'py-quast', 'py-pyani', 'biopieces'}
........................... check: py-quast
........................... check: py-pyani
........................... check: biopieces
.......................... check: meraculous
........................... direct: meraculous -> set()
.......................... check: paradiseo
.......................... check: octave
.......................... check: cohmm
........................... direct: cohmm -> set()
.......................... check: ruby-gnuplot
........................... direct: ruby-gnuplot -> {'biopieces'}
........................... check: biopieces
......................... check: wxpropgrid
.......................... direct: wxpropgrid -> {'paraver'}
.......................... check: paraver
......................... check: boinc-client
........................ check: grandr
......................... direct: grandr -> set()
........................ check: py-pygobject
........................ check: gtksourceview
......................... direct: gtksourceview -> set()
........................ check: icedtea
........................ check: graphviz
........................ check: py-pygtk
........................ check: libcanberra
......................... direct: libcanberra -> {'vizglow'}
......................... check: vizglow
.......................... direct: vizglow -> set()
........................ check: py-xdot
........................ check: emacs
......................... direct: emacs -> {'cask'}
......................... check: cask
.......................... direct: cask -> set()
........................ check: vesta
......................... direct: vesta -> set()
....................... check: magics
....................... check: plplot
....................... check: gnuplot
....................... check: openbabel
........................ direct: openbabel -> set()
....................... check: pangomm
........................ direct: pangomm -> {'gtkmm'}
........................ check: gtkmm
....................... check: wt
....................... check: gtksourceview
....................... check: genometools
........................ direct: genometools -> {'aegean'}
........................ check: aegean
......................... direct: aegean -> set()
....................... check: r
........................ direct: r -> {'r-phytools', 'r-annotationdbi', 'r-mnormt', 'r-praise', 'r-dygraphs', 'r-exomedepth', 'r-graphlayouts', 'r-mgraster', 'r-r-utils', 'r-ctc', 'r-bglr', 'r-iranges', 'r-r6', 'r-biocneighbors', 'r-fastcluster', 'r-fpc', 'r-pmcmr', 'r-yarn', 'r-isdparser', 'r-proxy', 'r-annotationforge', 'r-doparallel', 'r-checkmate', 'r-gtools', 'r-lazyeval', 'r-rcurl', 'r-rbgl', 'r-reportingtools', 'r-minqa', 'r-zoo', 'r-ggplot2', 'r-influencer', 'r-later', 'r-gensa', 'r-splitstackshape', 'r-rex', 'r-factominer', 'r-rcpparmadillo', 'r-iso', 'r-rsamtools', 'r-strucchange', 'r-gbm', 'r-progress', 'r-lars', 'r-ps', 'r-readbitmap', 'r-th-data', 'r-corhmm', 'r-earth', 'r-goseq', 'r-ordinal', 'r-base64enc', 'r-aims', 'r-pspline', 'r-plot3d', 'r-gridextra', 'r-illuminahumanmethylation450kanno-ilmn12-hg19', 'r-allelicimbalance', 'r-vipor', 'r-beeswarm', 'r-gh', 'r-magick', 'r-polynom', 'r-limma', 'r-pbkrtest', 'r-hexbin', 'r-summarizedexperiment', 'r-foreach', 'r-jomo', 'r-factoextra', 'r-amap', 'r-vioplot', 'r-mixtools', 'r-ncbit', 'r-selectr', 'r-rgexf', 'r-sessioninfo', 'r-plyr', 'r-affyio', 'r-crosstalk', 'r-trimcluster', 'r-rgeos', 'r-glue', 'r-category', 'r-sp', 'r-sandwich', 'r-rcppprogress', 'r-phyloseq', 'r-caracas', 'r-squarem', 'r-threejs', 'r-yaqcaffy', 'r-gstat', 'r-downloader', 'r-cdcfluview', 'r-genefilter', 'r-promises', 'r-gwmodel', 'r-modeltools', 'r-purrr', 'r-ipred', 'r-crayon', 'r-bit', 'r-pkgconfig', 'r-bit64', 'r-openxlsx', 'r-httpuv', 'r-dplyr', 'r-git2r', 'r-mgcv', 'r-tclust', 'r-hypergraph', 'r-janitor', 'r-xde', 'r-rcppparallel', 'r-fit-models', 'r-genomicalignments', 'py-rseqc', 'r-fgsea', 'r-futile-options', 'r-fs', 'r-vgam', 'r-diversitree', 'r-aroma-light', 'r-rodbc', 'r-gamlss', 'r-sm', 'r-polyclip', 'r-tensor', 'r-ini', 'r-laplacesdemon', 'r-dose', 'r-memoise', 'r-bookdown', 'r-clusterprofiler', 'r-lattice', 'r-xts', 'r-bsgenome', 'r-np', 'r-clustergeneration', 'r-lpsolve', 'r-spacetime', 'r-phantompeakqualtools', 'r-units', 'r-biocstyle', 'r-nimble', 'r-sourcetools', 'r-r-oo', 'r-abaenrichment', 'r-xlconnectjars', 'r-pathview', 'r-kernlab', 'r-plogr', 'r-fftwtools', 'r-viridislite', 'r-roc', 'r-qtl', 'r-flexmix', 'r-dynamictreecut', 'r-dada2', 'r-quantro', 'r-gamlss-data', 'r-rstan', 'r-gistr', 'r-affyqcreport', 'r-affycomp', 'r-biocsingular', 'r-ellipse', 'r-ggbeeswarm', 'r-gseabase', 'r-jaspar2018', 'r-abind', 'py-rpy2', 'r-nloptr', 'r-clisymbols', 'r-classint', 'r-cellranger', 'r-als', 'r-deldir', 'r-locfit', 'r-fracdiff', 'breseq', 'r-survey', 'r-future', 'r-mmwrweek', 'r-dendextend', 'r-rrblup', 'r-r-methodss3', 'r-vcd', 'r-ape', 'r-spatial', 'r-spdep', 'r-assertthat', 'r-agilp', 'r-rsvd', 'r-aneufinderdata', 'r-gsalib', 'r-yapsa', 'r-pkgload', 'r-upsetr', 'r-genomeinfodb', 'r-condop', 'r-aod', 'r-affyplm', 'r-htmltools', 'r-methylumi', 'r-mzid', 'r-rjava', 'r-geosphere', 'angsd', 'r-miniui', 'r-reshape2', 'r-bindrcpp', 'r-samr', 'r-ggmap', 'r-cubist', 'r-cner', 'r-recipes', 'r-reshape', 'r-ff', 'r-rcppeigen', 'r-xopen', 'r-acepack', 'r-spatialreg', 'r-alpine', 'r-geiger', 'r-sf', 'r-imager', 'r-kernsmooth', 'r-rcolorbrewer', 'r-scater', 'r-rsnns', 'r-tmixclust', 'r-prabclus', 'r-watermelon', 'r-affycontam', 'r-complexheatmap', 'r-lumi', 'r-pcapp', 'qorts', 'r-dtw', 'r-rda', 'r-webshot', 'vegas2', 'r-gsa', 'r-shortread', 'r-survival', 'r-flashclust', 'r-udunits2', 'r-httpcode', 'r-broom', 'r-xfun', 'r-shiny', 'r-rjson', 'r-zeallot', 'r-urltools', 'r-png', 'r-trust', 'r-matrixstats', 'r-diagrammer', 'trinity', 'r-energy', 'r-checkpoint', 'r-irlba', 'r-corpcor', 'r-networkd3', 'r-gsl', 'r-gtrellis', 'r-tidyselect', 'r-futile-logger', 'r-cli', 'r-mlr', 'r-paleotree', 'r-affyexpress', 'r-fastmatch', 'r-mitools', 'r-askpass', 'r-plotly', 'r-fitdistrplus', 'r-randomfieldsutils', 'r-annotationfilter', 'r-stanheaders', 'r-grbase', 'r-adabag', 'r-genomeinfodbdata', 'r-stargazer', 'r-dbplyr', 'r-ggsignif', 'r-mime', 'r-gamlss-dist', 'r-ggally', 'r-interactivedisplaybase', 'r-randomfields', 'r-siggenes', 'r-robustbase', 'r-xml2', 'r-dt', 'r-affypdnn', 'r-inum', 'r-corrplot', 'r-nortest', 'r-gviz', 'r-gcrma', 'r-snprelate', 'r-markdown', 'r-hmisc', 'r-withr', 'r-lme4', 'r-quantreg', 'r-roxygen2', 'r-goplot', 'r-nlme', 'r-domc', 'r-stringr', 'r-enrichplot', 'r-getopt', 'r-a4preproc', 'r-minfi', 'r-scrime', 'r-hms', 'r-aneufinder', 'r-dirichletmultinomial', 'r-network', 'r-codetools', 'r-dismo', 'r-oligoclasses', 'r-lsei', 'r-ggrepel', 'r-xnomial', 'r-gower', 'r-hdf5r', 'r-bibtex', 'r-lava', 'r-remotes', 'r-mzr', 'r-psych', 'r-biomartr', 'r-evd', 'r-philentropy', 'r-base64', 'r-diffusionmap', 'r-rrpp', 'r-s4vectors', 'r-igraph', 'r-rcpphnsw', 'shiny-server', 'r-seqlogo', 'r-jsonlite', 'r-gmp', 'r-pryr', 'r-misc3d', 'r-rcppannoy', 'r-tidygraph', 'r-covr', 'r-popvar', 'r-ggridges', 'r-gbrd', 'r-rms', 'r-fnn', 'r-genomicranges', 'r-irdisplay', 'r-permute', 'r-reticulate', 'r-copula', 'r-visnetwork', 'r-rsqlite', 'r-a4classif', 'r-aer', 'r-ica', 'r-exomecopy', 'r-preprocesscore', 'r-analysispageserver', 'r-spatstat', 'r-bbmisc', 'r-beanplot', 'r-caroline', 'r-readxl', 'r-bsgenome-hsapiens-ucsc-hg19', 'r-statnet-common', 'r-a4reporting', 'r-annaffy', 'r-labeling', 'r-testit', 'r-globals', 'r-flexclust', 'r-forecast', 'r-somaticsignatures', 'r-proj4', 'r-rstudioapi', 'r-mvtnorm', 'r-convevol', 'r-parallelmap', 'r-rgdal', 'r-seurat', 'r-ade4', 'r-matr', 'r-affyilm', 'r-gosemsim', 'r-rferns', 'r-sparsem', 'r-bitops', 'r-smoof', 'r-curl', 'r-txdb-hsapiens-ucsc-hg19-knowngene', 'r-genelendatabase', 'r-altcdfenvs', 'r-mapproj', 'r-mitml', 'r-gmodels', 'r-gdsfmt', 'r-varselrf', 'r-chron', 'r-highr', 'r-partykit', 'r-manipulatewidget', 'r-robust', 'r-snow', 'r-iterators', 'r-exactextractr', 'r-ellipsis', 'r-mapplots', 'r-topgo', 'r-sqldf', 'r-edger', 'r-ggdendro', 'r-argparse', 'r-adegenet', 'r-rcppcnpy', 'r-fansi', 'r-raster', 'r-expm', 'r-acme', 'r-rversions', 'r-pacman', 'r-kegggraph', 'r-nnet', 'r-qvalue', 'r-tidyr', 'r-biocparallel', 'r-tfmpvalue', 'r-rhtslib', 'r-gdata', 'r-foreign', 'r-readr', 'r-shinydashboard', 'r-picante', 'r-stringi', 'r-utf8', 'r-sys', 'r-multcomp', 'eq-r', 'r-mda', 'r-nnls', 'r-hoardr', 'r-simpleaffy', 'r-findpython', 'r-ggpubr', 'r-maptools', 'r-lhs', 'r-sitmo', 'r-shape', 'r-tiff', 'r-ada', 'r-proto', 'r-delayedarray', 'r-quadprog', 'r-ampliqueso', 'r-ggsci', 'r-gtable', 'r-triebeard', 'r-annotate', 'r-gostats', 'r-snpstats', 'r-ggforce', 'r-ncdf4', 'r-combinat', 'r-packrat', 'root', 'r-rpart', 'r-paramhelpers', 'r-hwriter', 'r-randomglm', 'r-rstantools', 'r-ggvis', 'r-rcppcctz', 'r-tarifx', 'r-variantannotation', 'r-optparse', 'r-glmnet', 'r-mergemaid', 'r-data-table', 'r-processx', 'r-car', 'r-sva', 'r-future-apply', 'r-compositions', 'r-geor', 'r-debugme', 'r-europepmc', 'r-ucminf', 'r-rmarkdown', 'r-tensora', 'r-snowfall', 'r-leaps', 'r-nleqslv', 'r-randomforest', 'r-rmpfr', 'sbml', 'r-globaloptions', 'cleaveland4', 'r-ldheatmap', 'r-testthat', 'r-spem', 'snphylo', 'r-a4', 'r-pbdzmq', 'r-ranger', 'r-rvest', 'r-fdb-infiniummethylation-hg19', 'r-decipher', 'r-rcmdcheck', 'r-pamr', 'r-formula', 'r-gplots', 'r-bayesm', 'r-ggjoy', 'r-sseq', 'r-absseq', 'r-rmutil', 'r-ergm', 'orthofiller', 'r-rematch2', 'r-scatterplot3d', 'r-biocinstaller', 'r-deoptim', 'r-keggrest', 'r-agimicrorna', 'r-digest', 'r-dosnow', 'r-codex', 'r-rpostgresql', 'r-scales', 'r-multicool', 'r-singlecellexperiment', 'r-pcamethods', 'r-affydata', 'r-tximport', 'r-affxparser', 'r-dichromat', 'r-polspline', 'r-txdb-hsapiens-ucsc-hg18-knowngene', 'r-tigris', 'r-fdb-infiniummethylation-hg18', 'r-genemeta', 'r-colorspace', 'r-emmli', 'r-timedate', 'r-biomformat', 'r-matlab', 'r-knitr', 'r-seqinr', 'r-lambda-r', 'r-rlang', 'r-ttr', 'r-splancs', 'r-makecdfenv', 'r-registry', 'r-mclust', 'r-nor1mix', 'r-rgooglemaps', 'r-xlsxjars', 'r-geonames', 'r-matrix', 'r-multitaper', 'r-segmented', 'r-mlbench', 'r-xlsx', 'r-yaml', 'r-bh', 'r-haven', 'r-leaflet', 'r-modelr', 'r-rcpp', 'r-rhdf5lib', 'r-quantmod', 'r-htmlwidgets', 'r-anaquin', 'r-kegg-db', 'r-genomicfeatures', 'r-devtools', 'r-stabledist', 'r-msnbase', 'r-rmariadb', 'r-htmltable', 'r-reprex', 'r-mass', 'r-xgboost', 'r-desc', 'r-e1071', 'r-maps', 'r-uwot', 'r-farver', 'r-annotationhub', 'r-diptest', 'r-spatstat-data', 'r-aldex2', 'kmergenie', 'r-xmapbridge', 'r-latticeextra', 'gatk', 'r-fields', 'r-phylostratr', 'r-gdalutils', 'r-log4r', 'r-rnaseqmap', 'r-learnbayes', 'r-rjags', 'r-multtest', 'r-spdata', 'r-nmof', 'r-generics', 'r-blockmodeling', 'r-ggraph', 'r-rsolnp', 'r-rdpack', 'r-bfast', 'r-openssl', 'r-urca', 'r-rann', 'r-spam', 'r-matrixmodels', 'r-c50', 'r-spatialeco', 'r-genie3', 'r-getoptlong', 'r-rvcheck', 'r-loo', 'r-callr', 'r-rprojroot', 'r-adgoftest', 'r-animation', 'r-abadata', 'r-clipr', 'r-evaluate', 'r-coda', 'r-ptw', 'r-truncdist', 'r-coin', 'r-construct', 'r-bamsignals', 'r-truncnorm', 'r-circlize', 'r-ggbio', 'r-delayedmatrixstats', 'r-prettyunits', 'r-ecp', 'r-affy', 'r-dqrng', 'r-rmysql', 'r-cubature', 'r-biomart', 'r-tibble', 'r-bmp', 'r-deseq2', 'r-geneplotter', 'r-biocmanager', 'r-vegan', 'r-rio', 'r-affycompatible', 'r-clue', 'r-rpart-plot', 'r-rngtools', 'r-dbi', 'r-mlrmbo', 'r-xml', 'r-gridgraphics', 'r-biostrings', 'r-viridis', 'r-desolve', 'r-libcoin', 'r-cardata', 'r-plotrix', 'r-wgcna', 'r-rgenoud', 'r-xtable', 'r-irkernel', 'r-xvector', 'r-rtsne', 'r-tinytex', 'r-rrcov', 'r-pscbs', 'r-protgenerics', 'r-yaimpute', 'r-rgraphviz', 'r-subplex', 'r-magrittr', 'r-lubridate', 'r-modelmetrics', 'r-speedglm', 'r-filehash', 'r-alsace', 'r-boruta', 'r-rnoaa', 'r-ks', 'r-taxizedb', 'r-biovizbase', 'r-zlibbioc', 'r-proj', 'r-geomorph', 'r-gridbase', 'r-pan', 'r-illuminaio', 'r-zip', 'r-spatialpack', 'r-rematch', 'r-rspectra', 'r-crul', 'r-rhmmer', 'r-bio3d', 'r-org-hs-eg-db', 'r-class', 'r-mpm', 'r-cowplot', 'turbine', 'r-deoptimr', 'r-tsne', 'r-rocr', 'r-rzmq', 'r-pls', 'r-rinside', 'r-rhdf5', 'r-leiden', 'r-hdf5array', 'r-numderiv', 'r-party', 'r-plotmo', 'r-powerlaw', 'r-squash', 'r-backports', 'r-caret', 'r-agdex', 'r-gofuncr', 'r-prodlim', 'r-affyrnadegradation', 'r-biasedurn', 'r-catools', 'r-som', 'r-intervals', 'r-tfbstools', 'r-rcppblaze', 'r-phangorn', 'r-tidycensus', 'r-xlconnect', 'r-popgenome', 'r-expint', 'r-formatr', 'r-biocgenerics', 'r-rminer', 'r-lfe', 'r-munsell', 'r-affycoretools', 'r-gss', 'r-sctransform', 'r-sn', 'r-genetics', 'r-statmod', 'r-tidyverse', 'r-tseries', 'r-adsplit', 'r-bindr', 'r-rappdirs', 'r-rgl', 'r-acgh', 'r-biobase', 'r-mice', 'r-rmpi', 'r-npsurv', 'r-blob', 'r-inline', 'r-goftest', 'r-sdmtools', 'rsem', 'r-do-db', 'r-forcats', 'r-nanotime', 'r-organismdbi', 'r-mco', 'r-dnacopy', 'r-chemometrics', 'r-commonmark', 'r-rtracklayer', 'r-brew', 'r-magic', 'r-pbapply', 'r-pkgmaker', 'r-ggplotify', 'r-sfsmisc', 'r-gsubfn', 'r-tweenr', 'r-r-cache', 'r-googlevis', 'r-a4core', 'r-reordercluster', 'r-spatstat-utils', 'r-biom-utils', 'r-kknn', 'r-biocfilecache', 'r-suppdists', 'r-runit', 'r-maldiquant', 'r-snakecase', 'r-teachingdemos', 'r-go-db', 'r-rots', 'r-dorng', 'r-nmf', 'r-dotcall64', 'r-vctrs', 'r-listenv', 'r-acde', 'r-lmtest', 'r-rbokeh', 'r-usethis', 'r-rjsonio', 'r-vfs', 'r-cluster', 'r-glimma', 'r-uuid', 'r-bfastspatial', 'r-shinyfiles', 'r-pillar', 'r-pkgbuild', 'r-beachmat', 'r-whisker', 'r-mcmcglmm', 'r-bumphunter', 'r-deseq', 'r-envstats', 'r-mlinterfaces', 'r-metap', 'r-cairo', 'r-repr', 'r-a4base', 'r-ensembldb', 'hic-pro', 'r-rook', 'r-graph', 'r-httr', 'r-jpeg', 'r-geoquery', 'r-impute', 'phantompeakqualtools', 'r-boot', 'r-vsn', 'r-pfam-db', 'r-dicekriging'}
........................ check: r-phytools
........................ check: r-annotationdbi
......................... direct: r-annotationdbi -> {'r-txdb-hsapiens-ucsc-hg18-knowngene', 'r-gofuncr', 'r-variantannotation', 'r-ggbio', 'r-fdb-infiniummethylation-hg18', 'r-gosemsim', 'r-dose', 'r-go-db', 'r-clusterprofiler', 'r-biovizbase', 'r-organismdbi', 'r-lumi', 'r-txdb-hsapiens-ucsc-hg19-knowngene', 'r-biomart', 'r-annotationforge', 'r-geneplotter', 'r-affycoretools', 'r-adsplit', 'r-reportingtools', 'r-category', 'r-genefilter', 'r-bumphunter', 'r-fdb-infiniummethylation-hg19', 'r-gviz', 'r-pathview', 'r-org-hs-eg-db', 'r-topgo', 'r-do-db', 'r-a4base', 'r-methylumi', 'r-ensembldb', 'r-goseq', 'r-annaffy', 'r-genomicfeatures', 'r-kegg-db', 'r-enrichplot', 'r-annotate', 'r-gostats', 'r-a4preproc', 'r-wgcna', 'r-allelicimbalance', 'r-gseabase', 'r-pfam-db', 'r-annotationhub'}
......................... check: r-txdb-hsapiens-ucsc-hg18-knowngene
......................... check: r-gofuncr
.......................... direct: r-gofuncr -> {'r-abaenrichment'}
.......................... check: r-abaenrichment
........................... direct: r-abaenrichment -> set()
......................... check: r-variantannotation
......................... check: r-ggbio
......................... check: r-fdb-infiniummethylation-hg18
......................... check: r-gosemsim
.......................... direct: r-gosemsim -> {'r-dose', 'r-clusterprofiler', 'r-enrichplot'}
.......................... check: r-dose
........................... direct: r-dose -> {'r-clusterprofiler', 'r-enrichplot'}
........................... check: r-clusterprofiler
............................ direct: r-clusterprofiler -> set()
........................... check: r-enrichplot
............................ direct: r-enrichplot -> {'r-clusterprofiler'}
............................ check: r-clusterprofiler
.......................... check: r-clusterprofiler
.......................... check: r-enrichplot
......................... check: r-dose
......................... check: r-go-db
.......................... direct: r-go-db -> {'r-goseq', 'r-wgcna', 'r-annaffy', 'r-gostats', 'r-topgo', 'r-gosemsim', 'r-cner', 'r-adsplit', 'r-clusterprofiler'}
.......................... check: r-goseq
.......................... check: r-wgcna
........................... direct: r-wgcna -> set()
.......................... check: r-annaffy
........................... direct: r-annaffy -> {'r-a4reporting', 'r-a4base'}
........................... check: r-a4reporting
............................ direct: r-a4reporting -> {'r-a4'}
............................ check: r-a4
............................. direct: r-a4 -> set()
........................... check: r-a4base
............................ direct: r-a4base -> {'r-a4'}
............................ check: r-a4
.......................... check: r-gostats
.......................... check: r-topgo
........................... direct: r-topgo -> set()
.......................... check: r-gosemsim
.......................... check: r-cner
........................... direct: r-cner -> {'r-tfbstools'}
........................... check: r-tfbstools
............................ direct: r-tfbstools -> set()
.......................... check: r-adsplit
........................... direct: r-adsplit -> set()
.......................... check: r-clusterprofiler
......................... check: r-clusterprofiler
......................... check: r-biovizbase
......................... check: r-organismdbi
......................... check: r-lumi
......................... check: r-txdb-hsapiens-ucsc-hg19-knowngene
......................... check: r-biomart
.......................... direct: r-biomart -> {'r-yarn', 'r-genomicfeatures', 'r-gviz', 'r-biomartr', 'r-scater'}
.......................... check: r-yarn
.......................... check: r-genomicfeatures
.......................... check: r-gviz
.......................... check: r-biomartr
........................... direct: r-biomartr -> set()
.......................... check: r-scater
........................... direct: r-scater -> {'r-glimma'}
........................... check: r-glimma
............................ direct: r-glimma -> {'trinity'}
............................ check: trinity
......................... check: r-annotationforge
.......................... direct: r-annotationforge -> {'r-gostats'}
.......................... check: r-gostats
......................... check: r-geneplotter
.......................... direct: r-geneplotter -> {'r-deseq', 'r-deseq2'}
.......................... check: r-deseq
........................... direct: r-deseq -> {'r-ampliqueso', 'r-rnaseqmap', 'py-tetoolkit'}
........................... check: r-ampliqueso
........................... check: r-rnaseqmap
............................ direct: r-rnaseqmap -> {'r-ampliqueso'}
............................ check: r-ampliqueso
........................... check: py-tetoolkit
.......................... check: r-deseq2
........................... direct: r-deseq2 -> {'r-anaquin', 'trinity', 'py-tetoolkit', 'homer', 'r-reportingtools'}
........................... check: r-anaquin
............................ direct: r-anaquin -> set()
........................... check: trinity
........................... check: py-tetoolkit
........................... check: homer
............................ direct: homer -> set()
........................... check: r-reportingtools
......................... check: r-affycoretools
......................... check: r-adsplit
......................... check: r-reportingtools
......................... check: r-category
.......................... direct: r-category -> {'r-reportingtools', 'r-gostats'}
.......................... check: r-reportingtools
.......................... check: r-gostats
......................... check: r-genefilter
.......................... direct: r-genefilter -> {'r-minfi', 'r-methylumi', 'r-simpleaffy', 'r-deseq2', 'r-deseq', 'r-genemeta', 'r-affyqcreport', 'r-mlinterfaces', 'r-sva', 'r-xde', 'r-ampliqueso', 'r-category', 'r-a4base'}
.......................... check: r-minfi
.......................... check: r-methylumi
.......................... check: r-simpleaffy
........................... direct: r-simpleaffy -> {'r-affyqcreport', 'r-yaqcaffy'}
........................... check: r-affyqcreport
............................ direct: r-affyqcreport -> set()
........................... check: r-yaqcaffy
............................ direct: r-yaqcaffy -> set()
.......................... check: r-deseq2
.......................... check: r-deseq
.......................... check: r-genemeta
........................... direct: r-genemeta -> {'r-xde'}
........................... check: r-xde
............................ direct: r-xde -> set()
.......................... check: r-affyqcreport
.......................... check: r-mlinterfaces
........................... direct: r-mlinterfaces -> {'r-a4classif'}
........................... check: r-a4classif
............................ direct: r-a4classif -> {'r-a4'}
............................ check: r-a4
.......................... check: r-sva
........................... direct: r-sva -> set()
.......................... check: r-xde
.......................... check: r-ampliqueso
.......................... check: r-category
.......................... check: r-a4base
......................... check: r-bumphunter
......................... check: r-fdb-infiniummethylation-hg19
......................... check: r-gviz
......................... check: r-pathview
......................... check: r-org-hs-eg-db
.......................... direct: r-org-hs-eg-db -> {'r-pathview', 'r-fdb-infiniummethylation-hg19', 'r-fdb-infiniummethylation-hg18'}
.......................... check: r-pathview
.......................... check: r-fdb-infiniummethylation-hg19
.......................... check: r-fdb-infiniummethylation-hg18
......................... check: r-topgo
......................... check: r-do-db
.......................... direct: r-do-db -> {'r-dose'}
.......................... check: r-dose
......................... check: r-a4base
......................... check: r-methylumi
......................... check: r-ensembldb
......................... check: r-goseq
......................... check: r-annaffy
......................... check: r-genomicfeatures
......................... check: r-kegg-db
.......................... direct: r-kegg-db -> {'r-adsplit', 'r-annaffy'}
.......................... check: r-adsplit
.......................... check: r-annaffy
......................... check: r-enrichplot
......................... check: r-annotate
.......................... direct: r-annotate -> {'r-lumi', 'r-gostats', 'r-genefilter', 'r-geneplotter', 'r-mlinterfaces', 'r-gseabase', 'r-cner', 'r-reportingtools', 'r-category', 'r-methylumi'}
.......................... check: r-lumi
.......................... check: r-gostats
.......................... check: r-genefilter
.......................... check: r-geneplotter
.......................... check: r-mlinterfaces
.......................... check: r-gseabase
........................... direct: r-gseabase -> {'r-reportingtools', 'r-category', 'r-agdex'}
........................... check: r-reportingtools
........................... check: r-category
........................... check: r-agdex
............................ direct: r-agdex -> set()
.......................... check: r-cner
.......................... check: r-reportingtools
.......................... check: r-category
.......................... check: r-methylumi
......................... check: r-gostats
......................... check: r-a4preproc
.......................... direct: r-a4preproc -> {'r-a4base', 'r-a4classif', 'r-a4'}
.......................... check: r-a4base
.......................... check: r-a4classif
.......................... check: r-a4
......................... check: r-wgcna
......................... check: r-allelicimbalance
......................... check: r-gseabase
......................... check: r-pfam-db
.......................... direct: r-pfam-db -> {'r-reportingtools'}
.......................... check: r-reportingtools
......................... check: r-annotationhub
.......................... direct: r-annotationhub -> {'r-ensembldb'}
.......................... check: r-ensembldb
........................ check: r-mnormt
......................... direct: r-mnormt -> {'r-phytools', 'r-sn', 'r-psych'}
......................... check: r-phytools
......................... check: r-sn
.......................... direct: r-sn -> set()
......................... check: r-psych
.......................... direct: r-psych -> {'r-broom'}
.......................... check: r-broom
........................... direct: r-broom -> {'r-modelr', 'r-tidyverse', 'r-mice'}
........................... check: r-modelr
............................ direct: r-modelr -> {'r-tidyverse'}
............................ check: r-tidyverse
........................... check: r-tidyverse
........................... check: r-mice
........................ check: r-praise
......................... direct: r-praise -> {'r-testthat'}
......................... check: r-testthat
.......................... direct: r-testthat -> {'r-devtools'}
.......................... check: r-devtools
........................ check: r-dygraphs
......................... direct: r-dygraphs -> set()
........................ check: r-exomedepth
......................... direct: r-exomedepth -> set()
........................ check: r-graphlayouts
......................... direct: r-graphlayouts -> {'r-ggraph'}
......................... check: r-ggraph
.......................... direct: r-ggraph -> {'r-enrichplot'}
.......................... check: r-enrichplot
........................ check: r-mgraster
......................... direct: r-mgraster -> {'r-matr'}
......................... check: r-matr
.......................... direct: r-matr -> {'py-mg-rast-tools'}
.......................... check: py-mg-rast-tools
........................ check: r-r-utils
......................... direct: r-r-utils -> {'r-aroma-light', 'r-biomartr', 'r-sdmtools', 'r-cner', 'r-reportingtools', 'r-gdalutils', 'r-pscbs', 'r-r-cache'}
......................... check: r-aroma-light
.......................... direct: r-aroma-light -> {'r-pscbs'}
.......................... check: r-pscbs
........................... direct: r-pscbs -> set()
......................... check: r-biomartr
......................... check: r-sdmtools
.......................... direct: r-sdmtools -> {'r-seurat'}
.......................... check: r-seurat
......................... check: r-cner
......................... check: r-reportingtools
......................... check: r-gdalutils
......................... check: r-pscbs
......................... check: r-r-cache
.......................... direct: r-r-cache -> {'r-pscbs'}
.......................... check: r-pscbs
........................ check: r-ctc
......................... direct: r-ctc -> {'trinity'}
......................... check: trinity
........................ check: r-bglr
......................... direct: r-bglr -> {'r-popvar'}
......................... check: r-popvar
.......................... direct: r-popvar -> set()
........................ check: r-iranges
......................... direct: r-iranges -> {'r-annotationdbi', 'r-bamsignals', 'r-aldex2', 'r-exomedepth', 'r-gofuncr', 'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-delayedmatrixstats', 'r-summarizedexperiment', 'r-tfbstools', 'r-rnaseqmap', 'r-biovizbase', 'r-bsgenome', 'r-deseq2', 'r-shortread', 'r-reportingtools', 'r-genomicranges', 'r-rsamtools', 'r-delayedarray', 'r-biostrings', 'r-bumphunter', 'r-decipher', 'r-gviz', 'r-genomeinfodb', 'r-condop', 'r-exomecopy', 'r-methylumi', 'r-organismdbi', 'r-ensembldb', 'r-genomicfeatures', 'r-dada2', 'r-msnbase', 'r-minfi', 'r-rtracklayer', 'r-xvector', 'r-somaticsignatures', 'r-hdf5array', 'r-aneufinder', 'r-dirichletmultinomial', 'r-allelicimbalance', 'r-codex', 'r-gtrellis', 'r-cner', 'r-genomicalignments', 'r-oligoclasses'}
......................... check: r-annotationdbi
......................... check: r-bamsignals
.......................... direct: r-bamsignals -> {'r-aneufinder'}
.......................... check: r-aneufinder
........................... direct: r-aneufinder -> set()
......................... check: r-aldex2
.......................... direct: r-aldex2 -> set()
......................... check: r-exomedepth
......................... check: r-gofuncr
......................... check: r-variantannotation
......................... check: r-ggbio
......................... check: r-alpine
......................... check: r-delayedmatrixstats
.......................... direct: r-delayedmatrixstats -> {'r-minfi', 'r-scater'}
.......................... check: r-minfi
.......................... check: r-scater
......................... check: r-summarizedexperiment
.......................... direct: r-summarizedexperiment -> {'r-aldex2', 'r-variantannotation', 'r-minfi', 'r-ggbio', 'r-alpine', 'r-deseq2', 'r-allelicimbalance', 'r-scater', 'r-singlecellexperiment', 'r-genomicalignments', 'r-oligoclasses', 'r-biovizbase', 'r-methylumi'}
.......................... check: r-aldex2
.......................... check: r-variantannotation
.......................... check: r-minfi
.......................... check: r-ggbio
.......................... check: r-alpine
.......................... check: r-deseq2
.......................... check: r-allelicimbalance
.......................... check: r-scater
.......................... check: r-singlecellexperiment
........................... direct: r-singlecellexperiment -> {'r-scater'}
........................... check: r-scater
.......................... check: r-genomicalignments
........................... direct: r-genomicalignments -> {'r-exomedepth', 'r-ggbio', 'r-rtracklayer', 'r-alpine', 'r-gviz', 'r-aneufinder', 'r-shortread', 'r-allelicimbalance', 'r-cner', 'r-rnaseqmap', 'r-biovizbase'}
........................... check: r-exomedepth
........................... check: r-ggbio
........................... check: r-rtracklayer
............................ direct: r-rtracklayer -> {'r-ensembldb', 'r-bsgenome', 'r-genomicfeatures', 'r-genelendatabase', 'r-variantannotation', 'r-ggbio', 'r-gviz', 'r-tfbstools', 'r-cner'}
............................ check: r-ensembldb
............................ check: r-bsgenome
............................. direct: r-bsgenome -> {'r-bsgenome-hsapiens-ucsc-hg19', 'r-variantannotation', 'r-ggbio', 'r-gviz', 'r-tfbstools', 'r-allelicimbalance'}
............................. check: r-bsgenome-hsapiens-ucsc-hg19
.............................. direct: r-bsgenome-hsapiens-ucsc-hg19 -> {'r-codex'}
.............................. check: r-codex
............................... direct: r-codex -> set()
............................. check: r-variantannotation
............................. check: r-ggbio
............................. check: r-gviz
............................. check: r-tfbstools
............................. check: r-allelicimbalance
............................ check: r-genomicfeatures
............................ check: r-genelendatabase
............................ check: r-variantannotation
............................ check: r-ggbio
............................ check: r-gviz
............................ check: r-tfbstools
............................ check: r-cner
........................... check: r-alpine
........................... check: r-gviz
........................... check: r-aneufinder
........................... check: r-shortread
............................ direct: r-shortread -> {'r-dada2'}
............................ check: r-dada2
............................. direct: r-dada2 -> set()
........................... check: r-allelicimbalance
........................... check: r-cner
........................... check: r-rnaseqmap
........................... check: r-biovizbase
.......................... check: r-oligoclasses
........................... direct: r-oligoclasses -> {'r-affycoretools'}
........................... check: r-affycoretools
.......................... check: r-biovizbase
.......................... check: r-methylumi
......................... check: r-tfbstools
......................... check: r-rnaseqmap
......................... check: r-biovizbase
......................... check: r-bsgenome
......................... check: r-deseq2
......................... check: r-shortread
......................... check: r-reportingtools
......................... check: r-genomicranges
.......................... direct: r-genomicranges -> {'r-bamsignals', 'r-aldex2', 'r-exomedepth', 'r-gofuncr', 'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-summarizedexperiment', 'r-tfbstools', 'r-annotationfilter', 'r-rnaseqmap', 'r-biovizbase', 'r-lumi', 'r-bsgenome', 'r-deseq2', 'r-shortread', 'r-yapsa', 'r-rsamtools', 'r-bumphunter', 'r-gviz', 'r-condop', 'r-exomecopy', 'r-methylumi', 'r-organismdbi', 'r-ensembldb', 'r-genomicfeatures', 'r-minfi', 'r-rtracklayer', 'r-somaticsignatures', 'r-aneufinder', 'r-allelicimbalance', 'r-gtrellis', 'r-cner', 'r-genomicalignments', 'r-oligoclasses'}
.......................... check: r-bamsignals
.......................... check: r-aldex2
.......................... check: r-exomedepth
.......................... check: r-gofuncr
.......................... check: r-variantannotation
.......................... check: r-ggbio
.......................... check: r-alpine
.......................... check: r-summarizedexperiment
.......................... check: r-tfbstools
.......................... check: r-annotationfilter
........................... direct: r-annotationfilter -> {'r-ensembldb', 'r-ggbio', 'r-biovizbase'}
........................... check: r-ensembldb
........................... check: r-ggbio
........................... check: r-biovizbase
.......................... check: r-rnaseqmap
.......................... check: r-biovizbase
.......................... check: r-lumi
.......................... check: r-bsgenome
.......................... check: r-deseq2
.......................... check: r-shortread
.......................... check: r-yapsa
.......................... check: r-rsamtools
........................... direct: r-rsamtools -> {'r-ensembldb', 'r-bsgenome', 'r-exomedepth', 'r-variantannotation', 'r-ggbio', 'r-rtracklayer', 'r-alpine', 'r-gviz', 'r-aneufinder', 'r-phantompeakqualtools', 'r-shortread', 'r-allelicimbalance', 'r-codex', 'r-exomecopy', 'r-rnaseqmap', 'r-genomicalignments', 'r-biovizbase'}
........................... check: r-ensembldb
........................... check: r-bsgenome
........................... check: r-exomedepth
........................... check: r-variantannotation
........................... check: r-ggbio
........................... check: r-rtracklayer
........................... check: r-alpine
........................... check: r-gviz
........................... check: r-aneufinder
........................... check: r-phantompeakqualtools
............................ direct: r-phantompeakqualtools -> {'phantompeakqualtools'}
............................ check: phantompeakqualtools
............................. direct: phantompeakqualtools -> set()
........................... check: r-shortread
........................... check: r-allelicimbalance
........................... check: r-codex
........................... check: r-exomecopy
............................ direct: r-exomecopy -> set()
........................... check: r-rnaseqmap
........................... check: r-genomicalignments
........................... check: r-biovizbase
.......................... check: r-bumphunter
.......................... check: r-gviz
.......................... check: r-condop
.......................... check: r-exomecopy
.......................... check: r-methylumi
.......................... check: r-organismdbi
.......................... check: r-ensembldb
.......................... check: r-genomicfeatures
.......................... check: r-minfi
.......................... check: r-rtracklayer
.......................... check: r-somaticsignatures
.......................... check: r-aneufinder
.......................... check: r-allelicimbalance
.......................... check: r-gtrellis
........................... direct: r-gtrellis -> {'r-yapsa'}
........................... check: r-yapsa
.......................... check: r-cner
.......................... check: r-genomicalignments
.......................... check: r-oligoclasses
......................... check: r-rsamtools
......................... check: r-delayedarray
.......................... direct: r-delayedarray -> {'r-beachmat', 'r-minfi', 'r-delayedmatrixstats', 'r-summarizedexperiment', 'r-hdf5array', 'r-scater', 'r-biocsingular'}
.......................... check: r-beachmat
........................... direct: r-beachmat -> {'r-scater', 'r-biocsingular'}
........................... check: r-scater
........................... check: r-biocsingular
............................ direct: r-biocsingular -> {'r-scater'}
............................ check: r-scater
.......................... check: r-minfi
.......................... check: r-delayedmatrixstats
.......................... check: r-summarizedexperiment
.......................... check: r-hdf5array
........................... direct: r-hdf5array -> {'r-minfi', 'r-delayedmatrixstats', 'r-beachmat'}
........................... check: r-minfi
........................... check: r-delayedmatrixstats
........................... check: r-beachmat
.......................... check: r-scater
.......................... check: r-biocsingular
......................... check: r-biostrings
.......................... direct: r-biostrings -> {'r-exomedepth', 'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-biomartr', 'r-fdb-infiniummethylation-hg18', 'r-tfbstools', 'r-phylostratr', 'r-biovizbase', 'r-bsgenome', 'r-altcdfenvs', 'r-shortread', 'r-affycompatible', 'r-phyloseq', 'r-rsamtools', 'r-gviz', 'r-fdb-infiniummethylation-hg19', 'r-decipher', 'r-gcrma', 'r-ensembldb', 'r-genomicfeatures', 'r-dada2', 'r-minfi', 'r-rtracklayer', 'r-keggrest', 'r-somaticsignatures', 'r-aneufinder', 'r-allelicimbalance', 'r-codex', 'r-cner', 'r-genomicalignments', 'r-oligoclasses'}
.......................... check: r-exomedepth
.......................... check: r-variantannotation
.......................... check: r-ggbio
.......................... check: r-alpine
.......................... check: r-biomartr
.......................... check: r-fdb-infiniummethylation-hg18
.......................... check: r-tfbstools
.......................... check: r-phylostratr
.......................... check: r-biovizbase
.......................... check: r-bsgenome
.......................... check: r-altcdfenvs
........................... direct: r-altcdfenvs -> set()
.......................... check: r-shortread
.......................... check: r-affycompatible
........................... direct: r-affycompatible -> set()
.......................... check: r-phyloseq
........................... direct: r-phyloseq -> set()
.......................... check: r-rsamtools
.......................... check: r-gviz
.......................... check: r-fdb-infiniummethylation-hg19
.......................... check: r-decipher
........................... direct: r-decipher -> set()
.......................... check: r-gcrma
........................... direct: r-gcrma -> {'r-affycoretools', 'r-simpleaffy', 'r-affyplm', 'r-affyilm'}
........................... check: r-affycoretools
........................... check: r-simpleaffy
........................... check: r-affyplm
............................ direct: r-affyplm -> {'r-affyqcreport'}
............................ check: r-affyqcreport
........................... check: r-affyilm
............................ direct: r-affyilm -> set()
.......................... check: r-ensembldb
.......................... check: r-genomicfeatures
.......................... check: r-dada2
.......................... check: r-minfi
.......................... check: r-rtracklayer
.......................... check: r-keggrest
........................... direct: r-keggrest -> {'r-pathview', 'r-yapsa', 'r-cner'}
........................... check: r-pathview
........................... check: r-yapsa
........................... check: r-cner
.......................... check: r-somaticsignatures
.......................... check: r-aneufinder
.......................... check: r-allelicimbalance
.......................... check: r-codex
.......................... check: r-cner
.......................... check: r-genomicalignments
.......................... check: r-oligoclasses
......................... check: r-bumphunter
......................... check: r-decipher
......................... check: r-gviz
......................... check: r-genomeinfodb
.......................... direct: r-genomeinfodb -> {'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-summarizedexperiment', 'r-tfbstools', 'r-biovizbase', 'r-bsgenome', 'r-shortread', 'r-genomicranges', 'r-yapsa', 'r-rsamtools', 'r-bumphunter', 'r-gviz', 'r-condop', 'r-exomecopy', 'r-methylumi', 'r-ensembldb', 'r-genomicfeatures', 'r-minfi', 'r-rtracklayer', 'r-somaticsignatures', 'r-aneufinder', 'r-allelicimbalance', 'r-codex', 'r-cner', 'r-genomicalignments'}
.......................... check: r-variantannotation
.......................... check: r-ggbio
.......................... check: r-alpine
.......................... check: r-summarizedexperiment
.......................... check: r-tfbstools
.......................... check: r-biovizbase
.......................... check: r-bsgenome
.......................... check: r-shortread
.......................... check: r-genomicranges
.......................... check: r-yapsa
.......................... check: r-rsamtools
.......................... check: r-bumphunter
.......................... check: r-gviz
.......................... check: r-condop
.......................... check: r-exomecopy
.......................... check: r-methylumi
.......................... check: r-ensembldb
.......................... check: r-genomicfeatures
.......................... check: r-minfi
.......................... check: r-rtracklayer
.......................... check: r-somaticsignatures
.......................... check: r-aneufinder
.......................... check: r-allelicimbalance
.......................... check: r-codex
.......................... check: r-cner
.......................... check: r-genomicalignments
......................... check: r-condop
......................... check: r-exomecopy
......................... check: r-methylumi
......................... check: r-organismdbi
......................... check: r-ensembldb
......................... check: r-genomicfeatures
......................... check: r-dada2
......................... check: r-msnbase
......................... check: r-minfi
......................... check: r-rtracklayer
......................... check: r-xvector
.......................... direct: r-xvector -> {'r-bsgenome', 'r-genomicfeatures', 'r-dada2', 'r-rsamtools', 'r-variantannotation', 'r-rtracklayer', 'r-biostrings', 'r-gcrma', 'r-decipher', 'r-gviz', 'r-shortread', 'r-tfbstools', 'r-cner', 'r-genomicranges'}
.......................... check: r-bsgenome
.......................... check: r-genomicfeatures
.......................... check: r-dada2
.......................... check: r-rsamtools
.......................... check: r-variantannotation
.......................... check: r-rtracklayer
.......................... check: r-biostrings
.......................... check: r-gcrma
.......................... check: r-decipher
.......................... check: r-gviz
.......................... check: r-shortread
.......................... check: r-tfbstools
.......................... check: r-cner
.......................... check: r-genomicranges
......................... check: r-somaticsignatures
......................... check: r-hdf5array
......................... check: r-aneufinder
......................... check: r-dirichletmultinomial
.......................... direct: r-dirichletmultinomial -> {'r-tfbstools'}
.......................... check: r-tfbstools
......................... check: r-allelicimbalance
......................... check: r-codex
......................... check: r-gtrellis
......................... check: r-cner
......................... check: r-genomicalignments
......................... check: r-oligoclasses
........................ check: r-r6
......................... direct: r-r6 -> {'r-hdf5r', 'r-processx', 'r-readr', 'r-selectr', 'r-dbplyr', 'r-tidygraph', 'r-hoardr', 'r-crosstalk', 'r-nimble', 'r-testthat', 'r-pkgbuild', 'r-pbdzmq', 'r-crul', 'r-shiny', 'r-promises', 'r-rcmdcheck', 'r-progress', 'r-roxygen2', 'r-argparse', 'r-callr', 'r-httpuv', 'r-dplyr', 'r-httr', 'r-desc', 'r-scales'}
......................... check: r-hdf5r
......................... check: r-processx
.......................... direct: r-processx -> {'r-callr', 'r-xopen'}
.......................... check: r-callr
........................... direct: r-callr -> {'r-pkgbuild', 'r-devtools', 'r-webshot', 'r-rcmdcheck', 'r-reprex'}
........................... check: r-pkgbuild
............................ direct: r-pkgbuild -> {'r-rcmdcheck', 'r-rstan', 'r-devtools', 'r-pkgload'}
............................ check: r-rcmdcheck
............................. direct: r-rcmdcheck -> {'r-devtools'}
............................. check: r-devtools
............................ check: r-rstan
............................ check: r-devtools
............................ check: r-pkgload
............................. direct: r-pkgload -> {'r-testthat', 'r-roxygen2', 'r-devtools'}
............................. check: r-testthat
............................. check: r-roxygen2
.............................. direct: r-roxygen2 -> {'r-devtools'}
.............................. check: r-devtools
............................. check: r-devtools
........................... check: r-devtools
........................... check: r-webshot
............................ direct: r-webshot -> {'r-manipulatewidget'}
............................ check: r-manipulatewidget
............................. direct: r-manipulatewidget -> {'r-rgl'}
............................. check: r-rgl
........................... check: r-rcmdcheck
........................... check: r-reprex
.......................... check: r-xopen
........................... direct: r-xopen -> {'r-rcmdcheck'}
........................... check: r-rcmdcheck
......................... check: r-readr
.......................... direct: r-readr -> {'r-phylostratr', 'r-yarn', 'r-rhmmer', 'r-cdcfluview', 'r-geoquery', 'r-biomartr', 'r-haven', 'r-spatialeco', 'r-tidycensus', 'r-tidyverse', 'r-cner', 'r-diagrammer'}
.......................... check: r-phylostratr
.......................... check: r-yarn
.......................... check: r-rhmmer
........................... direct: r-rhmmer -> {'r-phylostratr'}
........................... check: r-phylostratr
.......................... check: r-cdcfluview
.......................... check: r-geoquery
........................... direct: r-geoquery -> {'r-minfi'}
........................... check: r-minfi
.......................... check: r-biomartr
.......................... check: r-haven
........................... direct: r-haven -> {'r-mitml', 'r-tidyverse', 'r-rio'}
........................... check: r-mitml
........................... check: r-tidyverse
........................... check: r-rio
............................ direct: r-rio -> {'r-car'}
............................ check: r-car
.......................... check: r-spatialeco
.......................... check: r-tidycensus
.......................... check: r-tidyverse
.......................... check: r-cner
.......................... check: r-diagrammer
........................... direct: r-diagrammer -> set()
......................... check: r-selectr
.......................... direct: r-selectr -> {'r-rvest'}
.......................... check: r-rvest
........................... direct: r-rvest -> {'r-tidycensus', 'r-tidyverse'}
........................... check: r-tidycensus
........................... check: r-tidyverse
......................... check: r-dbplyr
.......................... direct: r-dbplyr -> {'r-taxizedb', 'r-tidyverse', 'r-biocfilecache'}
.......................... check: r-taxizedb
.......................... check: r-tidyverse
.......................... check: r-biocfilecache
........................... direct: r-biocfilecache -> {'r-annotationhub'}
........................... check: r-annotationhub
......................... check: r-tidygraph
.......................... direct: r-tidygraph -> {'r-ggraph'}
.......................... check: r-ggraph
......................... check: r-hoardr
.......................... direct: r-hoardr -> {'r-taxizedb', 'r-rnoaa'}
.......................... check: r-taxizedb
.......................... check: r-rnoaa
........................... direct: r-rnoaa -> set()
......................... check: r-crosstalk
.......................... direct: r-crosstalk -> {'r-threejs', 'r-rgl', 'r-plotly', 'r-leaflet', 'r-dt'}
.......................... check: r-threejs
........................... direct: r-threejs -> {'r-mlinterfaces'}
........................... check: r-mlinterfaces
.......................... check: r-rgl
.......................... check: r-plotly
........................... direct: r-plotly -> {'r-smoof', 'r-seurat'}
........................... check: r-smoof
............................ direct: r-smoof -> {'r-mlrmbo'}
............................ check: r-mlrmbo
............................. direct: r-mlrmbo -> set()
........................... check: r-seurat
.......................... check: r-leaflet
........................... direct: r-leaflet -> set()
.......................... check: r-dt
........................... direct: r-dt -> set()
......................... check: r-nimble
.......................... direct: r-nimble -> set()
......................... check: r-testthat
......................... check: r-pkgbuild
......................... check: r-pbdzmq
.......................... direct: r-pbdzmq -> {'r-irkernel'}
.......................... check: r-irkernel
......................... check: r-crul
.......................... direct: r-crul -> {'r-rnoaa'}
.......................... check: r-rnoaa
......................... check: r-shiny
.......................... direct: r-shiny -> {'r-adegenet', 'r-manipulatewidget', 'r-samr', 'r-miniui', 'r-interactivedisplaybase', 'r-rgl', 'r-shinydashboard', 'r-mlinterfaces', 'r-crosstalk', 'r-scater', 'r-ggvis', 'r-shinyfiles'}
.......................... check: r-adegenet
.......................... check: r-manipulatewidget
.......................... check: r-samr
........................... direct: r-samr -> {'r-ampliqueso'}
........................... check: r-ampliqueso
.......................... check: r-miniui
........................... direct: r-miniui -> {'r-manipulatewidget'}
........................... check: r-manipulatewidget
.......................... check: r-interactivedisplaybase
........................... direct: r-interactivedisplaybase -> {'r-annotationhub'}
........................... check: r-annotationhub
.......................... check: r-rgl
.......................... check: r-shinydashboard
........................... direct: r-shinydashboard -> {'r-scater'}
........................... check: r-scater
.......................... check: r-mlinterfaces
.......................... check: r-crosstalk
.......................... check: r-scater
.......................... check: r-ggvis
........................... direct: r-ggvis -> {'r-mlinterfaces'}
........................... check: r-mlinterfaces
.......................... check: r-shinyfiles
........................... direct: r-shinyfiles -> {'r-samr'}
........................... check: r-samr
......................... check: r-promises
.......................... direct: r-promises -> {'r-httpuv', 'r-shiny', 'r-plotly', 'r-shinydashboard', 'r-dt'}
.......................... check: r-httpuv
........................... direct: r-httpuv -> {'r-shiny'}
........................... check: r-shiny
.......................... check: r-shiny
.......................... check: r-plotly
.......................... check: r-shinydashboard
.......................... check: r-dt
......................... check: r-rcmdcheck
......................... check: r-progress
.......................... direct: r-progress -> {'r-biomart', 'r-readxl', 'r-europepmc', 'r-ggally'}
.......................... check: r-biomart
.......................... check: r-readxl
........................... direct: r-readxl -> {'r-rio', 'r-tidyverse'}
........................... check: r-rio
........................... check: r-tidyverse
.......................... check: r-europepmc
........................... direct: r-europepmc -> {'r-enrichplot'}
........................... check: r-enrichplot
.......................... check: r-ggally
........................... direct: r-ggally -> {'r-ggbio'}
........................... check: r-ggbio
......................... check: r-roxygen2
......................... check: r-argparse
.......................... direct: r-argparse -> {'trinity'}
.......................... check: trinity
......................... check: r-callr
......................... check: r-httpuv
......................... check: r-dplyr
.......................... direct: r-dplyr -> {'r-exomedepth', 'r-tidyr', 'r-tigris', 'r-seurat', 'r-plotly', 'r-biomartr', 'r-biocfilecache', 'r-philentropy', 'r-tidycensus', 'r-scater', 'r-phylostratr', 'r-taxizedb', 'r-rnoaa', 'r-europepmc', 'r-dbplyr', 'r-tidygraph', 'r-ggraph', 'r-recipes', 'r-ggpubr', 'r-tidyverse', 'r-broom', 'r-rhmmer', 'r-cdcfluview', 'r-mice', 'r-spatialeco', 'r-modelr', 'r-ergm', 'r-diagrammer', 'r-adegenet', 'r-geoquery', 'r-ggmap', 'r-gistr', 'r-janitor', 'r-ggvis', 'r-annotationhub'}
.......................... check: r-exomedepth
.......................... check: r-tidyr
........................... direct: r-tidyr -> {'r-broom', 'r-rhmmer', 'r-seurat', 'r-tidygraph', 'r-plotly', 'r-geoquery', 'r-ggmap', 'r-factoextra', 'r-janitor', 'r-tidycensus', 'r-ggpubr', 'r-tidyverse', 'r-modelr', 'r-phylostratr', 'r-recipes', 'r-rnoaa', 'r-clusterprofiler', 'r-diagrammer'}
........................... check: r-broom
........................... check: r-rhmmer
........................... check: r-seurat
........................... check: r-tidygraph
........................... check: r-plotly
........................... check: r-geoquery
........................... check: r-ggmap
............................ direct: r-ggmap -> set()
........................... check: r-factoextra
........................... check: r-janitor
............................ direct: r-janitor -> set()
........................... check: r-tidycensus
........................... check: r-ggpubr
............................ direct: r-ggpubr -> {'r-factoextra'}
............................ check: r-factoextra
........................... check: r-tidyverse
........................... check: r-modelr
........................... check: r-phylostratr
........................... check: r-recipes
............................ direct: r-recipes -> {'r-caret'}
............................ check: r-caret
........................... check: r-rnoaa
........................... check: r-clusterprofiler
........................... check: r-diagrammer
.......................... check: r-tigris
.......................... check: r-seurat
.......................... check: r-plotly
.......................... check: r-biomartr
.......................... check: r-biocfilecache
.......................... check: r-philentropy
........................... direct: r-philentropy -> {'r-biomartr'}
........................... check: r-biomartr
.......................... check: r-tidycensus
.......................... check: r-scater
.......................... check: r-phylostratr
.......................... check: r-taxizedb
.......................... check: r-rnoaa
.......................... check: r-europepmc
.......................... check: r-dbplyr
.......................... check: r-tidygraph
.......................... check: r-ggraph
.......................... check: r-recipes
.......................... check: r-ggpubr
.......................... check: r-tidyverse
.......................... check: r-broom
.......................... check: r-rhmmer
.......................... check: r-cdcfluview
.......................... check: r-mice
.......................... check: r-spatialeco
.......................... check: r-modelr
.......................... check: r-ergm
........................... direct: r-ergm -> set()
.......................... check: r-diagrammer
.......................... check: r-adegenet
.......................... check: r-geoquery
.......................... check: r-ggmap
.......................... check: r-gistr
.......................... check: r-janitor
.......................... check: r-ggvis
.......................... check: r-annotationhub
......................... check: r-httr
.......................... direct: r-httr -> {'r-devtools', 'r-rvest', 'r-biomart', 'r-cdcfluview', 'r-seurat', 'r-tigris', 'r-plotly', 'r-covr', 'r-biomartr', 'r-biocfilecache', 'r-geoquery', 'r-ggmap', 'r-gistr', 'r-keggrest', 'r-tidycensus', 'r-tidyverse', 'r-gh', 'r-europepmc', 'r-annotationhub'}
.......................... check: r-devtools
.......................... check: r-rvest
.......................... check: r-biomart
.......................... check: r-cdcfluview
.......................... check: r-seurat
.......................... check: r-tigris
.......................... check: r-plotly
.......................... check: r-covr
........................... direct: r-covr -> set()
.......................... check: r-biomartr
.......................... check: r-biocfilecache
.......................... check: r-geoquery
.......................... check: r-ggmap
.......................... check: r-gistr
.......................... check: r-keggrest
.......................... check: r-tidycensus
.......................... check: r-tidyverse
.......................... check: r-gh
........................... direct: r-gh -> {'r-usethis'}
........................... check: r-usethis
.......................... check: r-europepmc
.......................... check: r-annotationhub
......................... check: r-desc
.......................... direct: r-desc -> {'r-pkgbuild', 'r-pkgload', 'r-rcmdcheck', 'r-usethis', 'r-roxygen2'}
.......................... check: r-pkgbuild
.......................... check: r-pkgload
.......................... check: r-rcmdcheck
.......................... check: r-usethis
.......................... check: r-roxygen2
......................... check: r-scales
.......................... direct: r-scales -> {'r-ggbio', 'r-seurat', 'r-plotly', 'r-phylostratr', 'r-dose', 'r-rnoaa', 'r-biovizbase', 'r-ggridges', 'r-ggraph', 'r-rbokeh', 'r-ggpubr', 'r-ggplot2', 'r-phyloseq', 'r-upsetr', 'r-leaflet', 'r-cowplot', 'r-ggsci', 'r-methylumi', 'r-diagrammer', 'r-msnbase', 'r-ggforce', 'r-ggmap', 'r-ggrepel'}
.......................... check: r-ggbio
.......................... check: r-seurat
.......................... check: r-plotly
.......................... check: r-phylostratr
.......................... check: r-dose
.......................... check: r-rnoaa
.......................... check: r-biovizbase
.......................... check: r-ggridges
........................... direct: r-ggridges -> {'r-seurat', 'r-enrichplot', 'r-ggjoy'}
........................... check: r-seurat
........................... check: r-enrichplot
........................... check: r-ggjoy
............................ direct: r-ggjoy -> {'r-seurat'}
............................ check: r-seurat
.......................... check: r-ggraph
.......................... check: r-rbokeh
.......................... check: r-ggpubr
.......................... check: r-ggplot2
........................... direct: r-ggplot2 -> {'r-mlr', 'r-caret', 'r-qvalue', 'r-ggbio', 'r-seurat', 'r-plotly', 'r-factoextra', 'r-scater', 'r-phylostratr', 'r-dose', 'r-rnoaa', 'r-clusterprofiler', 'r-dendextend', 'r-smoof', 'r-nmf', 'r-ggsignif', 'r-ggally', 'r-deseq2', 'r-affycoretools', 'r-ggridges', 'r-crosstalk', 'r-ggraph', 'r-ggpubr', 'r-rbokeh', 'r-sctransform', 'r-tidyverse', 'r-reportingtools', 'r-rms', 'r-yapsa', 'r-phyloseq', 'r-upsetr', 'r-viridis', 'r-envstats', 'r-vsn', 'r-cowplot', 'r-ggjoy', 'r-ampliqueso', 'r-ggsci', 'r-hmisc', 'r-ggdendro', 'r-ggbeeswarm', 'r-adegenet', 'r-goplot', 'hic-pro', 'r-anaquin', 'r-methylumi', 'r-dada2', 'r-enrichplot', 'r-msnbase', 'r-quantro', 'r-ggforce', 'r-rstan', 'r-ggmap', 'r-forecast', 'r-somaticsignatures', 'r-aneufinder', 'r-ggplotify', 'r-cner', 'r-fgsea', 'r-ggrepel'}
........................... check: r-mlr
............................ direct: r-mlr -> {'r-mlrmbo'}
............................ check: r-mlrmbo
........................... check: r-caret
........................... check: r-qvalue
............................ direct: r-qvalue -> {'r-dose', 'r-anaquin', 'r-clusterprofiler', 'trinity'}
............................ check: r-dose
............................ check: r-anaquin
............................ check: r-clusterprofiler
............................ check: trinity
........................... check: r-ggbio
........................... check: r-seurat
........................... check: r-plotly
........................... check: r-factoextra
........................... check: r-scater
........................... check: r-phylostratr
........................... check: r-dose
........................... check: r-rnoaa
........................... check: r-clusterprofiler
........................... check: r-dendextend
............................ direct: r-dendextend -> {'r-factoextra', 'r-complexheatmap', 'r-yapsa'}
............................ check: r-factoextra
............................ check: r-complexheatmap
............................. direct: r-complexheatmap -> {'r-yapsa'}
............................. check: r-yapsa
............................ check: r-yapsa
........................... check: r-smoof
........................... check: r-nmf
............................ direct: r-nmf -> {'r-seurat', 'r-somaticsignatures'}
............................ check: r-seurat
............................ check: r-somaticsignatures
........................... check: r-ggsignif
............................ direct: r-ggsignif -> {'r-ggpubr'}
............................ check: r-ggpubr
........................... check: r-ggally
........................... check: r-deseq2
........................... check: r-affycoretools
........................... check: r-ggridges
........................... check: r-crosstalk
........................... check: r-ggraph
........................... check: r-ggpubr
........................... check: r-rbokeh
........................... check: r-sctransform
............................ direct: r-sctransform -> {'r-seurat'}
............................ check: r-seurat
........................... check: r-tidyverse
........................... check: r-reportingtools
........................... check: r-rms
............................ direct: r-rms -> {'r-spatialeco'}
............................ check: r-spatialeco
........................... check: r-yapsa
........................... check: r-phyloseq
........................... check: r-upsetr
............................ direct: r-upsetr -> {'r-enrichplot'}
............................ check: r-enrichplot
........................... check: r-viridis
............................ direct: r-viridis -> {'r-dendextend', 'r-leaflet', 'r-ggraph', 'r-scater', 'r-hmisc', 'r-diagrammer'}
............................ check: r-dendextend
............................ check: r-leaflet
............................ check: r-ggraph
............................ check: r-scater
............................ check: r-hmisc
............................. direct: r-hmisc -> {'r-wgcna', 'r-ggbio', 'r-seurat', 'r-deseq2', 'r-rms', 'r-biovizbase'}
............................. check: r-wgcna
............................. check: r-ggbio
............................. check: r-seurat
............................. check: r-deseq2
............................. check: r-rms
............................. check: r-biovizbase
............................ check: r-diagrammer
........................... check: r-envstats
............................ direct: r-envstats -> {'r-spatialeco'}
............................ check: r-spatialeco
........................... check: r-vsn
............................ direct: r-vsn -> {'r-msnbase'}
............................ check: r-msnbase
........................... check: r-cowplot
............................ direct: r-cowplot -> {'r-aneufinder', 'r-seurat', 'r-ggpubr', 'r-enrichplot'}
............................ check: r-aneufinder
............................ check: r-seurat
............................ check: r-ggpubr
............................ check: r-enrichplot
........................... check: r-ggjoy
........................... check: r-ampliqueso
........................... check: r-ggsci
............................ direct: r-ggsci -> {'r-ggpubr'}
............................ check: r-ggpubr
........................... check: r-hmisc
........................... check: r-ggdendro
............................ direct: r-ggdendro -> {'r-aneufinder', 'r-goplot'}
............................ check: r-aneufinder
............................ check: r-goplot
............................. direct: r-goplot -> {'trinity'}
............................. check: trinity
........................... check: r-ggbeeswarm
............................ direct: r-ggbeeswarm -> {'r-scater'}
............................ check: r-scater
........................... check: r-adegenet
........................... check: r-goplot
........................... check: hic-pro
........................... check: r-anaquin
........................... check: r-methylumi
........................... check: r-dada2
........................... check: r-enrichplot
........................... check: r-msnbase
........................... check: r-quantro
........................... check: r-ggforce
............................ direct: r-ggforce -> {'r-ggraph'}
............................ check: r-ggraph
........................... check: r-rstan
........................... check: r-ggmap
........................... check: r-forecast
............................ direct: r-forecast -> {'r-bfast'}
............................ check: r-bfast
............................. direct: r-bfast -> {'r-bfastspatial'}
............................. check: r-bfastspatial
........................... check: r-somaticsignatures
........................... check: r-aneufinder
........................... check: r-ggplotify
............................ direct: r-ggplotify -> {'r-enrichplot'}
............................ check: r-enrichplot
........................... check: r-cner
........................... check: r-fgsea
............................ direct: r-fgsea -> {'r-dose'}
............................ check: r-dose
........................... check: r-ggrepel
............................ direct: r-ggrepel -> {'r-seurat', 'r-aneufinder', 'r-ggraph', 'r-factoextra', 'r-ggpubr'}
............................ check: r-seurat
............................ check: r-aneufinder
............................ check: r-ggraph
............................ check: r-factoextra
............................ check: r-ggpubr
.......................... check: r-phyloseq
.......................... check: r-upsetr
.......................... check: r-leaflet
.......................... check: r-cowplot
.......................... check: r-ggsci
.......................... check: r-methylumi
.......................... check: r-diagrammer
.......................... check: r-msnbase
.......................... check: r-ggforce
.......................... check: r-ggmap
.......................... check: r-ggrepel
........................ check: r-biocneighbors
......................... direct: r-biocneighbors -> {'r-scater'}
......................... check: r-scater
........................ check: r-fastcluster
......................... direct: r-fastcluster -> {'r-wgcna', 'trinity'}
......................... check: r-wgcna
......................... check: trinity
........................ check: r-fpc
......................... direct: r-fpc -> {'r-mlinterfaces', 'r-seurat', 'r-dendextend'}
......................... check: r-mlinterfaces
......................... check: r-seurat
......................... check: r-dendextend
........................ check: r-pmcmr
......................... direct: r-pmcmr -> {'r-yapsa'}
......................... check: r-yapsa
........................ check: r-yarn
........................ check: r-isdparser
......................... direct: r-isdparser -> {'r-rnoaa'}
......................... check: r-rnoaa
........................ check: r-proxy
......................... direct: r-proxy -> {'r-dtw', 'r-somaticsignatures'}
......................... check: r-dtw
.......................... direct: r-dtw -> {'r-seurat'}
.......................... check: r-seurat
......................... check: r-somaticsignatures
........................ check: r-annotationforge
........................ check: r-doparallel
......................... direct: r-doparallel -> {'r-construct', 'r-mzid', 'r-wgcna', 'r-nmf', 'r-quantro', 'r-aneufinder', 'r-blockmodeling', 'r-randomglm', 'r-ampliqueso', 'r-adabag'}
......................... check: r-construct
......................... check: r-mzid
.......................... direct: r-mzid -> {'r-msnbase'}
.......................... check: r-msnbase
......................... check: r-wgcna
......................... check: r-nmf
......................... check: r-quantro
......................... check: r-aneufinder
......................... check: r-blockmodeling
.......................... direct: r-blockmodeling -> set()
......................... check: r-randomglm
.......................... direct: r-randomglm -> set()
......................... check: r-ampliqueso
......................... check: r-adabag
........................ check: r-checkmate
......................... direct: r-checkmate -> {'r-smoof', 'r-mlr', 'r-mlrmbo', 'r-parallelmap', 'r-htmltable', 'r-paramhelpers', 'r-bbmisc', 'r-loo'}
......................... check: r-smoof
......................... check: r-mlr
......................... check: r-mlrmbo
......................... check: r-parallelmap
.......................... direct: r-parallelmap -> {'r-mlr', 'r-mlrmbo'}
.......................... check: r-mlr
.......................... check: r-mlrmbo
......................... check: r-htmltable
.......................... direct: r-htmltable -> {'r-rms', 'r-hmisc'}
.......................... check: r-rms
.......................... check: r-hmisc
......................... check: r-paramhelpers
.......................... direct: r-paramhelpers -> {'r-smoof', 'r-mlr', 'r-mlrmbo'}
.......................... check: r-smoof
.......................... check: r-mlr
.......................... check: r-mlrmbo
......................... check: r-bbmisc
.......................... direct: r-bbmisc -> {'r-smoof', 'r-mlr', 'r-mlrmbo', 'r-parallelmap', 'r-paramhelpers'}
.......................... check: r-smoof
.......................... check: r-mlr
.......................... check: r-mlrmbo
.......................... check: r-parallelmap
.......................... check: r-paramhelpers
......................... check: r-loo
........................ check: r-gtools
......................... direct: r-gtools -> {'r-construct', 'r-phytools', 'r-gofuncr', 'r-abaenrichment', 'r-gdata', 'r-tfbstools', 'r-genetics', 'r-xde', 'r-gplots'}
......................... check: r-construct
......................... check: r-phytools
......................... check: r-gofuncr
......................... check: r-abaenrichment
......................... check: r-gdata
.......................... direct: r-gdata -> {'r-seurat', 'r-gmodels', 'r-mlinterfaces', 'r-genetics', 'r-gplots'}
.......................... check: r-seurat
.......................... check: r-gmodels
........................... direct: r-gmodels -> {'r-spatialreg', 'r-spdep'}
........................... check: r-spatialreg
........................... check: r-spdep
.......................... check: r-mlinterfaces
.......................... check: r-genetics
........................... direct: r-genetics -> {'r-ldheatmap'}
........................... check: r-ldheatmap
............................ direct: r-ldheatmap -> set()
.......................... check: r-gplots
........................... direct: r-gplots -> {'r-yarn', 'r-reordercluster', 'trinity', 'r-abaenrichment', 'r-seurat', 'r-affycoretools', 'r-ampliqueso', 'r-rocr', 'r-a4base'}
........................... check: r-yarn
........................... check: r-reordercluster
............................ direct: r-reordercluster -> {'r-aneufinder'}
............................ check: r-aneufinder
........................... check: trinity
........................... check: r-abaenrichment
........................... check: r-seurat
........................... check: r-affycoretools
........................... check: r-ampliqueso
........................... check: r-rocr
............................ direct: r-rocr -> {'r-a4classif', 'r-anaquin', 'r-seurat'}
............................ check: r-a4classif
............................ check: r-anaquin
............................ check: r-seurat
........................... check: r-a4base
......................... check: r-tfbstools
......................... check: r-genetics
......................... check: r-xde
......................... check: r-gplots
........................ check: r-lazyeval
......................... direct: r-lazyeval -> {'r-rex', 'r-tibble', 'r-plotly', 'r-rbokeh', 'r-crosstalk', 'r-annotationfilter', 'r-modelr', 'r-ggvis', 'r-ggplot2'}
......................... check: r-rex
.......................... direct: r-rex -> {'r-covr'}
.......................... check: r-covr
......................... check: r-tibble
.......................... direct: r-tibble -> {'r-cellranger', 'r-tidyr', 'r-plotly', 'r-biomartr', 'r-readr', 'r-phylostratr', 'r-rnoaa', 'r-isdparser', 'r-dbplyr', 'r-tidygraph', 'r-rio', 'r-tidyverse', 'r-ggplot2', 'r-shinyfiles', 'r-broom', 'r-purrr', 'r-haven', 'r-blob', 'r-modelr', 'r-forcats', 'r-readxl', 'r-ergm', 'r-diagrammer', 'r-rematch2', 'r-dplyr', 'r-ggmap', 'r-network', 'r-recipes'}
.......................... check: r-cellranger
........................... direct: r-cellranger -> {'r-readxl'}
........................... check: r-readxl
.......................... check: r-tidyr
.......................... check: r-plotly
.......................... check: r-biomartr
.......................... check: r-readr
.......................... check: r-phylostratr
.......................... check: r-rnoaa
.......................... check: r-isdparser
.......................... check: r-dbplyr
.......................... check: r-tidygraph
.......................... check: r-rio
.......................... check: r-tidyverse
.......................... check: r-ggplot2
.......................... check: r-shinyfiles
.......................... check: r-broom
.......................... check: r-purrr
........................... direct: r-purrr -> {'r-tidyr', 'r-plotly', 'r-biomartr', 'r-imager', 'r-tidycensus', 'r-phylostratr', 'r-europepmc', 'r-dbplyr', 'r-ggpubr', 'r-tidyverse', 'r-usethis', 'r-broom', 'r-cdcfluview', 'r-modelr', 'r-roxygen2', 'r-ergm', 'r-diagrammer', 'r-enrichplot', 'r-ggmap', 'r-janitor', 'r-tidyselect', 'r-recipes'}
........................... check: r-tidyr
........................... check: r-plotly
........................... check: r-biomartr
........................... check: r-imager
........................... check: r-tidycensus
........................... check: r-phylostratr
........................... check: r-europepmc
........................... check: r-dbplyr
........................... check: r-ggpubr
........................... check: r-tidyverse
........................... check: r-usethis
........................... check: r-broom
........................... check: r-cdcfluview
........................... check: r-modelr
........................... check: r-roxygen2
........................... check: r-ergm
........................... check: r-diagrammer
........................... check: r-enrichplot
........................... check: r-ggmap
........................... check: r-janitor
........................... check: r-tidyselect
............................ direct: r-tidyselect -> {'r-dplyr', 'r-dbplyr', 'r-tidyr', 'r-ggforce', 'r-recipes'}
............................ check: r-dplyr
............................ check: r-dbplyr
............................ check: r-tidyr
............................ check: r-ggforce
............................ check: r-recipes
........................... check: r-recipes
.......................... check: r-haven
.......................... check: r-blob
........................... direct: r-blob -> {'r-rsqlite'}
........................... check: r-rsqlite
............................ direct: r-rsqlite -> {'r-annotationdbi', 'r-ensembldb', 'r-lumi', 'r-genomicfeatures', 'r-annotationforge', 'r-decipher', 'r-biocfilecache', 'r-affycoretools', 'r-sqldf', 'r-tfbstools', 'r-cner', 'r-taxizedb', 'r-oligoclasses', 'r-annotationhub'}
............................ check: r-annotationdbi
............................ check: r-ensembldb
............................ check: r-lumi
............................ check: r-genomicfeatures
............................ check: r-annotationforge
............................ check: r-decipher
............................ check: r-biocfilecache
............................ check: r-affycoretools
............................ check: r-sqldf
............................. direct: r-sqldf -> set()
............................ check: r-tfbstools
............................ check: r-cner
............................ check: r-taxizedb
............................ check: r-oligoclasses
............................ check: r-annotationhub
.......................... check: r-modelr
.......................... check: r-forcats
........................... direct: r-forcats -> {'r-haven', 'r-tidyverse'}
........................... check: r-haven
........................... check: r-tidyverse
.......................... check: r-readxl
.......................... check: r-ergm
.......................... check: r-diagrammer
.......................... check: r-rematch2
........................... direct: r-rematch2 -> set()
.......................... check: r-dplyr
.......................... check: r-ggmap
.......................... check: r-network
........................... direct: r-network -> {'r-ergm'}
........................... check: r-ergm
.......................... check: r-recipes
......................... check: r-plotly
......................... check: r-rbokeh
......................... check: r-crosstalk
......................... check: r-annotationfilter
......................... check: r-modelr
......................... check: r-ggvis
......................... check: r-ggplot2
........................ check: r-rcurl
......................... direct: r-rcurl -> {'r-kegggraph', 'r-genomicfeatures', 'r-paleotree', 'r-annotate', 'r-biomart', 'r-rtracklayer', 'r-annotationforge', 'r-geoquery', 'r-biomartr', 'r-spatialeco', 'r-genomeinfodb', 'r-affycompatible'}
......................... check: r-kegggraph
.......................... direct: r-kegggraph -> {'r-pathview'}
.......................... check: r-pathview
......................... check: r-genomicfeatures
......................... check: r-paleotree
......................... check: r-annotate
......................... check: r-biomart
......................... check: r-rtracklayer
......................... check: r-annotationforge
......................... check: r-geoquery
......................... check: r-biomartr
......................... check: r-spatialeco
......................... check: r-genomeinfodb
......................... check: r-affycompatible
........................ check: r-rbgl
......................... direct: r-rbgl -> {'r-organismdbi', 'r-gostats', 'r-alpine', 'r-category', 'r-grbase'}
......................... check: r-organismdbi
......................... check: r-gostats
......................... check: r-alpine
......................... check: r-category
......................... check: r-grbase
.......................... direct: r-grbase -> set()
........................ check: r-reportingtools
........................ check: r-minqa
......................... direct: r-minqa -> {'r-lme4', 'r-survey'}
......................... check: r-lme4
......................... check: r-survey
.......................... direct: r-survey -> set()
........................ check: r-zoo
......................... direct: r-zoo -> {'r-xts', 'r-sandwich', 'r-dygraphs', 'r-gstat', 'r-ttr', 'r-strucchange', 'r-forecast', 'r-lmtest', 'r-spacetime', 'r-tseries', 'r-aer', 'r-party', 'r-tmixclust', 'r-vioplot', 'r-bfastspatial', 'r-quantmod', 'r-bfast', 'r-nanotime'}
......................... check: r-xts
.......................... direct: r-xts -> {'r-ttr', 'r-quantmod', 'r-dygraphs', 'r-spacetime'}
.......................... check: r-ttr
........................... direct: r-ttr -> {'r-quantmod'}
........................... check: r-quantmod
............................ direct: r-quantmod -> {'r-tseries'}
............................ check: r-tseries
............................. direct: r-tseries -> {'r-forecast'}
............................. check: r-forecast
.......................... check: r-quantmod
.......................... check: r-dygraphs
.......................... check: r-spacetime
........................... direct: r-spacetime -> {'r-gwmodel', 'r-gstat'}
........................... check: r-gwmodel
........................... check: r-gstat
............................ direct: r-gstat -> set()
......................... check: r-sandwich
.......................... direct: r-sandwich -> {'r-multcomp', 'r-lfe', 'r-strucchange', 'r-aer', 'r-party'}
.......................... check: r-multcomp
........................... direct: r-multcomp -> {'r-rms', 'r-coin'}
........................... check: r-rms
........................... check: r-coin
............................ direct: r-coin -> {'r-party'}
............................ check: r-party
............................. direct: r-party -> {'r-rminer'}
............................. check: r-rminer
.......................... check: r-lfe
........................... direct: r-lfe -> set()
.......................... check: r-strucchange
........................... direct: r-strucchange -> {'r-party', 'r-bfast'}
........................... check: r-party
........................... check: r-bfast
.......................... check: r-aer
.......................... check: r-party
......................... check: r-dygraphs
......................... check: r-gstat
......................... check: r-ttr
......................... check: r-strucchange
......................... check: r-forecast
......................... check: r-lmtest
.......................... direct: r-lmtest -> {'r-seurat', 'r-aer', 'r-vcd', 'r-forecast'}
.......................... check: r-seurat
.......................... check: r-aer
.......................... check: r-vcd
........................... direct: r-vcd -> set()
.......................... check: r-forecast
......................... check: r-spacetime
......................... check: r-tseries
......................... check: r-aer
......................... check: r-party
......................... check: r-tmixclust
.......................... direct: r-tmixclust -> set()
......................... check: r-vioplot
.......................... direct: r-vioplot -> {'r-gofuncr'}
.......................... check: r-gofuncr
......................... check: r-bfastspatial
......................... check: r-quantmod
......................... check: r-bfast
......................... check: r-nanotime
.......................... direct: r-nanotime -> set()
........................ check: r-ggplot2
........................ check: r-influencer
......................... direct: r-influencer -> {'r-diagrammer'}
......................... check: r-diagrammer
........................ check: r-later
......................... direct: r-later -> {'r-promises', 'r-httpuv', 'r-shiny'}
......................... check: r-promises
......................... check: r-httpuv
......................... check: r-shiny
........................ check: r-gensa
......................... direct: r-gensa -> {'r-corhmm'}
......................... check: r-corhmm
........................ check: r-splitstackshape
......................... direct: r-splitstackshape -> set()
........................ check: r-rex
........................ check: r-factominer
........................ check: r-rcpparmadillo
......................... direct: r-rcpparmadillo -> {'r-smoof', 'r-graphlayouts', 'r-gwmodel', 'r-deseq2', 'r-forecast', 'r-bayesm', 'r-grbase'}
......................... check: r-smoof
......................... check: r-graphlayouts
......................... check: r-gwmodel
......................... check: r-deseq2
......................... check: r-forecast
......................... check: r-bayesm
.......................... direct: r-bayesm -> {'r-compositions'}
.......................... check: r-compositions
........................... direct: r-compositions -> {'r-seurat'}
........................... check: r-seurat
......................... check: r-grbase
........................ check: r-iso
......................... direct: r-iso -> {'r-als'}
......................... check: r-als
.......................... direct: r-als -> {'r-alsace'}
.......................... check: r-alsace
........................ check: r-rsamtools
........................ check: r-strucchange
........................ check: r-gbm
......................... direct: r-gbm -> {'r-mlinterfaces'}
......................... check: r-mlinterfaces
........................ check: r-progress
........................ check: r-lars
......................... direct: r-lars -> {'r-seurat', 'r-chemometrics'}
......................... check: r-seurat
......................... check: r-chemometrics
.......................... direct: r-chemometrics -> set()
........................ check: r-ps
......................... direct: r-ps -> {'r-processx'}
......................... check: r-processx
........................ check: r-readbitmap
......................... direct: r-readbitmap -> {'r-imager'}
......................... check: r-imager
........................ check: r-th-data
......................... direct: r-th-data -> {'r-multcomp'}
......................... check: r-multcomp
........................ check: r-corhmm
........................ check: r-earth
......................... direct: r-earth -> {'r-condop'}
......................... check: r-condop
........................ check: r-goseq
........................ check: r-ordinal
......................... direct: r-ordinal -> {'r-jomo'}
......................... check: r-jomo
........................ check: r-base64enc
......................... direct: r-base64enc -> {'r-manipulatewidget', 'r-rmarkdown', 'r-threejs', 'r-plotly', 'r-leaflet', 'r-hmisc', 'r-repr'}
......................... check: r-manipulatewidget
......................... check: r-rmarkdown
......................... check: r-threejs
......................... check: r-plotly
......................... check: r-leaflet
......................... check: r-hmisc
......................... check: r-repr
.......................... direct: r-repr -> {'r-irdisplay', 'r-irkernel'}
.......................... check: r-irdisplay
........................... direct: r-irdisplay -> {'r-irkernel'}
........................... check: r-irkernel
.......................... check: r-irkernel
........................ check: r-aims
......................... direct: r-aims -> set()
........................ check: r-pspline
......................... direct: r-pspline -> {'r-copula'}
......................... check: r-copula
.......................... direct: r-copula -> set()
........................ check: r-plot3d
......................... direct: r-plot3d -> {'r-smoof'}
......................... check: r-smoof
........................ check: r-gridextra
......................... direct: r-gridextra -> {'r-enrichplot', 'r-upsetr', 'r-ggbio', 'r-rstan', 'r-seurat', 'r-viridis', 'r-sctransform', 'r-gbm', 'r-allelicimbalance', 'r-ggpubr', 'r-hmisc', 'r-phylostratr', 'r-rnoaa', 'r-fgsea', 'r-yapsa', 'r-goplot'}
......................... check: r-enrichplot
......................... check: r-upsetr
......................... check: r-ggbio
......................... check: r-rstan
......................... check: r-seurat
......................... check: r-viridis
......................... check: r-sctransform
......................... check: r-gbm
......................... check: r-allelicimbalance
......................... check: r-ggpubr
......................... check: r-hmisc
......................... check: r-phylostratr
......................... check: r-rnoaa
......................... check: r-fgsea
......................... check: r-yapsa
......................... check: r-goplot
........................ check: r-illuminahumanmethylation450kanno-ilmn12-hg19
........................ check: r-allelicimbalance
........................ check: r-vipor
......................... direct: r-vipor -> {'r-ggbeeswarm'}
......................... check: r-ggbeeswarm
........................ check: r-beeswarm
......................... direct: r-beeswarm -> {'r-ggbeeswarm'}
......................... check: r-ggbeeswarm
........................ check: r-gh
........................ check: r-magick
........................ check: r-polynom
......................... direct: r-polynom -> {'r-ggpubr'}
......................... check: r-ggpubr
........................ check: r-limma
......................... direct: r-limma -> {'r-yarn', 'r-affyexpress', 'r-minfi', 'r-bumphunter', 'r-geoquery', 'r-affycoretools', 'r-agimicrorna', 'r-sva', 'r-scater', 'r-vsn', 'r-reportingtools', 'r-edger', 'r-absseq', 'r-watermelon', 'r-a4base'}
......................... check: r-yarn
......................... check: r-affyexpress
.......................... direct: r-affyexpress -> set()
......................... check: r-minfi
......................... check: r-bumphunter
......................... check: r-geoquery
......................... check: r-affycoretools
......................... check: r-agimicrorna
......................... check: r-sva
......................... check: r-scater
......................... check: r-vsn
......................... check: r-reportingtools
......................... check: r-edger
.......................... direct: r-edger -> {'r-yarn', 'trinity', 'r-affycoretools', 'homer', 'r-scater', 'r-glimma', 'r-reportingtools', 'r-rnaseqmap', 'r-ampliqueso'}
.......................... check: r-yarn
.......................... check: trinity
.......................... check: r-affycoretools
.......................... check: homer
.......................... check: r-scater
.......................... check: r-glimma
.......................... check: r-reportingtools
.......................... check: r-rnaseqmap
.......................... check: r-ampliqueso
......................... check: r-absseq
.......................... direct: r-absseq -> set()
......................... check: r-watermelon
......................... check: r-a4base
........................ check: r-pbkrtest
........................ check: r-hexbin
......................... direct: r-hexbin -> {'r-rbokeh', 'r-plotly', 'r-vsn'}
......................... check: r-rbokeh
......................... check: r-plotly
......................... check: r-vsn
........................ check: r-summarizedexperiment
........................ check: r-foreach
......................... direct: r-foreach -> {'r-construct', 'r-caret', 'r-seurat', 'r-glmnet', 'r-gdalutils', 'r-adabag', 'r-dorng', 'r-nmf', 'r-doparallel', 'r-blockmodeling', 'r-phyloseq', 'r-bumphunter', 'r-ampliqueso', 'r-mzid', 'r-wgcna', 'r-domc', 'r-quantro', 'r-aneufinder', 'r-dosnow', 'r-randomglm', 'r-oligoclasses'}
......................... check: r-construct
......................... check: r-caret
......................... check: r-seurat
......................... check: r-glmnet
.......................... direct: r-glmnet -> {'r-a4core', 'r-a4classif', 'r-rminer', 'r-a4base'}
.......................... check: r-a4core
........................... direct: r-a4core -> {'r-a4base', 'r-a4classif', 'r-a4'}
........................... check: r-a4base
........................... check: r-a4classif
........................... check: r-a4
.......................... check: r-a4classif
.......................... check: r-rminer
.......................... check: r-a4base
......................... check: r-gdalutils
......................... check: r-adabag
......................... check: r-dorng
.......................... direct: r-dorng -> {'r-blockmodeling', 'r-bumphunter'}
.......................... check: r-blockmodeling
.......................... check: r-bumphunter
......................... check: r-nmf
......................... check: r-doparallel
......................... check: r-blockmodeling
......................... check: r-phyloseq
......................... check: r-bumphunter
......................... check: r-ampliqueso
......................... check: r-mzid
......................... check: r-wgcna
......................... check: r-domc
.......................... direct: r-domc -> set()
......................... check: r-quantro
......................... check: r-aneufinder
......................... check: r-dosnow
.......................... direct: r-dosnow -> {'r-seurat'}
.......................... check: r-seurat
......................... check: r-randomglm
......................... check: r-oligoclasses
........................ check: r-jomo
........................ check: r-factoextra
........................ check: r-amap
......................... direct: r-amap -> {'r-ctc'}
......................... check: r-ctc
........................ check: r-vioplot
........................ check: r-mixtools
......................... direct: r-mixtools -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-ncbit
......................... direct: r-ncbit -> {'r-geiger'}
......................... check: r-geiger
.......................... direct: r-geiger -> {'r-convevol', 'r-geomorph'}
.......................... check: r-convevol
.......................... check: r-geomorph
........................ check: r-selectr
........................ check: r-rgexf
......................... direct: r-rgexf -> {'r-diagrammer'}
......................... check: r-diagrammer
........................ check: r-sessioninfo
......................... direct: r-sessioninfo -> {'r-rcmdcheck', 'r-devtools'}
......................... check: r-rcmdcheck
......................... check: r-devtools
........................ check: r-plyr
......................... direct: r-plyr -> {'r-tarifx', 'r-caret', 'r-imager', 'r-scater', 'r-biomformat', 'r-clusterprofiler', 'r-europepmc', 'r-ggally', 'r-ggridges', 'r-ggpubr', 'r-ggplot2', 'r-phyloseq', 'r-broom', 'r-upsetr', 'r-condop', 'r-cowplot', 'r-mzid', 'r-anaquin', 'r-reshape2', 'r-msnbase', 'r-ggmap', 'r-scales', 'r-reshape'}
......................... check: r-tarifx
.......................... direct: r-tarifx -> set()
......................... check: r-caret
......................... check: r-imager
......................... check: r-scater
......................... check: r-biomformat
.......................... direct: r-biomformat -> {'r-phyloseq'}
.......................... check: r-phyloseq
......................... check: r-clusterprofiler
......................... check: r-europepmc
......................... check: r-ggally
......................... check: r-ggridges
......................... check: r-ggpubr
......................... check: r-ggplot2
......................... check: r-phyloseq
......................... check: r-broom
......................... check: r-upsetr
......................... check: r-condop
......................... check: r-cowplot
......................... check: r-mzid
......................... check: r-anaquin
......................... check: r-reshape2
.......................... direct: r-reshape2 -> {'r-tarifx', 'r-caret', 'r-qvalue', 'r-ggbio', 'r-seurat', 'r-factoextra', 'r-scater', 'r-phylostratr', 'r-dose', 'r-nmf', 'r-sctransform', 'r-ggplot2', 'r-yapsa', 'r-phyloseq', 'r-broom', 'r-genie3', 'r-methylumi', 'r-adegenet', 'r-dada2', 'r-enrichplot', 'r-ggmap', 'r-somaticsignatures', 'r-cubist', 'r-aneufinder', 'r-cner'}
.......................... check: r-tarifx
.......................... check: r-caret
.......................... check: r-qvalue
.......................... check: r-ggbio
.......................... check: r-seurat
.......................... check: r-factoextra
.......................... check: r-scater
.......................... check: r-phylostratr
.......................... check: r-dose
.......................... check: r-nmf
.......................... check: r-sctransform
.......................... check: r-ggplot2
.......................... check: r-yapsa
.......................... check: r-phyloseq
.......................... check: r-broom
.......................... check: r-genie3
........................... direct: r-genie3 -> set()
.......................... check: r-methylumi
.......................... check: r-adegenet
.......................... check: r-dada2
.......................... check: r-enrichplot
.......................... check: r-ggmap
.......................... check: r-somaticsignatures
.......................... check: r-cubist
........................... direct: r-cubist -> {'r-rminer', 'r-c50'}
........................... check: r-rminer
........................... check: r-c50
............................ direct: r-c50 -> set()
.......................... check: r-aneufinder
.......................... check: r-cner
......................... check: r-msnbase
......................... check: r-ggmap
......................... check: r-scales
......................... check: r-reshape
.......................... direct: r-reshape -> {'r-minfi', 'r-ggally'}
.......................... check: r-minfi
.......................... check: r-ggally
........................ check: r-affyio
......................... direct: r-affyio -> {'r-affy', 'r-oligoclasses', 'r-gcrma', 'r-makecdfenv'}
......................... check: r-affy
.......................... direct: r-affy -> {'r-affycontam', 'r-lumi', 'r-affyexpress', 'r-msnbase', 'r-altcdfenvs', 'r-affyrnadegradation', 'r-affyilm', 'r-gcrma', 'r-affycoretools', 'r-makecdfenv', 'r-affyqcreport', 'r-agimicrorna', 'r-simpleaffy', 'r-vsn', 'r-affyplm', 'r-affydata', 'r-affypdnn'}
.......................... check: r-affycontam
........................... direct: r-affycontam -> set()
.......................... check: r-lumi
.......................... check: r-affyexpress
.......................... check: r-msnbase
.......................... check: r-altcdfenvs
.......................... check: r-affyrnadegradation
........................... direct: r-affyrnadegradation -> set()
.......................... check: r-affyilm
.......................... check: r-gcrma
.......................... check: r-affycoretools
.......................... check: r-makecdfenv
........................... direct: r-makecdfenv -> {'r-altcdfenvs'}
........................... check: r-altcdfenvs
.......................... check: r-affyqcreport
.......................... check: r-agimicrorna
.......................... check: r-simpleaffy
.......................... check: r-vsn
.......................... check: r-affyplm
.......................... check: r-affydata
........................... direct: r-affydata -> {'r-affycontam'}
........................... check: r-affycontam
.......................... check: r-affypdnn
........................... direct: r-affypdnn -> set()
......................... check: r-oligoclasses
......................... check: r-gcrma
......................... check: r-makecdfenv
........................ check: r-crosstalk
........................ check: r-trimcluster
......................... direct: r-trimcluster -> {'r-fpc'}
......................... check: r-fpc
........................ check: r-rgeos
......................... direct: r-rgeos -> {'r-spatialeco', 'r-tigris'}
......................... check: r-spatialeco
......................... check: r-tigris
........................ check: r-glue
......................... direct: r-glue -> {'r-dplyr', 'r-dbplyr', 'r-stringr', 'r-tidyr', 'r-vctrs', 'r-ggmap', 'r-ggpubr', 'r-usethis', 'r-phylostratr', 'r-tidyselect', 'r-recipes', 'r-cli', 'r-diagrammer'}
......................... check: r-dplyr
......................... check: r-dbplyr
......................... check: r-stringr
.......................... direct: r-stringr -> {'r-bibtex', 'r-tigris', 'r-seurat', 'r-alpine', 'r-biomartr', 'r-imager', 'r-lubridate', 'r-tidycensus', 'r-snakecase', 'r-knitr', 'r-rmarkdown', 'r-selectr', 'r-nmf', 'r-pryr', 'r-biomart', 'r-tidyverse', 'r-bfastspatial', 'r-rngtools', 'r-broom', 'r-roxygen2', 'r-diagrammer', 'r-reshape2', 'r-ggmap', 'r-htmltable', 'r-xgboost', 'r-evaluate', 'r-pkgmaker'}
.......................... check: r-bibtex
........................... direct: r-bibtex -> {'r-rdpack', 'r-pkgmaker'}
........................... check: r-rdpack
............................ direct: r-rdpack -> {'r-metap'}
............................ check: r-metap
............................. direct: r-metap -> {'r-seurat'}
............................. check: r-seurat
........................... check: r-pkgmaker
............................ direct: r-pkgmaker -> {'r-dorng', 'r-nmf', 'r-rngtools'}
............................ check: r-dorng
............................ check: r-nmf
............................ check: r-rngtools
............................. direct: r-rngtools -> {'r-dorng', 'r-nmf'}
............................. check: r-dorng
............................. check: r-nmf
.......................... check: r-tigris
.......................... check: r-seurat
.......................... check: r-alpine
.......................... check: r-biomartr
.......................... check: r-imager
.......................... check: r-lubridate
........................... direct: r-lubridate -> {'r-recipes', 'r-tidyverse', 'r-rnoaa'}
........................... check: r-recipes
........................... check: r-tidyverse
........................... check: r-rnoaa
.......................... check: r-tidycensus
.......................... check: r-snakecase
........................... direct: r-snakecase -> {'r-janitor'}
........................... check: r-janitor
.......................... check: r-knitr
........................... direct: r-knitr -> {'r-biocstyle', 'r-anaquin', 'r-manipulatewidget', 'r-rmarkdown', 'r-roxygen2', 'r-rgl', 'r-gistr', 'r-htmltable', 'r-reprex', 'r-ampliqueso', 'r-roc', 'r-reportingtools', 'r-bookdown'}
........................... check: r-biocstyle
........................... check: r-anaquin
........................... check: r-manipulatewidget
........................... check: r-rmarkdown
........................... check: r-roxygen2
........................... check: r-rgl
........................... check: r-gistr
........................... check: r-htmltable
........................... check: r-reprex
........................... check: r-ampliqueso
........................... check: r-roc
............................ direct: r-roc -> {'r-watermelon'}
............................ check: r-watermelon
........................... check: r-reportingtools
........................... check: r-bookdown
.......................... check: r-rmarkdown
.......................... check: r-selectr
.......................... check: r-nmf
.......................... check: r-pryr
........................... direct: r-pryr -> {'r-rbokeh'}
........................... check: r-rbokeh
.......................... check: r-biomart
.......................... check: r-tidyverse
.......................... check: r-bfastspatial
.......................... check: r-rngtools
.......................... check: r-broom
.......................... check: r-roxygen2
.......................... check: r-diagrammer
.......................... check: r-reshape2
.......................... check: r-ggmap
.......................... check: r-htmltable
.......................... check: r-xgboost
........................... direct: r-xgboost -> {'r-rminer'}
........................... check: r-rminer
.......................... check: r-evaluate
........................... direct: r-evaluate -> {'r-rmarkdown', 'r-irkernel', 'r-knitr', 'r-testthat'}
........................... check: r-rmarkdown
........................... check: r-irkernel
........................... check: r-knitr
........................... check: r-testthat
.......................... check: r-pkgmaker
......................... check: r-tidyr
......................... check: r-vctrs
.......................... direct: r-vctrs -> {'r-blob', 'r-hms', 'r-pillar'}
.......................... check: r-blob
.......................... check: r-hms
........................... direct: r-hms -> {'r-rmariadb', 'r-haven', 'r-readr', 'r-tidyverse', 'r-progress'}
........................... check: r-rmariadb
........................... check: r-haven
........................... check: r-readr
........................... check: r-tidyverse
........................... check: r-progress
.......................... check: r-pillar
........................... direct: r-pillar -> {'r-tibble', 'r-repr', 'r-tidygraph'}
........................... check: r-tibble
........................... check: r-repr
........................... check: r-tidygraph
......................... check: r-ggmap
......................... check: r-ggpubr
......................... check: r-usethis
......................... check: r-phylostratr
......................... check: r-tidyselect
......................... check: r-recipes
......................... check: r-cli
.......................... direct: r-cli -> {'r-pkgbuild', 'r-devtools', 'r-tibble', 'r-sessioninfo', 'r-rcmdcheck', 'r-tidyverse', 'r-testthat', 'r-pillar'}
.......................... check: r-pkgbuild
.......................... check: r-devtools
.......................... check: r-tibble
.......................... check: r-sessioninfo
.......................... check: r-rcmdcheck
.......................... check: r-tidyverse
.......................... check: r-testthat
.......................... check: r-pillar
......................... check: r-diagrammer
........................ check: r-category
........................ check: r-sp
......................... direct: r-sp -> {'r-geosphere', 'r-raster', 'r-gstat', 'r-rgdal', 'r-tigris', 'r-gwmodel', 'r-spdep', 'r-randomfields', 'r-spacetime', 'r-spatialeco', 'r-leaflet', 'r-splancs', 'r-maptools', 'r-dismo', 'r-rgeos', 'r-gdalutils', 'r-bfastspatial', 'r-geor', 'r-bfast'}
......................... check: r-geosphere
.......................... direct: r-geosphere -> {'r-ggmap'}
.......................... check: r-ggmap
......................... check: r-raster
.......................... direct: r-raster -> {'r-exactextractr', 'r-spatialeco', 'r-leaflet', 'r-dismo', 'r-gdalutils', 'r-bfastspatial', 'r-bfast'}
.......................... check: r-exactextractr
.......................... check: r-spatialeco
.......................... check: r-leaflet
.......................... check: r-dismo
........................... direct: r-dismo -> set()
.......................... check: r-gdalutils
.......................... check: r-bfastspatial
.......................... check: r-bfast
......................... check: r-gstat
......................... check: r-rgdal
......................... check: r-tigris
......................... check: r-gwmodel
......................... check: r-spdep
......................... check: r-randomfields
.......................... direct: r-randomfields -> {'r-geor'}
.......................... check: r-geor
........................... direct: r-geor -> set()
......................... check: r-spacetime
......................... check: r-spatialeco
......................... check: r-leaflet
......................... check: r-splancs
.......................... direct: r-splancs -> {'r-geor'}
.......................... check: r-geor
......................... check: r-maptools
.......................... direct: r-maptools -> {'r-spatialeco', 'r-tigris', 'r-car', 'r-gwmodel'}
.......................... check: r-spatialeco
.......................... check: r-tigris
.......................... check: r-car
.......................... check: r-gwmodel
......................... check: r-dismo
......................... check: r-rgeos
......................... check: r-gdalutils
......................... check: r-bfastspatial
......................... check: r-geor
......................... check: r-bfast
........................ check: r-sandwich
........................ check: r-rcppprogress
......................... direct: r-rcppprogress -> {'r-uwot', 'r-seurat'}
......................... check: r-uwot
.......................... direct: r-uwot -> {'r-seurat'}
.......................... check: r-seurat
......................... check: r-seurat
........................ check: r-phyloseq
........................ check: r-caracas
......................... direct: r-caracas -> set()
........................ check: r-squarem
......................... direct: r-squarem -> {'r-lava'}
......................... check: r-lava
.......................... direct: r-lava -> {'r-prodlim'}
.......................... check: r-prodlim
........................... direct: r-prodlim -> {'r-ipred'}
........................... check: r-ipred
............................ direct: r-ipred -> {'r-recipes'}
............................ check: r-recipes
........................ check: r-threejs
........................ check: r-yaqcaffy
........................ check: r-gstat
........................ check: r-downloader
......................... direct: r-downloader -> {'r-yarn', 'r-imager', 'r-biomartr', 'r-diagrammer'}
......................... check: r-yarn
......................... check: r-imager
......................... check: r-biomartr
......................... check: r-diagrammer
........................ check: r-cdcfluview
........................ check: r-genefilter
........................ check: r-promises
........................ check: r-gwmodel
........................ check: r-modeltools
......................... direct: r-modeltools -> {'r-flexmix', 'r-coin', 'r-party', 'r-flexclust'}
......................... check: r-flexmix
.......................... direct: r-flexmix -> {'r-fpc'}
.......................... check: r-fpc
......................... check: r-coin
......................... check: r-party
......................... check: r-flexclust
.......................... direct: r-flexclust -> {'r-tmixclust'}
.......................... check: r-tmixclust
........................ check: r-purrr
........................ check: r-ipred
........................ check: r-crayon
......................... direct: r-crayon -> {'r-pkgbuild', 'r-shiny', 'r-tibble', 'r-irkernel', 'r-covr', 'r-processx', 'r-rcmdcheck', 'r-readr', 'r-usethis', 'r-progress', 'r-desc', 'r-tidyverse', 'r-testthat', 'r-debugme', 'r-cli', 'r-pillar'}
......................... check: r-pkgbuild
......................... check: r-shiny
......................... check: r-tibble
......................... check: r-irkernel
......................... check: r-covr
......................... check: r-processx
......................... check: r-rcmdcheck
......................... check: r-readr
......................... check: r-usethis
......................... check: r-progress
......................... check: r-desc
......................... check: r-tidyverse
......................... check: r-testthat
......................... check: r-debugme
.......................... direct: r-debugme -> {'r-processx'}
.......................... check: r-processx
......................... check: r-cli
......................... check: r-pillar
........................ check: r-bit
......................... direct: r-bit -> {'r-ff', 'r-bit64'}
......................... check: r-ff
.......................... direct: r-ff -> {'r-popgenome', 'r-oligoclasses'}
.......................... check: r-popgenome
........................... direct: r-popgenome -> set()
.......................... check: r-oligoclasses
......................... check: r-bit64
.......................... direct: r-bit64 -> {'r-rmariadb', 'r-hdf5r', 'r-rsqlite', 'r-nanotime'}
.......................... check: r-rmariadb
.......................... check: r-hdf5r
.......................... check: r-rsqlite
.......................... check: r-nanotime
........................ check: r-pkgconfig
......................... direct: r-pkgconfig -> {'r-dplyr', 'r-tibble', 'r-rsqlite', 'r-hms', 'r-igraph'}
......................... check: r-dplyr
......................... check: r-tibble
......................... check: r-rsqlite
......................... check: r-hms
......................... check: r-igraph
.......................... direct: r-igraph -> {'r-adegenet', 'r-nimble', 'r-phyloseq', 'r-threejs', 'r-enrichplot', 'r-graphlayouts', 'r-rgexf', 'r-seurat', 'r-tidygraph', 'r-kknn', 'r-imager', 'r-leiden', 'r-ggraph', 'r-networkd3', 'r-phangorn', 'r-dose', 'r-diffusionmap', 'r-grbase', 'r-influencer', 'r-diagrammer'}
.......................... check: r-adegenet
.......................... check: r-nimble
.......................... check: r-phyloseq
.......................... check: r-threejs
.......................... check: r-enrichplot
.......................... check: r-graphlayouts
.......................... check: r-rgexf
.......................... check: r-seurat
.......................... check: r-tidygraph
.......................... check: r-kknn
........................... direct: r-kknn -> {'r-rminer'}
........................... check: r-rminer
.......................... check: r-imager
.......................... check: r-leiden
........................... direct: r-leiden -> {'r-seurat'}
........................... check: r-seurat
.......................... check: r-ggraph
.......................... check: r-networkd3
........................... direct: r-networkd3 -> set()
.......................... check: r-phangorn
........................... direct: r-phangorn -> {'r-corhmm', 'r-phytools', 'snphylo', 'r-paleotree'}
........................... check: r-corhmm
........................... check: r-phytools
........................... check: snphylo
............................ direct: snphylo -> set()
........................... check: r-paleotree
.......................... check: r-dose
.......................... check: r-diffusionmap
........................... direct: r-diffusionmap -> {'r-seurat'}
........................... check: r-seurat
.......................... check: r-grbase
.......................... check: r-influencer
.......................... check: r-diagrammer
........................ check: r-bit64
........................ check: r-openxlsx
......................... direct: r-openxlsx -> {'r-rio', 'r-samr'}
......................... check: r-rio
......................... check: r-samr
........................ check: r-httpuv
........................ check: r-dplyr
........................ check: r-git2r
........................ check: r-mgcv
......................... direct: r-mgcv -> {'r-goseq', 'r-lumi', 'r-spatialeco', 'r-vegan', 'r-car', 'r-sva', 'r-spatstat', 'r-ks', 'r-ggplot2'}
......................... check: r-goseq
......................... check: r-lumi
......................... check: r-spatialeco
......................... check: r-vegan
.......................... direct: r-vegan -> {'r-adegenet', 'r-phyloseq', 'r-picante'}
.......................... check: r-adegenet
.......................... check: r-phyloseq
.......................... check: r-picante
........................... direct: r-picante -> set()
......................... check: r-car
......................... check: r-sva
......................... check: r-spatstat
.......................... direct: r-spatstat -> {'r-spatialeco'}
.......................... check: r-spatialeco
......................... check: r-ks
.......................... direct: r-ks -> set()
......................... check: r-ggplot2
........................ check: r-tclust
......................... direct: r-tclust -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-hypergraph
......................... direct: r-hypergraph -> {'r-altcdfenvs'}
......................... check: r-altcdfenvs
........................ check: r-janitor
........................ check: r-xde
........................ check: r-rcppparallel
......................... direct: r-rcppparallel -> {'r-uwot', 'r-dada2'}
......................... check: r-uwot
......................... check: r-dada2
........................ check: r-fit-models
......................... direct: r-fit-models -> {'r-robust'}
......................... check: r-robust
.......................... direct: r-robust -> {'r-wgcna'}
.......................... check: r-wgcna
........................ check: r-genomicalignments
........................ check: py-rseqc
........................ check: r-fgsea
........................ check: r-futile-options
......................... direct: r-futile-options -> {'r-futile-logger'}
......................... check: r-futile-logger
.......................... direct: r-futile-logger -> {'r-biocparallel'}
.......................... check: r-biocparallel
........................... direct: r-biocparallel -> {'r-aldex2', 'r-msnbase', 'r-minfi', 'r-delayedarray', 'r-rsamtools', 'r-delayedmatrixstats', 'r-deseq2', 'r-shortread', 'homer', 'r-scater', 'r-biocsingular', 'r-sva', 'r-tfbstools', 'r-dose', 'r-tmixclust', 'r-genomicalignments', 'r-fgsea', 'r-biocneighbors'}
........................... check: r-aldex2
........................... check: r-msnbase
........................... check: r-minfi
........................... check: r-delayedarray
........................... check: r-rsamtools
........................... check: r-delayedmatrixstats
........................... check: r-deseq2
........................... check: r-shortread
........................... check: homer
........................... check: r-scater
........................... check: r-biocsingular
........................... check: r-sva
........................... check: r-tfbstools
........................... check: r-dose
........................... check: r-tmixclust
........................... check: r-genomicalignments
........................... check: r-fgsea
........................... check: r-biocneighbors
........................ check: r-fs
......................... direct: r-fs -> {'r-usethis', 'r-reprex', 'r-shinyfiles', 'r-biomartr'}
......................... check: r-usethis
......................... check: r-reprex
......................... check: r-shinyfiles
......................... check: r-biomartr
........................ check: r-vgam
......................... direct: r-vgam -> {'r-seurat', 'r-exomedepth', 'r-powerlaw'}
......................... check: r-seurat
......................... check: r-exomedepth
......................... check: r-powerlaw
.......................... direct: r-powerlaw -> {'r-cner'}
.......................... check: r-cner
........................ check: r-diversitree
........................ check: r-aroma-light
........................ check: r-rodbc
......................... direct: r-rodbc -> set()
........................ check: r-gamlss
......................... direct: r-gamlss -> {'orthofiller'}
......................... check: orthofiller
........................ check: r-sm
......................... direct: r-sm -> {'r-vioplot', 'trinity'}
......................... check: r-vioplot
......................... check: trinity
........................ check: r-polyclip
......................... direct: r-polyclip -> {'r-ggforce', 'r-spatstat'}
......................... check: r-ggforce
......................... check: r-spatstat
........................ check: r-tensor
......................... direct: r-tensor -> {'r-spatstat'}
......................... check: r-spatstat
........................ check: r-ini
......................... direct: r-ini -> {'r-gh'}
......................... check: r-gh
........................ check: r-laplacesdemon
......................... direct: r-laplacesdemon -> set()
........................ check: r-dose
........................ check: r-memoise
......................... direct: r-memoise -> {'r-rsqlite', 'r-devtools'}
......................... check: r-rsqlite
......................... check: r-devtools
........................ check: r-bookdown
........................ check: r-clusterprofiler
........................ check: r-lattice
......................... direct: r-lattice -> {'r-caret', 'r-locfit', 'r-hexbin', 'r-psych', 'r-latticeextra', 'r-survey', 'r-lumi', 'r-ape', 'r-rminer', 'r-geneplotter', 'r-spacetime', 'r-affycoretools', 'r-vegan', 'r-shortread', 'r-maptools', 'r-reportingtools', 'r-rms', 'r-sp', 'r-zoo', 'r-matrix', 'r-robust', 'r-copula', 'r-factominer', 'r-gstat', 'r-gviz', 'r-mice', 'r-deseq', 'r-gbm', 'r-topgo', 'r-hmisc', 'r-metap', 'r-lme4', 'r-methylumi', 'r-flexmix', 'r-nlme', 'r-msnbase', 'r-minfi', 'r-flexclust', 'r-cubist', 'r-affyqcreport', 'r-allelicimbalance', 'r-coda', 'r-vsn', 'r-fit-models', 'r-rrcov'}
......................... check: r-caret
......................... check: r-locfit
.......................... direct: r-locfit -> {'r-anaquin', 'r-bumphunter', 'r-deseq2', 'r-deseq', 'r-edger', 'r-absseq'}
.......................... check: r-anaquin
.......................... check: r-bumphunter
.......................... check: r-deseq2
.......................... check: r-deseq
.......................... check: r-edger
.......................... check: r-absseq
......................... check: r-hexbin
......................... check: r-psych
......................... check: r-latticeextra
.......................... direct: r-latticeextra -> {'r-shortread', 'r-gviz', 'r-allelicimbalance', 'r-hmisc'}
.......................... check: r-shortread
.......................... check: r-gviz
.......................... check: r-allelicimbalance
.......................... check: r-hmisc
......................... check: r-survey
......................... check: r-lumi
......................... check: r-ape
.......................... direct: r-ape -> {'r-adegenet', 'r-convevol', 'r-diversitree', 'r-corhmm', 'r-geomorph', 'r-paleotree', 'r-phyloseq', 'r-mcmcglmm', 'r-phytools', 'trinity', 'r-seurat', 'r-geiger', 'r-picante', 'r-phangorn', 'r-phylostratr'}
.......................... check: r-adegenet
.......................... check: r-convevol
.......................... check: r-diversitree
.......................... check: r-corhmm
.......................... check: r-geomorph
.......................... check: r-paleotree
.......................... check: r-phyloseq
.......................... check: r-mcmcglmm
........................... direct: r-mcmcglmm -> set()
.......................... check: r-phytools
.......................... check: trinity
.......................... check: r-seurat
.......................... check: r-geiger
.......................... check: r-picante
.......................... check: r-phangorn
.......................... check: r-phylostratr
......................... check: r-rminer
......................... check: r-geneplotter
......................... check: r-spacetime
......................... check: r-affycoretools
......................... check: r-vegan
......................... check: r-shortread
......................... check: r-maptools
......................... check: r-reportingtools
......................... check: r-rms
......................... check: r-sp
......................... check: r-zoo
......................... check: r-matrix
.......................... direct: r-matrix -> {'r-rcppeigen', 'r-spatstat-data', 'r-spatialreg', 'r-pbkrtest', 'r-seurat', 'r-delayedmatrixstats', 'r-glmnet', 'r-kknn', 'r-summarizedexperiment', 'r-speedglm', 'r-scater', 'r-rcppblaze', 'r-phangorn', 'r-diffusionmap', 'r-biomformat', 'r-ks', 'r-grbase', 'r-igraph', 'r-survey', 'r-geomorph', 'r-spdep', 'r-lfe', 'r-sctransform', 'r-survival', 'r-blockmodeling', 'r-rsvd', 'r-category', 'r-influencer', 'r-reticulate', 'r-threejs', 'r-copula', 'r-ranger', 'r-mcmcglmm', 'r-rspectra', 'r-delayedarray', 'r-matrixmodels', 'r-lme4', 'r-quantreg', 'r-spatstat', 'r-ergm', 'r-ordinal', 'r-snpstats', 'r-irlba', 'r-mgcv', 'r-leiden', 'r-expm', 'r-xgboost', 'r-biocsingular', 'r-uwot', 'r-recipes', 'r-fgsea'}
.......................... check: r-rcppeigen
........................... direct: r-rcppeigen -> {'r-construct', 'r-ranger', 'r-rspectra', 'r-rstan', 'r-ggforce', 'r-seurat', 'r-sctransform', 'r-lme4', 'r-grbase'}
........................... check: r-construct
........................... check: r-ranger
............................ direct: r-ranger -> {'r-boruta', 'r-seurat'}
............................ check: r-boruta
............................. direct: r-boruta -> set()
............................ check: r-seurat
........................... check: r-rspectra
............................ direct: r-rspectra -> {'r-uwot'}
............................ check: r-uwot
........................... check: r-rstan
........................... check: r-ggforce
........................... check: r-seurat
........................... check: r-sctransform
........................... check: r-lme4
........................... check: r-grbase
.......................... check: r-spatstat-data
........................... direct: r-spatstat-data -> {'r-spatstat'}
........................... check: r-spatstat
.......................... check: r-spatialreg
.......................... check: r-pbkrtest
.......................... check: r-seurat
.......................... check: r-delayedmatrixstats
.......................... check: r-glmnet
.......................... check: r-kknn
.......................... check: r-summarizedexperiment
.......................... check: r-speedglm
........................... direct: r-speedglm -> {'r-alpine'}
........................... check: r-alpine
.......................... check: r-scater
.......................... check: r-rcppblaze
........................... direct: r-rcppblaze -> set()
.......................... check: r-phangorn
.......................... check: r-diffusionmap
.......................... check: r-biomformat
.......................... check: r-ks
.......................... check: r-grbase
.......................... check: r-igraph
.......................... check: r-survey
.......................... check: r-geomorph
.......................... check: r-spdep
.......................... check: r-lfe
.......................... check: r-sctransform
.......................... check: r-survival
........................... direct: r-survival -> {'r-mlr', 'r-lava', 'r-gamlss', 'r-mergemaid', 'r-prodlim', 'r-fitdistrplus', 'r-jomo', 'r-survey', 'r-mixtools', 'r-multcomp', 'r-multtest', 'r-rms', 'r-partykit', 'r-genefilter', 'r-acgh', 'r-mice', 'r-ipred', 'r-gbm', 'r-aer', 'r-pamr', 'r-hmisc', 'r-th-data', 'r-wgcna', 'r-snpstats', 'r-party', 'r-coin'}
........................... check: r-mlr
........................... check: r-lava
........................... check: r-gamlss
........................... check: r-mergemaid
............................ direct: r-mergemaid -> {'r-xde'}
............................ check: r-xde
........................... check: r-prodlim
........................... check: r-fitdistrplus
............................ direct: r-fitdistrplus -> {'r-seurat'}
............................ check: r-seurat
........................... check: r-jomo
........................... check: r-survey
........................... check: r-mixtools
........................... check: r-multcomp
........................... check: r-multtest
............................ direct: r-multtest -> {'r-phyloseq', 'r-aldex2', 'r-acgh', 'r-siggenes', 'r-adsplit', 'r-a4base'}
............................ check: r-phyloseq
............................ check: r-aldex2
............................ check: r-acgh
............................. direct: r-acgh -> set()
............................ check: r-siggenes
............................. direct: r-siggenes -> {'r-minfi', 'r-xde'}
............................. check: r-minfi
............................. check: r-xde
............................ check: r-adsplit
............................ check: r-a4base
........................... check: r-rms
........................... check: r-partykit
............................ direct: r-partykit -> {'r-c50'}
............................ check: r-c50
........................... check: r-genefilter
........................... check: r-acgh
........................... check: r-mice
........................... check: r-ipred
........................... check: r-gbm
........................... check: r-aer
........................... check: r-pamr
............................ direct: r-pamr -> {'r-a4classif'}
............................ check: r-a4classif
........................... check: r-hmisc
........................... check: r-th-data
........................... check: r-wgcna
........................... check: r-snpstats
............................ direct: r-snpstats -> {'r-ldheatmap'}
............................ check: r-ldheatmap
........................... check: r-party
........................... check: r-coin
.......................... check: r-blockmodeling
.......................... check: r-rsvd
........................... direct: r-rsvd -> {'r-seurat', 'r-biocsingular'}
........................... check: r-seurat
........................... check: r-biocsingular
.......................... check: r-category
.......................... check: r-influencer
.......................... check: r-reticulate
........................... direct: r-reticulate -> {'r-seurat', 'r-caracas', 'r-leiden'}
........................... check: r-seurat
........................... check: r-caracas
........................... check: r-leiden
.......................... check: r-threejs
.......................... check: r-copula
.......................... check: r-ranger
.......................... check: r-mcmcglmm
.......................... check: r-rspectra
.......................... check: r-delayedarray
.......................... check: r-matrixmodels
........................... direct: r-matrixmodels -> {'r-quantreg'}
........................... check: r-quantreg
............................ direct: r-quantreg -> {'r-rms', 'r-car', 'r-np'}
............................ check: r-rms
............................ check: r-car
............................ check: r-np
............................. direct: r-np -> set()
.......................... check: r-lme4
.......................... check: r-quantreg
.......................... check: r-spatstat
.......................... check: r-ergm
.......................... check: r-ordinal
.......................... check: r-snpstats
.......................... check: r-irlba
........................... direct: r-irlba -> {'r-uwot', 'r-igraph', 'r-biocsingular', 'r-seurat'}
........................... check: r-uwot
........................... check: r-igraph
........................... check: r-biocsingular
........................... check: r-seurat
.......................... check: r-mgcv
.......................... check: r-leiden
.......................... check: r-expm
........................... direct: r-expm -> {'r-corhmm', 'r-phytools', 'r-spatialreg', 'r-spdep'}
........................... check: r-corhmm
........................... check: r-phytools
........................... check: r-spatialreg
........................... check: r-spdep
.......................... check: r-xgboost
.......................... check: r-biocsingular
.......................... check: r-uwot
.......................... check: r-recipes
.......................... check: r-fgsea
......................... check: r-robust
......................... check: r-copula
......................... check: r-factominer
......................... check: r-gstat
......................... check: r-gviz
......................... check: r-mice
......................... check: r-deseq
......................... check: r-gbm
......................... check: r-topgo
......................... check: r-hmisc
......................... check: r-metap
......................... check: r-lme4
......................... check: r-methylumi
......................... check: r-flexmix
......................... check: r-nlme
.......................... direct: r-nlme -> {'r-phytools', 'r-broom', 'r-caret', 'r-ape', 'r-minfi', 'r-gamlss', 'r-spatialreg', 'r-spdep', 'r-mgcv', 'r-psych', 'r-car', 'r-allelicimbalance', 'r-picante', 'r-rms', 'r-lme4', 'r-spatstat', 'r-urca'}
.......................... check: r-phytools
.......................... check: r-broom
.......................... check: r-caret
.......................... check: r-ape
.......................... check: r-minfi
.......................... check: r-gamlss
.......................... check: r-spatialreg
.......................... check: r-spdep
.......................... check: r-mgcv
.......................... check: r-psych
.......................... check: r-car
.......................... check: r-allelicimbalance
.......................... check: r-picante
.......................... check: r-rms
.......................... check: r-lme4
.......................... check: r-spatstat
.......................... check: r-urca
........................... direct: r-urca -> {'r-forecast'}
........................... check: r-forecast
......................... check: r-msnbase
......................... check: r-minfi
......................... check: r-flexclust
......................... check: r-cubist
......................... check: r-affyqcreport
......................... check: r-allelicimbalance
......................... check: r-coda
.......................... direct: r-coda -> {'r-phytools', 'r-mcmcglmm', 'r-spatialreg', 'r-geiger', 'r-spdep', 'r-rjags', 'r-nimble', 'r-ergm', 'r-statnet-common'}
.......................... check: r-phytools
.......................... check: r-mcmcglmm
.......................... check: r-spatialreg
.......................... check: r-geiger
.......................... check: r-spdep
.......................... check: r-rjags
........................... direct: r-rjags -> set()
.......................... check: r-nimble
.......................... check: r-ergm
.......................... check: r-statnet-common
........................... direct: r-statnet-common -> {'r-ergm'}
........................... check: r-ergm
......................... check: r-vsn
......................... check: r-fit-models
......................... check: r-rrcov
.......................... direct: r-rrcov -> {'r-robust'}
.......................... check: r-robust
........................ check: r-xts
........................ check: r-bsgenome
........................ check: r-np
........................ check: r-clustergeneration
......................... direct: r-clustergeneration -> {'r-phytools'}
......................... check: r-phytools
........................ check: r-lpsolve
......................... direct: r-lpsolve -> {'r-ergm'}
......................... check: r-ergm
........................ check: r-spacetime
........................ check: r-phantompeakqualtools
........................ check: r-units
......................... direct: r-units -> {'r-cdcfluview', 'r-tidycensus', 'r-sf'}
......................... check: r-cdcfluview
......................... check: r-tidycensus
......................... check: r-sf
........................ check: r-biocstyle
........................ check: r-nimble
........................ check: r-sourcetools
......................... direct: r-sourcetools -> {'r-shiny'}
......................... check: r-shiny
........................ check: r-r-oo
......................... direct: r-r-oo -> {'r-r-utils', 'r-aroma-light', 'r-pscbs', 'r-r-cache'}
......................... check: r-r-utils
......................... check: r-aroma-light
......................... check: r-pscbs
......................... check: r-r-cache
........................ check: r-abaenrichment
........................ check: r-xlconnectjars
......................... direct: r-xlconnectjars -> {'r-xlconnect'}
......................... check: r-xlconnect
.......................... direct: r-xlconnect -> set()
........................ check: r-pathview
........................ check: r-kernlab
......................... direct: r-kernlab -> {'r-ks', 'r-rminer', 'r-fpc'}
......................... check: r-ks
......................... check: r-rminer
......................... check: r-fpc
........................ check: r-plogr
......................... direct: r-plogr -> {'r-bindrcpp', 'r-dplyr', 'r-rmariadb', 'r-rsqlite'}
......................... check: r-bindrcpp
.......................... direct: r-bindrcpp -> {'r-dplyr'}
.......................... check: r-dplyr
......................... check: r-dplyr
......................... check: r-rmariadb
......................... check: r-rsqlite
........................ check: r-fftwtools
........................ check: r-viridislite
......................... direct: r-viridislite -> {'r-plotly', 'r-ggplot2', 'r-viridis', 'r-scales'}
......................... check: r-plotly
......................... check: r-ggplot2
......................... check: r-viridis
......................... check: r-scales
........................ check: r-roc
........................ check: r-qtl
......................... direct: r-qtl -> {'r-popvar'}
......................... check: r-popvar
........................ check: r-flexmix
........................ check: r-dynamictreecut
......................... direct: r-dynamictreecut -> {'r-wgcna'}
......................... check: r-wgcna
........................ check: r-dada2
........................ check: r-quantro
........................ check: r-gamlss-data
......................... direct: r-gamlss-data -> {'r-gamlss'}
......................... check: r-gamlss
........................ check: r-rstan
........................ check: r-gistr
........................ check: r-affyqcreport
........................ check: r-affycomp
......................... direct: r-affycomp -> set()
........................ check: r-biocsingular
........................ check: r-ellipse
......................... direct: r-ellipse -> {'r-factominer'}
......................... check: r-factominer
........................ check: r-ggbeeswarm
........................ check: r-gseabase
........................ check: r-jaspar2018
......................... direct: r-jaspar2018 -> set()
........................ check: r-abind
......................... direct: r-abind -> {'r-factoextra', 'r-car', 'r-spatstat', 'r-magic'}
......................... check: r-factoextra
......................... check: r-car
......................... check: r-spatstat
......................... check: r-magic
.......................... direct: r-magic -> set()
........................ check: py-rpy2
......................... direct: py-rpy2 -> set()
........................ check: r-nloptr
........................ check: r-clisymbols
......................... direct: r-clisymbols -> {'r-usethis'}
......................... check: r-usethis
........................ check: r-classint
......................... direct: r-classint -> {'r-sf'}
......................... check: r-sf
........................ check: r-cellranger
........................ check: r-als
........................ check: r-deldir
......................... direct: r-deldir -> {'r-spatstat', 'r-spdep'}
......................... check: r-spatstat
......................... check: r-spdep
........................ check: r-locfit
........................ check: r-fracdiff
......................... direct: r-fracdiff -> {'r-forecast'}
......................... check: r-forecast
........................ check: breseq
......................... direct: breseq -> set()
........................ check: r-survey
........................ check: r-future
......................... direct: r-future -> {'r-sctransform', 'r-seurat', 'r-pscbs', 'r-future-apply'}
......................... check: r-sctransform
......................... check: r-seurat
......................... check: r-pscbs
......................... check: r-future-apply
.......................... direct: r-future-apply -> {'r-sctransform', 'r-seurat'}
.......................... check: r-sctransform
.......................... check: r-seurat
........................ check: r-mmwrweek
......................... direct: r-mmwrweek -> {'r-cdcfluview'}
......................... check: r-cdcfluview
........................ check: r-dendextend
........................ check: r-rrblup
......................... direct: r-rrblup -> {'r-popvar'}
......................... check: r-popvar
........................ check: r-r-methodss3
......................... direct: r-r-methodss3 -> {'r-aroma-light', 'r-r-utils', 'r-pscbs', 'r-r-cache', 'r-r-oo'}
......................... check: r-aroma-light
......................... check: r-r-utils
......................... check: r-pscbs
......................... check: r-r-cache
......................... check: r-r-oo
........................ check: r-vcd
........................ check: r-ape
........................ check: r-spatial
......................... direct: r-spatial -> set()
........................ check: r-spdep
........................ check: r-assertthat
......................... direct: r-assertthat -> {'r-dplyr', 'r-dbplyr', 'r-tibble', 'r-prettyunits', 'r-processx', 'r-gistr', 'r-desc', 'r-ggvis', 'r-cli'}
......................... check: r-dplyr
......................... check: r-dbplyr
......................... check: r-tibble
......................... check: r-prettyunits
.......................... direct: r-prettyunits -> {'r-rcmdcheck', 'r-blob', 'r-progress', 'r-pkgbuild'}
.......................... check: r-rcmdcheck
.......................... check: r-blob
.......................... check: r-progress
.......................... check: r-pkgbuild
......................... check: r-processx
......................... check: r-gistr
......................... check: r-desc
......................... check: r-ggvis
......................... check: r-cli
........................ check: r-agilp
......................... direct: r-agilp -> set()
........................ check: r-rsvd
........................ check: r-aneufinderdata
......................... direct: r-aneufinderdata -> {'r-aneufinder'}
......................... check: r-aneufinder
........................ check: r-gsalib
......................... direct: r-gsalib -> set()
........................ check: r-yapsa
........................ check: r-pkgload
........................ check: r-upsetr
........................ check: r-genomeinfodb
........................ check: r-condop
........................ check: r-aod
......................... direct: r-aod -> {'r-exomedepth'}
......................... check: r-exomedepth
........................ check: r-affyplm
........................ check: r-htmltools
......................... direct: r-htmltools -> {'r-dygraphs', 'r-plotly', 'r-shinydashboard', 'r-bookdown', 'r-rmarkdown', 'r-crosstalk', 'r-rms', 'r-shinyfiles', 'r-dt', 'r-manipulatewidget', 'r-visnetwork', 'r-shiny', 'r-rgl', 'r-leaflet', 'r-hmisc', 'r-repr', 'r-diagrammer', 'r-htmlwidgets', 'r-miniui', 'r-htmltable', 'r-ggvis'}
......................... check: r-dygraphs
......................... check: r-plotly
......................... check: r-shinydashboard
......................... check: r-bookdown
......................... check: r-rmarkdown
......................... check: r-crosstalk
......................... check: r-rms
......................... check: r-shinyfiles
......................... check: r-dt
......................... check: r-manipulatewidget
......................... check: r-visnetwork
.......................... direct: r-visnetwork -> {'r-diagrammer'}
.......................... check: r-diagrammer
......................... check: r-shiny
......................... check: r-rgl
......................... check: r-leaflet
......................... check: r-hmisc
......................... check: r-repr
......................... check: r-diagrammer
......................... check: r-htmlwidgets
.......................... direct: r-htmlwidgets -> {'r-manipulatewidget', 'r-threejs', 'r-dygraphs', 'r-visnetwork', 'r-rgl', 'r-plotly', 'r-htmltable', 'r-leaflet', 'r-networkd3', 'r-rbokeh', 'r-dt', 'r-diagrammer'}
.......................... check: r-manipulatewidget
.......................... check: r-threejs
.......................... check: r-dygraphs
.......................... check: r-visnetwork
.......................... check: r-rgl
.......................... check: r-plotly
.......................... check: r-htmltable
.......................... check: r-leaflet
.......................... check: r-networkd3
.......................... check: r-rbokeh
.......................... check: r-dt
.......................... check: r-diagrammer
......................... check: r-miniui
......................... check: r-htmltable
......................... check: r-ggvis
........................ check: r-methylumi
........................ check: r-mzid
........................ check: r-rjava
......................... direct: r-rjava -> {'r-xlsx', 'r-xlconnect', 'r-xlconnectjars', 'r-xlsxjars'}
......................... check: r-xlsx
.......................... direct: r-xlsx -> set()
......................... check: r-xlconnect
......................... check: r-xlconnectjars
......................... check: r-xlsxjars
.......................... direct: r-xlsxjars -> {'r-xlsx'}
.......................... check: r-xlsx
........................ check: r-geosphere
........................ check: angsd
......................... direct: angsd -> set()
........................ check: r-miniui
........................ check: r-reshape2
........................ check: r-bindrcpp
........................ check: r-samr
........................ check: r-ggmap
........................ check: r-cubist
........................ check: r-cner
........................ check: r-recipes
........................ check: r-reshape
........................ check: r-ff
........................ check: r-rcppeigen
........................ check: r-xopen
........................ check: r-acepack
......................... direct: r-acepack -> {'r-hmisc'}
......................... check: r-hmisc
........................ check: r-spatialreg
........................ check: r-alpine
........................ check: r-geiger
........................ check: r-sf
........................ check: r-imager
........................ check: r-kernsmooth
......................... direct: r-kernsmooth -> {'r-lumi', 'r-classint', 'r-seurat', 'r-prodlim', 'r-philentropy', 'r-mpm', 'r-gplots', 'r-ks'}
......................... check: r-lumi
......................... check: r-classint
......................... check: r-seurat
......................... check: r-prodlim
......................... check: r-philentropy
......................... check: r-mpm
.......................... direct: r-mpm -> {'r-a4base'}
.......................... check: r-a4base
......................... check: r-gplots
......................... check: r-ks
........................ check: r-rcolorbrewer
......................... direct: r-rcolorbrewer -> {'r-seurat', 'r-plotly', 'r-latticeextra', 'r-biovizbase', 'r-smoof', 'r-yarn', 'r-complexheatmap', 'r-nmf', 'r-ggally', 'r-geneplotter', 'r-gviz', 'r-deseq', 'r-leaflet', 'r-mlinterfaces', 'r-sseq', 'r-goplot', 'r-diagrammer', 'hic-pro', 'r-enrichplot', 'r-minfi', 'r-quantro', 'r-affyqcreport', 'r-xde', 'r-scales'}
......................... check: r-seurat
......................... check: r-plotly
......................... check: r-latticeextra
......................... check: r-biovizbase
......................... check: r-smoof
......................... check: r-yarn
......................... check: r-complexheatmap
......................... check: r-nmf
......................... check: r-ggally
......................... check: r-geneplotter
......................... check: r-gviz
......................... check: r-deseq
......................... check: r-leaflet
......................... check: r-mlinterfaces
......................... check: r-sseq
.......................... direct: r-sseq -> set()
......................... check: r-goplot
......................... check: r-diagrammer
......................... check: hic-pro
......................... check: r-enrichplot
......................... check: r-minfi
......................... check: r-quantro
......................... check: r-affyqcreport
......................... check: r-xde
......................... check: r-scales
........................ check: r-scater
........................ check: r-rsnns
......................... direct: r-rsnns -> set()
........................ check: r-tmixclust
........................ check: r-prabclus
......................... direct: r-prabclus -> {'r-fpc'}
......................... check: r-fpc
........................ check: r-watermelon
........................ check: r-affycontam
........................ check: r-complexheatmap
........................ check: r-lumi
........................ check: r-pcapp
......................... direct: r-pcapp -> {'r-rrcov', 'r-chemometrics', 'r-copula'}
......................... check: r-rrcov
......................... check: r-chemometrics
......................... check: r-copula
........................ check: qorts
........................ check: r-dtw
........................ check: r-rda
......................... direct: r-rda -> {'r-mlinterfaces'}
......................... check: r-mlinterfaces
........................ check: r-webshot
........................ check: vegas2
......................... direct: vegas2 -> set()
........................ check: r-gsa
......................... direct: r-gsa -> {'r-samr'}
......................... check: r-samr
........................ check: r-shortread
........................ check: r-survival
........................ check: r-flashclust
......................... direct: r-flashclust -> {'r-factominer'}
......................... check: r-factominer
........................ check: r-udunits2
......................... direct: r-udunits2 -> {'r-units'}
......................... check: r-units
........................ check: r-httpcode
......................... direct: r-httpcode -> {'r-crul'}
......................... check: r-crul
........................ check: r-broom
........................ check: r-xfun
......................... direct: r-xfun -> {'r-rmarkdown', 'r-tinytex', 'r-markdown', 'r-bookdown', 'r-knitr'}
......................... check: r-rmarkdown
......................... check: r-tinytex
.......................... direct: r-tinytex -> {'r-rmarkdown', 'r-bookdown'}
.......................... check: r-rmarkdown
.......................... check: r-bookdown
......................... check: r-markdown
.......................... direct: r-markdown -> {'r-leaflet', 'r-knitr'}
.......................... check: r-leaflet
.......................... check: r-knitr
......................... check: r-bookdown
......................... check: r-knitr
........................ check: r-shiny
........................ check: r-rjson
......................... direct: r-rjson -> {'r-geonames', 'r-ggmap', 'r-scater', 'r-getoptlong', 'r-analysispageserver'}
......................... check: r-geonames
.......................... direct: r-geonames -> {'r-rnoaa'}
.......................... check: r-rnoaa
......................... check: r-ggmap
......................... check: r-scater
......................... check: r-getoptlong
.......................... direct: r-getoptlong -> {'r-complexheatmap', 'r-gtrellis', 'r-yapsa'}
.......................... check: r-complexheatmap
.......................... check: r-gtrellis
.......................... check: r-yapsa
......................... check: r-analysispageserver
.......................... direct: r-analysispageserver -> set()
........................ check: r-zeallot
......................... direct: r-zeallot -> {'r-vctrs'}
......................... check: r-vctrs
........................ check: r-urltools
......................... direct: r-urltools -> {'r-europepmc', 'r-crul'}
......................... check: r-europepmc
......................... check: r-crul
........................ check: r-png
......................... direct: r-png -> {'r-complexheatmap', 'r-paleotree', 'r-seurat', 'r-ggmap', 'r-keggrest', 'r-imager', 'r-leaflet', 'r-pathview', 'r-rgooglemaps', 'r-readbitmap'}
......................... check: r-complexheatmap
......................... check: r-paleotree
......................... check: r-seurat
......................... check: r-ggmap
......................... check: r-keggrest
......................... check: r-imager
......................... check: r-leaflet
......................... check: r-pathview
......................... check: r-rgooglemaps
.......................... direct: r-rgooglemaps -> {'r-ggmap'}
.......................... check: r-ggmap
......................... check: r-readbitmap
........................ check: r-trust
......................... direct: r-trust -> {'r-ergm'}
......................... check: r-ergm
........................ check: r-matrixstats
......................... direct: r-matrixstats -> {'r-wgcna', 'r-yarn', 'r-aroma-light', 'r-samr', 'r-minfi', 'r-delayedarray', 'r-methylumi', 'r-delayedmatrixstats', 'r-bumphunter', 'r-gviz', 'r-sva', 'r-topgo', 'r-scater', 'r-coin', 'r-pscbs', 'r-watermelon', 'r-loo'}
......................... check: r-wgcna
......................... check: r-yarn
......................... check: r-aroma-light
......................... check: r-samr
......................... check: r-minfi
......................... check: r-delayedarray
......................... check: r-methylumi
......................... check: r-delayedmatrixstats
......................... check: r-bumphunter
......................... check: r-gviz
......................... check: r-sva
......................... check: r-topgo
......................... check: r-scater
......................... check: r-coin
......................... check: r-pscbs
......................... check: r-watermelon
......................... check: r-loo
........................ check: r-diagrammer
........................ check: trinity
........................ check: r-energy
......................... direct: r-energy -> {'r-compositions'}
......................... check: r-compositions
........................ check: r-checkpoint
......................... direct: r-checkpoint -> set()
........................ check: r-irlba
........................ check: r-corpcor
......................... direct: r-corpcor -> {'r-corhmm', 'vegas2', 'r-mcmcglmm'}
......................... check: r-corhmm
......................... check: vegas2
......................... check: r-mcmcglmm
........................ check: r-networkd3
........................ check: r-gsl
......................... direct: r-gsl -> {'r-copula'}
......................... check: r-copula
........................ check: r-gtrellis
........................ check: r-tidyselect
........................ check: r-futile-logger
........................ check: r-cli
........................ check: r-mlr
........................ check: r-paleotree
........................ check: r-affyexpress
........................ check: r-fastmatch
......................... direct: r-fastmatch -> {'r-paramhelpers', 'r-fgsea', 'r-phangorn'}
......................... check: r-paramhelpers
......................... check: r-fgsea
......................... check: r-phangorn
........................ check: r-mitools
......................... direct: r-mitools -> {'r-survey'}
......................... check: r-survey
........................ check: r-askpass
......................... direct: r-askpass -> {'r-openssl'}
......................... check: r-openssl
.......................... direct: r-openssl -> {'r-base64', 'r-httr'}
.......................... check: r-base64
........................... direct: r-base64 -> {'r-illuminaio'}
........................... check: r-illuminaio
............................ direct: r-illuminaio -> {'r-minfi', 'r-watermelon', 'r-methylumi'}
............................ check: r-minfi
............................ check: r-watermelon
............................ check: r-methylumi
.......................... check: r-httr
........................ check: r-plotly
........................ check: r-fitdistrplus
........................ check: r-randomfieldsutils
......................... direct: r-randomfieldsutils -> {'r-randomfields'}
......................... check: r-randomfields
........................ check: r-annotationfilter
........................ check: r-stanheaders
........................ check: r-grbase
........................ check: r-adabag
........................ check: r-genomeinfodbdata
......................... direct: r-genomeinfodbdata -> {'r-genomeinfodb'}
......................... check: r-genomeinfodb
........................ check: r-stargazer
......................... direct: r-stargazer -> set()
........................ check: r-dbplyr
........................ check: r-ggsignif
........................ check: r-mime
......................... direct: r-mime -> {'r-rmarkdown', 'r-shiny', 'r-crul', 'r-httr', 'r-markdown'}
......................... check: r-rmarkdown
......................... check: r-shiny
......................... check: r-crul
......................... check: r-httr
......................... check: r-markdown
........................ check: r-gamlss-dist
......................... direct: r-gamlss-dist -> {'r-gamlss'}
......................... check: r-gamlss
........................ check: r-ggally
........................ check: r-interactivedisplaybase
........................ check: r-randomfields
........................ check: r-siggenes
........................ check: r-robustbase
......................... direct: r-robustbase -> {'r-robust', 'r-chemometrics', 'r-gwmodel', 'r-compositions', 'r-rrcov', 'r-fpc', 'r-ergm'}
......................... check: r-robust
......................... check: r-chemometrics
......................... check: r-gwmodel
......................... check: r-compositions
......................... check: r-rrcov
......................... check: r-fpc
......................... check: r-ergm
........................ check: r-xml2
......................... direct: r-xml2 -> {'r-rvest', 'r-cdcfluview', 'r-geoquery', 'r-tidycensus', 'r-tidyverse', 'r-roxygen2', 'r-rnoaa', 'r-rversions', 'r-europepmc'}
......................... check: r-rvest
......................... check: r-cdcfluview
......................... check: r-geoquery
......................... check: r-tidycensus
......................... check: r-tidyverse
......................... check: r-roxygen2
......................... check: r-rnoaa
......................... check: r-rversions
.......................... direct: r-rversions -> set()
......................... check: r-europepmc
........................ check: r-dt
........................ check: r-affypdnn
........................ check: r-inum
......................... direct: r-inum -> {'r-partykit'}
......................... check: r-partykit
........................ check: r-corrplot
......................... direct: r-corrplot -> {'r-yapsa'}
......................... check: r-yapsa
........................ check: r-nortest
......................... direct: r-nortest -> {'r-envstats'}
......................... check: r-envstats
........................ check: r-gviz
........................ check: r-gcrma
........................ check: r-snprelate
......................... direct: r-snprelate -> {'snphylo'}
......................... check: snphylo
........................ check: r-markdown
........................ check: r-hmisc
........................ check: r-withr
......................... direct: r-withr -> {'r-pkgbuild', 'r-caret', 'r-devtools', 'r-pkgload', 'r-testthat', 'r-ggforce', 'r-sessioninfo', 'r-covr', 'r-ggridges', 'r-rcmdcheck', 'r-reprex', 'r-pkgmaker', 'r-usethis', 'r-recipes', 'r-ggplot2'}
......................... check: r-pkgbuild
......................... check: r-caret
......................... check: r-devtools
......................... check: r-pkgload
......................... check: r-testthat
......................... check: r-ggforce
......................... check: r-sessioninfo
......................... check: r-covr
......................... check: r-ggridges
......................... check: r-rcmdcheck
......................... check: r-reprex
......................... check: r-pkgmaker
......................... check: r-usethis
......................... check: r-recipes
......................... check: r-ggplot2
........................ check: r-lme4
........................ check: r-quantreg
........................ check: r-roxygen2
........................ check: r-goplot
........................ check: r-nlme
........................ check: r-domc
........................ check: r-stringr
........................ check: r-enrichplot
........................ check: r-getopt
......................... direct: r-getopt -> {'r-optparse', 'snphylo', 'r-argparse'}
......................... check: r-optparse
.......................... direct: r-optparse -> set()
......................... check: snphylo
......................... check: r-argparse
........................ check: r-a4preproc
........................ check: r-minfi
........................ check: r-scrime
......................... direct: r-scrime -> {'r-siggenes'}
......................... check: r-siggenes
........................ check: r-hms
........................ check: r-aneufinder
........................ check: r-dirichletmultinomial
........................ check: r-network
........................ check: r-codetools
......................... direct: r-codetools -> {'r-multcomp', 'r-manipulatewidget', 'r-pryr', 'r-globals', 'r-foreach', 'r-pkgmaker'}
......................... check: r-multcomp
......................... check: r-manipulatewidget
......................... check: r-pryr
......................... check: r-globals
.......................... direct: r-globals -> {'r-future', 'r-future-apply'}
.......................... check: r-future
.......................... check: r-future-apply
......................... check: r-foreach
......................... check: r-pkgmaker
........................ check: r-dismo
........................ check: r-oligoclasses
........................ check: r-lsei
......................... direct: r-lsei -> {'r-npsurv', 'r-yapsa'}
......................... check: r-npsurv
.......................... direct: r-npsurv -> {'r-fitdistrplus'}
.......................... check: r-fitdistrplus
......................... check: r-yapsa
........................ check: r-ggrepel
........................ check: r-xnomial
......................... direct: r-xnomial -> set()
........................ check: r-gower
......................... direct: r-gower -> {'r-recipes'}
......................... check: r-recipes
........................ check: r-hdf5r
........................ check: r-bibtex
........................ check: r-lava
........................ check: r-remotes
......................... direct: r-remotes -> {'r-pacman', 'r-devtools'}
......................... check: r-pacman
......................... check: r-devtools
........................ check: r-mzr
........................ check: r-psych
........................ check: r-biomartr
........................ check: r-evd
......................... direct: r-evd -> {'r-truncdist'}
......................... check: r-truncdist
.......................... direct: r-truncdist -> set()
........................ check: r-philentropy
........................ check: r-base64
........................ check: r-diffusionmap
........................ check: r-rrpp
......................... direct: r-rrpp -> {'r-geomorph'}
......................... check: r-geomorph
........................ check: r-s4vectors
......................... direct: r-s4vectors -> {'r-annotationdbi', 'r-aldex2', 'r-variantannotation', 'r-ggbio', 'r-alpine', 'r-delayedmatrixstats', 'r-summarizedexperiment', 'r-tfbstools', 'r-scater', 'r-iranges', 'r-dose', 'r-biocneighbors', 'r-biovizbase', 'r-bsgenome', 'r-annotationforge', 'r-altcdfenvs', 'r-deseq2', 'r-affycoretools', 'r-shortread', 'r-glimma', 'r-genomicranges', 'r-rsamtools', 'r-genefilter', 'r-delayedarray', 'r-biostrings', 'r-bumphunter', 'r-decipher', 'r-gviz', 'r-genomeinfodb', 'r-condop', 'r-organismdbi', 'r-ensembldb', 'r-genomicfeatures', 'r-msnbase', 'r-minfi', 'r-rtracklayer', 'r-xvector', 'r-somaticsignatures', 'r-hdf5array', 'r-aneufinder', 'r-dirichletmultinomial', 'r-allelicimbalance', 'r-biocsingular', 'r-codex', 'r-cner', 'r-singlecellexperiment', 'r-genomicalignments', 'r-oligoclasses', 'r-annotationhub'}
......................... check: r-annotationdbi
......................... check: r-aldex2
......................... check: r-variantannotation
......................... check: r-ggbio
......................... check: r-alpine
......................... check: r-delayedmatrixstats
......................... check: r-summarizedexperiment
......................... check: r-tfbstools
......................... check: r-scater
......................... check: r-iranges
......................... check: r-dose
......................... check: r-biocneighbors
......................... check: r-biovizbase
......................... check: r-bsgenome
......................... check: r-annotationforge
......................... check: r-altcdfenvs
......................... check: r-deseq2
......................... check: r-affycoretools
......................... check: r-shortread
......................... check: r-glimma
......................... check: r-genomicranges
......................... check: r-rsamtools
......................... check: r-genefilter
......................... check: r-delayedarray
......................... check: r-biostrings
......................... check: r-bumphunter
......................... check: r-decipher
......................... check: r-gviz
......................... check: r-genomeinfodb
......................... check: r-condop
......................... check: r-organismdbi
......................... check: r-ensembldb
......................... check: r-genomicfeatures
......................... check: r-msnbase
......................... check: r-minfi
......................... check: r-rtracklayer
......................... check: r-xvector
......................... check: r-somaticsignatures
......................... check: r-hdf5array
......................... check: r-aneufinder
......................... check: r-dirichletmultinomial
......................... check: r-allelicimbalance
......................... check: r-biocsingular
......................... check: r-codex
......................... check: r-cner
......................... check: r-singlecellexperiment
......................... check: r-genomicalignments
......................... check: r-oligoclasses
......................... check: r-annotationhub
........................ check: r-igraph
........................ check: r-rcpphnsw
......................... direct: r-rcpphnsw -> {'r-biocneighbors'}
......................... check: r-biocneighbors
........................ check: shiny-server
......................... direct: shiny-server -> set()
........................ check: r-seqlogo
......................... direct: r-seqlogo -> {'r-tfbstools'}
......................... check: r-tfbstools
........................ check: r-jsonlite
......................... direct: r-jsonlite -> {'r-googlevis', 'r-paleotree', 'r-plotly', 'r-biomartr', 'r-tidycensus', 'r-biomformat', 'r-rnoaa', 'r-europepmc', 'r-rmarkdown', 'r-webshot', 'r-covr', 'r-rbokeh', 'r-crosstalk', 'r-tidyverse', 'r-glimma', 'r-dt', 'r-shinyfiles', 'r-reticulate', 'r-threejs', 'r-visnetwork', 'r-shiny', 'r-crul', 'r-rgl', 'r-cdcfluview', 'r-repr', 'r-argparse', 'r-htmlwidgets', 'r-devtools', 'r-httr', 'r-gistr', 'r-ggvis', 'r-gh'}
......................... check: r-googlevis
.......................... direct: r-googlevis -> set()
......................... check: r-paleotree
......................... check: r-plotly
......................... check: r-biomartr
......................... check: r-tidycensus
......................... check: r-biomformat
......................... check: r-rnoaa
......................... check: r-europepmc
......................... check: r-rmarkdown
......................... check: r-webshot
......................... check: r-covr
......................... check: r-rbokeh
......................... check: r-crosstalk
......................... check: r-tidyverse
......................... check: r-glimma
......................... check: r-dt
......................... check: r-shinyfiles
......................... check: r-reticulate
......................... check: r-threejs
......................... check: r-visnetwork
......................... check: r-shiny
......................... check: r-crul
......................... check: r-rgl
......................... check: r-cdcfluview
......................... check: r-repr
......................... check: r-argparse
......................... check: r-htmlwidgets
......................... check: r-devtools
......................... check: r-httr
......................... check: r-gistr
......................... check: r-ggvis
......................... check: r-gh
........................ check: r-gmp
......................... direct: r-gmp -> {'r-rmpfr'}
......................... check: r-rmpfr
.......................... direct: r-rmpfr -> {'r-corhmm'}
.......................... check: r-corhmm
........................ check: r-pryr
........................ check: r-misc3d
......................... direct: r-misc3d -> {'r-plot3d'}
......................... check: r-plot3d
........................ check: r-rcppannoy
......................... direct: r-rcppannoy -> {'r-uwot', 'r-seurat', 'r-biocneighbors'}
......................... check: r-uwot
......................... check: r-seurat
......................... check: r-biocneighbors
........................ check: r-tidygraph
........................ check: r-covr
........................ check: r-popvar
........................ check: r-ggridges
........................ check: r-gbrd
......................... direct: r-gbrd -> {'r-rdpack'}
......................... check: r-rdpack
........................ check: r-rms
........................ check: r-fnn
......................... direct: r-fnn -> {'r-gstat', 'r-seurat', 'r-gwmodel', 'r-uwot', 'r-ks'}
......................... check: r-gstat
......................... check: r-seurat
......................... check: r-gwmodel
......................... check: r-uwot
......................... check: r-ks
........................ check: r-genomicranges
........................ check: r-irdisplay
........................ check: r-permute
......................... direct: r-permute -> {'r-vegan'}
......................... check: r-vegan
........................ check: r-reticulate
........................ check: r-copula
........................ check: r-visnetwork
........................ check: r-rsqlite
........................ check: r-a4classif
........................ check: r-aer
........................ check: r-ica
......................... direct: r-ica -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-exomecopy
........................ check: r-preprocesscore
......................... direct: r-preprocesscore -> {'r-wgcna', 'r-lumi', 'r-yarn', 'r-msnbase', 'r-minfi', 'r-affy', 'r-agimicrorna', 'r-affyplm'}
......................... check: r-wgcna
......................... check: r-lumi
......................... check: r-yarn
......................... check: r-msnbase
......................... check: r-minfi
......................... check: r-affy
......................... check: r-agimicrorna
......................... check: r-affyplm
........................ check: r-analysispageserver
........................ check: r-spatstat
........................ check: r-bbmisc
........................ check: r-beanplot
......................... direct: r-beanplot -> {'r-minfi'}
......................... check: r-minfi
........................ check: r-caroline
......................... direct: r-caroline -> {'r-construct'}
......................... check: r-construct
........................ check: r-readxl
........................ check: r-bsgenome-hsapiens-ucsc-hg19
........................ check: r-statnet-common
........................ check: r-a4reporting
........................ check: r-annaffy
........................ check: r-labeling
......................... direct: r-labeling -> {'r-scales'}
......................... check: r-scales
........................ check: r-testit
......................... direct: r-testit -> set()
........................ check: r-globals
........................ check: r-flexclust
........................ check: r-forecast
........................ check: r-somaticsignatures
........................ check: r-proj4
......................... direct: r-proj4 -> set()
........................ check: r-rstudioapi
......................... direct: r-rstudioapi -> {'r-devtools', 'r-pkgload', 'r-htmltable', 'r-usethis', 'r-tidyverse', 'r-diagrammer'}
......................... check: r-devtools
......................... check: r-pkgload
......................... check: r-htmltable
......................... check: r-usethis
......................... check: r-tidyverse
......................... check: r-diagrammer
........................ check: r-mvtnorm
......................... direct: r-mvtnorm -> {'r-multcomp', 'r-copula', 'r-pcapp', 'r-geiger', 'vegas2', 'r-tmixclust', 'r-genetics', 'r-libcoin', 'r-party', 'r-rrcov', 'r-xde', 'r-coin', 'r-ks', 'r-fpc', 'r-partykit'}
......................... check: r-multcomp
......................... check: r-copula
......................... check: r-pcapp
......................... check: r-geiger
......................... check: vegas2
......................... check: r-tmixclust
......................... check: r-genetics
......................... check: r-libcoin
.......................... direct: r-libcoin -> {'r-coin', 'r-inum', 'r-partykit'}
.......................... check: r-coin
.......................... check: r-inum
.......................... check: r-partykit
......................... check: r-party
......................... check: r-rrcov
......................... check: r-xde
......................... check: r-coin
......................... check: r-ks
......................... check: r-fpc
......................... check: r-partykit
........................ check: r-convevol
........................ check: r-parallelmap
........................ check: r-rgdal
........................ check: r-seurat
........................ check: r-ade4
......................... direct: r-ade4 -> {'r-adegenet', 'r-phyloseq', 'r-seqinr'}
......................... check: r-adegenet
......................... check: r-phyloseq
......................... check: r-seqinr
.......................... direct: r-seqinr -> {'r-adegenet', 'r-condop', 'r-allelicimbalance'}
.......................... check: r-adegenet
.......................... check: r-condop
.......................... check: r-allelicimbalance
........................ check: r-matr
........................ check: r-affyilm
........................ check: r-gosemsim
........................ check: r-rferns
......................... direct: r-rferns -> set()
........................ check: r-sparsem
......................... direct: r-sparsem -> {'r-rms', 'r-topgo', 'r-quantreg'}
......................... check: r-rms
......................... check: r-topgo
......................... check: r-quantreg
........................ check: r-bitops
......................... direct: r-bitops -> {'r-rsamtools', 'r-ggmap', 'r-catools', 'r-phantompeakqualtools', 'r-rcurl', 'r-bfastspatial'}
......................... check: r-rsamtools
......................... check: r-ggmap
......................... check: r-catools
.......................... direct: r-catools -> {'r-rmarkdown', 'r-phantompeakqualtools', 'r-tfbstools', 'r-sseq', 'r-gplots'}
.......................... check: r-rmarkdown
.......................... check: r-phantompeakqualtools
.......................... check: r-tfbstools
.......................... check: r-sseq
.......................... check: r-gplots
......................... check: r-phantompeakqualtools
......................... check: r-rcurl
......................... check: r-bfastspatial
........................ check: r-smoof
........................ check: r-curl
......................... direct: r-curl -> {'r-ensembldb', 'r-crul', 'r-httr', 'r-ttr', 'r-biomartr', 'r-biocfilecache', 'r-quantmod', 'r-rio', 'r-usethis', 'r-phylostratr', 'r-taxizedb', 'r-rversions', 'r-magick', 'r-annotationhub'}
......................... check: r-ensembldb
......................... check: r-crul
......................... check: r-httr
......................... check: r-ttr
......................... check: r-biomartr
......................... check: r-biocfilecache
......................... check: r-quantmod
......................... check: r-rio
......................... check: r-usethis
......................... check: r-phylostratr
......................... check: r-taxizedb
......................... check: r-rversions
......................... check: r-magick
......................... check: r-annotationhub
........................ check: r-txdb-hsapiens-ucsc-hg19-knowngene
........................ check: r-genelendatabase
........................ check: r-altcdfenvs
........................ check: r-mapproj
......................... direct: r-mapproj -> {'r-ggmap'}
......................... check: r-ggmap
........................ check: r-mitml
........................ check: r-gmodels
........................ check: r-gdsfmt
......................... direct: r-gdsfmt -> {'snphylo', 'r-snprelate'}
......................... check: snphylo
......................... check: r-snprelate
........................ check: r-varselrf
......................... direct: r-varselrf -> {'r-a4classif'}
......................... check: r-a4classif
........................ check: r-chron
......................... direct: r-chron -> {'r-sqldf'}
......................... check: r-sqldf
........................ check: r-highr
......................... direct: r-highr -> {'r-knitr'}
......................... check: r-knitr
........................ check: r-partykit
........................ check: r-manipulatewidget
........................ check: r-robust
........................ check: r-snow
......................... direct: r-snow -> {'r-phantompeakqualtools', 'r-dosnow', 'r-biocparallel', 'r-snowfall'}
......................... check: r-phantompeakqualtools
......................... check: r-dosnow
......................... check: r-biocparallel
......................... check: r-snowfall
.......................... direct: r-snowfall -> {'r-phantompeakqualtools'}
.......................... check: r-phantompeakqualtools
........................ check: r-iterators
......................... direct: r-iterators -> {'r-mzid', 'r-dorng', 'r-domc', 'r-quantro', 'r-doparallel', 'r-bumphunter', 'r-foreach', 'r-dosnow'}
......................... check: r-mzid
......................... check: r-dorng
......................... check: r-domc
......................... check: r-quantro
......................... check: r-doparallel
......................... check: r-bumphunter
......................... check: r-foreach
......................... check: r-dosnow
........................ check: r-exactextractr
........................ check: r-ellipsis
......................... direct: r-ellipsis -> {'r-vctrs', 'r-testthat', 'r-forcats'}
......................... check: r-vctrs
......................... check: r-testthat
......................... check: r-forcats
........................ check: r-mapplots
......................... direct: r-mapplots -> {'r-gofuncr'}
......................... check: r-gofuncr
........................ check: r-topgo
........................ check: r-sqldf
........................ check: r-edger
........................ check: r-ggdendro
........................ check: r-argparse
........................ check: r-adegenet
........................ check: r-rcppcnpy
......................... direct: r-rcppcnpy -> set()
........................ check: r-fansi
......................... direct: r-fansi -> {'r-tibble', 'r-cli', 'r-pillar'}
......................... check: r-tibble
......................... check: r-cli
......................... check: r-pillar
........................ check: r-raster
........................ check: r-expm
........................ check: r-acme
......................... direct: r-acme -> set()
........................ check: r-rversions
........................ check: r-pacman
........................ check: r-kegggraph
........................ check: r-nnet
......................... direct: r-nnet -> {'r-corhmm', 'r-flexmix', 'r-chemometrics', 'r-rminer', 'r-mice', 'r-forecast', 'r-ipred', 'r-car', 'r-hmisc'}
......................... check: r-corhmm
......................... check: r-flexmix
......................... check: r-chemometrics
......................... check: r-rminer
......................... check: r-mice
......................... check: r-forecast
......................... check: r-ipred
......................... check: r-car
......................... check: r-hmisc
........................ check: r-qvalue
........................ check: r-tidyr
........................ check: r-biocparallel
........................ check: r-tfmpvalue
......................... direct: r-tfmpvalue -> {'r-tfbstools'}
......................... check: r-tfbstools
........................ check: r-rhtslib
......................... direct: r-rhtslib -> {'r-variantannotation', 'r-bamsignals', 'r-rsamtools'}
......................... check: r-variantannotation
......................... check: r-bamsignals
......................... check: r-rsamtools
........................ check: r-gdata
........................ check: r-foreign
......................... direct: r-foreign -> {'r-rio', 'r-maptools', 'r-psych', 'r-hmisc'}
......................... check: r-rio
......................... check: r-maptools
......................... check: r-psych
......................... check: r-hmisc
........................ check: r-readr
........................ check: r-shinydashboard
........................ check: r-picante
........................ check: r-stringi
......................... direct: r-stringi -> {'r-mlr', 'r-stringr', 'r-tidyr', 'r-xgboost', 'r-pkgmaker', 'r-snakecase', 'r-roxygen2'}
......................... check: r-mlr
......................... check: r-stringr
......................... check: r-tidyr
......................... check: r-xgboost
......................... check: r-pkgmaker
......................... check: r-snakecase
......................... check: r-roxygen2
........................ check: r-utf8
......................... direct: r-utf8 -> {'r-pillar'}
......................... check: r-pillar
........................ check: r-sys
......................... direct: r-sys -> {'r-askpass'}
......................... check: r-askpass
........................ check: r-multcomp
........................ check: eq-r
........................ check: r-mda
......................... direct: r-mda -> {'r-rminer'}
......................... check: r-rminer
........................ check: r-nnls
......................... direct: r-nnls -> {'r-als'}
......................... check: r-als
........................ check: r-hoardr
........................ check: r-simpleaffy
........................ check: r-findpython
......................... direct: r-findpython -> {'r-argparse'}
......................... check: r-argparse
........................ check: r-ggpubr
........................ check: r-maptools
........................ check: r-lhs
......................... direct: r-lhs -> {'r-mlrmbo'}
......................... check: r-mlrmbo
........................ check: r-sitmo
......................... direct: r-sitmo -> {'r-dqrng'}
......................... check: r-dqrng
.......................... direct: r-dqrng -> {'r-uwot'}
.......................... check: r-uwot
........................ check: r-shape
......................... direct: r-shape -> {'r-circlize'}
......................... check: r-circlize
.......................... direct: r-circlize -> {'r-complexheatmap', 'r-gtrellis', 'r-yapsa'}
.......................... check: r-complexheatmap
.......................... check: r-gtrellis
.......................... check: r-yapsa
........................ check: r-tiff
......................... direct: r-tiff -> {'r-readbitmap'}
......................... check: r-readbitmap
........................ check: r-ada
......................... direct: r-ada -> set()
........................ check: r-proto
......................... direct: r-proto -> {'r-sqldf', 'r-gsubfn', 'r-ggmap', 'r-argparse'}
......................... check: r-sqldf
......................... check: r-gsubfn
.......................... direct: r-gsubfn -> {'r-sqldf'}
.......................... check: r-sqldf
......................... check: r-ggmap
......................... check: r-argparse
........................ check: r-delayedarray
........................ check: r-quadprog
......................... direct: r-quadprog -> {'r-minfi', 'r-tseries', 'r-np', 'r-phangorn'}
......................... check: r-minfi
......................... check: r-tseries
......................... check: r-np
......................... check: r-phangorn
........................ check: r-ampliqueso
........................ check: r-ggsci
........................ check: r-gtable
......................... direct: r-gtable -> {'r-ggally', 'r-ggbio', 'r-ggforce', 'r-gridextra', 'r-ggraph', 'r-cowplot', 'r-hmisc', 'r-ggplot2'}
......................... check: r-ggally
......................... check: r-ggbio
......................... check: r-ggforce
......................... check: r-gridextra
......................... check: r-ggraph
......................... check: r-cowplot
......................... check: r-hmisc
......................... check: r-ggplot2
........................ check: r-triebeard
......................... direct: r-triebeard -> {'r-urltools'}
......................... check: r-urltools
........................ check: r-annotate
........................ check: r-gostats
........................ check: r-snpstats
........................ check: r-ggforce
........................ check: r-ncdf4
........................ check: r-combinat
......................... direct: r-combinat -> {'r-phytools', 'r-genetics'}
......................... check: r-phytools
......................... check: r-genetics
........................ check: r-packrat
......................... direct: r-packrat -> set()
........................ check: root
........................ check: r-rpart
......................... direct: r-rpart -> {'r-ada', 'r-chemometrics', 'r-rminer', 'r-mice', 'r-ipred', 'r-mlinterfaces', 'r-hmisc', 'r-rms', 'r-partykit', 'r-rpart-plot', 'r-spatstat', 'r-adabag'}
......................... check: r-ada
......................... check: r-chemometrics
......................... check: r-rminer
......................... check: r-mice
......................... check: r-ipred
......................... check: r-mlinterfaces
......................... check: r-hmisc
......................... check: r-rms
......................... check: r-partykit
......................... check: r-rpart-plot
.......................... direct: r-rpart-plot -> set()
......................... check: r-spatstat
......................... check: r-adabag
........................ check: r-paramhelpers
........................ check: r-hwriter
......................... direct: r-hwriter -> {'r-affycoretools', 'r-mlinterfaces', 'r-reportingtools', 'r-shortread'}
......................... check: r-affycoretools
......................... check: r-mlinterfaces
......................... check: r-reportingtools
......................... check: r-shortread
........................ check: r-randomglm
........................ check: r-rstantools
........................ check: r-ggvis
........................ check: r-rcppcctz
......................... direct: r-rcppcctz -> {'r-nanotime'}
......................... check: r-nanotime
........................ check: r-tarifx
........................ check: r-variantannotation
........................ check: r-optparse
........................ check: r-glmnet
........................ check: r-mergemaid
........................ check: r-data-table
......................... direct: r-data-table -> {'r-splitstackshape', 'r-isdparser', 'r-mlr', 'r-mlrmbo', 'r-phyloseq', 'r-minfi', 'r-abaenrichment', 'r-plotly', 'r-biomartr', 'r-modelmetrics', 'r-xgboost', 'r-rio', 'r-scater', 'r-hmisc', 'r-fgsea'}
......................... check: r-splitstackshape
......................... check: r-isdparser
......................... check: r-mlr
......................... check: r-mlrmbo
......................... check: r-phyloseq
......................... check: r-minfi
......................... check: r-abaenrichment
......................... check: r-plotly
......................... check: r-biomartr
......................... check: r-modelmetrics
.......................... direct: r-modelmetrics -> {'r-caret'}
.......................... check: r-caret
......................... check: r-xgboost
......................... check: r-rio
......................... check: r-scater
......................... check: r-hmisc
......................... check: r-fgsea
........................ check: r-processx
........................ check: r-car
........................ check: r-sva
........................ check: r-future-apply
........................ check: r-compositions
........................ check: r-geor
........................ check: r-debugme
........................ check: r-europepmc
........................ check: r-ucminf
......................... direct: r-ucminf -> {'r-ordinal'}
......................... check: r-ordinal
........................ check: r-rmarkdown
........................ check: r-tensora
......................... direct: r-tensora -> {'r-compositions', 'r-mcmcglmm'}
......................... check: r-compositions
......................... check: r-mcmcglmm
........................ check: r-snowfall
........................ check: r-leaps
......................... direct: r-leaps -> {'r-factominer'}
......................... check: r-factominer
........................ check: r-nleqslv
......................... direct: r-nleqslv -> {'r-lumi', 'r-vfs'}
......................... check: r-lumi
......................... check: r-vfs
.......................... direct: r-vfs -> set()
........................ check: r-randomforest
......................... direct: r-randomforest -> {'r-condop', 'r-rminer', 'r-varselrf'}
......................... check: r-condop
......................... check: r-rminer
......................... check: r-varselrf
........................ check: r-rmpfr
........................ check: sbml
........................ check: r-globaloptions
......................... direct: r-globaloptions -> {'r-complexheatmap', 'r-circlize', 'r-getoptlong'}
......................... check: r-complexheatmap
......................... check: r-circlize
......................... check: r-getoptlong
........................ check: cleaveland4
......................... direct: cleaveland4 -> set()
........................ check: r-ldheatmap
........................ check: r-testthat
........................ check: r-spem
......................... direct: r-spem -> {'r-tmixclust'}
......................... check: r-tmixclust
........................ check: snphylo
........................ check: r-a4
........................ check: r-pbdzmq
........................ check: r-ranger
........................ check: r-rvest
........................ check: r-fdb-infiniummethylation-hg19
........................ check: r-decipher
........................ check: r-rcmdcheck
........................ check: r-pamr
........................ check: r-formula
......................... direct: r-formula -> {'r-earth', 'r-lfe', 'r-aer', 'r-hmisc', 'r-plotmo', 'r-partykit'}
......................... check: r-earth
......................... check: r-lfe
......................... check: r-aer
......................... check: r-hmisc
......................... check: r-plotmo
.......................... direct: r-plotmo -> {'r-earth'}
.......................... check: r-earth
......................... check: r-partykit
........................ check: r-gplots
........................ check: r-bayesm
........................ check: r-ggjoy
........................ check: r-sseq
........................ check: r-absseq
........................ check: r-rmutil
......................... direct: r-rmutil -> set()
........................ check: r-ergm
........................ check: orthofiller
........................ check: r-rematch2
........................ check: r-scatterplot3d
......................... direct: r-scatterplot3d -> {'r-phytools', 'r-diffusionmap', 'r-factominer'}
......................... check: r-phytools
......................... check: r-diffusionmap
......................... check: r-factominer
........................ check: r-biocinstaller
......................... direct: r-biocinstaller -> {'r-organismdbi', 'r-gcrma', 'r-affy', 'r-oligoclasses', 'r-annotationhub'}
......................... check: r-organismdbi
......................... check: r-gcrma
......................... check: r-affy
......................... check: r-oligoclasses
......................... check: r-annotationhub
........................ check: r-deoptim
......................... direct: r-deoptim -> set()
........................ check: r-keggrest
........................ check: r-agimicrorna
........................ check: r-digest
......................... direct: r-digest -> {'r-geiger', 'r-plotly', 'r-memoise', 'r-future', 'r-knitr', 'r-nmf', 'r-vctrs', 'r-hoardr', 'r-covr', 'r-rbokeh', 'r-ggraph', 'r-testthat', 'r-ggplot2', 'r-rngtools', 'r-shiny', 'r-downloader', 'r-gviz', 'r-rcmdcheck', 'r-roxygen2', 'r-htmltools', 'r-devtools', 'r-msnbase', 'r-irkernel', 'r-ggmap', 'r-pkgmaker', 'r-r-cache'}
......................... check: r-geiger
......................... check: r-plotly
......................... check: r-memoise
......................... check: r-future
......................... check: r-knitr
......................... check: r-nmf
......................... check: r-vctrs
......................... check: r-hoardr
......................... check: r-covr
......................... check: r-rbokeh
......................... check: r-ggraph
......................... check: r-testthat
......................... check: r-ggplot2
......................... check: r-rngtools
......................... check: r-shiny
......................... check: r-downloader
......................... check: r-gviz
......................... check: r-rcmdcheck
......................... check: r-roxygen2
......................... check: r-htmltools
......................... check: r-devtools
......................... check: r-msnbase
......................... check: r-irkernel
......................... check: r-ggmap
......................... check: r-pkgmaker
......................... check: r-r-cache
........................ check: r-dosnow
........................ check: r-codex
........................ check: r-rpostgresql
......................... direct: r-rpostgresql -> {'r-taxizedb'}
......................... check: r-taxizedb
........................ check: r-scales
........................ check: r-multicool
......................... direct: r-multicool -> {'r-ks'}
......................... check: r-ks
........................ check: r-singlecellexperiment
........................ check: r-pcamethods
......................... direct: r-pcamethods -> {'r-somaticsignatures', 'r-msnbase'}
......................... check: r-somaticsignatures
......................... check: r-msnbase
........................ check: r-affydata
........................ check: r-tximport
......................... direct: r-tximport -> {'r-scater'}
......................... check: r-scater
........................ check: r-affxparser
......................... direct: r-affxparser -> {'r-affyilm'}
......................... check: r-affyilm
........................ check: r-dichromat
......................... direct: r-dichromat -> {'r-scales', 'r-biovizbase'}
......................... check: r-scales
......................... check: r-biovizbase
........................ check: r-polspline
......................... direct: r-polspline -> {'r-rms'}
......................... check: r-rms
........................ check: r-txdb-hsapiens-ucsc-hg18-knowngene
........................ check: r-tigris
........................ check: r-fdb-infiniummethylation-hg18
........................ check: r-genemeta
........................ check: r-colorspace
......................... direct: r-colorspace -> {'r-complexheatmap', 'r-copula', 'r-circlize', 'r-nmf', 'r-vcd', 'r-geiger', 'r-forecast', 'r-munsell'}
......................... check: r-complexheatmap
......................... check: r-copula
......................... check: r-circlize
......................... check: r-nmf
......................... check: r-vcd
......................... check: r-geiger
......................... check: r-forecast
......................... check: r-munsell
.......................... direct: r-munsell -> {'r-scales'}
.......................... check: r-scales
........................ check: r-emmli
......................... direct: r-emmli -> set()
........................ check: r-timedate
......................... direct: r-timedate -> {'r-recipes', 'r-forecast'}
......................... check: r-recipes
......................... check: r-forecast
........................ check: r-biomformat
........................ check: r-matlab
......................... direct: r-matlab -> set()
........................ check: r-knitr
........................ check: r-seqinr
........................ check: r-lambda-r
......................... direct: r-lambda-r -> {'r-futile-logger'}
......................... check: r-futile-logger
........................ check: r-rlang
......................... direct: r-rlang -> {'r-tidyr', 'r-ggbio', 'r-seurat', 'r-plotly', 'r-phylostratr', 'r-biovizbase', 'r-dbplyr', 'r-ggally', 'r-tibble', 'r-tidygraph', 'r-vctrs', 'r-ggraph', 'r-ggpubr', 'r-tidyverse', 'r-usethis', 'r-testthat', 'r-ggplot2', 'r-later', 'r-pillar', 'r-shiny', 'r-pkgload', 'r-purrr', 'r-rhmmer', 'r-promises', 'r-mice', 'r-ellipsis', 'r-blob', 'r-cowplot', 'r-modelr', 'r-rvcheck', 'r-roxygen2', 'r-forcats', 'r-ergm', 'r-diagrammer', 'r-dplyr', 'r-ggforce', 'r-hms', 'r-janitor', 'r-reprex', 'r-tidyselect', 'r-recipes', 'r-tweenr'}
......................... check: r-tidyr
......................... check: r-ggbio
......................... check: r-seurat
......................... check: r-plotly
......................... check: r-phylostratr
......................... check: r-biovizbase
......................... check: r-dbplyr
......................... check: r-ggally
......................... check: r-tibble
......................... check: r-tidygraph
......................... check: r-vctrs
......................... check: r-ggraph
......................... check: r-ggpubr
......................... check: r-tidyverse
......................... check: r-usethis
......................... check: r-testthat
......................... check: r-ggplot2
......................... check: r-later
......................... check: r-pillar
......................... check: r-shiny
......................... check: r-pkgload
......................... check: r-purrr
......................... check: r-rhmmer
......................... check: r-promises
......................... check: r-mice
......................... check: r-ellipsis
......................... check: r-blob
......................... check: r-cowplot
......................... check: r-modelr
......................... check: r-rvcheck
.......................... direct: r-rvcheck -> {'r-dose', 'r-clusterprofiler', 'r-ggplotify'}
.......................... check: r-dose
.......................... check: r-clusterprofiler
.......................... check: r-ggplotify
......................... check: r-roxygen2
......................... check: r-forcats
......................... check: r-ergm
......................... check: r-diagrammer
......................... check: r-dplyr
......................... check: r-ggforce
......................... check: r-hms
......................... check: r-janitor
......................... check: r-reprex
......................... check: r-tidyselect
......................... check: r-recipes
......................... check: r-tweenr
.......................... direct: r-tweenr -> {'r-ggforce'}
.......................... check: r-ggforce
........................ check: r-ttr
........................ check: r-splancs
........................ check: r-makecdfenv
........................ check: r-registry
......................... direct: r-registry -> {'r-pkgmaker', 'r-nmf'}
......................... check: r-pkgmaker
......................... check: r-nmf
........................ check: r-mclust
......................... direct: r-mclust -> {'r-chemometrics', 'r-minfi', 'r-aneufinder', 'r-condop', 'r-prabclus', 'r-ks', 'r-fpc'}
......................... check: r-chemometrics
......................... check: r-minfi
......................... check: r-aneufinder
......................... check: r-condop
......................... check: r-prabclus
......................... check: r-ks
......................... check: r-fpc
........................ check: r-nor1mix
......................... direct: r-nor1mix -> {'r-minfi'}
......................... check: r-minfi
........................ check: r-rgooglemaps
........................ check: r-xlsxjars
........................ check: r-geonames
........................ check: r-matrix
........................ check: r-multitaper
......................... direct: r-multitaper -> set()
........................ check: r-segmented
......................... direct: r-segmented -> {'r-seqinr', 'r-mixtools'}
......................... check: r-seqinr
......................... check: r-mixtools
........................ check: r-mlbench
......................... direct: r-mlbench -> {'r-mlinterfaces', 'r-adabag'}
......................... check: r-mlinterfaces
......................... check: r-adabag
........................ check: r-xlsx
........................ check: r-yaml
......................... direct: r-yaml -> {'r-htmlwidgets', 'r-rmarkdown', 'r-covr', 'r-usethis', 'r-biocstyle', 'r-bookdown', 'r-knitr', 'r-annotationhub'}
......................... check: r-htmlwidgets
......................... check: r-rmarkdown
......................... check: r-covr
......................... check: r-usethis
......................... check: r-biocstyle
......................... check: r-bookdown
......................... check: r-knitr
......................... check: r-annotationhub
........................ check: r-bh
......................... direct: r-bh -> {'r-construct', 'r-dplyr', 'r-rstan', 'r-xml2', 'r-biocparallel', 'r-rmariadb', 'r-rsqlite', 'r-readr', 'r-dqrng', 'r-rcppblaze', 'r-rbgl', 'r-fgsea', 'r-later'}
......................... check: r-construct
......................... check: r-dplyr
......................... check: r-rstan
......................... check: r-xml2
......................... check: r-biocparallel
......................... check: r-rmariadb
......................... check: r-rsqlite
......................... check: r-readr
......................... check: r-dqrng
......................... check: r-rcppblaze
......................... check: r-rbgl
......................... check: r-fgsea
......................... check: r-later
........................ check: r-haven
........................ check: r-leaflet
........................ check: r-modelr
........................ check: r-rcpp
......................... direct: r-rcpp -> {'r-construct', 'r-bamsignals', 'r-rcppeigen', 'r-graphlayouts', 'r-tidyr', 'r-geiger', 'r-sf', 'r-tfmpvalue', 'r-ecp', 'r-imager', 'r-readr', 'r-scater', 'r-dqrng', 'r-rsnns', 'r-biocneighbors', 'eq-r', 'r-cubature', 'r-tibble', 'r-deseq2', 'r-minqa', 'r-lhs', 'r-sitmo', 'r-later', 'r-rcpparmadillo', 'r-urltools', 'r-triebeard', 'r-wgcna', 'r-energy', 'r-ggforce', 'root', 'r-rtsne', 'r-tidyselect', 'r-rcppcctz', 'r-magick', 'r-lubridate', 'r-modelmetrics', 'r-grbase', 'r-plyr', 'r-ldheatmap', 'r-xml2', 'r-rcppprogress', 'r-ranger', 'r-rspectra', 'r-promises', 'r-bio3d', 'r-gwmodel', 'r-bayesm', 'r-lme4', 'r-roxygen2', 'r-openxlsx', 'r-httpuv', 'r-dplyr', 'r-rinside', 'r-scales', 'r-multicool', 'r-pcamethods', 'r-fgsea', 'r-fs', 'r-ggrepel', 'r-diversitree', 'r-gofuncr', 'r-mzr', 'r-prodlim', 'r-philentropy', 'r-rcppblaze', 'r-phangorn', 'r-rcpphnsw', 'r-pryr', 'r-rcppannoy', 'r-tidygraph', 'r-units', 'r-sctransform', 'r-reticulate', 'r-abaenrichment', 'r-rsqlite', 'r-mice', 'r-haven', 'r-readxl', 'r-dada2', 'r-msnbase', 'r-rstan', 'r-rmariadb', 'r-forecast', 'r-biocsingular', 'r-uwot', 'r-tweenr', 'r-reordercluster', 'r-seurat', 'r-gosemsim', 'r-rots', 'r-smoof', 'r-ape', 'r-ggraph', 'r-beachmat', 'r-exactextractr', 'r-edger', 'r-htmltools', 'r-rcppcnpy', 'r-raster', 'r-reshape2', 'r-bindrcpp'}
......................... check: r-construct
......................... check: r-bamsignals
......................... check: r-rcppeigen
......................... check: r-graphlayouts
......................... check: r-tidyr
......................... check: r-geiger
......................... check: r-sf
......................... check: r-tfmpvalue
......................... check: r-ecp
.......................... direct: r-ecp -> {'r-aneufinder'}
.......................... check: r-aneufinder
......................... check: r-imager
......................... check: r-readr
......................... check: r-scater
......................... check: r-dqrng
......................... check: r-rsnns
......................... check: r-biocneighbors
......................... check: eq-r
......................... check: r-cubature
.......................... direct: r-cubature -> {'r-np', 'r-mcmcglmm'}
.......................... check: r-np
.......................... check: r-mcmcglmm
......................... check: r-tibble
......................... check: r-deseq2
......................... check: r-minqa
......................... check: r-lhs
......................... check: r-sitmo
......................... check: r-later
......................... check: r-rcpparmadillo
......................... check: r-urltools
......................... check: r-triebeard
......................... check: r-wgcna
......................... check: r-energy
......................... check: r-ggforce
......................... check: root
......................... check: r-rtsne
.......................... direct: r-rtsne -> {'r-seurat'}
.......................... check: r-seurat
......................... check: r-tidyselect
......................... check: r-rcppcctz
......................... check: r-magick
......................... check: r-lubridate
......................... check: r-modelmetrics
......................... check: r-grbase
......................... check: r-plyr
......................... check: r-ldheatmap
......................... check: r-xml2
......................... check: r-rcppprogress
......................... check: r-ranger
......................... check: r-rspectra
......................... check: r-promises
......................... check: r-bio3d
.......................... direct: r-bio3d -> set()
......................... check: r-gwmodel
......................... check: r-bayesm
......................... check: r-lme4
......................... check: r-roxygen2
......................... check: r-openxlsx
......................... check: r-httpuv
......................... check: r-dplyr
......................... check: r-rinside
.......................... direct: r-rinside -> {'turbine', 'eq-r', 'root'}
.......................... check: turbine
.......................... check: eq-r
.......................... check: root
......................... check: r-scales
......................... check: r-multicool
......................... check: r-pcamethods
......................... check: r-fgsea
......................... check: r-fs
......................... check: r-ggrepel
......................... check: r-diversitree
......................... check: r-gofuncr
......................... check: r-mzr
......................... check: r-prodlim
......................... check: r-philentropy
......................... check: r-rcppblaze
......................... check: r-phangorn
......................... check: r-rcpphnsw
......................... check: r-pryr
......................... check: r-rcppannoy
......................... check: r-tidygraph
......................... check: r-units
......................... check: r-sctransform
......................... check: r-reticulate
......................... check: r-abaenrichment
......................... check: r-rsqlite
......................... check: r-mice
......................... check: r-haven
......................... check: r-readxl
......................... check: r-dada2
......................... check: r-msnbase
......................... check: r-rstan
......................... check: r-rmariadb
......................... check: r-forecast
......................... check: r-biocsingular
......................... check: r-uwot
......................... check: r-tweenr
......................... check: r-reordercluster
......................... check: r-seurat
......................... check: r-gosemsim
......................... check: r-rots
.......................... direct: r-rots -> {'trinity'}
.......................... check: trinity
......................... check: r-smoof
......................... check: r-ape
......................... check: r-ggraph
......................... check: r-beachmat
......................... check: r-exactextractr
......................... check: r-edger
......................... check: r-htmltools
......................... check: r-rcppcnpy
......................... check: r-raster
......................... check: r-reshape2
......................... check: r-bindrcpp
........................ check: r-rhdf5lib
......................... direct: r-rhdf5lib -> {'r-beachmat', 'r-mzr', 'r-rhdf5', 'r-hdf5array', 'r-scater'}
......................... check: r-beachmat
......................... check: r-mzr
......................... check: r-rhdf5
.......................... direct: r-rhdf5 -> {'r-hdf5array', 'r-biomformat', 'r-beachmat', 'r-scater'}
.......................... check: r-hdf5array
.......................... check: r-biomformat
.......................... check: r-beachmat
.......................... check: r-scater
......................... check: r-hdf5array
......................... check: r-scater
........................ check: r-quantmod
........................ check: r-htmlwidgets
........................ check: r-anaquin
........................ check: r-kegg-db
........................ check: r-genomicfeatures
........................ check: r-devtools
........................ check: r-stabledist
......................... direct: r-stabledist -> {'r-copula'}
......................... check: r-copula
........................ check: r-msnbase
........................ check: r-rmariadb
........................ check: r-htmltable
........................ check: r-reprex
........................ check: r-mass
......................... direct: r-mass -> {'r-phytools', 'r-convevol', 'r-spatialreg', 'r-gamlss', 'r-pbkrtest', 'r-ade4', 'r-geiger', 'r-mergemaid', 'r-fitdistrplus', 'r-jomo', 'r-seurat', 'r-car', 'r-speedglm', 'r-geor', 'r-prabclus', 'r-fpc', 'r-mixtools', 'r-lumi', 'r-vcd', 'r-gamlss-dist', 'r-clustergeneration', 'r-spatial', 'r-rminer', 'r-spdep', 'r-multtest', 'r-gmodels', 'r-vegan', 'r-sctransform', 'r-ggraph', 'r-genetics', 'r-ggplot2', 'r-robust', 'r-factominer', 'r-mice', 'r-ipred', 'r-deseq', 'r-envstats', 'r-mlinterfaces', 'r-class', 'r-mpm', 'r-spatialeco', 'r-lme4', 'r-th-data', 'r-ggdendro', 'r-ergm', 'r-adegenet', 'r-ordinal', 'r-chemometrics', 'r-msnbase', 'r-minfi', 'r-ggforce', 'r-randomglm', 'r-pcamethods'}
......................... check: r-phytools
......................... check: r-convevol
......................... check: r-spatialreg
......................... check: r-gamlss
......................... check: r-pbkrtest
......................... check: r-ade4
......................... check: r-geiger
......................... check: r-mergemaid
......................... check: r-fitdistrplus
......................... check: r-jomo
......................... check: r-seurat
......................... check: r-car
......................... check: r-speedglm
......................... check: r-geor
......................... check: r-prabclus
......................... check: r-fpc
......................... check: r-mixtools
......................... check: r-lumi
......................... check: r-vcd
......................... check: r-gamlss-dist
......................... check: r-clustergeneration
......................... check: r-spatial
......................... check: r-rminer
......................... check: r-spdep
......................... check: r-multtest
......................... check: r-gmodels
......................... check: r-vegan
......................... check: r-sctransform
......................... check: r-ggraph
......................... check: r-genetics
......................... check: r-ggplot2
......................... check: r-robust
......................... check: r-factominer
......................... check: r-mice
......................... check: r-ipred
......................... check: r-deseq
......................... check: r-envstats
......................... check: r-mlinterfaces
......................... check: r-class
.......................... direct: r-class -> {'r-mda', 'r-classint', 'r-chemometrics', 'r-flexclust', 'r-ipred', 'r-e1071', 'r-fpc'}
.......................... check: r-mda
.......................... check: r-classint
.......................... check: r-chemometrics
.......................... check: r-flexclust
.......................... check: r-ipred
.......................... check: r-e1071
........................... direct: r-e1071 -> {'r-classint', 'r-chemometrics', 'r-aims', 'r-seurat', 'r-rminer', 'r-vfs'}
........................... check: r-classint
........................... check: r-chemometrics
........................... check: r-aims
........................... check: r-seurat
........................... check: r-rminer
........................... check: r-vfs
.......................... check: r-fpc
......................... check: r-mpm
......................... check: r-spatialeco
......................... check: r-lme4
......................... check: r-th-data
......................... check: r-ggdendro
......................... check: r-ergm
......................... check: r-adegenet
......................... check: r-ordinal
......................... check: r-chemometrics
......................... check: r-msnbase
......................... check: r-minfi
......................... check: r-ggforce
......................... check: r-randomglm
......................... check: r-pcamethods
........................ check: r-xgboost
........................ check: r-desc
........................ check: r-e1071
........................ check: r-maps
......................... direct: r-maps -> {'r-phytools', 'r-rbokeh', 'r-mapproj', 'r-fields'}
......................... check: r-phytools
......................... check: r-rbokeh
......................... check: r-mapproj
......................... check: r-fields
.......................... direct: r-fields -> set()
........................ check: r-uwot
........................ check: r-farver
......................... direct: r-farver -> {'r-tweenr'}
......................... check: r-tweenr
........................ check: r-annotationhub
........................ check: r-diptest
......................... direct: r-diptest -> {'r-fpc'}
......................... check: r-fpc
........................ check: r-spatstat-data
........................ check: r-aldex2
........................ check: kmergenie
......................... direct: kmergenie -> set()
........................ check: r-xmapbridge
......................... direct: r-xmapbridge -> set()
........................ check: r-latticeextra
........................ check: gatk
......................... direct: gatk -> {'phyluce'}
......................... check: phyluce
........................ check: r-fields
........................ check: r-phylostratr
........................ check: r-gdalutils
........................ check: r-log4r
......................... direct: r-log4r -> {'r-analysispageserver'}
......................... check: r-analysispageserver
........................ check: r-rnaseqmap
........................ check: r-learnbayes
......................... direct: r-learnbayes -> {'r-spatialreg', 'r-spdep'}
......................... check: r-spatialreg
......................... check: r-spdep
........................ check: r-rjags
........................ check: r-multtest
........................ check: r-spdata
......................... direct: r-spdata -> {'r-spatialreg', 'r-spdep'}
......................... check: r-spatialreg
......................... check: r-spdep
........................ check: r-nmof
......................... direct: r-nmof -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-generics
......................... direct: r-generics -> {'r-recipes', 'r-broom'}
......................... check: r-recipes
......................... check: r-broom
........................ check: r-blockmodeling
........................ check: r-ggraph
........................ check: r-rsolnp
......................... direct: r-rsolnp -> {'r-spem'}
......................... check: r-spem
........................ check: r-rdpack
........................ check: r-bfast
........................ check: r-openssl
........................ check: r-urca
........................ check: r-rann
......................... direct: r-rann -> {'r-spatialeco', 'r-seurat'}
......................... check: r-spatialeco
......................... check: r-seurat
........................ check: r-spam
......................... direct: r-spam -> {'r-fields'}
......................... check: r-fields
........................ check: r-matrixmodels
........................ check: r-c50
........................ check: r-spatialeco
........................ check: r-genie3
........................ check: r-getoptlong
........................ check: r-rvcheck
........................ check: r-loo
........................ check: r-callr
........................ check: r-rprojroot
......................... direct: r-rprojroot -> {'r-rmarkdown', 'r-pkgbuild', 'r-pkgload', 'r-rcmdcheck', 'r-usethis', 'r-desc'}
......................... check: r-rmarkdown
......................... check: r-pkgbuild
......................... check: r-pkgload
......................... check: r-rcmdcheck
......................... check: r-usethis
......................... check: r-desc
........................ check: r-adgoftest
......................... direct: r-adgoftest -> {'r-copula'}
......................... check: r-copula
........................ check: r-animation
........................ check: r-abadata
......................... direct: r-abadata -> {'r-abaenrichment'}
......................... check: r-abaenrichment
........................ check: r-clipr
......................... direct: r-clipr -> {'r-readr', 'r-reprex', 'r-usethis'}
......................... check: r-readr
......................... check: r-reprex
......................... check: r-usethis
........................ check: r-evaluate
........................ check: r-coda
........................ check: r-ptw
........................ check: r-truncdist
........................ check: r-coin
........................ check: r-construct
........................ check: r-bamsignals
........................ check: r-truncnorm
......................... direct: r-truncnorm -> {'r-rsolnp', 'r-bglr'}
......................... check: r-rsolnp
......................... check: r-bglr
........................ check: r-circlize
........................ check: r-ggbio
........................ check: r-delayedmatrixstats
........................ check: r-prettyunits
........................ check: r-ecp
........................ check: r-affy
........................ check: r-dqrng
........................ check: r-rmysql
........................ check: r-cubature
........................ check: r-biomart
........................ check: r-tibble
........................ check: r-bmp
......................... direct: r-bmp -> {'r-readbitmap'}
......................... check: r-readbitmap
........................ check: r-deseq2
........................ check: r-geneplotter
........................ check: r-biocmanager
......................... direct: r-biocmanager -> {'r-organismdbi', 'r-gcrma', 'r-affy', 'r-biocstyle', 'r-oligoclasses', 'r-annotationhub'}
......................... check: r-organismdbi
......................... check: r-gcrma
......................... check: r-affy
......................... check: r-biocstyle
......................... check: r-oligoclasses
......................... check: r-annotationhub
........................ check: r-vegan
........................ check: r-rio
........................ check: r-affycompatible
........................ check: r-clue
......................... direct: r-clue -> {'r-complexheatmap'}
......................... check: r-complexheatmap
........................ check: r-rpart-plot
........................ check: r-rngtools
........................ check: r-dbi
......................... direct: r-dbi -> {'r-annotationdbi', 'r-variantannotation', 'r-mitools', 'r-sf', 'r-biocfilecache', 'r-tfbstools', 'r-rmysql', 'r-taxizedb', 'r-rnaseqmap', 'r-lumi', 'r-dbplyr', 'r-annotationforge', 'r-affycoretools', 'r-category', 'r-rsqlite', 'r-decipher', 'r-topgo', 'r-sqldf', 'r-organismdbi', 'r-ensembldb', 'r-annaffy', 'r-genomicfeatures', 'r-annotate', 'r-rmariadb', 'r-rpostgresql', 'r-cner', 'r-oligoclasses'}
......................... check: r-annotationdbi
......................... check: r-variantannotation
......................... check: r-mitools
......................... check: r-sf
......................... check: r-biocfilecache
......................... check: r-tfbstools
......................... check: r-rmysql
......................... check: r-taxizedb
......................... check: r-rnaseqmap
......................... check: r-lumi
......................... check: r-dbplyr
......................... check: r-annotationforge
......................... check: r-affycoretools
......................... check: r-category
......................... check: r-rsqlite
......................... check: r-decipher
......................... check: r-topgo
......................... check: r-sqldf
......................... check: r-organismdbi
......................... check: r-ensembldb
......................... check: r-annaffy
......................... check: r-genomicfeatures
......................... check: r-annotate
......................... check: r-rmariadb
......................... check: r-rpostgresql
......................... check: r-cner
......................... check: r-oligoclasses
........................ check: r-mlrmbo
........................ check: r-xml
......................... direct: r-xml -> {'r-kegggraph', 'r-mzid', 'r-mlr', 'r-annotate', 'r-biomart', 'r-msnbase', 'r-annotationforge', 'r-rgexf', 'r-rtracklayer', 'r-geoquery', 'r-biomartr', 'r-pathview', 'r-tfbstools', 'r-gseabase', 'r-affycompatible', 'r-reportingtools', 'r-rnoaa'}
......................... check: r-kegggraph
......................... check: r-mzid
......................... check: r-mlr
......................... check: r-annotate
......................... check: r-biomart
......................... check: r-msnbase
......................... check: r-annotationforge
......................... check: r-rgexf
......................... check: r-rtracklayer
......................... check: r-geoquery
......................... check: r-biomartr
......................... check: r-pathview
......................... check: r-tfbstools
......................... check: r-gseabase
......................... check: r-affycompatible
......................... check: r-reportingtools
......................... check: r-rnoaa
........................ check: r-gridgraphics
......................... direct: r-gridgraphics -> {'r-ggplotify'}
......................... check: r-ggplotify
........................ check: r-biostrings
........................ check: r-viridis
........................ check: r-desolve
......................... direct: r-desolve -> {'r-diversitree', 'r-geiger'}
......................... check: r-diversitree
......................... check: r-geiger
........................ check: r-libcoin
........................ check: r-cardata
......................... direct: r-cardata -> {'r-car'}
......................... check: r-car
........................ check: r-plotrix
......................... direct: r-plotrix -> {'r-phytools', 'r-rminer', 'r-plotmo'}
......................... check: r-phytools
......................... check: r-rminer
......................... check: r-plotmo
........................ check: r-wgcna
........................ check: r-rgenoud
......................... direct: r-rgenoud -> set()
........................ check: r-xtable
......................... direct: r-xtable -> {'r-a4reporting', 'r-shiny', 'r-annotate', 'r-lfe', 'r-affycoretools', 'r-affyqcreport', 'r-pkgmaker', 'r-ampliqueso'}
......................... check: r-a4reporting
......................... check: r-shiny
......................... check: r-annotate
......................... check: r-lfe
......................... check: r-affycoretools
......................... check: r-affyqcreport
......................... check: r-pkgmaker
......................... check: r-ampliqueso
........................ check: r-irkernel
........................ check: r-xvector
........................ check: r-rtsne
........................ check: r-tinytex
........................ check: r-rrcov
........................ check: r-pscbs
........................ check: r-protgenerics
......................... direct: r-protgenerics -> {'r-mzid', 'r-ensembldb', 'r-mzr', 'r-msnbase'}
......................... check: r-mzid
......................... check: r-ensembldb
......................... check: r-mzr
......................... check: r-msnbase
........................ check: r-yaimpute
......................... direct: r-yaimpute -> {'r-spatialeco'}
......................... check: r-spatialeco
........................ check: r-rgraphviz
........................ check: r-subplex
......................... direct: r-subplex -> {'r-diversitree', 'r-geiger'}
......................... check: r-diversitree
......................... check: r-geiger
........................ check: r-magrittr
......................... direct: r-magrittr -> {'r-dygraphs', 'r-exomedepth', 'r-tidyr', 'r-tigris', 'r-prettyunits', 'r-plotly', 'r-sf', 'r-imager', 'r-phangorn', 'r-phylostratr', 'r-taxizedb', 'r-grbase', 'r-igraph', 'r-clusterprofiler', 'r-dendextend', 'r-tidygraph', 'r-webshot', 'r-rbokeh', 'r-recipes', 'r-ggpubr', 'r-tidyverse', 'r-testthat', 'r-dt', 'r-rex', 'r-visnetwork', 'r-rvest', 'r-purrr', 'r-rgl', 'r-rhmmer', 'r-promises', 'r-leaflet', 'r-modelr', 'r-forcats', 'r-diagrammer', 'r-dplyr', 'r-stringr', 'r-geoquery', 'r-ggmap', 'r-forecast', 'r-gistr', 'r-htmltable', 'r-janitor', 'r-network', 'r-networkd3', 'r-pkgmaker', 'r-xgboost', 'r-ggvis', 'r-magick', 'r-tweenr'}
......................... check: r-dygraphs
......................... check: r-exomedepth
......................... check: r-tidyr
......................... check: r-tigris
......................... check: r-prettyunits
......................... check: r-plotly
......................... check: r-sf
......................... check: r-imager
......................... check: r-phangorn
......................... check: r-phylostratr
......................... check: r-taxizedb
......................... check: r-grbase
......................... check: r-igraph
......................... check: r-clusterprofiler
......................... check: r-dendextend
......................... check: r-tidygraph
......................... check: r-webshot
......................... check: r-rbokeh
......................... check: r-recipes
......................... check: r-ggpubr
......................... check: r-tidyverse
......................... check: r-testthat
......................... check: r-dt
......................... check: r-rex
......................... check: r-visnetwork
......................... check: r-rvest
......................... check: r-purrr
......................... check: r-rgl
......................... check: r-rhmmer
......................... check: r-promises
......................... check: r-leaflet
......................... check: r-modelr
......................... check: r-forcats
......................... check: r-diagrammer
......................... check: r-dplyr
......................... check: r-stringr
......................... check: r-geoquery
......................... check: r-ggmap
......................... check: r-forecast
......................... check: r-gistr
......................... check: r-htmltable
......................... check: r-janitor
......................... check: r-network
......................... check: r-networkd3
......................... check: r-pkgmaker
......................... check: r-xgboost
......................... check: r-ggvis
......................... check: r-magick
......................... check: r-tweenr
........................ check: r-lubridate
........................ check: r-modelmetrics
........................ check: r-speedglm
........................ check: r-filehash
......................... direct: r-filehash -> set()
........................ check: r-alsace
........................ check: r-boruta
........................ check: r-rnoaa
........................ check: r-ks
........................ check: r-taxizedb
........................ check: r-biovizbase
........................ check: r-zlibbioc
......................... direct: r-zlibbioc -> {'r-bamsignals', 'r-rsamtools', 'r-variantannotation', 'r-rtracklayer', 'r-mzr', 'r-snpstats', 'r-rhdf5', 'r-xvector', 'r-makecdfenv', 'r-rhtslib', 'r-affyio', 'r-affy', 'r-shortread', 'r-affyplm'}
......................... check: r-bamsignals
......................... check: r-rsamtools
......................... check: r-variantannotation
......................... check: r-rtracklayer
......................... check: r-mzr
......................... check: r-snpstats
......................... check: r-rhdf5
......................... check: r-xvector
......................... check: r-makecdfenv
......................... check: r-rhtslib
......................... check: r-affyio
......................... check: r-affy
......................... check: r-shortread
......................... check: r-affyplm
........................ check: r-proj
......................... direct: r-proj -> set()
........................ check: r-geomorph
........................ check: r-gridbase
......................... direct: r-gridbase -> {'r-nmf'}
......................... check: r-nmf
........................ check: r-pan
......................... direct: r-pan -> {'r-mitml'}
......................... check: r-mitml
........................ check: r-illuminaio
........................ check: r-zip
......................... direct: r-zip -> {'r-openxlsx'}
......................... check: r-openxlsx
........................ check: r-spatialpack
......................... direct: r-spatialpack -> {'r-spatialeco'}
......................... check: r-spatialeco
........................ check: r-rematch
......................... direct: r-rematch -> {'r-cellranger'}
......................... check: r-cellranger
........................ check: r-rspectra
........................ check: r-crul
........................ check: r-rhmmer
........................ check: r-bio3d
........................ check: r-org-hs-eg-db
........................ check: r-class
........................ check: r-mpm
........................ check: r-cowplot
........................ check: turbine
........................ check: r-deoptimr
......................... direct: r-deoptimr -> {'r-robustbase'}
......................... check: r-robustbase
........................ check: r-tsne
......................... direct: r-tsne -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-rocr
........................ check: r-rzmq
......................... direct: r-rzmq -> set()
........................ check: r-pls
......................... direct: r-pls -> {'r-mlinterfaces', 'r-chemometrics', 'r-rminer'}
......................... check: r-mlinterfaces
......................... check: r-chemometrics
......................... check: r-rminer
........................ check: r-rinside
........................ check: r-rhdf5
........................ check: r-leiden
........................ check: r-hdf5array
........................ check: r-numderiv
......................... direct: r-numderiv -> {'r-corhmm', 'r-ordinal', 'r-phytools', 'r-copula', 'r-lava', 'r-sn', 'r-survey'}
......................... check: r-corhmm
......................... check: r-ordinal
......................... check: r-phytools
......................... check: r-copula
......................... check: r-lava
......................... check: r-sn
......................... check: r-survey
........................ check: r-party
........................ check: r-plotmo
........................ check: r-powerlaw
........................ check: r-squash
......................... direct: r-squash -> set()
........................ check: r-backports
......................... direct: r-backports -> {'r-mlr', 'r-broom', 'r-mlrmbo', 'r-rprojroot', 'r-vctrs', 'r-checkmate', 'r-paramhelpers'}
......................... check: r-mlr
......................... check: r-broom
......................... check: r-mlrmbo
......................... check: r-rprojroot
......................... check: r-vctrs
......................... check: r-checkmate
......................... check: r-paramhelpers
........................ check: r-caret
........................ check: r-agdex
........................ check: r-gofuncr
........................ check: r-prodlim
........................ check: r-affyrnadegradation
........................ check: r-biasedurn
......................... direct: r-biasedurn -> {'r-goseq'}
......................... check: r-goseq
........................ check: r-catools
........................ check: r-som
......................... direct: r-som -> {'r-chemometrics'}
......................... check: r-chemometrics
........................ check: r-intervals
......................... direct: r-intervals -> {'r-spacetime'}
......................... check: r-spacetime
........................ check: r-tfbstools
........................ check: r-rcppblaze
........................ check: r-phangorn
........................ check: r-tidycensus
........................ check: r-xlconnect
........................ check: r-popgenome
........................ check: r-expint
......................... direct: r-expint -> set()
........................ check: r-formatr
......................... direct: r-formatr -> {'r-knitr', 'r-lambda-r'}
......................... check: r-knitr
......................... check: r-lambda-r
........................ check: r-biocgenerics
......................... direct: r-biocgenerics -> {'r-annotationdbi', 'r-bamsignals', 'r-variantannotation', 'r-ggbio', 'r-mzr', 'r-summarizedexperiment', 'r-affy', 'r-tfbstools', 'r-scater', 'r-iranges', 'r-s4vectors', 'r-biocneighbors', 'r-biovizbase', 'r-bsgenome', 'r-interactivedisplaybase', 'r-annotationforge', 'r-altcdfenvs', 'r-deseq2', 'r-geneplotter', 'r-multtest', 'r-affycoretools', 'r-simpleaffy', 'r-shortread', 'homer', 'r-reportingtools', 'r-category', 'r-genomicranges', 'r-phyloseq', 'r-beachmat', 'r-rsamtools', 'r-delayedarray', 'r-biobase', 'r-biostrings', 'r-bumphunter', 'r-gviz', 'r-deseq', 'r-genomeinfodb', 'r-mlinterfaces', 'r-topgo', 'r-affyplm', 'r-methylumi', 'r-organismdbi', 'r-ensembldb', 'r-goseq', 'r-genomicfeatures', 'r-dada2', 'r-annotate', 'r-graph', 'r-minfi', 'r-msnbase', 'r-rtracklayer', 'r-snpstats', 'r-xvector', 'r-hdf5array', 'r-aneufinder', 'r-dirichletmultinomial', 'r-allelicimbalance', 'r-biocsingular', 'r-gseabase', 'r-cner', 'r-singlecellexperiment', 'r-acme', 'r-genomicalignments', 'r-pcamethods', 'r-xde', 'r-oligoclasses', 'r-annotationhub'}
......................... check: r-annotationdbi
......................... check: r-bamsignals
......................... check: r-variantannotation
......................... check: r-ggbio
......................... check: r-mzr
......................... check: r-summarizedexperiment
......................... check: r-affy
......................... check: r-tfbstools
......................... check: r-scater
......................... check: r-iranges
......................... check: r-s4vectors
......................... check: r-biocneighbors
......................... check: r-biovizbase
......................... check: r-bsgenome
......................... check: r-interactivedisplaybase
......................... check: r-annotationforge
......................... check: r-altcdfenvs
......................... check: r-deseq2
......................... check: r-geneplotter
......................... check: r-multtest
......................... check: r-affycoretools
......................... check: r-simpleaffy
......................... check: r-shortread
......................... check: homer
......................... check: r-reportingtools
......................... check: r-category
......................... check: r-genomicranges
......................... check: r-phyloseq
......................... check: r-beachmat
......................... check: r-rsamtools
......................... check: r-delayedarray
......................... check: r-biobase
.......................... direct: r-biobase -> {'r-a4core', 'r-annotationdbi', 'r-agdex', 'r-variantannotation', 'r-ggbio', 'r-mzr', 'r-mergemaid', 'r-summarizedexperiment', 'r-affyilm', 'r-genemeta', 'r-affy', 'r-tfbstools', 'r-scater', 'r-rnaseqmap', 'r-tmixclust', 'r-rots', 'r-watermelon', 'r-yarn', 'r-affycontam', 'r-lumi', 'r-annotationforge', 'r-altcdfenvs', 'r-deseq2', 'r-geneplotter', 'r-makecdfenv', 'r-affycoretools', 'r-multtest', 'r-shortread', 'r-siggenes', 'r-simpleaffy', 'r-adsplit', 'r-glimma', 'r-category', 'r-reportingtools', 'r-spem', 'r-phyloseq', 'r-genefilter', 'r-acgh', 'r-gviz', 'r-gcrma', 'r-deseq', 'r-mlinterfaces', 'r-topgo', 'r-vsn', 'r-analysispageserver', 'r-affyplm', 'r-a4base', 'r-methylumi', 'r-ensembldb', 'r-organismdbi', 'r-annaffy', 'r-genomicfeatures', 'trinity', 'r-aims', 'r-annotate', 'r-gostats', 'r-minfi', 'r-msnbase', 'r-quantro', 'r-geoquery', 'r-affyqcreport', 'r-agimicrorna', 'r-affycomp', 'r-xde', 'r-gseabase', 'r-somaticsignatures', 'r-acme', 'r-pcamethods', 'r-oligoclasses'}
.......................... check: r-a4core
.......................... check: r-annotationdbi
.......................... check: r-agdex
.......................... check: r-variantannotation
.......................... check: r-ggbio
.......................... check: r-mzr
.......................... check: r-mergemaid
.......................... check: r-summarizedexperiment
.......................... check: r-affyilm
.......................... check: r-genemeta
.......................... check: r-affy
.......................... check: r-tfbstools
.......................... check: r-scater
.......................... check: r-rnaseqmap
.......................... check: r-tmixclust
.......................... check: r-rots
.......................... check: r-watermelon
.......................... check: r-yarn
.......................... check: r-affycontam
.......................... check: r-lumi
.......................... check: r-annotationforge
.......................... check: r-altcdfenvs
.......................... check: r-deseq2
.......................... check: r-geneplotter
.......................... check: r-makecdfenv
.......................... check: r-affycoretools
.......................... check: r-multtest
.......................... check: r-shortread
.......................... check: r-siggenes
.......................... check: r-simpleaffy
.......................... check: r-adsplit
.......................... check: r-glimma
.......................... check: r-category
.......................... check: r-reportingtools
.......................... check: r-spem
.......................... check: r-phyloseq
.......................... check: r-genefilter
.......................... check: r-acgh
.......................... check: r-gviz
.......................... check: r-gcrma
.......................... check: r-deseq
.......................... check: r-mlinterfaces
.......................... check: r-topgo
.......................... check: r-vsn
.......................... check: r-analysispageserver
.......................... check: r-affyplm
.......................... check: r-a4base
.......................... check: r-methylumi
.......................... check: r-ensembldb
.......................... check: r-organismdbi
.......................... check: r-annaffy
.......................... check: r-genomicfeatures
.......................... check: trinity
.......................... check: r-aims
.......................... check: r-annotate
.......................... check: r-gostats
.......................... check: r-minfi
.......................... check: r-msnbase
.......................... check: r-quantro
.......................... check: r-geoquery
.......................... check: r-affyqcreport
.......................... check: r-agimicrorna
.......................... check: r-affycomp
.......................... check: r-xde
.......................... check: r-gseabase
.......................... check: r-somaticsignatures
.......................... check: r-acme
.......................... check: r-pcamethods
.......................... check: r-oligoclasses
......................... check: r-biostrings
......................... check: r-bumphunter
......................... check: r-gviz
......................... check: r-deseq
......................... check: r-genomeinfodb
......................... check: r-mlinterfaces
......................... check: r-topgo
......................... check: r-affyplm
......................... check: r-methylumi
......................... check: r-organismdbi
......................... check: r-ensembldb
......................... check: r-goseq
......................... check: r-genomicfeatures
......................... check: r-dada2
......................... check: r-annotate
......................... check: r-graph
.......................... direct: r-graph -> {'r-kegggraph', 'r-organismdbi', 'r-rgraphviz', 'r-gostats', 'r-alpine', 'r-pathview', 'r-hypergraph', 'r-topgo', 'r-gseabase', 'r-rbgl', 'r-category', 'r-grbase', 'r-analysispageserver'}
.......................... check: r-kegggraph
.......................... check: r-organismdbi
.......................... check: r-rgraphviz
.......................... check: r-gostats
.......................... check: r-alpine
.......................... check: r-pathview
.......................... check: r-hypergraph
.......................... check: r-topgo
.......................... check: r-gseabase
.......................... check: r-rbgl
.......................... check: r-category
.......................... check: r-grbase
.......................... check: r-analysispageserver
......................... check: r-minfi
......................... check: r-msnbase
......................... check: r-rtracklayer
......................... check: r-snpstats
......................... check: r-xvector
......................... check: r-hdf5array
......................... check: r-aneufinder
......................... check: r-dirichletmultinomial
......................... check: r-allelicimbalance
......................... check: r-biocsingular
......................... check: r-gseabase
......................... check: r-cner
......................... check: r-singlecellexperiment
......................... check: r-acme
......................... check: r-genomicalignments
......................... check: r-pcamethods
......................... check: r-xde
......................... check: r-oligoclasses
......................... check: r-annotationhub
........................ check: r-rminer
........................ check: r-lfe
........................ check: r-munsell
........................ check: r-affycoretools
........................ check: r-gss
......................... direct: r-gss -> {'r-tmixclust'}
......................... check: r-tmixclust
........................ check: r-sctransform
........................ check: r-sn
........................ check: r-genetics
........................ check: r-statmod
......................... direct: r-statmod -> {'r-ampliqueso'}
......................... check: r-ampliqueso
........................ check: r-tidyverse
........................ check: r-tseries
........................ check: r-adsplit
........................ check: r-bindr
......................... direct: r-bindr -> {'r-bindrcpp'}
......................... check: r-bindrcpp
........................ check: r-rappdirs
......................... direct: r-rappdirs -> {'r-reticulate', 'r-tigris', 'r-hoardr', 'r-biocfilecache', 'r-tidycensus', 'r-rnoaa', 'r-annotationhub'}
......................... check: r-reticulate
......................... check: r-tigris
......................... check: r-hoardr
......................... check: r-biocfilecache
......................... check: r-tidycensus
......................... check: r-rnoaa
......................... check: r-annotationhub
........................ check: r-rgl
........................ check: r-acgh
........................ check: r-biobase
........................ check: r-mice
........................ check: r-rmpi
......................... direct: r-rmpi -> set()
........................ check: r-npsurv
........................ check: r-blob
........................ check: r-inline
......................... direct: r-inline -> {'r-rstan'}
......................... check: r-rstan
........................ check: r-goftest
......................... direct: r-goftest -> {'r-spatstat'}
......................... check: r-spatstat
........................ check: r-sdmtools
........................ check: rsem
......................... direct: rsem -> {'trinity'}
......................... check: trinity
........................ check: r-do-db
........................ check: r-forcats
........................ check: r-nanotime
........................ check: r-organismdbi
........................ check: r-mco
......................... direct: r-mco -> {'r-smoof'}
......................... check: r-smoof
........................ check: r-dnacopy
......................... direct: r-dnacopy -> {'r-aneufinder', 'r-pscbs'}
......................... check: r-aneufinder
......................... check: r-pscbs
........................ check: r-chemometrics
........................ check: r-commonmark
......................... direct: r-commonmark -> {'r-roxygen2'}
......................... check: r-roxygen2
........................ check: r-rtracklayer
........................ check: r-brew
......................... direct: r-brew -> {'r-roxygen2', 'r-rook'}
......................... check: r-roxygen2
......................... check: r-rook
.......................... direct: r-rook -> {'r-rgexf'}
.......................... check: r-rgexf
........................ check: r-magic
........................ check: r-pbapply
......................... direct: r-pbapply -> {'r-seurat'}
......................... check: r-seurat
........................ check: r-pkgmaker
........................ check: r-ggplotify
........................ check: r-sfsmisc
......................... direct: r-sfsmisc -> {'r-mlinterfaces'}
......................... check: r-mlinterfaces
........................ check: r-gsubfn
........................ check: r-tweenr
........................ check: r-r-cache
........................ check: r-googlevis
........................ check: r-a4core
........................ check: r-reordercluster
........................ check: r-spatstat-utils
......................... direct: r-spatstat-utils -> {'r-spatstat', 'r-spatstat-data'}
......................... check: r-spatstat
......................... check: r-spatstat-data
........................ check: r-biom-utils
......................... direct: r-biom-utils -> {'r-matr'}
......................... check: r-matr
........................ check: r-kknn
........................ check: r-biocfilecache
........................ check: r-suppdists
......................... direct: r-suppdists -> set()
........................ check: r-runit
......................... direct: r-runit -> set()
........................ check: r-maldiquant
......................... direct: r-maldiquant -> {'r-msnbase'}
......................... check: r-msnbase
........................ check: r-snakecase
........................ check: r-teachingdemos
......................... direct: r-teachingdemos -> {'r-earth', 'r-plotmo'}
......................... check: r-earth
......................... check: r-plotmo
........................ check: r-go-db
........................ check: r-rots
........................ check: r-dorng
........................ check: r-nmf
........................ check: r-dotcall64
......................... direct: r-dotcall64 -> {'r-spam'}
......................... check: r-spam
........................ check: r-vctrs
........................ check: r-listenv
......................... direct: r-listenv -> {'r-future', 'r-pscbs'}
......................... check: r-future
......................... check: r-pscbs
........................ check: r-acde
......................... direct: r-acde -> set()
........................ check: r-lmtest
........................ check: r-rbokeh
........................ check: r-usethis
........................ check: r-rjsonio
......................... direct: r-rjsonio -> {'r-smoof', 'r-rgooglemaps'}
......................... check: r-smoof
......................... check: r-rgooglemaps
........................ check: r-vfs
........................ check: r-cluster
......................... direct: r-cluster -> {'r-convevol', 'r-phyloseq', 'r-factominer', 'r-nmf', 'r-seurat', 'r-acgh', 'r-spatialeco', 'r-factoextra', 'r-mlinterfaces', 'r-pamr', 'r-tmixclust', 'r-vegan', 'r-hmisc', 'r-rrcov', 'r-adsplit', 'r-clue', 'r-fpc'}
......................... check: r-convevol
......................... check: r-phyloseq
......................... check: r-factominer
......................... check: r-nmf
......................... check: r-seurat
......................... check: r-acgh
......................... check: r-spatialeco
......................... check: r-factoextra
......................... check: r-mlinterfaces
......................... check: r-pamr
......................... check: r-tmixclust
......................... check: r-vegan
......................... check: r-hmisc
......................... check: r-rrcov
......................... check: r-adsplit
......................... check: r-clue
......................... check: r-fpc
........................ check: r-glimma
........................ check: r-uuid
......................... direct: r-uuid -> {'r-tigris', 'r-irkernel'}
......................... check: r-tigris
......................... check: r-irkernel
........................ check: r-bfastspatial
........................ check: r-shinyfiles
........................ check: r-pillar
........................ check: r-pkgbuild
........................ check: r-beachmat
........................ check: r-whisker
......................... direct: r-whisker -> {'r-reprex', 'r-usethis', 'r-devtools', 'r-dendextend'}
......................... check: r-reprex
......................... check: r-usethis
......................... check: r-devtools
......................... check: r-dendextend
........................ check: r-mcmcglmm
........................ check: r-bumphunter
........................ check: r-deseq
........................ check: r-envstats
........................ check: r-mlinterfaces
........................ check: r-metap
........................ check: r-cairo
......................... direct: r-cairo -> {'r-imager'}
......................... check: r-imager
........................ check: r-repr
........................ check: r-a4base
........................ check: r-ensembldb
........................ check: hic-pro
........................ check: r-rook
........................ check: r-graph
........................ check: r-httr
........................ check: r-jpeg
......................... direct: r-jpeg -> {'r-geomorph', 'r-readbitmap', 'r-ggmap', 'r-imager'}
......................... check: r-geomorph
......................... check: r-readbitmap
......................... check: r-ggmap
......................... check: r-imager
........................ check: r-geoquery
........................ check: r-impute
......................... direct: r-impute -> {'r-wgcna', 'r-samr', 'r-msnbase'}
......................... check: r-wgcna
......................... check: r-samr
......................... check: r-msnbase
........................ check: phantompeakqualtools
........................ check: r-boot
......................... direct: r-boot -> {'r-adegenet', 'r-np', 'r-spatialreg', 'r-energy', 'r-spdep', 'r-acde', 'r-lme4'}
......................... check: r-adegenet
......................... check: r-np
......................... check: r-spatialreg
......................... check: r-energy
......................... check: r-spdep
......................... check: r-acde
......................... check: r-lme4
........................ check: r-vsn
........................ check: r-pfam-db
........................ check: r-dicekriging
......................... direct: r-dicekriging -> set()
...................... check: texlive
...................... check: qt
..................... check: guacamole-server
...................... direct: guacamole-server -> set()
..................... check: i3
..................... check: perl-cairo
...................... direct: perl-cairo -> {'prinseq-lite'}
...................... check: prinseq-lite
....................... direct: prinseq-lite -> set()
..................... check: ncl
..................... check: fio
..................... check: gtkplus
..................... check: pdf2svg
...................... direct: pdf2svg -> {'py-weblogo'}
...................... check: py-weblogo
..................... check: gnuplot
..................... check: pango
..................... check: py-py2cairo
...................... direct: py-py2cairo -> {'py-pygtk', 'py-pygobject'}
...................... check: py-pygtk
...................... check: py-pygobject
..................... check: cairomm
...................... direct: cairomm -> {'pangomm', 'gtkmm'}
...................... check: pangomm
...................... check: gtkmm
..................... check: hwloc
..................... check: r-cairo
..................... check: texlive
..................... check: intel-gpu-tools
...................... direct: intel-gpu-tools -> set()
..................... check: r
..................... check: librsvg
..................... check: graphviz
..................... check: grass
..................... check: py-pygtk
..................... check: mapnik
..................... check: poppler
...................... direct: poppler -> {'graphviz', 'libvips', 'texstudio', 'pdf2svg', 'texlive', 'swftools', 'gdal'}
...................... check: graphviz
...................... check: libvips
...................... check: texstudio
....................... direct: texstudio -> set()
...................... check: pdf2svg
...................... check: texlive
...................... check: swftools
...................... check: gdal
..................... check: openbabel
..................... check: vesta
..................... check: gobject-introspection
...................... direct: gobject-introspection -> {'libnotify', 'librsvg', 'atk', 'libpeas', 'poppler', 'gtkplus', 'py-pygobject', 'libsecret', 'gdk-pixbuf', 'pango', 'at-spi2-core', 'gtksourceview'}
...................... check: libnotify
...................... check: librsvg
...................... check: atk
....................... direct: atk -> {'py-pygtk', 'libpeas', 'py-xdot', 'gtkplus', 'gtkmm', 'gtksourceview', 'at-spi2-atk'}
....................... check: py-pygtk
....................... check: libpeas
....................... check: py-xdot
....................... check: gtkplus
....................... check: gtkmm
....................... check: gtksourceview
....................... check: at-spi2-atk
........................ direct: at-spi2-atk -> {'gtkplus'}
........................ check: gtkplus
...................... check: libpeas
...................... check: poppler
...................... check: gtkplus
...................... check: py-pygobject
...................... check: libsecret
....................... direct: libsecret -> {'qtkeychain'}
....................... check: qtkeychain
........................ direct: qtkeychain -> {'qgis'}
........................ check: qgis
...................... check: gdk-pixbuf
....................... direct: gdk-pixbuf -> {'librsvg', 'libpeas', 'py-xdot', 'gtkplus', 'gtksourceview'}
....................... check: librsvg
....................... check: libpeas
....................... check: py-xdot
....................... check: gtkplus
....................... check: gtksourceview
...................... check: pango
...................... check: at-spi2-core
....................... direct: at-spi2-core -> {'at-spi2-atk'}
....................... check: at-spi2-atk
...................... check: gtksourceview
..................... check: genometools
................... check: py-setuptools-rust
.................... direct: py-setuptools-rust -> {'py-tokenizers'}
.................... check: py-tokenizers
..................... direct: py-tokenizers -> {'py-transformers'}
..................... check: py-transformers
................... check: py-tokenizers
................... check: fd-find
.................... direct: fd-find -> set()
................... check: bat
.................... direct: bat -> set()
................... check: ripgrep
.................... direct: ripgrep -> set()
................... check: exa
.................... direct: exa -> set()
................. check: exodusii
................. check: nest
................. check: cppad
.................. direct: cppad -> set()
................. check: faodel
................. check: llvm
................. check: bamtools
.................. direct: bamtools -> {'ea-utils', 'express', 'braker', 'sniffles', 'sga', 'ncbi-toolkit', 'augustus'}
.................. check: ea-utils
................... direct: ea-utils -> set()
.................. check: express
................... direct: express -> {'trinity'}
................... check: trinity
.................. check: braker
.................. check: sniffles
................... direct: sniffles -> set()
.................. check: sga
................... direct: sga -> set()
.................. check: ncbi-toolkit
................... direct: ncbi-toolkit -> set()
.................. check: augustus
................. check: sympol
.................. direct: sympol -> set()
................. check: libmng
.................. direct: libmng -> {'qt'}
.................. check: qt
................. check: mofem-cephas
................. check: openfoam-org
................. check: opensubdiv
................. check: templight-tools
.................. direct: templight-tools -> set()
................. check: hipsycl
.................. direct: hipsycl -> set()
................. check: trilinos
................. check: dysco
................. check: mpilander
.................. direct: mpilander -> {'powerapi', 'pennant', 'mesquite', 'modylas', 'citcoms', 'strumpack', 'warpx', 'pruners-ninja', 'mpi-bash', 'libsharp', 'hydrogen', 'cgns', 'abyss', 'py-adios', 'exodusii', 'nest', 'faodel', 'mofem-cephas', 'openfoam-org', 'hpgmg', 'trilinos', 'libcircle', 'stat', 'py-pypar', 'revocap-refiner', 'kahip', 'parmetis', 'mitos', 'paradiseo', 'dealii', 'mpifileutils', 'elmerfem', 'libmesh', 'ppopen-appl-bem-at', 'heffte', 'qmd-progress', 'octopus', 'eztrace', 'kripke', 'fftw', 'hpl', 'linsys-v', 'libvdwxc', 'exasp2', 'samrai', 'latte', 'charmpp', 'parallel-netcdf', 'phasta', 'py-torch', 'scr', 'cosp2', 'hpx5', 'scorec-core', 'sw4lite', 'siesta', 'rempi', 'exampm', 'MeshSimAdvanced', 'boost', 'minismac2d', 'draco', 'icet', 'ppopen-appl-fdm', 'libsplash', 'scotch', 'xsbench', 'adios', 'phylobayesmpi', 'arborx', 'cosmomc', 'dihydrogen', 'tioga', 'catalyst', 'bertini', 'revocap-coupler', 'valgrind', 'ember', 'alquimia', 'picsar', 'flecsi', 'pism', 'py-tensorflow', 'mrbayes', 'ppopen-math-vis', 'pmlib', 'ross', 'cram', 'veloc', 'akantu', 'openfoam', 'hpx', 'minivite', 'funhpc', 'grackle', 'hpctoolkit', 'damaris', 'adept-utils', 'elpa', 'py-espressopp', 'intel-mpi-benchmarks', 'cloverleaf3d', 'ebms', 'xbraid', 'panda', 'sirius', 'ampliconnoise', 'netcdf-fortran', 'bookleaf-cpp', 'muster', 'albany', 'aluminum', 'aspa', 'mrcpp', 'ascent', 'minighost', 'kokkos-nvcc-wrapper', 'dbcsr', 'converge', 'vtk-m', 'phist', 'qmcpack', 'minitri', 'lulesh', 'minife', 'pagmo', 'opencoarrays', 'gslib', 'megadock', 'vtk', 'flecsph', 'hpccg', 'elemental', 'lwgrp', 'adlbx', 'pbmpi', 'yambo', 'hoomd-blue', 'tau', 'vpfft', 'openstf', 'dftfe', 'boxlib', 'mpileaks', 'mfem', 'sosflow', 'minimd', 'spfft', 'ppopen-appl-fem', 'wannier90', 'amp', 'ior', 'cbench', 'ntpoly', 'ppopen-appl-fdm-at', 'sst-macro', 'dataspaces', 'parsplice', 'lwm2', 'camellia', 'mpe2', 'macsio', 'upcxx', 'ppopen-appl-bem', 'scorep', 'cabana', 'scalasca', 'globalarrays', 'simul', 'qbox', 'mercury', 'med', 'bml', 'nccl-tests', 'fenics', 'amber', 'liggghts', 'hmmer', 'hdf5', 'athena', 'precice', 'slate', 'quicksilver', 'exabayes', 'extrae', 'ray', 'meshkit', 'cardioid', 'hypre', 'eq-r', 'regcm', 'axom', 'seacas', 'ccs-qcd', 'meep', 'raft', 'omega-h', 'textparser', 'adiak', 'lbann', 'callpath', 'adios2', 'amgx', 'minixyce', 'py-gpaw', 'osu-micro-benchmarks', 'nalu-wind', 'py-h5py', 'ppopen-appl-amr-fdm', 'py-espresso', 'fast-global-file-status', 'sundials', 'simplemoc', 'steps', 'shuffile', 'channelflow', 'ffr', 'tracer', 'netcdf-c', 'thornado-mini', 'gmsh', 'cntk', 'meme', 'py-mpi4py', 'amg2013', 'ppopen-appl-dem-util', 'mpip', 'relion', 'infernal', 'camx', 'vampirtrace', 'opencv', 'chatterbug', 'sgpp', 'nekcem', 'hwloc', 'cloverleaf', 'papyrus', 'geopm', 'elk', 'dtcmp', 'swiftsim', 'tasmanian', 'timemory', 'redset', 'snap', 'nalu', 'nek5000', 'h5cpp', 'flann', 'mpix-launch-swift', 'ompss', 'er', 'miniamr', 'p3dfft3', 'tealeaf', 'elsi', 'damselfly', 'py-horovod', 'zoltan', 'sst-core', 'mdtest', 'visit', 'henson', 'caliper', 'foam-extend', 'pflotran', 'jali', 'nekbone', 'hiop', 'nektar', 'picsarlite', 'libnbc', 'libhio', 'netgauge', 'iq-tree', 'pfft', 'revbayes', 'moab', 'openfdtd', 'openmx', 'openfast', 'lammps', 'cosma', 'amrex', 'ppopen-math-mp', 'libquo', 'helics', 'swfft', 'amrvis', 'gromacs', 'maker', 'isaac', 'h5hut', 'raxml', 'vpic', 'filo', 'p4est', 'abinit', 'frontistr', 'rankstr', 'npb', 'gasnet', 'diy', 'netgen', 'pidx', 'xdmf3', 'parmgridgen', 'mumps', 'shengbte', 'everytrace', 'mstk', 'kvtree', 'turbine', 'ppopen-appl-fvm', 'pnmpi', 'openpmd-api', 'amg', 'clamr', 'py-python-meep', 'conduit', 'spath', 'arpack-ng', 'miniqmc', 'cp2k', 'librom', 'asagi', 'superlu-dist', 'petsc', 'eem', 'ghost', 'coevp', 'py-libensemble', 'codes', 'pumi', 'neuron', 'butterflypack', 'darshan-runtime', 'tycho2', 'plumed', 'dakota', 'portage', 'comd', 'branson', 'r-rmpi', 'unifyfs', 'minigmg', 'automaded', 'typhon', 'h5part', 'netlib-scalapack', 'chombo', 'vtk-h', 'silo', 'xsdktrilinos', 'esmf', 'fastmath', 'pfunit', 'savanna', 'xios', 'graph500', 'cgm', 'paraview', 'examinimd', 'nwchem', 'quantum-espresso', 'typhonio', 'multiverso'}
.................. check: powerapi
.................. check: pennant
.................. check: mesquite
.................. check: modylas
.................. check: citcoms
.................. check: strumpack
.................. check: warpx
.................. check: pruners-ninja
.................. check: mpi-bash
.................. check: libsharp
.................. check: hydrogen
.................. check: cgns
.................. check: abyss
.................. check: py-adios
.................. check: exodusii
.................. check: nest
.................. check: faodel
.................. check: mofem-cephas
.................. check: openfoam-org
.................. check: hpgmg
.................. check: trilinos
.................. check: libcircle
.................. check: stat
.................. check: py-pypar
.................. check: revocap-refiner
.................. check: kahip
.................. check: parmetis
.................. check: mitos
.................. check: paradiseo
.................. check: dealii
.................. check: mpifileutils
.................. check: elmerfem
.................. check: libmesh
.................. check: ppopen-appl-bem-at
.................. check: heffte
.................. check: qmd-progress
.................. check: octopus
.................. check: eztrace
.................. check: kripke
.................. check: fftw
.................. check: hpl
.................. check: linsys-v
.................. check: libvdwxc
.................. check: exasp2
.................. check: samrai
.................. check: latte
.................. check: charmpp
.................. check: parallel-netcdf
.................. check: phasta
.................. check: py-torch
.................. check: scr
.................. check: cosp2
.................. check: hpx5
.................. check: scorec-core
.................. check: sw4lite
.................. check: siesta
.................. check: rempi
.................. check: exampm
.................. check: MeshSimAdvanced
.................. check: boost
.................. check: minismac2d
.................. check: draco
.................. check: icet
.................. check: ppopen-appl-fdm
.................. check: libsplash
.................. check: scotch
.................. check: xsbench
.................. check: adios
.................. check: phylobayesmpi
.................. check: arborx
.................. check: cosmomc
.................. check: dihydrogen
.................. check: tioga
.................. check: catalyst
.................. check: bertini
.................. check: revocap-coupler
.................. check: valgrind
.................. check: ember
.................. check: alquimia
.................. check: picsar
.................. check: flecsi
.................. check: pism
.................. check: py-tensorflow
.................. check: mrbayes
.................. check: ppopen-math-vis
.................. check: pmlib
.................. check: ross
.................. check: cram
.................. check: veloc
.................. check: akantu
.................. check: openfoam
.................. check: hpx
.................. check: minivite
.................. check: funhpc
.................. check: grackle
.................. check: hpctoolkit
.................. check: damaris
.................. check: adept-utils
.................. check: elpa
.................. check: py-espressopp
.................. check: intel-mpi-benchmarks
.................. check: cloverleaf3d
.................. check: ebms
.................. check: xbraid
.................. check: panda
.................. check: sirius
.................. check: ampliconnoise
.................. check: netcdf-fortran
.................. check: bookleaf-cpp
.................. check: muster
.................. check: albany
.................. check: aluminum
.................. check: aspa
.................. check: mrcpp
.................. check: ascent
.................. check: minighost
.................. check: kokkos-nvcc-wrapper
.................. check: dbcsr
.................. check: converge
.................. check: vtk-m
.................. check: phist
.................. check: qmcpack
.................. check: minitri
.................. check: lulesh
.................. check: minife
.................. check: pagmo
.................. check: opencoarrays
.................. check: gslib
.................. check: megadock
.................. check: vtk
.................. check: flecsph
.................. check: hpccg
.................. check: elemental
.................. check: lwgrp
.................. check: adlbx
.................. check: pbmpi
.................. check: yambo
.................. check: hoomd-blue
.................. check: tau
.................. check: vpfft
.................. check: openstf
.................. check: dftfe
.................. check: boxlib
.................. check: mpileaks
.................. check: mfem
.................. check: sosflow
.................. check: minimd
.................. check: spfft
.................. check: ppopen-appl-fem
.................. check: wannier90
.................. check: amp
.................. check: ior
.................. check: cbench
.................. check: ntpoly
.................. check: ppopen-appl-fdm-at
.................. check: sst-macro
.................. check: dataspaces
.................. check: parsplice
.................. check: lwm2
.................. check: camellia
.................. check: mpe2
.................. check: macsio
.................. check: upcxx
.................. check: ppopen-appl-bem
.................. check: scorep
.................. check: cabana
.................. check: scalasca
.................. check: globalarrays
.................. check: simul
.................. check: qbox
.................. check: mercury
.................. check: med
.................. check: bml
.................. check: nccl-tests
.................. check: fenics
.................. check: amber
.................. check: liggghts
.................. check: hmmer
.................. check: hdf5
.................. check: athena
.................. check: precice
.................. check: slate
.................. check: quicksilver
.................. check: exabayes
.................. check: extrae
.................. check: ray
.................. check: meshkit
.................. check: cardioid
.................. check: hypre
.................. check: eq-r
.................. check: regcm
.................. check: axom
.................. check: seacas
.................. check: ccs-qcd
.................. check: meep
.................. check: raft
.................. check: omega-h
.................. check: textparser
.................. check: adiak
.................. check: lbann
.................. check: callpath
.................. check: adios2
.................. check: amgx
.................. check: minixyce
.................. check: py-gpaw
.................. check: osu-micro-benchmarks
.................. check: nalu-wind
.................. check: py-h5py
.................. check: ppopen-appl-amr-fdm
.................. check: py-espresso
.................. check: fast-global-file-status
.................. check: sundials
.................. check: simplemoc
.................. check: steps
.................. check: shuffile
.................. check: channelflow
.................. check: ffr
.................. check: tracer
.................. check: netcdf-c
.................. check: thornado-mini
.................. check: gmsh
.................. check: cntk
.................. check: meme
.................. check: py-mpi4py
.................. check: amg2013
.................. check: ppopen-appl-dem-util
.................. check: mpip
.................. check: relion
.................. check: infernal
.................. check: camx
.................. check: vampirtrace
.................. check: opencv
.................. check: chatterbug
.................. check: sgpp
.................. check: nekcem
.................. check: hwloc
.................. check: cloverleaf
................... direct: cloverleaf -> set()
.................. check: papyrus
................... direct: papyrus -> set()
.................. check: geopm
.................. check: elk
.................. check: dtcmp
.................. check: swiftsim
.................. check: tasmanian
................... direct: tasmanian -> {'xsdk'}
................... check: xsdk
.................. check: timemory
.................. check: redset
................... direct: redset -> {'er'}
................... check: er
.................. check: snap
................... direct: snap -> set()
.................. check: nalu
.................. check: nek5000
................... direct: nek5000 -> {'ceed'}
................... check: ceed
.................. check: h5cpp
.................. check: flann
.................. check: mpix-launch-swift
.................. check: ompss
.................. check: er
.................. check: miniamr
................... direct: miniamr -> {'ecp-proxy-apps'}
................... check: ecp-proxy-apps
.................. check: p3dfft3
.................. check: tealeaf
................... direct: tealeaf -> set()
.................. check: elsi
.................. check: damselfly
................... direct: damselfly -> set()
.................. check: py-horovod
.................. check: zoltan
.................. check: sst-core
.................. check: mdtest
................... direct: mdtest -> set()
.................. check: visit
.................. check: henson
................... direct: henson -> set()
.................. check: caliper
.................. check: foam-extend
.................. check: pflotran
.................. check: jali
.................. check: nekbone
................... direct: nekbone -> {'ceed', 'ecp-proxy-apps'}
................... check: ceed
................... check: ecp-proxy-apps
.................. check: hiop
.................. check: nektar
.................. check: picsarlite
.................. check: libnbc
................... direct: libnbc -> set()
.................. check: libhio
.................. check: netgauge
................... direct: netgauge -> set()
.................. check: iq-tree
................... direct: iq-tree -> set()
.................. check: pfft
.................. check: revbayes
................... direct: revbayes -> set()
.................. check: moab
.................. check: openfdtd
................... direct: openfdtd -> set()
.................. check: openmx
.................. check: openfast
.................. check: lammps
.................. check: cosma
.................. check: amrex
.................. check: ppopen-math-mp
................... direct: ppopen-math-mp -> set()
.................. check: libquo
................... direct: libquo -> {'draco'}
................... check: draco
.................. check: helics
................... direct: helics -> {'gridlab-d', 'ns-3-dev'}
................... check: gridlab-d
.................... direct: gridlab-d -> set()
................... check: ns-3-dev
.................... direct: ns-3-dev -> set()
.................. check: swfft
.................. check: amrvis
................... direct: amrvis -> set()
.................. check: gromacs
.................. check: maker
.................. check: isaac
.................. check: h5hut
.................. check: raxml
................... direct: raxml -> {'treesub', 'phyluce'}
................... check: treesub
................... check: phyluce
.................. check: vpic
................... direct: vpic -> set()
.................. check: filo
................... direct: filo -> {'scr'}
................... check: scr
.................. check: p4est
................... direct: p4est -> {'dealii'}
................... check: dealii
.................. check: abinit
.................. check: frontistr
.................. check: rankstr
................... direct: rankstr -> {'redset', 'scr'}
................... check: redset
................... check: scr
.................. check: npb
................... direct: npb -> set()
.................. check: gasnet
................... direct: gasnet -> {'legion', 'tau'}
................... check: legion
................... check: tau
.................. check: diy
................... direct: diy -> set()
.................. check: netgen
.................. check: pidx
................... direct: pidx -> set()
.................. check: xdmf3
.................. check: parmgridgen
................... direct: parmgridgen -> {'openfoam', 'foam-extend'}
................... check: openfoam
................... check: foam-extend
.................. check: mumps
.................. check: shengbte
................... direct: shengbte -> set()
.................. check: everytrace
................... direct: everytrace -> {'pism', 'everytrace-example', 'ibmisc'}
................... check: pism
................... check: everytrace-example
.................... direct: everytrace-example -> set()
................... check: ibmisc
.................. check: mstk
.................. check: kvtree
................... direct: kvtree -> {'scr', 'shuffile', 'er', 'filo', 'axl', 'redset'}
................... check: scr
................... check: shuffile
................... check: er
................... check: filo
................... check: axl
.................... direct: axl -> {'veloc', 'filo'}
.................... check: veloc
.................... check: filo
................... check: redset
.................. check: turbine
.................. check: ppopen-appl-fvm
.................. check: pnmpi
.................. check: openpmd-api
.................. check: amg
................... direct: amg -> {'ecp-proxy-apps'}
................... check: ecp-proxy-apps
.................. check: clamr
.................. check: py-python-meep
.................. check: conduit
.................. check: spath
................... direct: spath -> {'filo', 'scr'}
................... check: filo
................... check: scr
.................. check: arpack-ng
................... direct: arpack-ng -> {'octopus', 'armadillo', 'nektar', 'slepc', 'fastmath', 'dealii', 'butterflypack', 'octave', 'lazyten'}
................... check: octopus
................... check: armadillo
................... check: nektar
................... check: slepc
................... check: fastmath
................... check: dealii
................... check: butterflypack
.................... direct: butterflypack -> {'xsdk', 'strumpack'}
.................... check: xsdk
.................... check: strumpack
................... check: octave
................... check: lazyten
.................. check: miniqmc
................... direct: miniqmc -> {'ecp-proxy-apps'}
................... check: ecp-proxy-apps
.................. check: cp2k
.................. check: librom
.................. check: asagi
.................. check: superlu-dist
.................. check: petsc
.................. check: eem
................... direct: eem -> set()
.................. check: ghost
.................. check: coevp
.................. check: py-libensemble
.................. check: codes
.................. check: pumi
.................. check: neuron
................... direct: neuron -> set()
.................. check: butterflypack
.................. check: darshan-runtime
................... direct: darshan-runtime -> {'ecp-io-sdk'}
................... check: ecp-io-sdk
.................. check: tycho2
................... direct: tycho2 -> set()
.................. check: plumed
................... direct: plumed -> {'cp2k', 'gromacs'}
................... check: cp2k
................... check: gromacs
.................. check: dakota
................... direct: dakota -> set()
.................. check: portage
................... direct: portage -> set()
.................. check: comd
.................. check: branson
.................. check: r-rmpi
.................. check: unifyfs
.................. check: minigmg
................... direct: minigmg -> set()
.................. check: automaded
.................. check: typhon
................... direct: typhon -> {'bookleaf-cpp'}
................... check: bookleaf-cpp
.................. check: h5part
.................. check: netlib-scalapack
................... direct: netlib-scalapack -> {'elpa', 'frontistr', 'petsc', 'dealii', 'py-gpaw', 'mumps', 'strumpack', 'sirius', 'butterflypack', 'elemental', 'cosma', 'hydrogen', 'siesta', 'yambo', 'octopus', 'elsi', 'akantu', 'dftfe', 'nwchem', 'globalarrays', 'quantum-espresso', 'trilinos', 'linsys-v', 'qbox', 'cp2k', 'abinit'}
................... check: elpa
................... check: frontistr
................... check: petsc
................... check: dealii
................... check: py-gpaw
................... check: mumps
................... check: strumpack
................... check: sirius
................... check: butterflypack
................... check: elemental
................... check: cosma
................... check: hydrogen
................... check: siesta
................... check: yambo
................... check: octopus
................... check: elsi
................... check: akantu
................... check: dftfe
................... check: nwchem
.................... direct: nwchem -> set()
................... check: globalarrays
................... check: quantum-espresso
................... check: trilinos
................... check: linsys-v
................... check: qbox
................... check: cp2k
................... check: abinit
.................. check: chombo
.................. check: vtk-h
.................. check: silo
.................. check: xsdktrilinos
.................. check: esmf
.................. check: fastmath
.................. check: pfunit
................... direct: pfunit -> set()
.................. check: savanna
.................. check: xios
.................. check: graph500
................... direct: graph500 -> set()
.................. check: cgm
................... direct: cgm -> {'meshkit', 'moab'}
................... check: meshkit
................... check: moab
.................. check: paraview
.................. check: examinimd
.................. check: nwchem
.................. check: quantum-espresso
.................. check: typhonio
.................. check: multiverso
................... direct: multiverso -> {'cntk'}
................... check: cntk
................. check: fish
.................. direct: fish -> set()
................. check: mpark-variant
.................. direct: mpark-variant -> {'openpmd-api'}
.................. check: openpmd-api
................. check: mofem-users-modules
................. check: rdma-core
.................. direct: rdma-core -> {'openmpi', 'ucx', 'libfabric', 'nccl', 'mvapich2'}
.................. check: openmpi
.................. check: ucx
.................. check: libfabric
................... direct: libfabric -> {'openmpi', 'pbmpi', 'fabtests', 'mpich', 'mvapich2', 'adios2', 'mercury', 'faodel'}
................... check: openmpi
................... check: pbmpi
................... check: fabtests
.................... direct: fabtests -> set()
................... check: mpich
................... check: mvapich2
.................... direct: mvapich2 -> {'powerapi', 'pennant', 'mesquite', 'modylas', 'citcoms', 'strumpack', 'warpx', 'pruners-ninja', 'mpi-bash', 'libsharp', 'hydrogen', 'cgns', 'abyss', 'py-adios', 'exodusii', 'nest', 'faodel', 'mofem-cephas', 'openfoam-org', 'hpgmg', 'trilinos', 'libcircle', 'stat', 'py-pypar', 'revocap-refiner', 'kahip', 'parmetis', 'mitos', 'paradiseo', 'dealii', 'mpifileutils', 'elmerfem', 'libmesh', 'ppopen-appl-bem-at', 'heffte', 'qmd-progress', 'octopus', 'eztrace', 'kripke', 'fftw', 'hpl', 'linsys-v', 'libvdwxc', 'exasp2', 'samrai', 'latte', 'charmpp', 'parallel-netcdf', 'phasta', 'py-torch', 'scr', 'cosp2', 'hpx5', 'scorec-core', 'sw4lite', 'siesta', 'rempi', 'exampm', 'MeshSimAdvanced', 'boost', 'minismac2d', 'draco', 'icet', 'ppopen-appl-fdm', 'libsplash', 'scotch', 'xsbench', 'adios', 'phylobayesmpi', 'arborx', 'cosmomc', 'dihydrogen', 'tioga', 'catalyst', 'bertini', 'revocap-coupler', 'valgrind', 'ember', 'alquimia', 'picsar', 'flecsi', 'pism', 'py-tensorflow', 'mrbayes', 'ppopen-math-vis', 'pmlib', 'ross', 'cram', 'veloc', 'akantu', 'openfoam', 'hpx', 'minivite', 'funhpc', 'grackle', 'hpctoolkit', 'damaris', 'adept-utils', 'elpa', 'py-espressopp', 'intel-mpi-benchmarks', 'cloverleaf3d', 'ebms', 'xbraid', 'panda', 'sirius', 'ampliconnoise', 'netcdf-fortran', 'bookleaf-cpp', 'muster', 'albany', 'aluminum', 'aspa', 'mrcpp', 'ascent', 'minighost', 'kokkos-nvcc-wrapper', 'dbcsr', 'converge', 'vtk-m', 'phist', 'qmcpack', 'minitri', 'lulesh', 'minife', 'pagmo', 'opencoarrays', 'gslib', 'megadock', 'vtk', 'flecsph', 'hpccg', 'elemental', 'lwgrp', 'adlbx', 'pbmpi', 'yambo', 'hoomd-blue', 'tau', 'vpfft', 'openstf', 'dftfe', 'boxlib', 'mpileaks', 'mfem', 'sosflow', 'minimd', 'spfft', 'ppopen-appl-fem', 'wannier90', 'amp', 'ior', 'cbench', 'ntpoly', 'ppopen-appl-fdm-at', 'sst-macro', 'dataspaces', 'parsplice', 'lwm2', 'camellia', 'mpe2', 'macsio', 'upcxx', 'ppopen-appl-bem', 'scorep', 'cabana', 'scalasca', 'globalarrays', 'simul', 'qbox', 'mercury', 'med', 'bml', 'nccl-tests', 'fenics', 'amber', 'liggghts', 'hmmer', 'hdf5', 'athena', 'precice', 'slate', 'quicksilver', 'exabayes', 'extrae', 'ray', 'meshkit', 'cardioid', 'hypre', 'eq-r', 'regcm', 'axom', 'seacas', 'ccs-qcd', 'meep', 'raft', 'omega-h', 'textparser', 'adiak', 'lbann', 'callpath', 'adios2', 'amgx', 'minixyce', 'py-gpaw', 'osu-micro-benchmarks', 'nalu-wind', 'py-h5py', 'ppopen-appl-amr-fdm', 'py-espresso', 'fast-global-file-status', 'sundials', 'simplemoc', 'steps', 'shuffile', 'channelflow', 'ffr', 'cbtf-krell', 'tracer', 'netcdf-c', 'thornado-mini', 'gmsh', 'cntk', 'meme', 'py-mpi4py', 'amg2013', 'ppopen-appl-dem-util', 'mpip', 'relion', 'infernal', 'camx', 'vampirtrace', 'opencv', 'chatterbug', 'sgpp', 'nekcem', 'hwloc', 'cloverleaf', 'papyrus', 'geopm', 'elk', 'dtcmp', 'swiftsim', 'tasmanian', 'timemory', 'redset', 'snap', 'nalu', 'nek5000', 'h5cpp', 'flann', 'mpix-launch-swift', 'ompss', 'er', 'miniamr', 'p3dfft3', 'tealeaf', 'elsi', 'damselfly', 'py-horovod', 'zoltan', 'sst-core', 'mdtest', 'visit', 'henson', 'caliper', 'foam-extend', 'pflotran', 'jali', 'nekbone', 'hiop', 'nektar', 'picsarlite', 'libnbc', 'libhio', 'netgauge', 'iq-tree', 'pfft', 'revbayes', 'moab', 'openfdtd', 'openmx', 'openfast', 'lammps', 'cosma', 'amrex', 'ppopen-math-mp', 'libquo', 'helics', 'swfft', 'amrvis', 'gromacs', 'maker', 'isaac', 'h5hut', 'raxml', 'vpic', 'filo', 'p4est', 'abinit', 'frontistr', 'rankstr', 'npb', 'gasnet', 'diy', 'netgen', 'pidx', 'xdmf3', 'parmgridgen', 'mumps', 'shengbte', 'everytrace', 'mstk', 'kvtree', 'turbine', 'ppopen-appl-fvm', 'pnmpi', 'openpmd-api', 'amg', 'clamr', 'py-python-meep', 'conduit', 'spath', 'arpack-ng', 'miniqmc', 'cp2k', 'librom', 'asagi', 'superlu-dist', 'petsc', 'eem', 'ghost', 'coevp', 'py-libensemble', 'codes', 'pumi', 'neuron', 'butterflypack', 'darshan-runtime', 'tycho2', 'plumed', 'dakota', 'portage', 'comd', 'branson', 'r-rmpi', 'unifyfs', 'minigmg', 'automaded', 'typhon', 'h5part', 'netlib-scalapack', 'chombo', 'rose', 'vtk-h', 'silo', 'xsdktrilinos', 'esmf', 'fastmath', 'pfunit', 'savanna', 'xios', 'graph500', 'cgm', 'paraview', 'examinimd', 'nwchem', 'quantum-espresso', 'typhonio', 'multiverso'}
.................... check: powerapi
.................... check: pennant
.................... check: mesquite
.................... check: modylas
.................... check: citcoms
.................... check: strumpack
.................... check: warpx
.................... check: pruners-ninja
.................... check: mpi-bash
.................... check: libsharp
.................... check: hydrogen
.................... check: cgns
.................... check: abyss
.................... check: py-adios
.................... check: exodusii
.................... check: nest
.................... check: faodel
.................... check: mofem-cephas
.................... check: openfoam-org
.................... check: hpgmg
.................... check: trilinos
.................... check: libcircle
.................... check: stat
.................... check: py-pypar
.................... check: revocap-refiner
.................... check: kahip
.................... check: parmetis
.................... check: mitos
.................... check: paradiseo
.................... check: dealii
.................... check: mpifileutils
.................... check: elmerfem
.................... check: libmesh
.................... check: ppopen-appl-bem-at
.................... check: heffte
.................... check: qmd-progress
.................... check: octopus
.................... check: eztrace
.................... check: kripke
.................... check: fftw
.................... check: hpl
.................... check: linsys-v
.................... check: libvdwxc
.................... check: exasp2
.................... check: samrai
.................... check: latte
.................... check: charmpp
.................... check: parallel-netcdf
.................... check: phasta
.................... check: py-torch
.................... check: scr
.................... check: cosp2
.................... check: hpx5
.................... check: scorec-core
.................... check: sw4lite
.................... check: siesta
.................... check: rempi
.................... check: exampm
.................... check: MeshSimAdvanced
.................... check: boost
.................... check: minismac2d
.................... check: draco
.................... check: icet
.................... check: ppopen-appl-fdm
.................... check: libsplash
.................... check: scotch
.................... check: xsbench
.................... check: adios
.................... check: phylobayesmpi
.................... check: arborx
.................... check: cosmomc
.................... check: dihydrogen
.................... check: tioga
.................... check: catalyst
.................... check: bertini
.................... check: revocap-coupler
.................... check: valgrind
.................... check: ember
.................... check: alquimia
.................... check: picsar
.................... check: flecsi
.................... check: pism
.................... check: py-tensorflow
.................... check: mrbayes
.................... check: ppopen-math-vis
.................... check: pmlib
.................... check: ross
.................... check: cram
.................... check: veloc
.................... check: akantu
.................... check: openfoam
.................... check: hpx
.................... check: minivite
.................... check: funhpc
.................... check: grackle
.................... check: hpctoolkit
.................... check: damaris
.................... check: adept-utils
.................... check: elpa
.................... check: py-espressopp
.................... check: intel-mpi-benchmarks
.................... check: cloverleaf3d
.................... check: ebms
.................... check: xbraid
.................... check: panda
.................... check: sirius
.................... check: ampliconnoise
.................... check: netcdf-fortran
.................... check: bookleaf-cpp
.................... check: muster
.................... check: albany
.................... check: aluminum
.................... check: aspa
.................... check: mrcpp
.................... check: ascent
.................... check: minighost
.................... check: kokkos-nvcc-wrapper
.................... check: dbcsr
.................... check: converge
.................... check: vtk-m
.................... check: phist
.................... check: qmcpack
.................... check: minitri
.................... check: lulesh
.................... check: minife
.................... check: pagmo
.................... check: opencoarrays
.................... check: gslib
.................... check: megadock
.................... check: vtk
.................... check: flecsph
.................... check: hpccg
.................... check: elemental
.................... check: lwgrp
.................... check: adlbx
.................... check: pbmpi
.................... check: yambo
.................... check: hoomd-blue
.................... check: tau
.................... check: vpfft
.................... check: openstf
.................... check: dftfe
.................... check: boxlib
.................... check: mpileaks
.................... check: mfem
.................... check: sosflow
.................... check: minimd
.................... check: spfft
.................... check: ppopen-appl-fem
.................... check: wannier90
.................... check: amp
.................... check: ior
.................... check: cbench
.................... check: ntpoly
.................... check: ppopen-appl-fdm-at
.................... check: sst-macro
.................... check: dataspaces
.................... check: parsplice
.................... check: lwm2
.................... check: camellia
.................... check: mpe2
.................... check: macsio
.................... check: upcxx
.................... check: ppopen-appl-bem
.................... check: scorep
.................... check: cabana
.................... check: scalasca
.................... check: globalarrays
.................... check: simul
.................... check: qbox
.................... check: mercury
.................... check: med
.................... check: bml
.................... check: nccl-tests
.................... check: fenics
.................... check: amber
.................... check: liggghts
.................... check: hmmer
.................... check: hdf5
.................... check: athena
.................... check: precice
.................... check: slate
.................... check: quicksilver
.................... check: exabayes
.................... check: extrae
.................... check: ray
.................... check: meshkit
.................... check: cardioid
.................... check: hypre
.................... check: eq-r
.................... check: regcm
.................... check: axom
.................... check: seacas
.................... check: ccs-qcd
.................... check: meep
.................... check: raft
.................... check: omega-h
.................... check: textparser
.................... check: adiak
.................... check: lbann
.................... check: callpath
.................... check: adios2
.................... check: amgx
.................... check: minixyce
.................... check: py-gpaw
.................... check: osu-micro-benchmarks
.................... check: nalu-wind
.................... check: py-h5py
.................... check: ppopen-appl-amr-fdm
.................... check: py-espresso
.................... check: fast-global-file-status
.................... check: sundials
.................... check: simplemoc
.................... check: steps
.................... check: shuffile
.................... check: channelflow
.................... check: ffr
.................... check: cbtf-krell
.................... check: tracer
.................... check: netcdf-c
.................... check: thornado-mini
.................... check: gmsh
.................... check: cntk
.................... check: meme
.................... check: py-mpi4py
.................... check: amg2013
.................... check: ppopen-appl-dem-util
.................... check: mpip
.................... check: relion
.................... check: infernal
.................... check: camx
.................... check: vampirtrace
.................... check: opencv
.................... check: chatterbug
.................... check: sgpp
.................... check: nekcem
.................... check: hwloc
.................... check: cloverleaf
.................... check: papyrus
.................... check: geopm
.................... check: elk
.................... check: dtcmp
.................... check: swiftsim
.................... check: tasmanian
.................... check: timemory
.................... check: redset
.................... check: snap
.................... check: nalu
.................... check: nek5000
.................... check: h5cpp
.................... check: flann
.................... check: mpix-launch-swift
.................... check: ompss
.................... check: er
.................... check: miniamr
.................... check: p3dfft3
.................... check: tealeaf
.................... check: elsi
.................... check: damselfly
.................... check: py-horovod
.................... check: zoltan
.................... check: sst-core
.................... check: mdtest
.................... check: visit
.................... check: henson
.................... check: caliper
.................... check: foam-extend
.................... check: pflotran
.................... check: jali
.................... check: nekbone
.................... check: hiop
.................... check: nektar
.................... check: picsarlite
.................... check: libnbc
.................... check: libhio
.................... check: netgauge
.................... check: iq-tree
.................... check: pfft
.................... check: revbayes
.................... check: moab
.................... check: openfdtd
.................... check: openmx
.................... check: openfast
.................... check: lammps
.................... check: cosma
.................... check: amrex
.................... check: ppopen-math-mp
.................... check: libquo
.................... check: helics
.................... check: swfft
.................... check: amrvis
.................... check: gromacs
.................... check: maker
.................... check: isaac
.................... check: h5hut
.................... check: raxml
.................... check: vpic
.................... check: filo
.................... check: p4est
.................... check: abinit
.................... check: frontistr
.................... check: rankstr
.................... check: npb
.................... check: gasnet
.................... check: diy
.................... check: netgen
.................... check: pidx
.................... check: xdmf3
.................... check: parmgridgen
.................... check: mumps
.................... check: shengbte
.................... check: everytrace
.................... check: mstk
.................... check: kvtree
.................... check: turbine
.................... check: ppopen-appl-fvm
.................... check: pnmpi
.................... check: openpmd-api
.................... check: amg
.................... check: clamr
.................... check: py-python-meep
.................... check: conduit
.................... check: spath
.................... check: arpack-ng
.................... check: miniqmc
.................... check: cp2k
.................... check: librom
.................... check: asagi
.................... check: superlu-dist
.................... check: petsc
.................... check: eem
.................... check: ghost
.................... check: coevp
.................... check: py-libensemble
.................... check: codes
.................... check: pumi
.................... check: neuron
.................... check: butterflypack
.................... check: darshan-runtime
.................... check: tycho2
.................... check: plumed
.................... check: dakota
.................... check: portage
.................... check: comd
.................... check: branson
.................... check: r-rmpi
.................... check: unifyfs
.................... check: minigmg
.................... check: automaded
.................... check: typhon
.................... check: h5part
.................... check: netlib-scalapack
.................... check: chombo
.................... check: rose
..................... direct: rose -> {'chill'}
..................... check: chill
...................... direct: chill -> set()
.................... check: vtk-h
.................... check: silo
.................... check: xsdktrilinos
.................... check: esmf
.................... check: fastmath
.................... check: pfunit
.................... check: savanna
.................... check: xios
.................... check: graph500
.................... check: cgm
.................... check: paraview
.................... check: examinimd
.................... check: nwchem
.................... check: quantum-espresso
.................... check: typhonio
.................... check: multiverso
................... check: adios2
................... check: mercury
................... check: faodel
.................. check: nccl
................... direct: nccl -> {'nccl-tests', 'aluminum', 'lbann', 'py-tensorflow', 'py-horovod', 'cntk', 'py-torch'}
................... check: nccl-tests
................... check: aluminum
................... check: lbann
................... check: py-tensorflow
................... check: py-horovod
................... check: cntk
................... check: py-torch
.................. check: mvapich2
................. check: libtree
.................. direct: libtree -> set()
................. check: py-osqp
................. check: arrow
................. check: parmetis
................. check: mitos
................. check: paradiseo
................. check: dealii
................. check: json-fortran
.................. direct: json-fortran -> set()
................. check: graphlib
.................. direct: graphlib -> {'stat'}
.................. check: stat
................. check: plplot
................. check: mpifileutils
................. check: elmerfem
................. check: sst-transports
................. check: votca-ctp
................. check: raja
.................. direct: raja -> {'mfem', 'axom', 'sundials'}
.................. check: mfem
.................. check: axom
.................. check: sundials
................. check: span-lite
.................. direct: span-lite -> set()
................. check: eccodes
................. check: libical
.................. direct: libical -> {'openpbs'}
.................. check: openpbs
................. check: xgboost
.................. direct: xgboost -> {'grnboost'}
.................. check: grnboost
................. check: qgis
................. check: libkml
................. check: graphite2
.................. direct: graphite2 -> {'harfbuzz'}
.................. check: harfbuzz
................. check: nut
.................. direct: nut -> set()
................. check: heffte
................. check: libibumad
.................. direct: libibumad -> set()
................. check: qmd-progress
................. check: seqan
.................. direct: seqan -> set()
................. check: protobuf
.................. direct: protobuf -> {'py-onnx', 'mosh', 'tensorflow-serving-client', 'py-protobuf', 'nanopb', 'brpc', 'protobuf-c', 'grpc', 'ps-lite', 'opencv', 'py-tensorflow', 'cntk', 'caffe', 'py-torch'}
.................. check: py-onnx
.................. check: mosh
................... direct: mosh -> set()
.................. check: tensorflow-serving-client
................... direct: tensorflow-serving-client -> set()
.................. check: py-protobuf
................... direct: py-protobuf -> {'py-mysql-connector-python', 'py-chainer', 'py-onnx', 'nanopb', 'py-google-api-core', 'py-tensorboardx', 'lbann', 'py-tensorflow', 'py-googleapis-common-protos', 'py-tensorboard'}
................... check: py-mysql-connector-python
.................... direct: py-mysql-connector-python -> set()
................... check: py-chainer
................... check: py-onnx
................... check: nanopb
.................... direct: nanopb -> set()
................... check: py-google-api-core
.................... direct: py-google-api-core -> {'py-google-cloud-core'}
.................... check: py-google-cloud-core
..................... direct: py-google-cloud-core -> {'py-google-cloud-storage'}
..................... check: py-google-cloud-storage
...................... direct: py-google-cloud-storage -> {'py-geeadd', 'py-geeup', 'py-smart-open', 'py-gee-asset-manager'}
...................... check: py-geeadd
....................... direct: py-geeadd -> set()
...................... check: py-geeup
...................... check: py-smart-open
....................... direct: py-smart-open -> {'py-gensim'}
....................... check: py-gensim
...................... check: py-gee-asset-manager
....................... direct: py-gee-asset-manager -> set()
................... check: py-tensorboardx
.................... direct: py-tensorboardx -> set()
................... check: lbann
................... check: py-tensorflow
................... check: py-googleapis-common-protos
.................... direct: py-googleapis-common-protos -> {'py-google-api-core'}
.................... check: py-google-api-core
................... check: py-tensorboard
.................. check: nanopb
.................. check: brpc
................... direct: brpc -> set()
.................. check: protobuf-c
................... direct: protobuf-c -> set()
.................. check: grpc
................... direct: grpc -> {'tensorflow-serving-client'}
................... check: tensorflow-serving-client
.................. check: ps-lite
................... direct: ps-lite -> {'mxnet'}
................... check: mxnet
.................. check: opencv
.................. check: py-tensorflow
.................. check: cntk
.................. check: caffe
.................. check: py-torch
................. check: votca-xtp
................. check: spiral
.................. direct: spiral -> {'hcol'}
.................. check: hcol
................... direct: hcol -> set()
................. check: kripke
................. check: glew
.................. direct: glew -> {'py-pymol', 'tulip', 'opensubdiv', 'root', 'rodinia', 'vtk', 'gource', 'gplates'}
.................. check: py-pymol
................... direct: py-pymol -> set()
.................. check: tulip
................... direct: tulip -> set()
.................. check: opensubdiv
.................. check: root
.................. check: rodinia
................... direct: rodinia -> set()
.................. check: vtk
.................. check: gource
................... direct: gource -> set()
.................. check: gplates
................. check: yajl
.................. direct: yajl -> {'tulip', 'i3'}
.................. check: tulip
.................. check: i3
................. check: sentencepiece
.................. direct: sentencepiece -> {'py-sentencepiece'}
.................. check: py-sentencepiece
................... direct: py-sentencepiece -> {'py-transformers', 'py-torchtext'}
................... check: py-transformers
................... check: py-torchtext
................. check: denovogear
.................. direct: denovogear -> set()
................. check: opencascade
................. check: uvw
................. check: aoflagger
................. check: cppzmq
.................. direct: cppzmq -> {'xeus'}
.................. check: xeus
................... direct: xeus -> set()
................. check: latte
................. check: racon
.................. direct: racon -> {'py-unicycler'}
.................. check: py-unicycler
................. check: dealii-parameter-gui
................. check: cotter
................. check: tethex
.................. direct: tethex -> set()
................. check: phasta
................. check: py-torch
................. check: libjpeg-turbo
.................. direct: libjpeg-turbo -> {'fox', 'isaac-server', 'lcms', 'mapserver', 'py-pillow', 'qt', 'gdl', 'graphicsmagick', 'ghostscript', 'py-pynio', 'liggghts', 'opencv', 'vigra', 'motif', 'ncl', 'vtk', 'gource', 'lammps', 'r-tiff', 'jasper', 'hdf', 'libgd', 'libgeotiff', 'py-rasterio', 'unblur', 'gdk-pixbuf', 'swftools', 'r-readbitmap', 'libtiff', 'ncbi-toolkit', 'r', 'icedtea', 'isaac', 'paraview', 'imagemagick', 'tulip', 'libmng', 'poppler', 'r-jpeg', 'root', 'pslib', 'emacs', 'openslide', 'virtualgl', 'blast-plus', 'gdal', 'imlib2'}
.................. check: fox
.................. check: isaac-server
................... direct: isaac-server -> set()
.................. check: lcms
................... direct: lcms -> {'icedtea', 'graphicsmagick', 'libmng', 'ghostscript', 'poppler', 'py-pillow'}
................... check: icedtea
................... check: graphicsmagick
................... check: libmng
................... check: ghostscript
................... check: poppler
................... check: py-pillow
.................. check: mapserver
.................. check: py-pillow
.................. check: qt
.................. check: gdl
.................. check: graphicsmagick
.................. check: ghostscript
.................. check: py-pynio
.................. check: liggghts
.................. check: opencv
.................. check: vigra
.................. check: motif
................... direct: motif -> {'dislin', 'geant4', 'amrvis', 'opendx'}
................... check: dislin
................... check: geant4
................... check: amrvis
................... check: opendx
.................... direct: opendx -> set()
.................. check: ncl
.................. check: vtk
.................. check: gource
.................. check: lammps
.................. check: r-tiff
.................. check: jasper
................... direct: jasper -> {'graphicsmagick', 'eccodes', 'openscenegraph', 'opencv', 'gdal', 'grib-api'}
................... check: graphicsmagick
................... check: eccodes
................... check: openscenegraph
.................... direct: openscenegraph -> {'sumo'}
.................... check: sumo
................... check: opencv
................... check: gdal
................... check: grib-api
.................. check: hdf
................... direct: hdf -> {'gdl', 'netcdf-c', 'h5utils', 'ncl', 'gdal'}
................... check: gdl
................... check: netcdf-c
................... check: h5utils
................... check: ncl
................... check: gdal
.................. check: libgd
................... direct: libgd -> {'graphviz', 'mscgen', 'perl-gd', 'emboss', 'gnuplot', 'texlive'}
................... check: graphviz
................... check: mscgen
.................... direct: mscgen -> {'doxygen'}
.................... check: doxygen
................... check: perl-gd
.................... direct: perl-gd -> {'circos', 'perl-gdtextutil', 'perl-gdgraph'}
.................... check: circos
..................... direct: circos -> set()
.................... check: perl-gdtextutil
..................... direct: perl-gdtextutil -> {'perl-gdgraph'}
..................... check: perl-gdgraph
...................... direct: perl-gdgraph -> {'fastq-screen', 'breakdancer'}
...................... check: fastq-screen
....................... direct: fastq-screen -> set()
...................... check: breakdancer
....................... direct: breakdancer -> set()
.................... check: perl-gdgraph
................... check: emboss
.................... direct: emboss -> {'py-crispresso', 'tppred'}
.................... check: py-crispresso
.................... check: tppred
................... check: gnuplot
................... check: texlive
.................. check: libgeotiff
................... direct: libgeotiff -> {'saga-gis', 'gdal', 'liblas'}
................... check: saga-gis
................... check: gdal
................... check: liblas
.................. check: py-rasterio
.................. check: unblur
.................. check: gdk-pixbuf
.................. check: swftools
.................. check: r-readbitmap
.................. check: libtiff
................... direct: libtiff -> {'fox', 'lcms', 'libvips', 'r-imager', 'py-pillow', 'qt', 'relion', 'graphicsmagick', 'ghostscript', 'openimageio', 'motioncor2', 'opencv', 'vigra', 'vtk', 'r-tiff', 'unblur', 'libgeotiff', 'libgd', 'glvis', 'gdk-pixbuf', 'dcmtk', 'mrtrix3', 'ncbi-toolkit', 'r', 'paraview', 'imagemagick', 'grass', 'openscenegraph', 'mapnik', 'poppler', 'emacs', 'openslide', 'gdal', 'imlib2'}
................... check: fox
................... check: lcms
................... check: libvips
................... check: r-imager
................... check: py-pillow
................... check: qt
................... check: relion
................... check: graphicsmagick
................... check: ghostscript
................... check: openimageio
................... check: motioncor2
.................... direct: motioncor2 -> set()
................... check: opencv
................... check: vigra
................... check: vtk
................... check: r-tiff
................... check: unblur
................... check: libgeotiff
................... check: libgd
................... check: glvis
................... check: gdk-pixbuf
................... check: dcmtk
.................... direct: dcmtk -> set()
................... check: mrtrix3
................... check: ncbi-toolkit
................... check: r
................... check: paraview
................... check: imagemagick
................... check: grass
................... check: openscenegraph
................... check: mapnik
................... check: poppler
................... check: emacs
................... check: openslide
.................... direct: openslide -> {'py-openslide-python'}
.................... check: py-openslide-python
................... check: gdal
................... check: imlib2
.................... direct: imlib2 -> {'feh'}
.................... check: feh
..................... direct: feh -> set()
.................. check: ncbi-toolkit
.................. check: r
.................. check: icedtea
.................. check: isaac
.................. check: paraview
.................. check: imagemagick
.................. check: tulip
.................. check: libmng
.................. check: poppler
.................. check: r-jpeg
.................. check: root
.................. check: pslib
................... direct: pslib -> {'gdl'}
................... check: gdl
.................. check: emacs
.................. check: openslide
.................. check: virtualgl
................... direct: virtualgl -> set()
.................. check: blast-plus
................... direct: blast-plus -> {'transposome', 'shortbred', 'tcoffee', 'trinity', 'py-vcf-kit', 'maker', 'microbiomeutil', 'py-pyani', 'busco', 'py-unicycler', 'prokka', 'blast2go', 'hybpiper', 'orthofinder', 'orthomcl', 'biopieces'}
................... check: transposome
.................... direct: transposome -> set()
................... check: shortbred
.................... direct: shortbred -> set()
................... check: tcoffee
.................... direct: tcoffee -> set()
................... check: trinity
................... check: py-vcf-kit
................... check: maker
................... check: microbiomeutil
.................... direct: microbiomeutil -> set()
................... check: py-pyani
................... check: busco
................... check: py-unicycler
................... check: prokka
................... check: blast2go
.................... direct: blast2go -> set()
................... check: hybpiper
.................... direct: hybpiper -> set()
................... check: orthofinder
................... check: orthomcl
................... check: biopieces
.................. check: gdal
.................. check: imlib2
................. check: scr
................. check: shark
.................. direct: shark -> set()
................. check: scorec-core
................. check: py-tfdlpack
................. check: jsoncpp
.................. direct: jsoncpp -> {'vtk'}
.................. check: vtk
................. check: pocl
................. check: flibcpp
.................. direct: flibcpp -> set()
................. check: packmol
.................. direct: packmol -> set()
................. check: exampm
................. check: percept
................. check: f18
.................. direct: f18 -> set()
................. check: xsdk-examples
................. check: bpp-seq
.................. direct: bpp-seq -> {'bpp-suite', 'bpp-phyl'}
.................. check: bpp-suite
................... direct: bpp-suite -> {'prank'}
................... check: prank
.................... direct: prank -> {'guidance'}
.................... check: guidance
..................... direct: guidance -> set()
.................. check: bpp-phyl
................... direct: bpp-phyl -> {'bpp-suite'}
................... check: bpp-suite
................. check: tulip
................. check: iegenlib
.................. direct: iegenlib -> {'chill'}
.................. check: chill
................. check: draco
................. check: icet
................. check: ethminer
.................. direct: ethminer -> set()
................. check: bam-readcount
.................. direct: bam-readcount -> set()
................. check: kallisto
................. check: libaec
.................. direct: libaec -> {'adios', 'cdo', 'hdf', 'eccodes', 'hdf5', 'ncl', 'grib-api'}
.................. check: adios
.................. check: cdo
.................. check: hdf
.................. check: eccodes
.................. check: hdf5
.................. check: ncl
.................. check: grib-api
................. check: libsplash
................. check: assimp
.................. direct: assimp -> {'dd4hep', 'dealii'}
.................. check: dd4hep
.................. check: dealii
................. check: pugixml
.................. direct: pugixml -> {'quinoa'}
.................. check: quinoa
................. check: arborx
................. check: eigen
.................. direct: eigen -> {'imp', 'ceres-solver', 'fenics', 'paradiseo', 'py-torch', 'gdl', 'iq-tree', 'opencv', 'vtk', 'libfive', 'libmesh', 'parsplice', 'ibmisc', 'sumo', 'memsurfer', 'channelflow', 'mrcpp', 'mrtrix3', 'precice', 'vpfft', 'denovogear', 'cantera', 'votca-tools', 'acts', 'openbabel', 'phist'}
.................. check: imp
.................. check: ceres-solver
.................. check: fenics
.................. check: paradiseo
.................. check: py-torch
.................. check: gdl
.................. check: iq-tree
.................. check: opencv
.................. check: vtk
.................. check: libfive
................... direct: libfive -> set()
.................. check: libmesh
.................. check: parsplice
.................. check: ibmisc
.................. check: sumo
.................. check: memsurfer
.................. check: channelflow
.................. check: mrcpp
.................. check: mrtrix3
.................. check: precice
.................. check: vpfft
.................. check: denovogear
.................. check: cantera
.................. check: votca-tools
.................. check: acts
.................. check: openbabel
.................. check: phist
................. check: grib-api
................. check: jchronoss
.................. direct: jchronoss -> set()
................. check: dihydrogen
................. check: sas
................. check: tioga
................. check: brotli
.................. direct: brotli -> {'nix'}
.................. check: nix
................... direct: nix -> set()
................. check: catalyst
................. check: alquimia
................. check: flecsi
................. check: pism
................. check: apex
.................. direct: apex -> set()
................. check: uriparser
................. check: magics
................. check: prmon
................. check: pmlib
................. check: ross
................. check: cram
................. check: axl
................. check: rapidjson
.................. direct: rapidjson -> {'arrow', 'openbabel', 'opencascade'}
.................. check: arrow
.................. check: openbabel
.................. check: opencascade
................. check: tinyxml
.................. direct: tinyxml -> {'nektar'}
.................. check: nektar
................. check: veloc
................. check: wt
................. check: akantu
................. check: ompt-openmp
.................. direct: ompt-openmp -> {'apex'}
.................. check: apex
................. check: openfoam
................. check: hpx
................. check: openscenegraph
................. check: funhpc
................. check: superlu
.................. direct: superlu -> {'armadillo', 'trilinos', 'kokkos-kernels'}
.................. check: armadillo
.................. check: trilinos
.................. check: kokkos-kernels
................. check: xeus
................. check: wireshark
................. check: jhpcn-df
.................. direct: jhpcn-df -> set()
................. check: xtl
.................. direct: xtl -> {'xeus', 'xtensor-python', 'xtensor'}
.................. check: xeus
.................. check: xtensor-python
.................. check: xtensor
................... direct: xtensor -> {'xtensor-python'}
................... check: xtensor-python
................. check: libgpuarray
.................. direct: libgpuarray -> {'py-pygpu', 'py-theano'}
.................. check: py-pygpu
.................. check: py-theano
................. check: freeglut
.................. direct: freeglut -> {'gl2ps', 'rodinia', 'boinc-client', 'py-pymol'}
.................. check: gl2ps
.................. check: rodinia
.................. check: boinc-client
.................. check: py-pymol
................. check: damaris
................. check: lordec
................. check: adept-utils
................. check: blaze
.................. direct: blaze -> set()
................. check: osqp
.................. direct: osqp -> set()
................. check: bohrium
................. check: cpuinfo
.................. direct: cpuinfo -> set()
................. check: py-espressopp
................. check: nlohmann-json
.................. direct: nlohmann-json -> {'openpmd-api', 'xeus', 'prmon', 'acts', 'gitconddb'}
.................. check: openpmd-api
.................. check: xeus
.................. check: prmon
.................. check: acts
.................. check: gitconddb
................. check: mapserver
................. check: freebayes
.................. direct: freebayes -> set()
................. check: ftgl
................. check: podio
................. check: flang
.................. direct: flang -> set()
................. check: rust
................. check: mariadb
................. check: aom
.................. direct: aom -> {'ffmpeg'}
.................. check: ffmpeg
................... direct: ffmpeg -> {'openscenegraph', 'openimageio', 'r-animation', 'gmt', 'opencv', 'py-imageio', 'py-youtube-dl', 'vtk', 'py-matplotlib', 'lammps', 'py-torch', 'sumo'}
................... check: openscenegraph
................... check: openimageio
................... check: r-animation
................... check: gmt
................... check: opencv
................... check: py-imageio
................... check: py-youtube-dl
.................... direct: py-youtube-dl -> set()
................... check: vtk
................... check: py-matplotlib
................... check: lammps
................... check: py-torch
................... check: sumo
................. check: intel-tbb
.................. direct: intel-tbb -> {'salmon', 'dealii', 'xtensor', 'py-torch', 'onednn', 'bowtie', 'suite-sparse', 'libmesh', 'rocksdb', 'sailfish', 'bowtie2', 'dyninst', 'hpx', 'opencascade', 'opensubdiv', 'root', 'acts', 'hpctoolkit', 'oce', 'vtk-m', 'gaudi'}
.................. check: salmon
.................. check: dealii
.................. check: xtensor
.................. check: py-torch
.................. check: onednn
.................. check: bowtie
................... direct: bowtie -> {'shortstack', 'py-methylcode', 'trinity', 'rsem', 'cleaveland4', 'butter', 'fastq-screen', 'biopieces', 'mirdeep2'}
................... check: shortstack
.................... direct: shortstack -> set()
................... check: py-methylcode
................... check: trinity
................... check: rsem
................... check: cleaveland4
................... check: butter
.................... direct: butter -> set()
................... check: fastq-screen
................... check: biopieces
................... check: mirdeep2
.................... direct: mirdeep2 -> set()
.................. check: suite-sparse
.................. check: libmesh
.................. check: rocksdb
................... direct: rocksdb -> set()
.................. check: sailfish
................... direct: sailfish -> set()
.................. check: bowtie2
................... direct: bowtie2 -> {'hic-pro', 'bismark', 'trinity', 'sparta', 'py-unicycler', 'rsem', 'tophat', 'fastq-screen'}
................... check: hic-pro
................... check: bismark
.................... direct: bismark -> set()
................... check: trinity
................... check: sparta
................... check: py-unicycler
................... check: rsem
................... check: tophat
.................... direct: tophat -> set()
................... check: fastq-screen
.................. check: dyninst
................... direct: dyninst -> {'cbtf-krell', 'openspeedshop', 'stat', 'extrae', 'caliper', 'hpctoolkit', 'mitos', 'callpath', 'openspeedshop-utils', 'timemory'}
................... check: cbtf-krell
................... check: openspeedshop
................... check: stat
................... check: extrae
................... check: caliper
................... check: hpctoolkit
................... check: mitos
................... check: callpath
................... check: openspeedshop-utils
................... check: timemory
.................. check: hpx
.................. check: opencascade
.................. check: opensubdiv
.................. check: root
.................. check: acts
.................. check: hpctoolkit
.................. check: oce
................... direct: oce -> {'cgm', 'dealii', 'gmsh', 'netgen'}
................... check: cgm
................... check: dealii
................... check: gmsh
................... check: netgen
.................. check: vtk-m
.................. check: gaudi
................. check: relax
................. check: openmc
................. check: panda
................. check: sirius
................. check: blaspp
................. check: cbtf-argonavis-gui
................. check: bookleaf-cpp
................. check: benchmark
.................. direct: benchmark -> set()
................. check: brpc
................. check: albany
................. check: muster
................. check: aluminum
................. check: mrcpp
................. check: ascent
................. check: pngwriter
.................. direct: pngwriter -> {'rmlab'}
.................. check: rmlab
................... direct: rmlab -> set()
................. check: cereal
.................. direct: cereal -> {'funhpc', 'lbann'}
.................. check: funhpc
.................. check: lbann
................. check: catch2
.................. direct: catch2 -> {'dihydrogen', 'openpmd-api', 'lbann'}
.................. check: dihydrogen
.................. check: openpmd-api
.................. check: lbann
................. check: breakdancer
................. check: kokkos-nvcc-wrapper
................. check: ecp-io-sdk
................. check: dbcsr
................. check: nfs-ganesha
.................. direct: nfs-ganesha -> set()
................. check: vtk-m
................. check: phist
................. check: yaml-cpp
.................. direct: yaml-cpp -> {'flux-sched', 'nalu', 'nalu-wind', 'bookleaf-cpp', 'asdf-cxx', 'percept', 'of-precice', 'flux-core', 'openfast'}
.................. check: flux-sched
.................. check: nalu
.................. check: nalu-wind
.................. check: bookleaf-cpp
.................. check: asdf-cxx
.................. check: percept
.................. check: of-precice
.................. check: flux-core
.................. check: openfast
................. check: symengine
................. check: qmcpack
................. check: iwyu
.................. direct: iwyu -> set()
................. check: ldc
.................. direct: ldc -> {'sambamba'}
.................. check: sambamba
................... direct: sambamba -> set()
................. check: somatic-sniper
.................. direct: somatic-sniper -> set()
................. check: pythia6
.................. direct: pythia6 -> {'root'}
.................. check: root
................. check: pagmo
................. check: opencoarrays
................. check: onednn
................. check: libdivsufsort
.................. direct: libdivsufsort -> {'andi'}
.................. check: andi
................... direct: andi -> set()
................. check: meraculous
................. check: trilinos-catalyst-ioss-adapter
................. check: vtk
................. check: pgmath
.................. direct: pgmath -> {'flang'}
.................. check: flang
................. check: flecsph
................. check: sleef
.................. direct: sleef -> set()
................. check: sniffles
................. check: highfive
................. check: lazyten
................. check: elemental
................. check: everytrace-example
................. check: mono
.................. direct: mono -> {'sbml'}
.................. check: sbml
................. check: hoomd-blue
................. check: libssh2
.................. direct: libssh2 -> {'aria2', 'curl', 'libgit2', 'rust', 'mc'}
.................. check: aria2
................... direct: aria2 -> set()
.................. check: curl
................... direct: curl -> {'cdo', 'git', 'tixi', 'salmon', 'cosbench', 'mapserver', 'gmt', 'ldc-bootstrap', 'r-rhtslib', 'feh', 'py-pybigwig', 'cmake', 'ldc', 'skilion-onedrive', 'slurm', 'mariadb', 'gpdb', 'nix', 'r-curl', 'py-cyvcf2', 'py-pycurl', 'ncl', 'py-tensorflow', 'r-rcurl', 'julia', 'cfitsio', 'eagle', 'libdap4', 'hyphy', 'libgit2', 'htslib', 'octave', 'boinc-client', 'augustus', 'dmd', 'r', 'cnvnator', 'py-pysam', 'angsd', 'netcdf-c', 'mariadb-c-client', 'poppler', 'kcov', 'ethminer', 'gdal'}
................... check: cdo
................... check: git
.................... direct: git -> {'singularity', 'ruby-svn2git', 'rose', 'vcsh', 'git-imerge', 'py-setuptools-git', 'py-git-review', 'shiny-server', 'lua-luafilesystem', 'catalyst', 'rr', 'julia', 'git-fat-git', 'fairlogger', 'helics', 'oclint', 'hpx', 'wireshark', 'go', 'git-lfs', 'go-bootstrap'}
.................... check: singularity
..................... direct: singularity -> set()
.................... check: ruby-svn2git
..................... direct: ruby-svn2git -> set()
.................... check: rose
.................... check: vcsh
..................... direct: vcsh -> set()
.................... check: git-imerge
..................... direct: git-imerge -> set()
.................... check: py-setuptools-git
..................... direct: py-setuptools-git -> set()
.................... check: py-git-review
..................... direct: py-git-review -> set()
.................... check: shiny-server
.................... check: lua-luafilesystem
..................... direct: lua-luafilesystem -> {'lmod'}
..................... check: lmod
...................... direct: lmod -> set()
.................... check: catalyst
.................... check: rr
..................... direct: rr -> set()
.................... check: julia
..................... direct: julia -> {'simulationio'}
..................... check: simulationio
.................... check: git-fat-git
..................... direct: git-fat-git -> set()
.................... check: fairlogger
..................... direct: fairlogger -> set()
.................... check: helics
.................... check: oclint
..................... direct: oclint -> set()
.................... check: hpx
.................... check: wireshark
.................... check: go
..................... direct: go -> {'singularity', 'hub', 'fzf', 'direnv', 'go-md2man', 'trident', 'skopeo', 'etcd', 'umoci', 'hugo', 'git-lfs', 'rclone', 'the-platinum-searcher', 'kubernetes'}
..................... check: singularity
..................... check: hub
...................... direct: hub -> set()
..................... check: fzf
...................... direct: fzf -> set()
..................... check: direnv
...................... direct: direnv -> set()
..................... check: go-md2man
...................... direct: go-md2man -> {'skopeo', 'umoci'}
...................... check: skopeo
....................... direct: skopeo -> {'charliecloud'}
....................... check: charliecloud
........................ direct: charliecloud -> set()
...................... check: umoci
....................... direct: umoci -> {'charliecloud'}
....................... check: charliecloud
..................... check: trident
...................... direct: trident -> set()
..................... check: skopeo
..................... check: etcd
...................... direct: etcd -> set()
..................... check: umoci
..................... check: hugo
...................... direct: hugo -> set()
..................... check: git-lfs
...................... direct: git-lfs -> set()
..................... check: rclone
...................... direct: rclone -> set()
..................... check: the-platinum-searcher
...................... direct: the-platinum-searcher -> set()
..................... check: kubernetes
...................... direct: kubernetes -> set()
.................... check: git-lfs
.................... check: go-bootstrap
..................... direct: go-bootstrap -> {'go'}
..................... check: go
................... check: tixi
.................... direct: tixi -> set()
................... check: salmon
................... check: cosbench
................... check: mapserver
................... check: gmt
................... check: ldc-bootstrap
.................... direct: ldc-bootstrap -> {'ldc'}
.................... check: ldc
................... check: r-rhtslib
................... check: feh
................... check: py-pybigwig
.................... direct: py-pybigwig -> {'py-deeptools', 'py-crossmap', 'py-rseqc'}
.................... check: py-deeptools
.................... check: py-crossmap
.................... check: py-rseqc
................... check: cmake
................... check: ldc
................... check: skilion-onedrive
.................... direct: skilion-onedrive -> set()
................... check: slurm
................... check: mariadb
................... check: gpdb
.................... direct: gpdb -> set()
................... check: nix
................... check: r-curl
................... check: py-cyvcf2
................... check: py-pycurl
.................... direct: py-pycurl -> {'py-nbconvert'}
.................... check: py-nbconvert
..................... direct: py-nbconvert -> {'py-jupyter', 'py-spyder', 'py-notebook'}
..................... check: py-jupyter
...................... direct: py-jupyter -> {'py-abipy', 'py-spatialist'}
...................... check: py-abipy
...................... check: py-spatialist
..................... check: py-spyder
..................... check: py-notebook
...................... direct: py-notebook -> {'py-jupyter', 'py-jupyterlab', 'py-jupyterlab-server', 'py-widgetsnbextension', 'py-jupyterhub'}
...................... check: py-jupyter
...................... check: py-jupyterlab
....................... direct: py-jupyterlab -> set()
...................... check: py-jupyterlab-server
....................... direct: py-jupyterlab-server -> {'py-jupyterlab'}
....................... check: py-jupyterlab
...................... check: py-widgetsnbextension
....................... direct: py-widgetsnbextension -> {'py-ipywidgets'}
....................... check: py-ipywidgets
........................ direct: py-ipywidgets -> {'py-jupyter', 'py-spatialist', 'py-notebook'}
........................ check: py-jupyter
........................ check: py-spatialist
........................ check: py-notebook
...................... check: py-jupyterhub
................... check: ncl
................... check: py-tensorflow
................... check: r-rcurl
................... check: julia
................... check: cfitsio
.................... direct: cfitsio -> {'ccfits', 'py-astropy', 'healpix-cxx', 'planck-likelihood', 'wcslib', 'root', 'gdal', 'aoflagger', 'casacore'}
.................... check: ccfits
..................... direct: ccfits -> set()
.................... check: py-astropy
.................... check: healpix-cxx
.................... check: planck-likelihood
..................... direct: planck-likelihood -> {'cosmomc'}
..................... check: cosmomc
.................... check: wcslib
..................... direct: wcslib -> {'py-astropy', 'casacore'}
..................... check: py-astropy
..................... check: casacore
.................... check: root
.................... check: gdal
.................... check: aoflagger
.................... check: casacore
................... check: eagle
.................... direct: eagle -> set()
................... check: libdap4
.................... direct: libdap4 -> set()
................... check: hyphy
.................... direct: hyphy -> set()
................... check: libgit2
................... check: htslib
.................... direct: htslib -> {'denovogear', 'cnvnator', 'blasr', 'py-pysam', 'delly2', 'angsd', 'eagle', 'samtools', 'pindel', 'lumpy-sv', 'scallop', 'bcftools', 'platypus', 'pbbam', 'augustus'}
.................... check: denovogear
.................... check: cnvnator
.................... check: blasr
.................... check: py-pysam
.................... check: delly2
.................... check: angsd
.................... check: eagle
.................... check: samtools
..................... direct: samtools -> {'py-vcf-kit', 'py-unicycler', 'py-misopy', 'stringtie', 'discovardenovo', 'hybpiper', 'fastq-screen', 'pgdspider', 'bismark', 'py-breakseq2', 'cleaveland4', 'preseq', 'shortstack', 'portcullis', 'phyluce', 'ncbi-toolkit', 'braker', 'augustus', 'cnvnator', 'hic-pro', 'py-pysam', 'trinity', 'phantompeakqualtools', 'butter'}
..................... check: py-vcf-kit
..................... check: py-unicycler
..................... check: py-misopy
..................... check: stringtie
...................... direct: stringtie -> set()
..................... check: discovardenovo
...................... direct: discovardenovo -> set()
..................... check: hybpiper
..................... check: fastq-screen
..................... check: pgdspider
..................... check: bismark
..................... check: py-breakseq2
..................... check: cleaveland4
..................... check: preseq
...................... direct: preseq -> set()
..................... check: shortstack
..................... check: portcullis
..................... check: phyluce
..................... check: ncbi-toolkit
..................... check: braker
..................... check: augustus
..................... check: cnvnator
..................... check: hic-pro
..................... check: py-pysam
..................... check: trinity
..................... check: phantompeakqualtools
..................... check: butter
.................... check: pindel
..................... direct: pindel -> set()
.................... check: lumpy-sv
..................... direct: lumpy-sv -> set()
.................... check: scallop
..................... direct: scallop -> set()
.................... check: bcftools
.................... check: platypus
..................... direct: platypus -> set()
.................... check: pbbam
.................... check: augustus
................... check: octave
................... check: boinc-client
................... check: augustus
................... check: dmd
.................... direct: dmd -> {'skilion-onedrive', 'msmc'}
.................... check: skilion-onedrive
.................... check: msmc
..................... direct: msmc -> set()
................... check: r
................... check: cnvnator
................... check: py-pysam
................... check: angsd
................... check: netcdf-c
................... check: mariadb-c-client
.................... direct: mariadb-c-client -> {'root', 'r-rmariadb', 'julea'}
.................... check: root
.................... check: r-rmariadb
.................... check: julea
................... check: poppler
................... check: kcov
.................... direct: kcov -> set()
................... check: ethminer
................... check: gdal
.................. check: libgit2
.................. check: rust
.................. check: mc
................... direct: mc -> set()
................. check: gitconddb
................. check: satsuma2
.................. direct: satsuma2 -> set()
................. check: molcas
................. check: ecflow
.................. direct: ecflow -> set()
................. check: dftfe
................. check: laszip
.................. direct: laszip -> {'liblas'}
.................. check: liblas
................. check: clara
.................. direct: clara -> {'lbann'}
.................. check: lbann
................. check: boxlib
................. check: tinker
................. check: cpu-features
.................. direct: cpu-features -> set()
................. check: virtualgl
................. check: cbtf
.................. direct: cbtf -> {'cbtf-argonavis-gui', 'openspeedshop', 'cbtf-krell', 'cbtf-argonavis', 'openspeedshop-utils', 'cbtf-lanl'}
.................. check: cbtf-argonavis-gui
.................. check: openspeedshop
.................. check: cbtf-krell
.................. check: cbtf-argonavis
.................. check: openspeedshop-utils
.................. check: cbtf-lanl
................. check: sosflow
................. check: glog
.................. direct: glog -> {'folly', 'caffe', 'ceres-solver'}
.................. check: folly
................... direct: folly -> set()
.................. check: caffe
.................. check: ceres-solver
................. check: imp
................. check: intel-llvm
.................. direct: intel-llvm -> set()
................. check: spfft
................. check: py-pyside2
................. check: cnmem
.................. direct: cnmem -> set()
................. check: ctre
.................. direct: ctre -> set()
................. check: mindthegap
.................. direct: mindthegap -> set()
................. check: qtkeychain
................. check: shiny-server
................. check: amp
................. check: ntpoly
................. check: mmg
................. check: veccore
.................. direct: veccore -> {'vecgeom'}
.................. check: vecgeom
................. check: neovim
.................. direct: neovim -> set()
................. check: cuda-memtest
.................. direct: cuda-memtest -> set()
................. check: parsplice
................. check: cnpy
.................. direct: cnpy -> {'lbann'}
.................. check: lbann
................. check: py-symengine
................. check: camellia
................. check: macsio
................. check: py-dgl
................. check: cabana
................. check: doxygen
................. check: mariadb-c-client
................. check: gtkorvo-dill
.................. direct: gtkorvo-dill -> {'libffs'}
.................. check: libffs
................. check: kim-api
.................. direct: kim-api -> {'openkim-models'}
.................. check: openkim-models
................... direct: openkim-models -> set()
................. check: mercury
................. check: med
................. check: openmm
................. check: bml
................. check: talass
.................. direct: talass -> set()
................. check: claw
................. check: py-pybind11
.................. direct: py-pybind11 -> {'py-projectq', 'openpmd-api', 'xtensor-python', 'py-spdlog', 'openimageio', 'xcfun', 'sirius', 'py-scipy', 'akantu', 'py-torch'}
.................. check: py-projectq
.................. check: openpmd-api
.................. check: xtensor-python
.................. check: py-spdlog
................... direct: py-spdlog -> set()
.................. check: openimageio
.................. check: xcfun
.................. check: sirius
.................. check: py-scipy
.................. check: akantu
.................. check: py-torch
................. check: votca-csg-tutorials
................. check: fenics
................. check: gearshifft
................. check: log4cplus
.................. direct: log4cplus -> {'kea'}
.................. check: kea
................... direct: kea -> set()
................. check: spglib
.................. direct: spglib -> {'shengbte', 'py-thirdorder', 'dftfe', 'sirius'}
.................. check: shengbte
.................. check: py-thirdorder
.................. check: dftfe
.................. check: sirius
................. check: libfive
................. check: heaptrack
.................. direct: heaptrack -> set()
................. check: sollve
.................. direct: sollve -> set()
................. check: memaxes
.................. direct: memaxes -> set()
................. check: uchardet
.................. direct: uchardet -> set()
................. check: tinyxml2
.................. direct: tinyxml2 -> set()
................. check: llvm-openmp-ompt
................. check: xrootd
.................. direct: xrootd -> {'root'}
.................. check: root
................. check: cbtf-argonavis
................. check: lemon
.................. direct: lemon -> set()
................. check: mad-numdiff
.................. direct: mad-numdiff -> {'quinoa'}
.................. check: quinoa
................. check: oclint
................. check: manta
.................. direct: manta -> set()
................. check: sph2pipe
.................. direct: sph2pipe -> {'kaldi'}
.................. check: kaldi
................... direct: kaldi -> {'cntk'}
................... check: cntk
................. check: gmodel
.................. direct: gmodel -> set()
................. check: precice
................. check: capstone
.................. direct: capstone -> set()
................. check: pbbam
................. check: strelka
.................. direct: strelka -> set()
................. check: dyninst
................. check: poppler
................. check: ufo-filters
.................. direct: ufo-filters -> set()
................. check: nnvm
.................. direct: nnvm -> {'mxnet'}
.................. check: mxnet
................. check: ants
.................. direct: ants -> set()
................. check: tixi
................. check: gloo
.................. direct: gloo -> {'py-torch'}
.................. check: py-torch
................. check: rmlab
................. check: zfp
.................. direct: zfp -> {'adios', 'conduit', 'adios2', 'ecp-viz-sdk', 'h5z-zfp'}
.................. check: adios
.................. check: conduit
.................. check: adios2
.................. check: ecp-viz-sdk
.................. check: h5z-zfp
................. check: ray
................. check: c-ares
.................. direct: c-ares -> {'aria2', 'py-grpcio', 'wireshark', 'grpc'}
.................. check: aria2
.................. check: py-grpcio
................... direct: py-grpcio -> {'py-tensorflow', 'py-tensorboard'}
................... check: py-tensorflow
................... check: py-tensorboard
.................. check: wireshark
.................. check: grpc
................. check: xtensor
................. check: libtlx
.................. direct: libtlx -> {'libnetworkit'}
.................. check: libnetworkit
................... direct: libnetworkit -> set()
................. check: cardioid
................. check: qca
.................. direct: qca -> {'qgis'}
.................. check: qgis
................. check: glfw
................. check: mofem-fracture-module
................. check: poppler-data
.................. direct: poppler-data -> {'poppler'}
.................. check: poppler
................. check: double-conversion
.................. direct: double-conversion -> {'folly', 'qt', 'vtk'}
.................. check: folly
.................. check: qt
.................. check: vtk
................. check: bpp-phyl
................. check: krims
.................. direct: krims -> {'lazyten'}
.................. check: lazyten
................. check: numap
.................. direct: numap -> {'numamma'}
.................. check: numamma
................. check: cleverleaf
................. check: mutationpp
.................. direct: mutationpp -> set()
................. check: ibmisc
................. check: axom
................. check: py-onnx
................. check: ginkgo
.................. direct: ginkgo -> {'xsdk', 'dealii'}
.................. check: xsdk
.................. check: dealii
................. check: jasper
................. check: memsurfer
................. check: seacas
................. check: sensei
................. check: minisign
.................. direct: minisign -> set()
................. check: gunrock
.................. direct: gunrock -> set()
................. check: dcmtk
................. check: utf8proc
.................. direct: utf8proc -> {'subversion'}
.................. check: subversion
................... direct: subversion -> {'ea-utils', 'libbeagle', 'oclint', 'ruby-svn2git'}
................... check: ea-utils
................... check: libbeagle
................... check: oclint
................... check: ruby-svn2git
................. check: raft
................. check: omega-h
................. check: root
................. check: bpp-suite
................. check: kokkos-kernels
................. check: libcint
................. check: textparser
................. check: adiak
................. check: cminpack
.................. direct: cminpack -> set()
................. check: callpath
................. check: lbann
................. check: oce
................. check: optional-lite
.................. direct: optional-lite -> set()
................. check: adios2
................. check: sfcgal
.................. direct: sfcgal -> set()
................. check: umpire
.................. direct: umpire -> {'mfem', 'axom'}
.................. check: mfem
.................. check: axom
................. check: amgx
................. check: isaac-server
................. check: mofem-minimal-surface-equation
................. check: hepmc
................. check: nalu-wind
................. check: uqtk
.................. direct: uqtk -> set()
................. check: armadillo
................. check: liblas
................. check: py-espresso
................. check: sbml
................. check: vigra
................. check: libssh
.................. direct: libssh -> {'ffmpeg', 'curl', 'wireshark'}
.................. check: ffmpeg
.................. check: curl
.................. check: wireshark
................. check: sundials
................. check: c-blosc
.................. direct: c-blosc -> {'adios', 'py-tables', 'adios2', 'hdf5-blosc'}
.................. check: adios
.................. check: py-tables
.................. check: adios2
.................. check: hdf5-blosc
................. check: julia
................. check: glm
.................. direct: glm -> {'gource', 'trilinos', 'py-pymol'}
.................. check: gource
.................. check: trilinos
.................. check: py-pymol
................. check: string-view-lite
.................. direct: string-view-lite -> set()
................. check: clfft
................. check: tensorflow-serving-client
................. check: unittest-cpp
.................. direct: unittest-cpp -> {'mstk', 'jali'}
.................. check: mstk
.................. check: jali
................. check: steps
................. check: shuffile
................. check: channelflow
................. check: openjpeg
.................. direct: openjpeg -> {'eccodes', 'poppler', 'openimageio', 'ffmpeg', 'openslide', 'py-pillow', 'gdal', 'grib-api'}
.................. check: eccodes
.................. check: poppler
.................. check: openimageio
.................. check: ffmpeg
.................. check: openslide
.................. check: py-pillow
.................. check: gdal
.................. check: grib-api
................. check: psi4
................. check: cbtf-krell
................. check: spdlog
.................. direct: spdlog -> set()
................. check: openbabel
................. check: gmsh
................. check: range-v3
................. check: chgcentre
................. check: ntirpc
.................. direct: ntirpc -> {'nfs-ganesha'}
.................. check: nfs-ganesha
................. check: codec2
.................. direct: codec2 -> set()
................. check: ldc-bootstrap
................. check: relion
................. check: nanoflann
.................. direct: nanoflann -> {'dealii'}
.................. check: dealii
................. check: jansson
.................. direct: jansson -> {'isaac', 'isaac-server', 'flux-core'}
.................. check: isaac
.................. check: isaac-server
.................. check: flux-core
................. check: opencv
................. check: vc
.................. direct: vc -> {'root'}
.................. check: root
................. check: rr
................. check: sumo
................. check: clhep
.................. direct: clhep -> {'geant4', 'relax', 'gaudi'}
.................. check: geant4
.................. check: relax
.................. check: gaudi
................. check: accfft
................. check: llvm-openmp
.................. direct: llvm-openmp -> {'onednn', 'opensubdiv', 'py-scikit-learn', 'py-dgl', 'py-torch'}
.................. check: onednn
.................. check: opensubdiv
.................. check: py-scikit-learn
.................. check: py-dgl
.................. check: py-torch
................. check: pegtl
.................. direct: pegtl -> {'quinoa'}
.................. check: quinoa
................. check: libmmtf-cpp
.................. direct: libmmtf-cpp -> {'py-pymol'}
.................. check: py-pymol
................. check: fairlogger
................. check: mgis
.................. direct: mgis -> set()
................. check: papyrus
................. check: perfstubs
.................. direct: perfstubs -> set()
................. check: variorum
.................. direct: variorum -> set()
................. check: gaudi
................. check: tasmanian
................. check: libffs
................. check: timemory
................. check: redset
................. check: task
.................. direct: task -> set()
................. check: nalu
................. check: h5cpp
................. check: ps-lite
................. check: umap
.................. direct: umap -> set()
................. check: caffe
................. check: votca-csgapps
................. check: flann
................. check: hyperscan
.................. direct: hyperscan -> set()
................. check: gdl
................. check: ufo-core
.................. direct: ufo-core -> {'ufo-filters'}
.................. check: ufo-filters
................. check: mallocmc
.................. direct: mallocmc -> set()
................. check: mongo-cxx-driver
.................. direct: mongo-cxx-driver -> set()
................. check: er
................. check: openspeedshop-utils
................. check: xsimd
.................. direct: xsimd -> {'xtensor'}
.................. check: xtensor
................. check: openspeedshop
................. check: express
................. check: fann
.................. direct: fann -> {'pktools'}
.................. check: pktools
................. check: pktools
................. check: votca-csg
................. check: elsi
................. check: gflags
.................. direct: gflags -> {'rocksdb', 'glog', 'brpc', 'folly', 'caffe'}
.................. check: rocksdb
.................. check: glog
.................. check: brpc
.................. check: folly
.................. check: caffe
................. check: cquery
.................. direct: cquery -> set()
................. check: clingo
................. check: damselfly
................. check: py-horovod
................. check: mongo-c-driver
.................. direct: mongo-c-driver -> {'mongo-cxx-driver', 'julea'}
.................. check: mongo-cxx-driver
.................. check: julea
................. check: kokkos-legacy
................. check: visit
................. check: henson
................. check: caliper
................. check: foam-extend
................. check: libnetworkit
................. check: jali
................. check: hiop
................. check: nlopt
................. check: nektar
................. check: salmon
................. check: taskd
.................. direct: taskd -> set()
................. check: ceres-solver
................. check: gmt
................. check: mysql
................. check: casacore
................. check: simulationio
................. check: exiv2
.................. direct: exiv2 -> {'qgis'}
.................. check: qgis
................. check: py-pyside
.................. direct: py-pyside -> {'py-pydv', 'py-qtpy', 'py-pyface', 'py-traitsui'}
.................. check: py-pydv
.................. check: py-qtpy
.................. check: py-pyface
.................. check: py-traitsui
................. check: iq-tree
................. check: revbayes
................. check: sortmerna
.................. direct: sortmerna -> set()
................. check: lapackpp
................. check: lammps
................. check: openfast
................. check: cosma
................. check: xtensor-python
................. check: bear
.................. direct: bear -> set()
................. check: ngmlr
.................. direct: ngmlr -> set()
................. check: amrex
................. check: helics
................. check: stinger
.................. direct: stinger -> set()
................. check: sz
................. check: openmolcas
................. check: gromacs
................. check: isaac
................. check: libwebsockets
.................. direct: libwebsockets -> {'isaac-server', 'jchronoss'}
.................. check: isaac-server
.................. check: jchronoss
................. check: llvm-flang
.................. direct: llvm-flang -> {'flang'}
.................. check: flang
................. check: vpic
................. check: xcfun
................. check: qhull
.................. direct: qhull -> {'octave', 'gdal', 'tulip', 'plplot'}
.................. check: octave
.................. check: gdal
.................. check: tulip
.................. check: plplot
................. check: sdl2
.................. direct: sdl2 -> {'gource', 'ffmpeg', 'sdl2-image'}
.................. check: gource
.................. check: ffmpeg
.................. check: sdl2-image
................... direct: sdl2-image -> {'gource'}
................... check: gource
................. check: filo
................. check: cppgsl
.................. direct: cppgsl -> {'gaudi'}
.................. check: gaudi
................. check: unqlite
.................. direct: unqlite -> set()
................. check: fmt
.................. direct: fmt -> {'fairlogger', 'cantera', 'gitconddb'}
.................. check: fairlogger
.................. check: cantera
.................. check: gitconddb
................. check: bpp-core
.................. direct: bpp-core -> {'bpp-seq', 'bpp-suite', 'bpp-phyl'}
.................. check: bpp-seq
.................. check: bpp-suite
.................. check: bpp-phyl
................. check: metall
.................. direct: metall -> set()
................. check: frontistr
................. check: mt-metis
.................. direct: mt-metis -> set()
................. check: flatcc
.................. direct: flatcc -> {'unifyfs'}
.................. check: unifyfs
................. check: rankstr
................. check: spades
.................. direct: spades -> {'hybpiper', 'phyluce', 'py-unicycler'}
.................. check: hybpiper
.................. check: phyluce
.................. check: py-unicycler
................. check: flecsale
................. check: diy
................. check: pidx
................. check: xdmf3
................. check: simgrid
.................. direct: simgrid -> set()
................. check: qjson
.................. direct: qjson -> {'qgis'}
.................. check: qgis
................. check: sdsl-lite
.................. direct: sdsl-lite -> {'biobloom'}
.................. check: biobloom
................... direct: biobloom -> set()
................. check: snappy
.................. direct: snappy -> {'rocksdb', 'leveldb', 'c-blosc2', 'arrow', 'ffmpeg', 'mongo-c-driver', 'c-blosc'}
.................. check: rocksdb
.................. check: leveldb
................... direct: leveldb -> {'caffe', 'brpc', 'unifyfs', 'julea', 'py-torch'}
................... check: caffe
................... check: brpc
................... check: unifyfs
................... check: julea
................... check: py-torch
.................. check: c-blosc2
................... direct: c-blosc2 -> set()
.................. check: arrow
.................. check: ffmpeg
.................. check: mongo-c-driver
.................. check: c-blosc
................. check: c-blosc2
................. check: everytrace
................. check: openimageio
................. check: dmlc-core
.................. direct: dmlc-core -> {'mxnet', 'nnvm'}
.................. check: mxnet
.................. check: nnvm
................. check: grpc
................. check: rtags
.................. direct: rtags -> set()
................. check: swipl
.................. direct: swipl -> set()
................. check: bcl2fastq2
.................. direct: bcl2fastq2 -> {'supernova'}
.................. check: supernova
................... direct: supernova -> set()
................. check: libpmemobj-cpp
.................. direct: libpmemobj-cpp -> set()
................. check: googletest
.................. direct: googletest -> {'flann', 'xsimd', 'sumo', 'snappy', 'funhpc', 'cantera', 'msgpack-c', 'percept', 'libkml', 'gitconddb', 'py-mypy', 'uriparser', 'faodel', 'ibmisc'}
.................. check: flann
.................. check: xsimd
.................. check: sumo
.................. check: snappy
.................. check: funhpc
.................. check: cantera
.................. check: msgpack-c
................... direct: msgpack-c -> {'py-pymol', 'lua-mpack', 'mariadb', 'libmmtf-cpp', 'neovim'}
................... check: py-pymol
................... check: lua-mpack
.................... direct: lua-mpack -> {'neovim'}
.................... check: neovim
................... check: mariadb
................... check: libmmtf-cpp
................... check: neovim
.................. check: percept
.................. check: libkml
.................. check: gitconddb
.................. check: py-mypy
................... direct: py-mypy -> {'py-sphinx', 'py-pytest-mypy'}
................... check: py-sphinx
.................... direct: py-sphinx -> {'py-opppy', 'py-sphinxcontrib-bibtex', 'py-repoze-lru', 'py-espressopp', 'py-pbr', 'py-sphinxautomodapi', 'py-qtconsole', 'gmt', 'fenics', 'kitty', 'py-pyside2', 'cmake', 'py-breathe', 'py-wand', 'py-pyside', 'py-sphinxcontrib-issuetracker', 'charliecloud', 'py-spyder', 'py-sphinxcontrib-programoutput', 'py-elephant', 'vigra', 'seqan', 'axom', 'fio', 'flibcpp', 'portcullis', 'py-numpydoc', 'py-shiboken', 'ascent', 'py-brian2', 'py-pydotplus', 'py-graphviz', 'py-pythonqwt', 'variorum', 'conduit', 'libnetworkit'}
.................... check: py-opppy
.................... check: py-sphinxcontrib-bibtex
..................... direct: py-sphinxcontrib-bibtex -> set()
.................... check: py-repoze-lru
..................... direct: py-repoze-lru -> set()
.................... check: py-espressopp
.................... check: py-pbr
..................... direct: py-pbr -> {'py-linecache2', 'py-stevedore', 'py-python-jenkins', 'py-traceback2', 'py-symfit', 'py-pytest-check-links', 'py-python-swiftclient', 'py-git-review'}
..................... check: py-linecache2
...................... direct: py-linecache2 -> {'py-codecov'}
...................... check: py-codecov
....................... direct: py-codecov -> set()
..................... check: py-stevedore
...................... direct: py-stevedore -> {'py-virtualenvwrapper'}
...................... check: py-virtualenvwrapper
....................... direct: py-virtualenvwrapper -> set()
..................... check: py-python-jenkins
...................... direct: py-python-jenkins -> set()
..................... check: py-traceback2
...................... direct: py-traceback2 -> {'py-unittest2'}
...................... check: py-unittest2
....................... direct: py-unittest2 -> {'py-linecache2', 'py-protobuf', 'py-xmlrunner', 'py-logilab-common', 'cantera', 'py-codecov', 'py-oauthlib', 'py-filemagic', 'py-psutil', 'py-funcsigs', 'py-traceback2'}
....................... check: py-linecache2
....................... check: py-protobuf
....................... check: py-xmlrunner
........................ direct: py-xmlrunner -> set()
....................... check: py-logilab-common
........................ direct: py-logilab-common -> set()
....................... check: cantera
....................... check: py-codecov
....................... check: py-oauthlib
........................ direct: py-oauthlib -> {'py-jupyterhub', 'py-requests-oauthlib'}
........................ check: py-jupyterhub
........................ check: py-requests-oauthlib
......................... direct: py-requests-oauthlib -> {'py-google-auth-oauthlib'}
......................... check: py-google-auth-oauthlib
.......................... direct: py-google-auth-oauthlib -> {'py-tensorboard'}
.......................... check: py-tensorboard
....................... check: py-filemagic
........................ direct: py-filemagic -> set()
....................... check: py-psutil
........................ direct: py-psutil -> {'py-merlin', 'py-spyder', 'geopm', 'py-geeup', 'py-distributed', 'py-wand', 'py-memory-profiler', 'py-pysocks', 'py-blosc', 'py-horovod', 'py-petastorm', 'py-torch'}
........................ check: py-merlin
........................ check: py-spyder
........................ check: geopm
........................ check: py-geeup
........................ check: py-distributed
......................... direct: py-distributed -> {'py-dask'}
......................... check: py-dask
........................ check: py-wand
........................ check: py-memory-profiler
......................... direct: py-memory-profiler -> set()
........................ check: py-pysocks
......................... direct: py-pysocks -> {'httpie', 'py-urllib3', 'py-requests'}
......................... check: httpie
.......................... direct: httpie -> set()
......................... check: py-urllib3
.......................... direct: py-urllib3 -> {'py-requests', 'py-botocore', 'py-py2neo', 'py-elasticsearch', 'py-selenium'}
.......................... check: py-requests
........................... direct: py-requests -> {'py-bokeh', 'py-cookiecutter', 'py-hdfs', 'py-vcf-kit', 'py-requests-futures', 'py-gluoncv', 'py-wradlib', 'py-werkzeug', 'py-pyepsg', 'py-sphinx', 'py-plotly', 'py-pymatgen', 'py-tensorboard', 'py-git-review', 'py-geeadd', 'py-multiqc', 'candle-benchmarks', 'py-spacy', 'charliecloud', 'py-sphinxcontrib-issuetracker', 'py-usgs', 'py-geeup', 'qgis', 'httpie', 'py-dask', 'py-gee-asset-manager', 'py-python-jenkins', 'py-requests-toolbelt', 'py-jupyterhub', 'py-python-swiftclient', 'py-projectq', 'py-cdsapi', 'py-smart-open', 'snakemake', 'py-owslib', 'py-pipits', 'py-python-gitlab', 'py-requests-oauthlib', 'py-pycbc', 'py-jupyterlab-server', 'py-dgl', 'py-hvac', 'py-mg-rast-tools', 'py-biomine', 'py-torchtext', 'py-jupyterlab', 'py-transformers', 'py-codecov', 'py-google-api-core', 'py-grequests'}
........................... check: py-bokeh
........................... check: py-cookiecutter
............................ direct: py-cookiecutter -> {'py-hpcbench'}
............................ check: py-hpcbench
............................. direct: py-hpcbench -> set()
........................... check: py-hdfs
............................ direct: py-hdfs -> set()
........................... check: py-vcf-kit
........................... check: py-requests-futures
............................ direct: py-requests-futures -> {'py-usgs'}
............................ check: py-usgs
............................. direct: py-usgs -> set()
........................... check: py-gluoncv
........................... check: py-wradlib
........................... check: py-werkzeug
............................ direct: py-werkzeug -> {'py-flask', 'py-flask-socketio', 'py-httpbin', 'py-tensorboard'}
............................ check: py-flask
............................. direct: py-flask -> {'py-ase', 'py-flask-socketio', 'py-flask-compress', 'py-gdbgui', 'py-httpbin', 'py-raven', 'py-pytest-httpbin'}
............................. check: py-ase
............................. check: py-flask-socketio
.............................. direct: py-flask-socketio -> {'py-gdbgui'}
.............................. check: py-gdbgui
............................... direct: py-gdbgui -> set()
............................. check: py-flask-compress
.............................. direct: py-flask-compress -> {'py-gdbgui'}
.............................. check: py-gdbgui
............................. check: py-gdbgui
............................. check: py-httpbin
.............................. direct: py-httpbin -> {'py-pytest-httpbin'}
.............................. check: py-pytest-httpbin
............................... direct: py-pytest-httpbin -> {'py-requests'}
............................... check: py-requests
............................. check: py-raven
.............................. direct: py-raven -> {'py-httpbin'}
.............................. check: py-httpbin
............................. check: py-pytest-httpbin
............................ check: py-flask-socketio
............................ check: py-httpbin
............................ check: py-tensorboard
........................... check: py-pyepsg
............................ direct: py-pyepsg -> {'py-cartopy'}
............................ check: py-cartopy
........................... check: py-sphinx
........................... check: py-plotly
............................ direct: py-plotly -> {'py-deeptools'}
............................ check: py-deeptools
........................... check: py-pymatgen
........................... check: py-tensorboard
........................... check: py-git-review
........................... check: py-geeadd
........................... check: py-multiqc
........................... check: candle-benchmarks
........................... check: py-spacy
........................... check: charliecloud
........................... check: py-sphinxcontrib-issuetracker
............................ direct: py-sphinxcontrib-issuetracker -> set()
........................... check: py-usgs
........................... check: py-geeup
........................... check: qgis
........................... check: httpie
........................... check: py-dask
........................... check: py-gee-asset-manager
........................... check: py-python-jenkins
........................... check: py-requests-toolbelt
............................ direct: py-requests-toolbelt -> {'py-geeadd', 'py-mg-rast-tools', 'py-geeup', 'py-gee-asset-manager', 'py-twine'}
............................ check: py-geeadd
............................ check: py-mg-rast-tools
............................ check: py-geeup
............................ check: py-gee-asset-manager
............................ check: py-twine
............................. direct: py-twine -> {'py-graphviz'}
............................. check: py-graphviz
.............................. direct: py-graphviz -> {'lbann'}
.............................. check: lbann
........................... check: py-jupyterhub
........................... check: py-python-swiftclient
............................ direct: py-python-swiftclient -> set()
........................... check: py-projectq
........................... check: py-cdsapi
............................ direct: py-cdsapi -> set()
........................... check: py-smart-open
........................... check: snakemake
............................ direct: snakemake -> set()
........................... check: py-owslib
............................ direct: py-owslib -> {'qgis', 'py-cartopy'}
............................ check: qgis
............................ check: py-cartopy
........................... check: py-pipits
........................... check: py-python-gitlab
............................ direct: py-python-gitlab -> set()
........................... check: py-requests-oauthlib
........................... check: py-pycbc
........................... check: py-jupyterlab-server
........................... check: py-dgl
........................... check: py-hvac
............................ direct: py-hvac -> set()
........................... check: py-mg-rast-tools
........................... check: py-biomine
........................... check: py-torchtext
........................... check: py-jupyterlab
........................... check: py-transformers
........................... check: py-codecov
........................... check: py-google-api-core
........................... check: py-grequests
............................ direct: py-grequests -> set()
.......................... check: py-botocore
........................... direct: py-botocore -> {'awscli', 'py-s3transfer', 'py-boto3'}
........................... check: awscli
............................ direct: awscli -> set()
........................... check: py-s3transfer
............................ direct: py-s3transfer -> {'awscli', 'py-boto3'}
............................ check: awscli
............................ check: py-boto3
............................. direct: py-boto3 -> {'py-rasterio', 'aws-parallelcluster', 'py-smart-open', 'py-transformers'}
............................. check: py-rasterio
............................. check: aws-parallelcluster
.............................. direct: aws-parallelcluster -> set()
............................. check: py-smart-open
............................. check: py-transformers
........................... check: py-boto3
.......................... check: py-py2neo
........................... direct: py-py2neo -> set()
.......................... check: py-elasticsearch
........................... direct: py-elasticsearch -> {'py-hpcbench'}
........................... check: py-hpcbench
.......................... check: py-selenium
........................... direct: py-selenium -> {'py-geeup'}
........................... check: py-geeup
......................... check: py-requests
........................ check: py-blosc
......................... direct: py-blosc -> set()
........................ check: py-horovod
........................ check: py-petastorm
........................ check: py-torch
....................... check: py-funcsigs
........................ direct: py-funcsigs -> {'py-numba', 'py-mock', 'py-pytest', 'py-symfit', 'py-tensorflow-estimator'}
........................ check: py-numba
........................ check: py-mock
......................... direct: py-mock -> {'py-hpcbench', 'py-cartopy', 'py-mako', 'py-pbr', 'py-qtconsole', 'py-ipywidgets', 'py-arrow', 'py-tables', 'py-s3transfer', 'py-usgs', 'py-oauthlib', 'py-urllib3', 'py-tensorflow', 'ambari', 'py-pynn', 'py-nose2', 'py-srsly', 'py-dateparser', 'py-rasterio', 'py-requests-oauthlib', 'py-botocore', 'py-humanize', 'awscli', 'py-horovod', 'py-thinc', 'py-freezegun', 'geopm', 'py-graphviz', 'py-isort', 'py-readme-renderer', 'py-sqlalchemy', 'py-filemagic', 'py-psutil'}
......................... check: py-hpcbench
......................... check: py-cartopy
......................... check: py-mako
.......................... direct: py-mako -> {'py-pycuda', 'mesa', 'py-pygpu', 'py-alembic', 'py-pycbc', 'py-jupyterhub'}
.......................... check: py-pycuda
.......................... check: mesa
.......................... check: py-pygpu
.......................... check: py-alembic
.......................... check: py-pycbc
.......................... check: py-jupyterhub
......................... check: py-pbr
......................... check: py-qtconsole
.......................... direct: py-qtconsole -> {'py-jupyter', 'py-spyder'}
.......................... check: py-jupyter
.......................... check: py-spyder
......................... check: py-ipywidgets
......................... check: py-arrow
.......................... direct: py-arrow -> {'py-ics', 'py-jinja2-time'}
.......................... check: py-ics
........................... direct: py-ics -> set()
.......................... check: py-jinja2-time
........................... direct: py-jinja2-time -> {'py-cookiecutter'}
........................... check: py-cookiecutter
......................... check: py-tables
......................... check: py-s3transfer
......................... check: py-usgs
......................... check: py-oauthlib
......................... check: py-urllib3
......................... check: py-tensorflow
......................... check: ambari
.......................... direct: ambari -> set()
......................... check: py-pynn
......................... check: py-nose2
.......................... direct: py-nose2 -> set()
......................... check: py-srsly
......................... check: py-dateparser
.......................... direct: py-dateparser -> set()
......................... check: py-rasterio
......................... check: py-requests-oauthlib
......................... check: py-botocore
......................... check: py-humanize
.......................... direct: py-humanize -> set()
......................... check: awscli
......................... check: py-horovod
......................... check: py-thinc
......................... check: py-freezegun
.......................... direct: py-freezegun -> {'py-babel'}
.......................... check: py-babel
........................... direct: py-babel -> {'py-agate', 'py-sphinx', 'py-jinja2'}
........................... check: py-agate
............................ direct: py-agate -> {'py-agate-sql', 'py-csvkit', 'py-agate-dbf', 'py-agate-excel'}
............................ check: py-agate-sql
............................ check: py-csvkit
............................ check: py-agate-dbf
............................. direct: py-agate-dbf -> {'py-csvkit'}
............................. check: py-csvkit
............................ check: py-agate-excel
............................. direct: py-agate-excel -> {'py-csvkit'}
............................. check: py-csvkit
........................... check: py-sphinx
........................... check: py-jinja2
............................ direct: py-jinja2 -> {'py-hpcbench', 'py-bokeh', 'py-cookiecutter', 'py-jinja2-time', 'py-vcf-kit', 'py-sphinx', 'py-multiqc', 'qgis', 'py-notebook', 'py-flask', 'py-jupyterhub', 'py-gcovr', 'py-mpld3', 'py-pynn', 'py-nbconvert', 'py-pycbc', 'py-jupyterlab-server', 'py-brian2', 'py-ansible', 'py-jupyterlab', 'py-pyprof2html', 'py-rpy2'}
............................ check: py-hpcbench
............................ check: py-bokeh
............................ check: py-cookiecutter
............................ check: py-jinja2-time
............................ check: py-vcf-kit
............................ check: py-sphinx
............................ check: py-multiqc
............................ check: qgis
............................ check: py-notebook
............................ check: py-flask
............................ check: py-jupyterhub
............................ check: py-gcovr
............................. direct: py-gcovr -> set()
............................ check: py-mpld3
............................ check: py-pynn
............................ check: py-nbconvert
............................ check: py-pycbc
............................ check: py-jupyterlab-server
............................ check: py-brian2
............................. direct: py-brian2 -> set()
............................ check: py-ansible
............................. direct: py-ansible -> set()
............................ check: py-jupyterlab
............................ check: py-pyprof2html
............................. direct: py-pyprof2html -> set()
............................ check: py-rpy2
......................... check: geopm
......................... check: py-graphviz
......................... check: py-isort
.......................... direct: py-isort -> {'py-pylint', 'py-pytest-isort'}
.......................... check: py-pylint
........................... direct: py-pylint -> {'geopm', 'flux-core', 'py-spyder'}
........................... check: geopm
........................... check: flux-core
........................... check: py-spyder
.......................... check: py-pytest-isort
........................... direct: py-pytest-isort -> {'py-cairocffi'}
........................... check: py-cairocffi
............................ direct: py-cairocffi -> {'py-matplotlib'}
............................ check: py-matplotlib
......................... check: py-readme-renderer
.......................... direct: py-readme-renderer -> {'py-twine', 'py-restview'}
.......................... check: py-twine
.......................... check: py-restview
........................... direct: py-restview -> set()
......................... check: py-sqlalchemy
......................... check: py-filemagic
......................... check: py-psutil
........................ check: py-pytest
......................... direct: py-pytest -> {'py-cffi', 'py-pyjwt', 'py-mock', 'py-cartopy', 'py-mako', 'py-scikit-optimize', 'py-spdlog', 'py-pytest-pep8', 'py-pybind11', 'py-pytest-forked', 'py-werkzeug', 'py-sphinx', 'py-pytest-mock', 'py-pillow', 'py-pytest-cov', 'py-pytest-httpbin', 'py-wand', 'py-pytest-mypy', 'py-pytest-cache', 'py-scipy', 'py-geeadd', 'py-preshed', 'py-fiscalyear', 'py-intervaltree', 'py-psyclone', 'py-babel', 'py-testinfra', 'py-usgs', 'py-geeup', 'py-apipkg', 'py-pyarrow', 'py-pytest-isort', 'py-statsmodels', 'py-dask', 'py-oauthlib', 'py-gee-asset-manager', 'py-goatools', 'py-basis-set-exchange', 'py-pysocks', 'py-pytest-xdist', 'py-pandas', 'py-urllib3', 'py-iminuit', 'py-asteval', 'py-pycuda', 'py-shapely', 'py-astropy', 'py-geoalchemy2', 'py-projectq', 'py-srsly', 'py-merlin', 'py-rasterio', 'py-chardet', 'py-wxmplot', 'py-luigi', 'py-mypy', 'py-matplotlib', 'py-parso', 'py-py2cairo', 'py-wheel', 'py-fparser', 'py-horovod', 'py-wasabi', 'py-mccabe', 'py-thinc', 'py-pydotplus', 'py-requests', 'py-graphviz', 'py-readme-renderer', 'py-h5sh', 'py-isort', 'py-jupyterlab', 'py-numpy', 'py-scikit-learn', 'py-pytest-flake8', 'py-sqlalchemy', 'lbann', 'py-opt-einsum', 'py-overpy', 'py-attrs', 'py-pyinstrument', 'py-python-utils', 'py-wub', 'py-rpy2'}
......................... check: py-cffi
.......................... direct: py-cffi -> {'py-pycares', 'py-pyzmq', 'py-gevent', 'geopm', 'py-rpy2', 'py-brotlipy', 'py-cryptography', 'py-soundfile', 'py-pygit2', 'py-horovod', 'py-gf256', 'py-cairocffi', 'flux-core', 'py-bcrypt'}
.......................... check: py-pycares
........................... direct: py-pycares -> set()
.......................... check: py-pyzmq
........................... direct: py-pyzmq -> {'py-spyder', 'nrm', 'py-jupyter-client', 'py-dask', 'py-notebook', 'py-scoop', 'py-petastorm'}
........................... check: py-spyder
........................... check: nrm
........................... check: py-jupyter-client
............................ direct: py-jupyter-client -> {'py-nbconvert', 'py-qtconsole', 'py-jupyter-console', 'py-notebook', 'py-ipykernel'}
............................ check: py-nbconvert
............................ check: py-qtconsole
............................ check: py-jupyter-console
............................. direct: py-jupyter-console -> {'py-jupyter'}
............................. check: py-jupyter
............................ check: py-notebook
............................ check: py-ipykernel
............................. direct: py-ipykernel -> {'py-jupyter', 'py-qtconsole', 'py-jupyter-console', 'py-notebook', 'py-ipywidgets'}
............................. check: py-jupyter
............................. check: py-qtconsole
............................. check: py-jupyter-console
............................. check: py-notebook
............................. check: py-ipywidgets
........................... check: py-dask
........................... check: py-notebook
........................... check: py-scoop
............................ direct: py-scoop -> {'py-pyrosar', 'py-spatialist'}
............................ check: py-pyrosar
............................ check: py-spatialist
........................... check: py-petastorm
.......................... check: py-gevent
........................... direct: py-gevent -> {'py-grequests', 'py-gdbgui'}
........................... check: py-grequests
........................... check: py-gdbgui
.......................... check: geopm
.......................... check: py-rpy2
.......................... check: py-brotlipy
........................... direct: py-brotlipy -> {'py-httpbin'}
........................... check: py-httpbin
.......................... check: py-cryptography
........................... direct: py-cryptography -> {'py-pymysql', 'py-merlin', 'py-pyopenssl', 'py-ansible', 'py-oauthlib', 'py-urllib3', 'py-paramiko'}
........................... check: py-pymysql
............................ direct: py-pymysql -> {'py-cogent', 'py-sqlalchemy'}
............................ check: py-cogent
............................ check: py-sqlalchemy
........................... check: py-merlin
........................... check: py-pyopenssl
............................ direct: py-pyopenssl -> {'py-certipy', 'py-lscsoft-glue', 'py-urllib3'}
............................ check: py-certipy
............................. direct: py-certipy -> {'py-jupyterhub'}
............................. check: py-jupyterhub
............................ check: py-lscsoft-glue
............................. direct: py-lscsoft-glue -> {'py-pycbc'}
............................. check: py-pycbc
............................ check: py-urllib3
........................... check: py-ansible
........................... check: py-oauthlib
........................... check: py-urllib3
........................... check: py-paramiko
............................ direct: py-paramiko -> {'py-scp', 'py-testinfra'}
............................ check: py-scp
............................. direct: py-scp -> set()
............................ check: py-testinfra
............................. direct: py-testinfra -> set()
.......................... check: py-soundfile
.......................... check: py-pygit2
.......................... check: py-horovod
.......................... check: py-gf256
........................... direct: py-gf256 -> {'py-subrosa'}
........................... check: py-subrosa
............................ direct: py-subrosa -> set()
.......................... check: py-cairocffi
.......................... check: flux-core
.......................... check: py-bcrypt
........................... direct: py-bcrypt -> set()
......................... check: py-pyjwt
.......................... direct: py-pyjwt -> {'py-oauthlib'}
.......................... check: py-oauthlib
......................... check: py-mock
......................... check: py-cartopy
......................... check: py-mako
......................... check: py-scikit-optimize
......................... check: py-spdlog
......................... check: py-pytest-pep8
.......................... direct: py-pytest-pep8 -> {'py-opt-einsum'}
.......................... check: py-opt-einsum
........................... direct: py-opt-einsum -> {'py-tensorflow'}
........................... check: py-tensorflow
......................... check: py-pybind11
......................... check: py-pytest-forked
.......................... direct: py-pytest-forked -> {'py-horovod', 'py-pytest-xdist'}
.......................... check: py-horovod
.......................... check: py-pytest-xdist
........................... direct: py-pytest-xdist -> {'py-testinfra', 'py-sqlalchemy', 'py-requests', 'py-pandas', 'py-wand'}
........................... check: py-testinfra
........................... check: py-sqlalchemy
........................... check: py-requests
........................... check: py-pandas
........................... check: py-wand
......................... check: py-werkzeug
......................... check: py-sphinx
......................... check: py-pytest-mock
.......................... direct: py-pytest-mock -> {'py-graphviz', 'py-requests', 'py-fiscalyear'}
.......................... check: py-graphviz
.......................... check: py-requests
.......................... check: py-fiscalyear
........................... direct: py-fiscalyear -> set()
......................... check: py-pillow
......................... check: py-pytest-cov
.......................... direct: py-pytest-cov -> {'py-shapely', 'py-geoalchemy2', 'py-pyjwt', 'py-mock', 'py-graphviz', 'py-oauthlib', 'py-sphinx', 'py-opt-einsum', 'py-basis-set-exchange', 'py-wheel', 'py-requests', 'py-cairocffi'}
.......................... check: py-shapely
........................... direct: py-shapely -> {'py-geoalchemy2', 'py-geopandas', 'py-cartopy'}
........................... check: py-geoalchemy2
........................... check: py-geopandas
........................... check: py-cartopy
.......................... check: py-geoalchemy2
.......................... check: py-pyjwt
.......................... check: py-mock
.......................... check: py-graphviz
.......................... check: py-oauthlib
.......................... check: py-sphinx
.......................... check: py-opt-einsum
.......................... check: py-basis-set-exchange
........................... direct: py-basis-set-exchange -> set()
.......................... check: py-wheel
........................... direct: py-wheel -> {'py-spacy', 'py-srsly', 'py-auxlib', 'py-graphviz', 'py-pbr', 'py-jupyterlab', 'py-cymem', 'py-importlib-resources', 'py-murmurhash', 'py-astunparse', 'py-tensorflow', 'py-pyside2', 'py-tensorboard-plugin-wit', 'py-coilmq', 'py-scikit-build', 'py-tensorboard'}
........................... check: py-spacy
........................... check: py-srsly
........................... check: py-auxlib
............................ direct: py-auxlib -> set()
........................... check: py-graphviz
........................... check: py-pbr
........................... check: py-jupyterlab
........................... check: py-cymem
............................ direct: py-cymem -> {'py-thinc', 'py-preshed', 'py-spacy'}
............................ check: py-thinc
............................ check: py-preshed
............................. direct: py-preshed -> {'py-thinc', 'py-spacy'}
............................. check: py-thinc
............................. check: py-spacy
............................ check: py-spacy
........................... check: py-importlib-resources
............................ direct: py-importlib-resources -> {'py-importlib-metadata', 'py-pre-commit', 'py-merlin'}
............................ check: py-importlib-metadata
............................. direct: py-importlib-metadata -> {'py-kombu', 'py-pre-commit', 'py-tox', 'py-pytest', 'py-flake8', 'py-pluggy', 'py-catalogue'}
............................. check: py-kombu
.............................. direct: py-kombu -> {'py-celery'}
.............................. check: py-celery
............................... direct: py-celery -> {'py-merlin'}
............................... check: py-merlin
............................. check: py-pre-commit
.............................. direct: py-pre-commit -> set()
............................. check: py-tox
.............................. direct: py-tox -> {'py-graphviz', 'py-diskcache'}
.............................. check: py-graphviz
.............................. check: py-diskcache
............................... direct: py-diskcache -> {'py-petastorm'}
............................... check: py-petastorm
............................. check: py-pytest
............................. check: py-flake8
.............................. direct: py-flake8 -> {'py-geoalchemy2', 'py-theano', 'py-graphviz', 'py-pytest-flake8', 'py-sphinx', 'py-flake8-polyfill', 'py-tqdm'}
.............................. check: py-geoalchemy2
.............................. check: py-theano
.............................. check: py-graphviz
.............................. check: py-pytest-flake8
............................... direct: py-pytest-flake8 -> {'py-cairocffi'}
............................... check: py-cairocffi
.............................. check: py-sphinx
.............................. check: py-flake8-polyfill
............................... direct: py-flake8-polyfill -> {'py-pep8-naming'}
............................... check: py-pep8-naming
................................ direct: py-pep8-naming -> {'py-graphviz'}
................................ check: py-graphviz
.............................. check: py-tqdm
............................... direct: py-tqdm -> {'py-thinc', 'candle-benchmarks', 'py-cdsapi', 'py-spacy', 'py-torchtext', 'py-gluoncv', 'py-transformers', 'py-nltk', 'py-wub', 'py-pymc3', 'py-sacremoses', 'py-abipy', 'py-atropos', 'py-twine'}
............................... check: py-thinc
............................... check: candle-benchmarks
............................... check: py-cdsapi
............................... check: py-spacy
............................... check: py-torchtext
............................... check: py-gluoncv
............................... check: py-transformers
............................... check: py-nltk
................................ direct: py-nltk -> set()
............................... check: py-wub
............................... check: py-pymc3
............................... check: py-sacremoses
................................ direct: py-sacremoses -> {'py-transformers'}
................................ check: py-transformers
............................... check: py-abipy
............................... check: py-atropos
............................... check: py-twine
............................. check: py-pluggy
.............................. direct: py-pluggy -> {'py-pytest', 'py-tox'}
.............................. check: py-pytest
.............................. check: py-tox
............................. check: py-catalogue
.............................. direct: py-catalogue -> {'py-thinc', 'py-spacy'}
.............................. check: py-thinc
.............................. check: py-spacy
............................ check: py-pre-commit
............................ check: py-merlin
........................... check: py-murmurhash
............................ direct: py-murmurhash -> {'py-thinc', 'py-preshed', 'py-spacy'}
............................ check: py-thinc
............................ check: py-preshed
............................ check: py-spacy
........................... check: py-astunparse
............................ direct: py-astunparse -> {'py-astor', 'py-tensorflow'}
............................ check: py-astor
............................. direct: py-astor -> {'py-tensorflow'}
............................. check: py-tensorflow
............................ check: py-tensorflow
........................... check: py-tensorflow
........................... check: py-pyside2
........................... check: py-tensorboard-plugin-wit
............................ direct: py-tensorboard-plugin-wit -> {'py-tensorboard'}
............................ check: py-tensorboard
........................... check: py-coilmq
............................ direct: py-coilmq -> {'ambari'}
............................ check: ambari
........................... check: py-scikit-build
............................ direct: py-scikit-build -> {'py-blosc'}
............................ check: py-blosc
........................... check: py-tensorboard
.......................... check: py-requests
.......................... check: py-cairocffi
......................... check: py-pytest-httpbin
......................... check: py-wand
......................... check: py-pytest-mypy
.......................... direct: py-pytest-mypy -> {'py-tatsu'}
.......................... check: py-tatsu
........................... direct: py-tatsu -> {'py-ics'}
........................... check: py-ics
......................... check: py-pytest-cache
.......................... direct: py-pytest-cache -> {'py-pytest-pep8'}
.......................... check: py-pytest-pep8
......................... check: py-scipy
......................... check: py-geeadd
......................... check: py-preshed
......................... check: py-fiscalyear
......................... check: py-intervaltree
.......................... direct: py-intervaltree -> {'py-vcf-kit'}
.......................... check: py-vcf-kit
......................... check: py-psyclone
......................... check: py-babel
......................... check: py-testinfra
......................... check: py-usgs
......................... check: py-geeup
......................... check: py-apipkg
.......................... direct: py-apipkg -> {'py-execnet'}
.......................... check: py-execnet
........................... direct: py-execnet -> {'py-pytest-xdist', 'py-pytest-cache'}
........................... check: py-pytest-xdist
........................... check: py-pytest-cache
......................... check: py-pyarrow
......................... check: py-pytest-isort
......................... check: py-statsmodels
......................... check: py-dask
......................... check: py-oauthlib
......................... check: py-gee-asset-manager
......................... check: py-goatools
......................... check: py-basis-set-exchange
......................... check: py-pysocks
......................... check: py-pytest-xdist
......................... check: py-pandas
......................... check: py-urllib3
......................... check: py-iminuit
......................... check: py-asteval
.......................... direct: py-asteval -> set()
......................... check: py-pycuda
......................... check: py-shapely
......................... check: py-astropy
......................... check: py-geoalchemy2
......................... check: py-projectq
......................... check: py-srsly
......................... check: py-merlin
......................... check: py-rasterio
......................... check: py-chardet
.......................... direct: py-chardet -> {'py-requests', 'py-spyder', 'py-binaryornot'}
.......................... check: py-requests
.......................... check: py-spyder
.......................... check: py-binaryornot
........................... direct: py-binaryornot -> {'py-cookiecutter'}
........................... check: py-cookiecutter
......................... check: py-wxmplot
......................... check: py-luigi
.......................... direct: py-luigi -> set()
......................... check: py-mypy
......................... check: py-matplotlib
......................... check: py-parso
.......................... direct: py-parso -> {'py-jedi'}
.......................... check: py-jedi
........................... direct: py-jedi -> {'py-ipython', 'py-spyder'}
........................... check: py-ipython
............................ direct: py-ipython -> {'py-line-profiler', 'py-ipdb', 'py-yt', 'py-jupyter-console', 'py-abipy', 'py-spatialist', 'py-ipywidgets', 'py-ipykernel'}
............................ check: py-line-profiler
............................. direct: py-line-profiler -> set()
............................ check: py-ipdb
............................. direct: py-ipdb -> set()
............................ check: py-yt
............................ check: py-jupyter-console
............................ check: py-abipy
............................ check: py-spatialist
............................ check: py-ipywidgets
............................ check: py-ipykernel
........................... check: py-spyder
......................... check: py-py2cairo
......................... check: py-wheel
......................... check: py-fparser
......................... check: py-horovod
......................... check: py-wasabi
.......................... direct: py-wasabi -> {'py-thinc'}
.......................... check: py-thinc
......................... check: py-mccabe
.......................... direct: py-mccabe -> {'py-pylint', 'py-flake8'}
.......................... check: py-pylint
.......................... check: py-flake8
......................... check: py-thinc
......................... check: py-pydotplus
.......................... direct: py-pydotplus -> set()
......................... check: py-requests
......................... check: py-graphviz
......................... check: py-readme-renderer
......................... check: py-h5sh
......................... check: py-isort
......................... check: py-jupyterlab
......................... check: py-numpy
......................... check: py-scikit-learn
......................... check: py-pytest-flake8
......................... check: py-sqlalchemy
......................... check: lbann
......................... check: py-opt-einsum
......................... check: py-overpy
.......................... direct: py-overpy -> set()
......................... check: py-attrs
.......................... direct: py-attrs -> {'py-black', 'py-jsonschema', 'py-fiona', 'py-rasterio', 'py-pytest', 'py-hypothesis', 'py-packaging'}
.......................... check: py-black
........................... direct: py-black -> set()
.......................... check: py-jsonschema
........................... direct: py-jsonschema -> {'nrm', 'py-warlock', 'py-asdf', 'py-nbformat', 'py-hepdata-validator', 'py-jupyterlab-server', 'py-basis-set-exchange', 'flux-core'}
........................... check: nrm
........................... check: py-warlock
............................ direct: py-warlock -> {'nrm'}
............................ check: nrm
........................... check: py-asdf
............................ direct: py-asdf -> {'py-astropy'}
............................ check: py-astropy
........................... check: py-nbformat
............................ direct: py-nbformat -> {'py-nbconvert', 'py-abipy', 'py-notebook', 'py-ipywidgets', 'py-plotly'}
............................ check: py-nbconvert
............................ check: py-abipy
............................ check: py-notebook
............................ check: py-ipywidgets
............................ check: py-plotly
........................... check: py-hepdata-validator
............................ direct: py-hepdata-validator -> set()
........................... check: py-jupyterlab-server
........................... check: py-basis-set-exchange
........................... check: flux-core
.......................... check: py-fiona
.......................... check: py-rasterio
.......................... check: py-pytest
.......................... check: py-hypothesis
........................... direct: py-hypothesis -> {'py-thinc', 'py-rasterio', 'py-chardet', 'py-pyarrow', 'py-werkzeug', 'py-blis', 'py-attrs', 'py-pandas', 'py-torch'}
........................... check: py-thinc
........................... check: py-rasterio
........................... check: py-chardet
........................... check: py-pyarrow
........................... check: py-werkzeug
........................... check: py-blis
............................ direct: py-blis -> {'py-thinc', 'py-spacy'}
............................ check: py-thinc
............................ check: py-spacy
........................... check: py-attrs
........................... check: py-pandas
........................... check: py-torch
.......................... check: py-packaging
........................... direct: py-packaging -> {'py-bokeh', 'py-tox', 'py-rasterio', 'py-importlib-metadata', 'py-pytest', 'py-deprecation', 'py-setuptools', 'py-arviz', 'py-sphinx', 'py-petastorm', 'py-scikit-build'}
........................... check: py-bokeh
........................... check: py-tox
........................... check: py-rasterio
........................... check: py-importlib-metadata
........................... check: py-pytest
........................... check: py-deprecation
............................ direct: py-deprecation -> {'py-wradlib'}
............................ check: py-wradlib
........................... check: py-setuptools
............................ direct: py-setuptools -> {'py-unidecode', 'py-agate-dbf', 'py-aspy-yaml', 'py-hdfs', 'py-jsonpointer', 'py-pytest-pep8', 'py-s3cmd', 'py-colorlog', 'py-torchaudio', 'minigan', 'py-xvfbwrapper', 'py-breathe', 'py-wcsaxes', 'py-pycodestyle', 'py-fiscalyear', 'py-openslide-python', 'nrm', 'py-setuptools-scm-git-archive', 'py-breakseq2', 'py-pamela', 'py-brotlipy', 'py-stestr', 'py-ligo-segments', 'py-urllib3', 'py-click', 'py-pydispatcher', 'aws-parallelcluster', 'py-diskcache', 'py-astpretty', 'py-psycopg2', 'py-openpmd-validator', 'py-fusepy', 'py-pygit2', 'py-arviz', 'py-matplotlib', 'nest', 'py-python-daemon', 'py-sphinx-bootstrap-theme', 'py-hvac', 'py-hacking', 'py-paramiko', 'py-thinc', 'py-line-profiler', 'py-python-memcached', 'py-sphinx-rtd-theme', 'py-scikit-learn', 'py-html5lib', 'py-astunparse', 'py-pyfaidx', 'py-colorama', 'py-ipykernel', 'py-psutil', 'gdal', 'py-cvxopt', 'py-parse', 'py-pydot', 'py-sphinxcontrib-applehelp', 'py-texttable', 'py-pyperf', 'py-sphinxcontrib-bibtex', 'py-wxpython', 'py-argparse', 'py-lzstring', 'py-htmlgen', 'py-gluoncv', 'py-osqp', 'py-astor', 'py-scikit-optimize', 'py-dendropy', 'py-pytest-forked', 'py-pyinstrument-cext', 'mxnet', 'py-sphinx', 'py-decorator', 'py-typed-ast', 'py-wand', 'py-munch', 'py-scipy', 'py-cython', 'py-google-pasta', 'py-testinfra', 'py-netifaces', 'py-pygpu', 'py-sphinxcontrib-programoutput', 'httpie', 'py-backports-tempfile', 'py-addict', 'py-pysocks', 'py-testresources', 'py-pyomo', 'py-cmake-format', 'py-lru-dict', 'py-zipp', 'py-numba', 'py-merlin', 'py-text-unidecode', 'py-yapf', 'py-chardet', 'py-ipdb', 'py-pipits', 'py-python-oauth2', 'py-yamlreader', 'py-vermin', 'py-jupyterlab-server', 'py-gf256', 'py-humanize', 'py-fastcluster', 'py-petastorm', 'py-bottleneck', 'py-pyliblzma', 'py-google-auth', 'jube', 'py-selenium', 'py-agate', 'py-spatialite', 'py-theano', 'py-cligj', 'py-importlib-metadata', 'py-pytest-flake8', 'bumpversion', 'py-flake8', 'py-requests', 'py-sphinxcontrib-devhelp', 'py-rseqc', 'py-progressbar2', 'py-pywavelets', 'py-xmltodict', 'py-pkginfo', 'py-poyo', 'py-sphinxautomodapi', 'py-backports-weakref', 'py-grpcio', 'py-subprocess32', 'py-misopy', 'z3', 'py-future', 'py-eventlet', 'py-pytest-cache', 'py-torch', 'py-intervaltree', 'py-tokenizers', 'py-neo', 'py-blinker', 'py-fastcache', 'py-pyrad', 'py-zope-interface', 'py-simplegeneric', 'py-pyqi', 'py-pandas', 'py-pymongo', 'py-griddataformats', 'py-httplib2', 'py-msgpack', 'py-tfdlpack', 'py-smart-open', 'py-envisage', 'py-flask-socketio', 'py-kitchen', 'py-dnaio', 'py-nltk', 'py-wxmplot', 'py-sphinxcontrib-htmlhelp', 'py-cnvkit', 'py-pyasn1-modules', 'py-opentuner', 'py-lazy', 'py-pysam', 'py-nosexcover', 'py-biopython', 'py-rtree', 'py-isort', 'py-numcodecs', 'py-codecov', 'py-sqlalchemy', 'py-retrying', 'py-certipy', 'py-unittest2py3k', 'py-xarray', 'py-ipython', 'py-leather', 'py-nose', 'py-pycairo', 'py-rpy2', 'py-gast', 'py-mdanalysis', 'py-repoze-lru', 'py-lockfile', 'py-distributed', 'py-argcomplete', 'py-invoke', 'py-palettable', 'py-pytools', 'py-tensorboardx', 'py-audioread', 'py-scoop', 'py-pint', 'py-tensorboard', 'py-geeadd', 'py-markupsafe', 'py-prompt-toolkit', 'py-modred', 'py-pyani', 'py-voluptuous', 'py-docopt', 'py-bsddb3', 'py-tensorflow', 'py-flake8-polyfill', 'py-pytest-check-links', 'py-myhdl', 'py-gcovr', 'ambari', 'py-dateparser', 'py-fixtures', 'py-pyspark', 'py-kiwisolver', 'py-multiprocess', 'py-pytailf', 'py-warlock', 'py-execnet', 'py-pyrsistent', 'py-fasteners', 'py-singledispatch', 'py-black', 'py-twisted', 'py-contextlib2', 'py-torchtext', 'py-webencodings', 'py-pyudev', 'py-lazy-property', 'py-certifi', 'py-overpy', 'py-elasticsearch', 'py-cvxpy', 'py-subrosa', 'py-deprecated', 'py-billiard', 'py-mypy-extensions', 'py-pyshp', 'py-tox', 'py-simplejson', 'py-cryptography', 'py-memory-profiler', 'py-sacremoses', 'py-performance', 'py-numexpr', 'py-picrust', 'py-pytz', 'py-pyface', 'py-cdo', 'py-statsmodels', 'py-pyquaternion', 'py-slurm-pipeline', 'py-jinja2', 'py-enum34', 'py-coilmq', 'py-cftime', 'py-python-swiftclient', 'py-avro', 'falcon', 'py-mpld3', 'py-pygments', 'py-pyvcf', 'portcullis', 'py-python-subunit', 'py-mysqldb1', 'py-pymc3', 'py-botocore', 'py-libconf', 'py-google-api-python-client', 'py-openpyxl', 'py-mysqlclient', 'py-et-xmlfile', 'py-tzlocal', 'py-semver', 'py-stratify', 'py-scandir', 'py-petsc4py', 'py-easybuild-framework', 'py-virtualenv-clone', 'py-attrs', 'py-cutadapt', 'py-pkgconfig', 'py-versioneer', 'py-google-cloud-storage', 'py-auxlib', 'py-mako', 'py-pispino', 'py-google-auth-oauthlib', 'py-neotime', 'py-pickleshare', 'py-apptools', 'py-plotly', 'py-urwid', 'py-typing-extensions', 'py-whichcraft', 'py-umi-tools', 'ont-albacore', 'py-jupyter-client', 'py-progress', 'py-gee-asset-manager', 'py-youtube-dl', 'py-funcsigs', 'py-googleapis-common-protos', 'py-projectq', 'py-pykml', 'py-prometheus-client', 'py-resampy', 'py-blessings', 'py-cheroot', 'py-deeptoolsintervals', 'py-periodictable', 'py-hatchet', 'py-pycbc', 'py-toolz', 'py-gdc-client', 'py-pyfftw', 'py-qtawesome', 'py-setuptools-scm', 'py-testscenarios', 'py-extras', 'py-tatsu', 'py-patsy', 'py-ansible', 'py-irpf90', 'py-localcider', 'py-parameterized', 'py-google-api-core', 'py-httpbin', 'py-pycifrw', 'py-yajl', 'py-pox', 'py-mysql-connector-python', 'py-xattr', 'py-heapdict', 'py-uwsgi', 'py-agate-sql', 'py-pip', 'py-pyside2', 'py-crossmap', 'py-symfit', 'py-jprops', 'py-git-review', 'py-rsa', 'py-click-plugins', 'py-sphinxcontrib-issuetracker', 'py-usgs', 'py-keras', 'py-jmespath', 'py-elephant', 'py-flask', 'py-requests-toolbelt', 'py-colormath', 'py-backports-abc', 'py-snowballstemmer', 'py-pymysql', 'py-nbconvert', 'py-lit', 'py-symengine', 'py-cdat-lite', 'py-ppft', 'py-python-gitlab', 'py-fn-py', 'py-pyasn1', 'py-us', 'py-pyperclip', 'py-pygobject', 'py-dgl', 'py-wasabi', 'py-binaryornot', 'py-jellyfish', 'py-clipboard', 'py-d2to1', 'py-asdf', 'py-pyprof2html', 'py-cycler', 'py-prettytable', 'py-pyfits', 'py-pyglet', 'py-pybtex', 'py-kmodes', 'py-gevent', 'py-jinja2-time', 'py-pyopenssl', 'py-xmlrunner', 'py-pybind11', 'py-astroid', 'fenics', 'py-defusedxml', 'py-current', 'py-pybigwig', 'py-maestrowf', 'py-magic', 'py-astropy-helpers', 'py-latexcodec', 'py-virtualenv', 'py-pep8-naming', 'py-jedi', 'py-basis-set-exchange', 'py-deeptools', 'py-asserts', 'py-lark-parser', 'py-dxchange', 'py-metasv', 'py-cdsapi', 'py-jupyter-core', 'py-nose-cov', 'py-neobolt', 'py-zxcvbn', 'py-apscheduler', 'py-sphinxcontrib-jsmath', 'py-twine', 'py-json5', 'py-pyrosar', 'py-avro-json-serializer', 'py-fsspec', 'py-markdown', 'py-pyscaf', 'py-h5sh', 'py-spacy-models-en-core-web-sm', 'py-hepdata-validator', 'py-raven', 'py-opt-einsum', 'py-keras-preprocessing', 'py-scp', 'py-traits', 'py-cartopy', 'py-methylcode', 'py-storm', 'py-requests-futures', 'py-spdlog', 'py-unicycler', 'py-pyodbc', 'kitty', 'py-tempora', 'py-py', 'py-setuptools-git', 'py-ics', 'py-pytest-httpbin', 'py-monty', 'py-arrow', 'py-alabaster', 'py-backports-shutil-get-terminal-size', 'py-pytimeparse', 'py-bleach', 'py-apipkg', 'py-natsort', 'py-sh', 'py-oauthlib', 'py-notebook', 'py-jupyterhub', 'py-blessed', 'py-partd', 'py-slepc4py', 'py-onnx', 'mercurial', 'py-py-cpuinfo', 'py-illumina-utils', 'py-pycparser', 'py-logilab-common', 'py-redis', 'py-yt', 'py-sortedcontainers', 'py-lxml', 'py-mypy', 'py-atomicwrites', 'py-mechanize', 'py-fparser', 'py-pep8', 'py-llvmlite', 'py-freezegun', 'py-pathspec', 'py-setuptools-rust', 'py-dill', 'py-xdot', 'py-syned', 'py-stsci-distutils', 'py-librosa', 'lbann', 'py-ecos', 'py-poster', 'py-pyinstrument', 'py-zarr', 'py-vcf-kit', 'py-pypeflow', 'py-bintrees', 'py-python-igraph', 'py-pymeeus', 'py-werkzeug', 'py-pymatgen', 'py-typing', 'py-plac', 'py-macholib', 'py-h5py', 'py-tetoolkit', 'py-altgraph', 'py-amqp', 'py-geeup', 'py-requests-mock', 'py-jdcal', 'py-soundfile', 'py-spefile', 'py-radical-utils', 'py-send2trash', 'py-spykeutils', 'py-utils', 'py-macs2', 'py-geoalchemy2', 'py-guiqwt', 'py-py4j', 'scons', 'py-imagesize', 'py-pathos', 'py-machotools', 'py-weave', 'py-docutils-stubs', 'py-tabulate', 'py-graphviz', 'py-flask-compress', 'py-jupyterlab', 'py-xenv', 'py-emcee', 'py-python-utils', 'py-spatialist', 'py-sphinxcontrib-websupport', 'py-tensorboard-plugin-wit', 'py-cinema-lib', 'py-cffi', 'py-seaborn', 'py-mpi4py', 'py-oauth2client', 'py-pyelftools', 'py-importlib-resources', 'py-wradlib', 'py-idna', 'py-ujson', 'py-apache-libcloud', 'py-scikit-build', 'py-svgpathtools', 'py-testtools', 'py-psyclone', 'py-preshed', 'py-rope', 'py-s3transfer', 'py-mayavi', 'py-spglib', 'py-sphinxcontrib-serializinghtml', 'py-louie', 'py-toml', 'py-python-mapnik', 'py-numexpr3', 'py-pomegranate', 'py-python-levenshtein', 'py-virtualenvwrapper', 'py-asgiref', 'py-abipy', 'py-descartes', 'sumo', 'py-pycuda', 'py-clustershell', 'py-kombu', 'py-umalqurra', 'py-srsly', 'py-shroud', 'py-rasterio', 'py-itsdangerous', 'py-ratelim', 'py-binwalk', 'sgpp', 'py-traceback2', 'py-gensim', 'py-lmfit', 'geopm', 'py-pathlib2', 'py-readme-renderer', 'py-tomopy', 'py-path-py', 'py-sqlparse', 'py-fypp', 'gaudi', 'py-filemagic', 'py-jaraco-functools', 'py-boltons', 'py-fits-tools', 'py-affine', 'py-torch-nvidia-apex', 'py-sfepy', 'py-py2bit', 'py-widgetsnbextension', 'py-hpcbench', 'py-xopen', 'py-pyjwt', 'kmergenie', 'py-cookiecutter', 'py-coverage', 'py-xlwt', 'py-scikit-image', 'py-editdistance', 'py-pyepsg', 'py-configparser', 'py-saga-python', 'py-bcbio-gff', 'py-spacy', 'py-zc-lockfile', 'py-cloudpickle', 'py-advancedhtmlparser', 'py-celery', 'py-pytest-xdist', 'py-async-generator', 'py-pluggy', 'py-zope-event', 'py-asteval', 'py-catalogue', 'py-sncosmo', 'py-flake8-import-order', 'py-owslib', 'py-cfgv', 'py-asciitree', 'py-python-rapidjson', 'py-horovod', 'py-pydeps', 'py-mccabe', 'py-chai', 'py-docutils', 'py-mlxtend', 'py-pydotplus', 'py-pygresql', 'py-absl-py', 'py-isodate', 'py-pythonqwt', 'py-vsc-install', 'py-testrepository', 'py-earthengine-api', 'py-lscsoft-glue', 'py-portend', 'py-rnacocktail', 'py-cachetools', 'py-cogent', 'py-basemap', 'py-torchvision', 'py-hypothesis', 'py-awesome-slugify', 'py-pywcs', 'py-tables', 'py-multiqc', 'py-pyside', 'py-junit-xml', 'py-ecdsa', 'py-csvkit', 'py-python-jenkins', 'py-queryablelist', 'py-soupsieve', 'py-setproctitle', 'py-pykwalify', 'py-astropy', 'py-parsedatetime', 'py-gdbgui', 'py-numpydoc', 'py-requests-oauthlib', 'py-parso', 'py-tqdm', 'py-atropos', 'py-regex', 'py-fastaindex', 'py-pymorph', 'py-html2text', 'py-numpy', 'py-jpype1', 'py-pycmd', 'py-py2neo', 'py-futures', 'py-bcrypt', 'py-edffile', 'py-sonlib', 'py-traitsui', 'py-dataclasses', 'py-mock', 'py-clint', 'py-pygdal', 'py-pbr', 'py-pid', 'py-python-ldap', 'py-pillow', 'py-stdlib-list', 'py-vcversioner', 'py-pytest-mypy', 'py-wcwidth', 'py-babel', 'py-cymem', 'py-dask', 'py-humanfriendly', 'py-joblib', 'py-pylint', 'nghttp2', 'py-dbfread', 'py-pyserial', 'py-qtpy', 'snakemake', 'py-scs', 'py-guidata', 'py-profilehooks', 'py-luigi', 'py-bitarray', 'py-coloredlogs', 'py-pyfasta', 'py-mistune', 'py-biom-format', 'py-pre-commit', 'py-cov-core', 'py-moltemplate', 'py-sphinxcontrib-qthelp', 'py-tblib', 'py-backports-functools-lru-cache', 'py-pyproj', 'py-transformers', 'py-torchsummary', 'py-monotonic', 'py-aenum', 'py-alembic', 'py-pytest-runner', 'py-geopandas', 'py-svgwrite', 'py-uritemplate', 'py-agate-excel', 'py-django', 'py-portalocker', 'py-chainer', 'py-google-resumable-media', 'py-jsonschema', 'py-semantic-version', 'py-autopep8', 'py-pyflakes', 'py-python-engineio', 'py-counter', 'py-args', 'py-cherrypy', 'py-google-auth-httplib2', 'py-python-slugify', 'py-pybedtools', 'py-lazy-object-proxy', 'py-antlr4-python3-runtime', 'py-cached-property', 'py-libensemble', 'py-cyvcf2', 'py-python-editor', 'py-crispresso', 'py-pyugrid', 'py-boto3', 'py-genshi', 'py-jsonpatch', 'py-beautifulsoup4', 'py-pybtex-docutils', 'py-ruamel-yaml', 'py-cairocffi', 'py-whatshap', 'py-iminuit', 'py-tap-py', 'py-linecache2', 'py-protobuf', 'py-tifffile', 'py-pytest', 'py-m2r', 'meson', 'py-shiboken', 'py-unittest2', 'py-spectra', 'phyluce', 'py-wheel', 'py-zc-buildout', 'py-oset', 'py-pyeda', 'py-mg-rast-tools', 'py-quast', 'py-python-socketio', 'py-weblogo', 'py-tornado', 'py-doxypypy', 'py-keras-applications', 'py-ipaddress', 'py-grequests', 'py-pyminifier', 'py-python-dateutil', 'py-restview', 'py-opppy', 'py-sentencepiece', 'py-deprecation', 'py-murmurhash', 'py-networkx', 'py-pytest-mock', 'py-pytest-cov', 'py-pygdbmi', 'py-vine', 'py-vsc-base', 'py-exodus-bundler', 'py-pudb', 'py-pysmartdl', 'py-dxfile', 'py-flexx', 'py-pyarrow', 'py-google-cloud-core', 'py-nodeenv', 'py-convertdate', 'py-asn1crypto', 'py-blis', 'py-shapely', 'py-nose2', 'py-fiona', 'py-bx-python', 'py-pydot2', 'py-imageio', 'py-more-itertools', 'awscli', 'py-identify', 'py-cf-units', 'py-pyparsing', 'py-brian2', 'py-ont-fast5-api', 'py-htseq', 'py-python-magic', 'py-flye', 'py-zict', 'py-hpccm', 'py-wub', 'py-netcdf4'}
............................ check: py-unidecode
............................. direct: py-unidecode -> {'py-awesome-slugify'}
............................. check: py-awesome-slugify
.............................. direct: py-awesome-slugify -> {'py-vcf-kit'}
.............................. check: py-vcf-kit
............................ check: py-agate-dbf
............................ check: py-aspy-yaml
............................. direct: py-aspy-yaml -> {'py-pre-commit'}
............................. check: py-pre-commit
............................ check: py-hdfs
............................ check: py-jsonpointer
............................. direct: py-jsonpointer -> {'py-jsonpatch'}
............................. check: py-jsonpatch
.............................. direct: py-jsonpatch -> {'py-warlock'}
.............................. check: py-warlock
............................ check: py-pytest-pep8
............................ check: py-s3cmd
............................. direct: py-s3cmd -> set()
............................ check: py-colorlog
............................. direct: py-colorlog -> set()
............................ check: py-torchaudio
............................ check: minigan
............................ check: py-xvfbwrapper
............................. direct: py-xvfbwrapper -> {'py-dryscrape'}
............................. check: py-dryscrape
.............................. direct: py-dryscrape -> set()
............................ check: py-breathe
............................ check: py-wcsaxes
............................ check: py-pycodestyle
............................. direct: py-pycodestyle -> {'py-geoalchemy2', 'py-spyder', 'py-flake8-import-order', 'py-autopep8', 'py-flake8'}
............................. check: py-geoalchemy2
............................. check: py-spyder
............................. check: py-flake8-import-order
.............................. direct: py-flake8-import-order -> {'py-sphinx'}
.............................. check: py-sphinx
............................. check: py-autopep8
.............................. direct: py-autopep8 -> set()
............................. check: py-flake8
............................ check: py-fiscalyear
............................ check: py-openslide-python
............................ check: nrm
............................ check: py-setuptools-scm-git-archive
............................. direct: py-setuptools-scm-git-archive -> {'py-cheroot'}
............................. check: py-cheroot
.............................. direct: py-cheroot -> {'py-cherrypy'}
.............................. check: py-cherrypy
............................... direct: py-cherrypy -> set()
............................ check: py-breakseq2
............................ check: py-pamela
............................. direct: py-pamela -> {'py-jupyterhub'}
............................. check: py-jupyterhub
............................ check: py-brotlipy
............................ check: py-stestr
............................. direct: py-stestr -> {'py-pbr'}
............................. check: py-pbr
............................ check: py-ligo-segments
............................. direct: py-ligo-segments -> {'py-pycbc', 'py-lscsoft-glue'}
............................. check: py-pycbc
............................. check: py-lscsoft-glue
............................ check: py-urllib3
............................ check: py-click
............................. direct: py-click -> {'py-click-plugins', 'py-multiqc', 'py-black', 'py-cookiecutter', 'py-fiona', 'py-cligj', 'py-rasterio', 'py-distributed', 'py-usgs', 'py-cyvcf2', 'py-nltk', 'py-sacremoses', 'py-snuggs', 'py-py2neo', 'py-flask', 'py-biom-format', 'py-coilmq'}
............................. check: py-click-plugins
.............................. direct: py-click-plugins -> {'py-rasterio', 'py-fiona'}
.............................. check: py-rasterio
.............................. check: py-fiona
............................. check: py-multiqc
............................. check: py-black
............................. check: py-cookiecutter
............................. check: py-fiona
............................. check: py-cligj
.............................. direct: py-cligj -> {'py-rasterio', 'py-fiona'}
.............................. check: py-rasterio
.............................. check: py-fiona
............................. check: py-rasterio
............................. check: py-distributed
............................. check: py-usgs
............................. check: py-cyvcf2
............................. check: py-nltk
............................. check: py-sacremoses
............................. check: py-snuggs
............................. check: py-py2neo
............................. check: py-flask
............................. check: py-biom-format
............................. check: py-coilmq
............................ check: py-pydispatcher
............................. direct: py-pydispatcher -> {'py-abipy', 'py-pymatgen'}
............................. check: py-abipy
............................. check: py-pymatgen
............................ check: aws-parallelcluster
............................ check: py-diskcache
............................ check: py-astpretty
............................. direct: py-astpretty -> set()
............................ check: py-psycopg2
............................. direct: py-psycopg2 -> {'py-sqlalchemy', 'qgis'}
............................. check: py-sqlalchemy
............................. check: qgis
............................ check: py-openpmd-validator
............................ check: py-fusepy
............................. direct: py-fusepy -> set()
............................ check: py-pygit2
............................ check: py-arviz
............................ check: py-matplotlib
............................ check: nest
............................ check: py-python-daemon
............................. direct: py-python-daemon -> {'py-luigi', 'py-coilmq'}
............................. check: py-luigi
............................. check: py-coilmq
............................ check: py-sphinx-bootstrap-theme
............................. direct: py-sphinx-bootstrap-theme -> set()
............................ check: py-hvac
............................ check: py-hacking
............................. direct: py-hacking -> {'py-pbr'}
............................. check: py-pbr
............................ check: py-paramiko
............................ check: py-thinc
............................ check: py-line-profiler
............................ check: py-python-memcached
............................. direct: py-python-memcached -> set()
............................ check: py-sphinx-rtd-theme
............................. direct: py-sphinx-rtd-theme -> {'charliecloud', 'py-pydotplus', 'py-graphviz', 'conduit', 'ascent', 'py-sphinx'}
............................. check: charliecloud
............................. check: py-pydotplus
............................. check: py-graphviz
............................. check: conduit
............................. check: ascent
............................. check: py-sphinx
............................ check: py-scikit-learn
............................ check: py-html5lib
............................. direct: py-html5lib -> {'py-mechanize', 'py-astropy', 'py-sphinx', 'py-lxml'}
............................. check: py-mechanize
.............................. direct: py-mechanize -> set()
............................. check: py-astropy
............................. check: py-sphinx
............................. check: py-lxml
.............................. direct: py-lxml -> {'py-dryscrape', 'py-pykml', 'py-geeup', 'py-mypy', 'py-gcovr'}
.............................. check: py-dryscrape
.............................. check: py-pykml
............................... direct: py-pykml -> set()
.............................. check: py-geeup
.............................. check: py-mypy
.............................. check: py-gcovr
............................ check: py-astunparse
............................ check: py-pyfaidx
............................. direct: py-pyfaidx -> {'py-whatshap', 'py-cnvkit'}
............................. check: py-whatshap
............................. check: py-cnvkit
............................ check: py-colorama
............................. direct: py-colorama -> {'py-radical-utils', 'awscli', 'py-typesentry', 'py-py2neo'}
............................. check: py-radical-utils
.............................. direct: py-radical-utils -> {'py-saga-python'}
.............................. check: py-saga-python
............................... direct: py-saga-python -> set()
............................. check: awscli
............................. check: py-typesentry
.............................. direct: py-typesentry -> set()
............................. check: py-py2neo
............................ check: py-ipykernel
............................ check: py-psutil
............................ check: gdal
............................ check: py-cvxopt
............................ check: py-parse
............................. direct: py-parse -> {'py-merlin'}
............................. check: py-merlin
............................ check: py-pydot
............................ check: py-sphinxcontrib-applehelp
............................. direct: py-sphinxcontrib-applehelp -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-texttable
............................. direct: py-texttable -> {'lbann'}
............................. check: lbann
............................ check: py-pyperf
............................. direct: py-pyperf -> {'py-performance'}
............................. check: py-performance
.............................. direct: py-performance -> set()
............................ check: py-sphinxcontrib-bibtex
............................ check: py-wxpython
............................ check: py-argparse
............................. direct: py-argparse -> {'py-plac', 'py-opppy', 'py-numba', 'py-fiona', 'nrm', 'py-codecov', 'httpie', 'oclint', 'py-csvkit', 'py-pyfaidx', 'lbann', 'py-unittest2', 'py-scoop', 'git-imerge', 'awscli', 'py-future', 'py-opentuner'}
............................. check: py-plac
.............................. direct: py-plac -> {'py-thinc', 'py-spacy'}
.............................. check: py-thinc
.............................. check: py-spacy
............................. check: py-opppy
............................. check: py-numba
............................. check: py-fiona
............................. check: nrm
............................. check: py-codecov
............................. check: httpie
............................. check: oclint
............................. check: py-csvkit
............................. check: py-pyfaidx
............................. check: lbann
............................. check: py-unittest2
............................. check: py-scoop
............................. check: git-imerge
............................. check: awscli
............................. check: py-future
.............................. direct: py-future -> {'aws-parallelcluster', 'py-geeadd', 'py-multiqc', 'py-projectq', 'py-cookiecutter', 'geopm', 'py-lzstring', 'py-geeup', 'py-osqp', 'py-umi-tools', 'py-pyglet', 'py-cnvkit', 'py-gee-asset-manager', 'py-picrust', 'py-tensorflow', 'py-biom-format', 'py-petastorm', 'py-opentuner', 'py-torch'}
.............................. check: aws-parallelcluster
.............................. check: py-geeadd
.............................. check: py-multiqc
.............................. check: py-projectq
.............................. check: py-cookiecutter
.............................. check: geopm
.............................. check: py-lzstring
............................... direct: py-lzstring -> {'py-multiqc'}
............................... check: py-multiqc
.............................. check: py-geeup
.............................. check: py-osqp
.............................. check: py-umi-tools
.............................. check: py-pyglet
............................... direct: py-pyglet -> set()
.............................. check: py-cnvkit
.............................. check: py-gee-asset-manager
.............................. check: py-picrust
.............................. check: py-tensorflow
.............................. check: py-biom-format
.............................. check: py-petastorm
.............................. check: py-opentuner
.............................. check: py-torch
............................. check: py-opentuner
............................ check: py-lzstring
............................ check: py-htmlgen
............................. direct: py-htmlgen -> {'py-h5glance'}
............................. check: py-h5glance
............................ check: py-gluoncv
............................ check: py-osqp
............................ check: py-astor
............................ check: py-scikit-optimize
............................ check: py-dendropy
............................. direct: py-dendropy -> {'pasta', 'py-checkm-genome'}
............................. check: pasta
............................. check: py-checkm-genome
............................ check: py-pytest-forked
............................ check: py-pyinstrument-cext
............................. direct: py-pyinstrument-cext -> {'py-pyinstrument'}
............................. check: py-pyinstrument
.............................. direct: py-pyinstrument -> set()
............................ check: mxnet
............................ check: py-sphinx
............................ check: py-decorator
............................. direct: py-decorator -> {'py-pycuda', 'py-traitlets', 'py-ratelim', 'py-networkx', 'py-pytools', 'py-pycbc', 'py-librosa', 'py-httpbin', 'py-ipython', 'py-plotly', 'py-pytest-httpbin'}
............................. check: py-pycuda
............................. check: py-traitlets
.............................. direct: py-traitlets -> {'py-nbconvert', 'py-jupyter-core', 'py-ipdb', 'py-qtconsole', 'py-nbformat', 'py-jupyter-client', 'py-ipywidgets', 'py-notebook', 'py-ipykernel', 'py-ipython', 'py-jupyterhub'}
.............................. check: py-nbconvert
.............................. check: py-jupyter-core
............................... direct: py-jupyter-core -> {'py-nbconvert', 'py-qtconsole', 'py-nbformat', 'py-jupyter-client', 'py-notebook'}
............................... check: py-nbconvert
............................... check: py-qtconsole
............................... check: py-nbformat
............................... check: py-jupyter-client
............................... check: py-notebook
.............................. check: py-ipdb
.............................. check: py-qtconsole
.............................. check: py-nbformat
.............................. check: py-jupyter-client
.............................. check: py-ipywidgets
.............................. check: py-notebook
.............................. check: py-ipykernel
.............................. check: py-ipython
.............................. check: py-jupyterhub
............................. check: py-ratelim
.............................. direct: py-ratelim -> set()
............................. check: py-networkx
.............................. direct: py-networkx -> {'py-yahmm', 'falcon', 'pagmo', 'py-mdanalysis', 'py-vcf-kit', 'py-pypeflow', 'py-pomegranate', 'pbsuite', 'py-scikit-image', 'gaudi', 'py-dgl', 'py-colormath', 'py-whatshap'}
.............................. check: py-yahmm
.............................. check: falcon
.............................. check: pagmo
.............................. check: py-mdanalysis
.............................. check: py-vcf-kit
.............................. check: py-pypeflow
............................... direct: py-pypeflow -> {'falcon'}
............................... check: falcon
.............................. check: py-pomegranate
.............................. check: pbsuite
.............................. check: py-scikit-image
.............................. check: gaudi
.............................. check: py-dgl
.............................. check: py-colormath
.............................. check: py-whatshap
............................. check: py-pytools
.............................. direct: py-pytools -> set()
............................. check: py-pycbc
............................. check: py-librosa
............................. check: py-httpbin
............................. check: py-ipython
............................. check: py-plotly
............................. check: py-pytest-httpbin
............................ check: py-typed-ast
............................. direct: py-typed-ast -> {'py-mypy', 'py-astpretty', 'py-astroid'}
............................. check: py-mypy
............................. check: py-astpretty
............................. check: py-astroid
.............................. direct: py-astroid -> {'py-pylint'}
.............................. check: py-pylint
............................ check: py-wand
............................ check: py-munch
............................. direct: py-munch -> {'py-fiona'}
............................. check: py-fiona
............................ check: py-scipy
............................ check: py-cython
............................. direct: py-cython -> {'openmm', 'py-cogent', 'py-pywavelets', 'py-mdanalysis', 'py-mpi4py', 'py-cartopy', 'bohrium', 'py-gevent', 'py-vcf-kit', 'py-gluoncv', 'py-grpcio', 'py-scikit-image', 'libseccomp', 'py-crossmap', 'py-tables', 'py-pybedtools', 'py-spacy', 'py-h5py', 'py-pygpu', 'py-breakseq2', 'py-cyvcf2', 'py-pomegranate', 'py-espresso', 'py-pyarrow', 'py-abipy', 'nghttp2', 'py-pandas', 'py-whatshap', 'py-iminuit', 'py-cftime', 'ibmisc', 'py-pyomo', 'plumed', 'py-mo-pack', 'py-shapely', 'py-srsly', 'py-yahmm', 'memsurfer', 'py-fiona', 'py-rasterio', 'py-symengine', 'py-bx-python', 'py-yt', 'steps', 'py-adios', 'py-mmcv', 'py-pycbc', 'platypus', 'py-pyfftw', 'nest', 'py-atropos', 'py-cf-units', 'py-biom-format', 'py-dgl', 'py-brian2', 'py-faststructure', 'py-line-profiler', 'py-htseq', 'py-pysam', 'py-pyzmq', 'py-stratify', 'cantera', 'py-numpy', 'py-pyproj', 'py-scikit-learn', 'py-petsc4py', 'py-rseqc', 'minimap2', 'py-netcdf4'}
............................. check: openmm
............................. check: py-cogent
............................. check: py-pywavelets
............................. check: py-mdanalysis
............................. check: py-mpi4py
............................. check: py-cartopy
............................. check: bohrium
............................. check: py-gevent
............................. check: py-vcf-kit
............................. check: py-gluoncv
............................. check: py-grpcio
............................. check: py-scikit-image
............................. check: libseccomp
.............................. direct: libseccomp -> {'singularity', 'nix'}
.............................. check: singularity
.............................. check: nix
............................. check: py-crossmap
............................. check: py-tables
............................. check: py-pybedtools
............................. check: py-spacy
............................. check: py-h5py
............................. check: py-pygpu
............................. check: py-breakseq2
............................. check: py-cyvcf2
............................. check: py-pomegranate
............................. check: py-espresso
............................. check: py-pyarrow
............................. check: py-abipy
............................. check: nghttp2
.............................. direct: nghttp2 -> {'curl', 'wireshark'}
.............................. check: curl
.............................. check: wireshark
............................. check: py-pandas
............................. check: py-whatshap
............................. check: py-iminuit
............................. check: py-cftime
............................. check: ibmisc
............................. check: py-pyomo
.............................. direct: py-pyomo -> set()
............................. check: plumed
............................. check: py-mo-pack
............................. check: py-shapely
............................. check: py-srsly
............................. check: py-yahmm
............................. check: memsurfer
............................. check: py-fiona
............................. check: py-rasterio
............................. check: py-symengine
............................. check: py-bx-python
.............................. direct: py-bx-python -> {'py-crossmap', 'hic-pro', 'py-rseqc'}
.............................. check: py-crossmap
.............................. check: hic-pro
.............................. check: py-rseqc
............................. check: py-yt
............................. check: steps
............................. check: py-adios
............................. check: py-mmcv
............................. check: py-pycbc
............................. check: platypus
............................. check: py-pyfftw
............................. check: nest
............................. check: py-atropos
............................. check: py-cf-units
............................. check: py-biom-format
............................. check: py-dgl
............................. check: py-brian2
............................. check: py-faststructure
............................. check: py-line-profiler
............................. check: py-htseq
............................. check: py-pysam
............................. check: py-pyzmq
............................. check: py-stratify
............................. check: cantera
............................. check: py-numpy
............................. check: py-pyproj
.............................. direct: py-pyproj -> {'py-owslib', 'py-basemap', 'py-geopandas'}
.............................. check: py-owslib
.............................. check: py-basemap
.............................. check: py-geopandas
............................. check: py-scikit-learn
............................. check: py-petsc4py
............................. check: py-rseqc
............................. check: minimap2
.............................. direct: minimap2 -> set()
............................. check: py-netcdf4
............................ check: py-google-pasta
............................. direct: py-google-pasta -> {'py-tensorflow'}
............................. check: py-tensorflow
............................ check: py-testinfra
............................ check: py-netifaces
............................. direct: py-netifaces -> {'py-radical-utils'}
............................. check: py-radical-utils
............................ check: py-pygpu
............................ check: py-sphinxcontrib-programoutput
............................. direct: py-sphinxcontrib-programoutput -> set()
............................ check: httpie
............................ check: py-backports-tempfile
............................. direct: py-backports-tempfile -> set()
............................ check: py-addict
............................. direct: py-addict -> {'py-mmcv'}
............................. check: py-mmcv
............................ check: py-pysocks
............................ check: py-testresources
............................. direct: py-testresources -> {'py-pbr'}
............................. check: py-pbr
............................ check: py-pyomo
............................ check: py-cmake-format
............................. direct: py-cmake-format -> set()
............................ check: py-lru-dict
............................. direct: py-lru-dict -> set()
............................ check: py-zipp
............................. direct: py-zipp -> {'py-importlib-metadata'}
............................. check: py-importlib-metadata
............................ check: py-numba
............................ check: py-merlin
............................ check: py-text-unidecode
............................. direct: py-text-unidecode -> {'py-python-slugify'}
............................. check: py-python-slugify
.............................. direct: py-python-slugify -> {'py-agate'}
.............................. check: py-agate
............................ check: py-yapf
............................. direct: py-yapf -> set()
............................ check: py-chardet
............................ check: py-ipdb
............................ check: py-pipits
............................ check: py-python-oauth2
............................. direct: py-python-oauth2 -> {'py-jupyterhub'}
............................. check: py-jupyterhub
............................ check: py-yamlreader
............................. direct: py-yamlreader -> set()
............................ check: py-vermin
............................. direct: py-vermin -> set()
............................ check: py-jupyterlab-server
............................ check: py-gf256
............................ check: py-humanize
............................ check: py-fastcluster
............................ check: py-petastorm
............................ check: py-bottleneck
............................ check: py-pyliblzma
............................. direct: py-pyliblzma -> set()
............................ check: py-google-auth
............................. direct: py-google-auth -> {'py-google-auth-oauthlib', 'py-google-api-core', 'py-earthengine-api', 'py-google-api-python-client', 'py-google-auth-httplib2', 'py-google-cloud-storage', 'py-tensorboard'}
............................. check: py-google-auth-oauthlib
............................. check: py-google-api-core
............................. check: py-earthengine-api
.............................. direct: py-earthengine-api -> {'py-geeadd', 'py-geeup', 'py-gee-asset-manager'}
.............................. check: py-geeadd
.............................. check: py-geeup
.............................. check: py-gee-asset-manager
............................. check: py-google-api-python-client
.............................. direct: py-google-api-python-client -> {'py-earthengine-api'}
.............................. check: py-earthengine-api
............................. check: py-google-auth-httplib2
.............................. direct: py-google-auth-httplib2 -> {'py-google-api-python-client', 'py-earthengine-api'}
.............................. check: py-google-api-python-client
.............................. check: py-earthengine-api
............................. check: py-google-cloud-storage
............................. check: py-tensorboard
............................ check: jube
............................. direct: jube -> set()
............................ check: py-selenium
............................ check: py-agate
............................ check: py-spatialite
............................. direct: py-spatialite -> set()
............................ check: py-theano
............................ check: py-cligj
............................ check: py-importlib-metadata
............................ check: py-pytest-flake8
............................ check: bumpversion
............................. direct: bumpversion -> set()
............................ check: py-flake8
............................ check: py-requests
............................ check: py-sphinxcontrib-devhelp
............................. direct: py-sphinxcontrib-devhelp -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-rseqc
............................ check: py-progressbar2
............................. direct: py-progressbar2 -> {'py-pyrosar', 'py-pipits', 'py-spatialist'}
............................. check: py-pyrosar
............................. check: py-pipits
............................. check: py-spatialist
............................ check: py-pywavelets
............................ check: py-xmltodict
............................. direct: py-xmltodict -> {'py-wradlib'}
............................. check: py-wradlib
............................ check: py-pkginfo
............................. direct: py-pkginfo -> {'py-twine'}
............................. check: py-twine
............................ check: py-poyo
............................. direct: py-poyo -> {'py-cookiecutter'}
............................. check: py-cookiecutter
............................ check: py-sphinxautomodapi
............................. direct: py-sphinxautomodapi -> set()
............................ check: py-backports-weakref
............................. direct: py-backports-weakref -> {'py-backports-tempfile', 'py-tensorflow'}
............................. check: py-backports-tempfile
............................. check: py-tensorflow
............................ check: py-grpcio
............................ check: py-subprocess32
............................. direct: py-subprocess32 -> {'py-matplotlib'}
............................. check: py-matplotlib
............................ check: py-misopy
............................ check: z3
............................. direct: z3 -> {'llvm', 'rose'}
............................. check: llvm
............................. check: rose
............................ check: py-future
............................ check: py-eventlet
............................. direct: py-eventlet -> {'py-python-socketio'}
............................. check: py-python-socketio
.............................. direct: py-python-socketio -> {'py-flask-socketio'}
.............................. check: py-flask-socketio
............................ check: py-pytest-cache
............................ check: py-torch
............................ check: py-intervaltree
............................ check: py-tokenizers
............................ check: py-neo
............................ check: py-blinker
............................. direct: py-blinker -> {'py-oauthlib', 'py-raven'}
............................. check: py-oauthlib
............................. check: py-raven
............................ check: py-fastcache
............................. direct: py-fastcache -> set()
............................ check: py-pyrad
............................ check: py-zope-interface
............................. direct: py-zope-interface -> {'py-attrs'}
............................. check: py-attrs
............................ check: py-simplegeneric
............................. direct: py-simplegeneric -> {'py-ipython', 'py-rpy2'}
............................. check: py-ipython
............................. check: py-rpy2
............................ check: py-pyqi
............................. direct: py-pyqi -> {'py-biom-format'}
............................. check: py-biom-format
............................ check: py-pandas
............................ check: py-pymongo
............................. direct: py-pymongo -> set()
............................ check: py-griddataformats
............................ check: py-httplib2
............................. direct: py-httplib2 -> {'py-google-api-python-client', 'py-google-auth-httplib2', 'py-earthengine-api', 'py-oauth2client'}
............................. check: py-google-api-python-client
............................. check: py-google-auth-httplib2
............................. check: py-earthengine-api
............................. check: py-oauth2client
.............................. direct: py-oauth2client -> {'py-geeadd', 'py-geeup'}
.............................. check: py-geeadd
.............................. check: py-geeup
............................ check: py-msgpack
............................. direct: py-msgpack -> {'py-zarr', 'py-distributed', 'py-numcodecs'}
............................. check: py-zarr
............................. check: py-distributed
............................. check: py-numcodecs
.............................. direct: py-numcodecs -> {'py-zarr'}
.............................. check: py-zarr
............................ check: py-tfdlpack
............................ check: py-smart-open
............................ check: py-envisage
............................ check: py-flask-socketio
............................ check: py-kitchen
............................. direct: py-kitchen -> set()
............................ check: py-dnaio
............................. direct: py-dnaio -> {'py-cutadapt'}
............................. check: py-cutadapt
.............................. direct: py-cutadapt -> {'trimgalore'}
.............................. check: trimgalore
............................... direct: trimgalore -> set()
............................ check: py-nltk
............................ check: py-wxmplot
............................ check: py-sphinxcontrib-htmlhelp
............................. direct: py-sphinxcontrib-htmlhelp -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-cnvkit
............................ check: py-pyasn1-modules
............................. direct: py-pyasn1-modules -> {'py-python-ldap', 'py-oauth2client', 'py-google-auth'}
............................. check: py-python-ldap
.............................. direct: py-python-ldap -> set()
............................. check: py-oauth2client
............................. check: py-google-auth
............................ check: py-opentuner
............................ check: py-lazy
............................. direct: py-lazy -> set()
............................ check: py-pysam
............................ check: py-nosexcover
............................. direct: py-nosexcover -> set()
............................ check: py-biopython
............................. direct: py-biopython -> {'shortbred', 'py-mdanalysis', 'py-vcf-kit', 'py-breakseq2', 'py-pyani', 'py-crispresso', 'py-cnvkit', 'phyluce', 'hybpiper', 'py-wub', 'py-pauvre', 'py-bcbio-gff', 'orthofiller'}
............................. check: shortbred
............................. check: py-mdanalysis
............................. check: py-vcf-kit
............................. check: py-breakseq2
............................. check: py-pyani
............................. check: py-crispresso
............................. check: py-cnvkit
............................. check: phyluce
............................. check: hybpiper
............................. check: py-wub
............................. check: py-pauvre
............................. check: py-bcbio-gff
.............................. direct: py-bcbio-gff -> set()
............................. check: orthofiller
............................ check: py-rtree
............................. direct: py-rtree -> set()
............................ check: py-isort
............................ check: py-numcodecs
............................ check: py-codecov
............................ check: py-sqlalchemy
............................ check: py-retrying
............................. direct: py-retrying -> {'py-geeadd', 'py-geeup', 'py-gee-asset-manager'}
............................. check: py-geeadd
............................. check: py-geeup
............................. check: py-gee-asset-manager
............................ check: py-certipy
............................ check: py-unittest2py3k
............................. direct: py-unittest2py3k -> {'cantera'}
............................. check: cantera
............................ check: py-xarray
............................ check: py-ipython
............................ check: py-leather
............................. direct: py-leather -> {'py-agate'}
............................. check: py-agate
............................ check: py-nose
............................. direct: py-nose -> {'py-pkginfo', 'py-astor', 'py-awesome-slugify', 'py-pyinstrument-cext', 'py-pyutilib', 'py-ipywidgets', 'py-args', 'py-psyclone', 'py-pygpu', 'py-jmespath', 'py-elephant', 'vigra', 'py-oauthlib', 'py-goatools', 'py-zope-interface', 'py-deeptools', 'py-tap-py', 'py-yahmm', 'py-pykml', 'seqan', 'py-dateparser', 'py-nose-cov', 'py-botocore', 'py-py4j', 'nest', 'py-fparser', 'awscli', 'py-tqdm', 'py-bottleneck', 'py-brian2', 'py-freezegun', 'py-patsy', 'py-nosexcover', 'py-theano', 'py-numpy', 'gaudi', 'py-ecos', 'py-pkgconfig', 'py-cvxpy', 'py-rseqc', 'py-grequests'}
............................. check: py-pkginfo
............................. check: py-astor
............................. check: py-awesome-slugify
............................. check: py-pyinstrument-cext
............................. check: py-pyutilib
.............................. direct: py-pyutilib -> {'py-pyomo'}
.............................. check: py-pyomo
............................. check: py-ipywidgets
............................. check: py-args
.............................. direct: py-args -> {'py-clint'}
.............................. check: py-clint
............................... direct: py-clint -> {'py-vcf-kit'}
............................... check: py-vcf-kit
............................. check: py-psyclone
............................. check: py-pygpu
............................. check: py-jmespath
.............................. direct: py-jmespath -> {'py-boto3', 'py-botocore'}
.............................. check: py-boto3
.............................. check: py-botocore
............................. check: py-elephant
............................. check: vigra
............................. check: py-oauthlib
............................. check: py-goatools
............................. check: py-zope-interface
............................. check: py-deeptools
............................. check: py-tap-py
.............................. direct: py-tap-py -> set()
............................. check: py-yahmm
............................. check: py-pykml
............................. check: seqan
............................. check: py-dateparser
............................. check: py-nose-cov
.............................. direct: py-nose-cov -> set()
............................. check: py-botocore
............................. check: py-py4j
.............................. direct: py-py4j -> {'py-pyspark'}
.............................. check: py-pyspark
............................... direct: py-pyspark -> {'py-horovod', 'py-petastorm'}
............................... check: py-horovod
............................... check: py-petastorm
............................. check: nest
............................. check: py-fparser
............................. check: awscli
............................. check: py-tqdm
............................. check: py-bottleneck
............................. check: py-brian2
............................. check: py-freezegun
............................. check: py-patsy
............................. check: py-nosexcover
............................. check: py-theano
............................. check: py-numpy
............................. check: gaudi
............................. check: py-ecos
............................. check: py-pkgconfig
.............................. direct: py-pkgconfig -> {'py-h5py'}
.............................. check: py-h5py
............................. check: py-cvxpy
............................. check: py-rseqc
............................. check: py-grequests
............................ check: py-pycairo
............................ check: py-rpy2
............................ check: py-gast
............................. direct: py-gast -> {'py-tensorflow'}
............................. check: py-tensorflow
............................ check: py-mdanalysis
............................ check: py-repoze-lru
............................ check: py-lockfile
............................. direct: py-lockfile -> {'py-python-daemon'}
............................. check: py-python-daemon
............................ check: py-distributed
............................ check: py-argcomplete
............................. direct: py-argcomplete -> {'py-basis-set-exchange'}
............................. check: py-basis-set-exchange
............................ check: py-invoke
............................. direct: py-invoke -> set()
............................ check: py-palettable
............................. direct: py-palettable -> {'py-pymatgen'}
............................. check: py-pymatgen
............................ check: py-pytools
............................ check: py-tensorboardx
............................ check: py-audioread
............................. direct: py-audioread -> {'py-librosa'}
............................. check: py-librosa
............................ check: py-scoop
............................ check: py-pint
............................. direct: py-pint -> set()
............................ check: py-tensorboard
............................ check: py-geeadd
............................ check: py-markupsafe
............................. direct: py-markupsafe -> {'py-mako', 'py-httpbin', 'py-jinja2'}
............................. check: py-mako
............................. check: py-httpbin
............................. check: py-jinja2
............................ check: py-prompt-toolkit
............................. direct: py-prompt-toolkit -> {'py-ipdb', 'py-h5sh', 'py-jupyter-console', 'py-spatialist', 'py-py2neo', 'py-ipython'}
............................. check: py-ipdb
............................. check: py-h5sh
............................. check: py-jupyter-console
............................. check: py-spatialist
............................. check: py-py2neo
............................. check: py-ipython
............................ check: py-modred
............................. direct: py-modred -> set()
............................ check: py-pyani
............................ check: py-voluptuous
............................. direct: py-voluptuous -> {'sirius'}
............................. check: sirius
............................ check: py-docopt
............................. direct: py-docopt -> {'py-hpcbench', 'py-pykwalify', 'py-hdfs', 'py-vcf-kit', 'py-parso'}
............................. check: py-hpcbench
............................. check: py-pykwalify
.............................. direct: py-pykwalify -> set()
............................. check: py-hdfs
............................. check: py-vcf-kit
............................. check: py-parso
............................ check: py-bsddb3
............................. direct: py-bsddb3 -> {'py-methylcode'}
............................. check: py-methylcode
............................ check: py-tensorflow
............................ check: py-flake8-polyfill
............................ check: py-pytest-check-links
............................. direct: py-pytest-check-links -> {'py-jupyterlab'}
............................. check: py-jupyterlab
............................ check: py-myhdl
............................. direct: py-myhdl -> set()
............................ check: py-gcovr
............................ check: ambari
............................ check: py-dateparser
............................ check: py-fixtures
............................. direct: py-fixtures -> {'py-linecache2', 'py-pbr', 'py-testrepository', 'py-traceback2'}
............................. check: py-linecache2
............................. check: py-pbr
............................. check: py-testrepository
.............................. direct: py-testrepository -> {'py-pbr'}
.............................. check: py-pbr
............................. check: py-traceback2
............................ check: py-pyspark
............................ check: py-kiwisolver
............................. direct: py-kiwisolver -> {'py-matplotlib'}
............................. check: py-matplotlib
............................ check: py-multiprocess
............................. direct: py-multiprocess -> {'py-cvxpy', 'py-pathos'}
............................. check: py-cvxpy
............................. check: py-pathos
.............................. direct: py-pathos -> {'py-pyrosar', 'py-spatialist'}
.............................. check: py-pyrosar
.............................. check: py-spatialist
............................ check: py-pytailf
............................. direct: py-pytailf -> set()
............................ check: py-warlock
............................ check: py-execnet
............................ check: py-pyrsistent
............................. direct: py-pyrsistent -> {'py-jsonschema'}
............................. check: py-jsonschema
............................ check: py-fasteners
............................. direct: py-fasteners -> {'py-zarr'}
............................. check: py-zarr
............................ check: py-singledispatch
............................. direct: py-singledispatch -> {'py-numba', 'py-distributed', 'py-tornado', 'py-astroid', 'py-pylint', 'py-rpy2'}
............................. check: py-numba
............................. check: py-distributed
............................. check: py-tornado
.............................. direct: py-tornado -> {'py-nbconvert', 'py-terminado', 'py-bokeh', 'nrm', 'py-distributed', 'py-flexx', 'py-jupyterlab', 'py-jupyter-client', 'py-luigi', 'py-urllib3', 'py-matplotlib', 'py-notebook', 'py-ipykernel', 'py-jupyterhub'}
.............................. check: py-nbconvert
.............................. check: py-terminado
............................... direct: py-terminado -> {'py-notebook'}
............................... check: py-notebook
.............................. check: py-bokeh
.............................. check: nrm
.............................. check: py-distributed
.............................. check: py-flexx
............................... direct: py-flexx -> set()
.............................. check: py-jupyterlab
.............................. check: py-jupyter-client
.............................. check: py-luigi
.............................. check: py-urllib3
.............................. check: py-matplotlib
.............................. check: py-notebook
.............................. check: py-ipykernel
.............................. check: py-jupyterhub
............................. check: py-astroid
............................. check: py-pylint
............................. check: py-rpy2
............................ check: py-black
............................ check: py-twisted
............................. direct: py-twisted -> {'py-prometheus-client'}
............................. check: py-prometheus-client
.............................. direct: py-prometheus-client -> {'py-jupyterhub', 'py-notebook'}
.............................. check: py-jupyterhub
.............................. check: py-notebook
............................ check: py-contextlib2
............................. direct: py-contextlib2 -> {'py-importlib-metadata', 'py-traceback2'}
............................. check: py-importlib-metadata
............................. check: py-traceback2
............................ check: py-torchtext
............................ check: py-webencodings
............................. direct: py-webencodings -> {'py-html5lib', 'py-bleach'}
............................. check: py-html5lib
............................. check: py-bleach
.............................. direct: py-bleach -> {'py-readme-renderer', 'py-astropy', 'py-nbconvert'}
.............................. check: py-readme-renderer
.............................. check: py-astropy
.............................. check: py-nbconvert
............................ check: py-pyudev
............................. direct: py-pyudev -> set()
............................ check: py-lazy-property
............................. direct: py-lazy-property -> set()
............................ check: py-certifi
............................. direct: py-certifi -> {'mercurial', 'py-tornado', 'py-urllib3', 'py-py2neo', 'py-requests'}
............................. check: mercurial
.............................. direct: mercurial -> {'httpress', 'slate'}
.............................. check: httpress
............................... direct: httpress -> set()
.............................. check: slate
............................. check: py-tornado
............................. check: py-urllib3
............................. check: py-py2neo
............................. check: py-requests
............................ check: py-overpy
............................ check: py-elasticsearch
............................ check: py-cvxpy
............................ check: py-subrosa
............................ check: py-deprecated
............................. direct: py-deprecated -> set()
............................ check: py-billiard
............................. direct: py-billiard -> {'py-celery'}
............................. check: py-celery
............................ check: py-mypy-extensions
............................. direct: py-mypy-extensions -> {'py-mypy'}
............................. check: py-mypy
............................ check: py-pyshp
............................. direct: py-pyshp -> {'py-basemap', 'py-cartopy'}
............................. check: py-basemap
............................. check: py-cartopy
............................ check: py-tox
............................ check: py-simplejson
............................. direct: py-simplejson -> {'py-csvkit', 'py-avro-json-serializer', 'py-botocore', 'py-multiqc'}
............................. check: py-csvkit
............................. check: py-avro-json-serializer
.............................. direct: py-avro-json-serializer -> set()
............................. check: py-botocore
............................. check: py-multiqc
............................ check: py-cryptography
............................ check: py-memory-profiler
............................ check: py-sacremoses
............................ check: py-performance
............................ check: py-numexpr
............................ check: py-picrust
............................ check: py-pytz
............................. direct: py-pytz -> {'py-plotly', 'py-astropy', 'py-tzlocal', 'py-srsly', 'py-dateparser', 'py-babel', 'py-logilab-common', 'py-owslib', 'py-neotime', 'py-tempora', 'py-google-api-core', 'py-celery', 'py-apscheduler', 'py-convertdate', 'py-matplotlib', 'py-py2neo', 'py-pandas', 'py-arrow', 'py-django'}
............................. check: py-plotly
............................. check: py-astropy
............................. check: py-tzlocal
.............................. direct: py-tzlocal -> {'py-apscheduler', 'py-dateparser'}
.............................. check: py-apscheduler
............................... direct: py-apscheduler -> {'py-abipy'}
............................... check: py-abipy
.............................. check: py-dateparser
............................. check: py-srsly
............................. check: py-dateparser
............................. check: py-babel
............................. check: py-logilab-common
............................. check: py-owslib
............................. check: py-neotime
.............................. direct: py-neotime -> {'py-py2neo'}
.............................. check: py-py2neo
............................. check: py-tempora
.............................. direct: py-tempora -> {'py-portend'}
.............................. check: py-portend
............................... direct: py-portend -> {'py-cherrypy'}
............................... check: py-cherrypy
............................. check: py-google-api-core
............................. check: py-celery
............................. check: py-apscheduler
............................. check: py-convertdate
.............................. direct: py-convertdate -> {'py-dateparser'}
.............................. check: py-dateparser
............................. check: py-matplotlib
............................. check: py-py2neo
............................. check: py-pandas
............................. check: py-arrow
............................. check: py-django
.............................. direct: py-django -> set()
............................ check: py-pyface
............................ check: py-cdo
............................ check: py-statsmodels
............................ check: py-pyquaternion
............................ check: py-slurm-pipeline
............................. direct: py-slurm-pipeline -> set()
............................ check: py-jinja2
............................ check: py-enum34
............................. direct: py-enum34 -> {'stat', 'py-auxlib', 'py-hypothesis', 'py-astroid', 'py-cryptography', 'py-grpcio', 'py-maestrowf', 'py-eventlet', 'py-multiqc', 'py-pyarrow', 'py-brotlipy', 'py-traitlets', 'py-tensorflow', 'aws-parallelcluster', 'py-fiona', 'py-flake8-import-order', 'py-rasterio', 'py-pydeps', 'py-pyqt5', 'py-llvmlite', 'py-absl-py', 'py-flake8', 'py-hpccm'}
............................. check: stat
............................. check: py-auxlib
............................. check: py-hypothesis
............................. check: py-astroid
............................. check: py-cryptography
............................. check: py-grpcio
............................. check: py-maestrowf
.............................. direct: py-maestrowf -> {'py-merlin'}
.............................. check: py-merlin
............................. check: py-eventlet
............................. check: py-multiqc
............................. check: py-pyarrow
............................. check: py-brotlipy
............................. check: py-traitlets
............................. check: py-tensorflow
............................. check: aws-parallelcluster
............................. check: py-fiona
............................. check: py-flake8-import-order
............................. check: py-rasterio
............................. check: py-pydeps
.............................. direct: py-pydeps -> set()
............................. check: py-pyqt5
.............................. direct: py-pyqt5 -> {'py-pymol', 'py-qtpy', 'py-traitsui', 'py-tuiview', 'tulip', 'py-qtconsole', 'qscintilla', 'qgis', 'py-matplotlib', 'py-pyface'}
.............................. check: py-pymol
.............................. check: py-qtpy
.............................. check: py-traitsui
.............................. check: py-tuiview
.............................. check: tulip
.............................. check: py-qtconsole
.............................. check: qscintilla
............................... direct: qscintilla -> {'qgis'}
............................... check: qgis
.............................. check: qgis
.............................. check: py-matplotlib
.............................. check: py-pyface
............................. check: py-llvmlite
............................. check: py-absl-py
.............................. direct: py-absl-py -> {'py-tensorflow', 'py-tensorboard'}
.............................. check: py-tensorflow
.............................. check: py-tensorboard
............................. check: py-flake8
............................. check: py-hpccm
.............................. direct: py-hpccm -> set()
............................ check: py-coilmq
............................ check: py-cftime
............................ check: py-python-swiftclient
............................ check: py-avro
............................. direct: py-avro -> {'py-avro-json-serializer'}
............................. check: py-avro-json-serializer
............................ check: falcon
............................ check: py-mpld3
............................ check: py-pygments
............................. direct: py-pygments -> {'py-traitsui', 'py-qtconsole', 'py-sphinx', 'cppcheck', 'py-pyface', 'py-alabaster', 'gtk-doc', 'py-pudb', 'py-mayavi', 'py-spyder', 'qgis', 'httpie', 'py-tap-py', 'py-nbconvert', 'mercurial', 'py-gdbgui', 'py-readme-renderer', 'py-h5sh', 'py-jupyter-console', 'py-py2neo', 'py-ipython', 'py-restview'}
............................. check: py-traitsui
............................. check: py-qtconsole
............................. check: py-sphinx
............................. check: cppcheck
.............................. direct: cppcheck -> {'axom'}
.............................. check: axom
............................. check: py-pyface
............................. check: py-alabaster
.............................. direct: py-alabaster -> {'py-sphinx'}
.............................. check: py-sphinx
............................. check: gtk-doc
.............................. direct: gtk-doc -> set()
............................. check: py-pudb
.............................. direct: py-pudb -> set()
............................. check: py-mayavi
............................. check: py-spyder
............................. check: qgis
............................. check: httpie
............................. check: py-tap-py
............................. check: py-nbconvert
............................. check: mercurial
............................. check: py-gdbgui
............................. check: py-readme-renderer
............................. check: py-h5sh
............................. check: py-jupyter-console
............................. check: py-py2neo
............................. check: py-ipython
............................. check: py-restview
............................ check: py-pyvcf
............................. direct: py-pyvcf -> {'py-metasv', 'py-biomine', 'py-whatshap'}
............................. check: py-metasv
............................. check: py-biomine
............................. check: py-whatshap
............................ check: portcullis
............................ check: py-python-subunit
............................. direct: py-python-subunit -> {'py-testrepository'}
............................. check: py-testrepository
............................ check: py-mysqldb1
............................ check: py-pymc3
............................ check: py-botocore
............................ check: py-libconf
............................. direct: py-libconf -> set()
............................ check: py-google-api-python-client
............................ check: py-openpyxl
............................. direct: py-openpyxl -> {'py-csvkit', 'py-agate-excel'}
............................. check: py-csvkit
............................. check: py-agate-excel
............................ check: py-mysqlclient
............................ check: py-et-xmlfile
............................. direct: py-et-xmlfile -> {'py-openpyxl'}
............................. check: py-openpyxl
............................ check: py-tzlocal
............................ check: py-semver
............................. direct: py-semver -> {'py-wradlib'}
............................. check: py-wradlib
............................ check: py-stratify
............................ check: py-scandir
............................. direct: py-scandir -> {'py-pathlib2', 'py-zarr'}
............................. check: py-pathlib2
.............................. direct: py-pathlib2 -> {'py-wxpython', 'py-importlib-metadata', 'py-pytest', 'py-pyarrow', 'py-importlib-resources', 'py-pickleshare', 'py-ipython'}
.............................. check: py-wxpython
.............................. check: py-importlib-metadata
.............................. check: py-pytest
.............................. check: py-pyarrow
.............................. check: py-importlib-resources
.............................. check: py-pickleshare
............................... direct: py-pickleshare -> {'py-ipython', 'py-spyder'}
............................... check: py-ipython
............................... check: py-spyder
.............................. check: py-ipython
............................. check: py-zarr
............................ check: py-petsc4py
............................ check: py-easybuild-framework
............................. direct: py-easybuild-framework -> {'py-easybuild-easyblocks', 'easybuild', 'py-easybuild-easyconfigs'}
............................. check: py-easybuild-easyblocks
.............................. direct: py-easybuild-easyblocks -> {'py-easybuild-easyconfigs', 'easybuild'}
.............................. check: py-easybuild-easyconfigs
............................... direct: py-easybuild-easyconfigs -> {'easybuild'}
............................... check: easybuild
................................ direct: easybuild -> set()
.............................. check: easybuild
............................. check: easybuild
............................. check: py-easybuild-easyconfigs
............................ check: py-virtualenv-clone
............................. direct: py-virtualenv-clone -> {'py-virtualenvwrapper'}
............................. check: py-virtualenvwrapper
............................ check: py-attrs
............................ check: py-cutadapt
............................ check: py-pkgconfig
............................ check: py-versioneer
............................. direct: py-versioneer -> set()
............................ check: py-google-cloud-storage
............................ check: py-auxlib
............................ check: py-mako
............................ check: py-pispino
............................. direct: py-pispino -> {'py-pipits'}
............................. check: py-pipits
............................ check: py-google-auth-oauthlib
............................ check: py-neotime
............................ check: py-pickleshare
............................ check: py-apptools
............................ check: py-plotly
............................ check: py-urwid
............................. direct: py-urwid -> {'py-pudb'}
............................. check: py-pudb
............................ check: py-typing-extensions
............................. direct: py-typing-extensions -> {'py-onnx', 'py-chainer', 'py-mypy'}
............................. check: py-onnx
............................. check: py-chainer
............................. check: py-mypy
............................ check: py-whichcraft
............................. direct: py-whichcraft -> {'py-cookiecutter'}
............................. check: py-cookiecutter
............................ check: py-umi-tools
............................ check: ont-albacore
............................ check: py-jupyter-client
............................ check: py-progress
............................. direct: py-progress -> set()
............................ check: py-gee-asset-manager
............................ check: py-youtube-dl
............................ check: py-funcsigs
............................ check: py-googleapis-common-protos
............................ check: py-projectq
............................ check: py-pykml
............................ check: py-prometheus-client
............................ check: py-resampy
............................ check: py-blessings
............................. direct: py-blessings -> set()
............................ check: py-cheroot
............................ check: py-deeptoolsintervals
............................. direct: py-deeptoolsintervals -> {'py-deeptools'}
............................. check: py-deeptools
............................ check: py-periodictable
............................ check: py-hatchet
............................ check: py-pycbc
............................ check: py-toolz
............................. direct: py-toolz -> {'py-partd', 'py-distributed', 'py-dask'}
............................. check: py-partd
.............................. direct: py-partd -> {'py-dask'}
.............................. check: py-dask
............................. check: py-distributed
............................. check: py-dask
............................ check: py-gdc-client
............................. direct: py-gdc-client -> set()
............................ check: py-pyfftw
............................ check: py-qtawesome
............................ check: py-setuptools-scm
............................. direct: py-setuptools-scm -> {'py-hpcbench', 'py-python-dateutil', 'py-jsonschema', 'py-pytest-forked', 'py-tempora', 'py-py', 'py-pytest-mock', 'py-cherrypy', 'py-pint', 'py-setuptools-scm-git-archive', 'py-apipkg', 'py-pyarrow', 'py-pluggy', 'py-zipp', 'py-pytest', 'py-cheroot', 'py-execnet', 'py-backports-functools-lru-cache', 'py-importlib-metadata', 'py-asdf', 'py-numcodecs', 'py-portend', 'py-pytest-runner', 'py-jaraco-functools', 'py-spatialist', 'py-zarr'}
............................. check: py-hpcbench
............................. check: py-python-dateutil
.............................. direct: py-python-dateutil -> {'py-pykwalify', 'py-freezegun', 'py-bokeh', 'py-dateparser', 'py-owslib', 'ont-albacore', 'py-openpmd-validator', 'py-s3cmd', 'py-alembic', 'py-csvkit', 'py-jupyter-client', 'py-pydv', 'py-luigi', 'py-botocore', 'py-matplotlib', 'py-ics', 'py-pandas', 'py-jupyterhub', 'py-arrow'}
.............................. check: py-pykwalify
.............................. check: py-freezegun
.............................. check: py-bokeh
.............................. check: py-dateparser
.............................. check: py-owslib
.............................. check: ont-albacore
.............................. check: py-openpmd-validator
.............................. check: py-s3cmd
.............................. check: py-alembic
.............................. check: py-csvkit
.............................. check: py-jupyter-client
.............................. check: py-pydv
.............................. check: py-luigi
.............................. check: py-botocore
.............................. check: py-matplotlib
.............................. check: py-ics
.............................. check: py-pandas
.............................. check: py-jupyterhub
.............................. check: py-arrow
............................. check: py-jsonschema
............................. check: py-pytest-forked
............................. check: py-tempora
............................. check: py-py
.............................. direct: py-py -> {'py-cffi', 'py-pyzmq', 'py-tox', 'py-pytest', 'py-apipkg', 'py-pycmd'}
.............................. check: py-cffi
.............................. check: py-pyzmq
.............................. check: py-tox
.............................. check: py-pytest
.............................. check: py-apipkg
.............................. check: py-pycmd
............................... direct: py-pycmd -> {'py-wub'}
............................... check: py-wub
............................. check: py-pytest-mock
............................. check: py-cherrypy
............................. check: py-pint
............................. check: py-setuptools-scm-git-archive
............................. check: py-apipkg
............................. check: py-pyarrow
............................. check: py-pluggy
............................. check: py-zipp
............................. check: py-pytest
............................. check: py-cheroot
............................. check: py-execnet
............................. check: py-backports-functools-lru-cache
.............................. direct: py-backports-functools-lru-cache -> {'py-isort', 'py-arrow', 'py-cheroot', 'py-astroid', 'py-soupsieve', 'py-jaraco-functools', 'py-matplotlib', 'py-pylint', 'py-pydv', 'py-checkm-genome'}
.............................. check: py-isort
.............................. check: py-arrow
.............................. check: py-cheroot
.............................. check: py-astroid
.............................. check: py-soupsieve
............................... direct: py-soupsieve -> {'py-beautifulsoup4'}
............................... check: py-beautifulsoup4
................................ direct: py-beautifulsoup4 -> {'py-geeadd', 'py-astropy', 'py-geeup', 'py-lxml', 'py-pycbc', 'py-gee-asset-manager'}
................................ check: py-geeadd
................................ check: py-astropy
................................ check: py-geeup
................................ check: py-lxml
................................ check: py-pycbc
................................ check: py-gee-asset-manager
.............................. check: py-jaraco-functools
............................... direct: py-jaraco-functools -> {'py-tempora'}
............................... check: py-tempora
.............................. check: py-matplotlib
.............................. check: py-pylint
.............................. check: py-pydv
.............................. check: py-checkm-genome
............................. check: py-importlib-metadata
............................. check: py-asdf
............................. check: py-numcodecs
............................. check: py-portend
............................. check: py-pytest-runner
.............................. direct: py-pytest-runner -> {'py-fiscalyear', 'py-pyjwt', 'py-cf-units', 'py-chardet', 'py-spdlog', 'py-dask', 'py-audioread', 'py-overpy', 'py-python-utils', 'py-pillow', 'py-pyinstrument', 'py-cairocffi'}
.............................. check: py-fiscalyear
.............................. check: py-pyjwt
.............................. check: py-cf-units
.............................. check: py-chardet
.............................. check: py-spdlog
.............................. check: py-dask
.............................. check: py-audioread
.............................. check: py-overpy
.............................. check: py-python-utils
............................... direct: py-python-utils -> {'py-progressbar2'}
............................... check: py-progressbar2
.............................. check: py-pillow
.............................. check: py-pyinstrument
.............................. check: py-cairocffi
............................. check: py-jaraco-functools
............................. check: py-spatialist
............................. check: py-zarr
............................ check: py-testscenarios
............................. direct: py-testscenarios -> {'py-pbr'}
............................. check: py-pbr
............................ check: py-extras
............................. direct: py-extras -> {'py-python-subunit'}
............................. check: py-python-subunit
............................ check: py-tatsu
............................ check: py-patsy
............................ check: py-ansible
............................ check: py-irpf90
............................. direct: py-irpf90 -> set()
............................ check: py-localcider
............................ check: py-parameterized
............................. direct: py-parameterized -> {'py-dateparser', 'py-theano'}
............................. check: py-dateparser
............................. check: py-theano
............................ check: py-google-api-core
............................ check: py-httpbin
............................ check: py-pycifrw
............................. direct: py-pycifrw -> set()
............................ check: py-yajl
............................. direct: py-yajl -> set()
............................ check: py-pox
............................. direct: py-pox -> {'py-pathos'}
............................. check: py-pathos
............................ check: py-mysql-connector-python
............................ check: py-xattr
............................. direct: py-xattr -> set()
............................ check: py-heapdict
............................. direct: py-heapdict -> {'py-zict'}
............................. check: py-zict
.............................. direct: py-zict -> {'py-distributed'}
.............................. check: py-distributed
............................ check: py-uwsgi
............................. direct: py-uwsgi -> set()
............................ check: py-agate-sql
............................ check: py-pip
............................. direct: py-pip -> {'py-illumina-utils', 'ont-albacore', 'ascent', 'py-mypy'}
............................. check: py-illumina-utils
............................. check: ont-albacore
............................. check: ascent
............................. check: py-mypy
............................ check: py-pyside2
............................ check: py-crossmap
............................ check: py-symfit
............................ check: py-jprops
............................. direct: py-jprops -> set()
............................ check: py-git-review
............................ check: py-rsa
............................. direct: py-rsa -> {'awscli', 'py-oauth2client', 'py-google-auth'}
............................. check: awscli
............................. check: py-oauth2client
............................. check: py-google-auth
............................ check: py-click-plugins
............................ check: py-sphinxcontrib-issuetracker
............................ check: py-usgs
............................ check: py-keras
............................ check: py-jmespath
............................ check: py-elephant
............................ check: py-flask
............................ check: py-requests-toolbelt
............................ check: py-colormath
............................ check: py-backports-abc
............................. direct: py-backports-abc -> {'py-tornado'}
............................. check: py-tornado
............................ check: py-snowballstemmer
............................. direct: py-snowballstemmer -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-pymysql
............................ check: py-nbconvert
............................ check: py-lit
............................. direct: py-lit -> {'templight'}
............................. check: templight
............................ check: py-symengine
............................ check: py-cdat-lite
............................ check: py-ppft
............................. direct: py-ppft -> {'py-pathos'}
............................. check: py-pathos
............................ check: py-python-gitlab
............................ check: py-fn-py
............................. direct: py-fn-py -> {'py-opentuner'}
............................. check: py-opentuner
............................ check: py-pyasn1
............................. direct: py-pyasn1 -> {'py-oauth2client', 'py-python-ldap', 'py-pyasn1-modules', 'py-rsa', 'py-paramiko'}
............................. check: py-oauth2client
............................. check: py-python-ldap
............................. check: py-pyasn1-modules
............................. check: py-rsa
............................. check: py-paramiko
............................ check: py-us
............................. direct: py-us -> set()
............................ check: py-pyperclip
............................. direct: py-pyperclip -> {'py-clipboard'}
............................. check: py-clipboard
.............................. direct: py-clipboard -> {'py-geeadd'}
.............................. check: py-geeadd
............................ check: py-pygobject
............................ check: py-dgl
............................ check: py-wasabi
............................ check: py-binaryornot
............................ check: py-jellyfish
............................. direct: py-jellyfish -> {'py-us'}
............................. check: py-us
............................ check: py-clipboard
............................ check: py-d2to1
............................. direct: py-d2to1 -> {'py-stsci-distutils', 'py-pywcs'}
............................. check: py-stsci-distutils
.............................. direct: py-stsci-distutils -> {'py-pywcs'}
.............................. check: py-pywcs
............................. check: py-pywcs
............................ check: py-asdf
............................ check: py-pyprof2html
............................ check: py-cycler
............................. direct: py-cycler -> {'geopm', 'py-pydv', 'py-matplotlib'}
............................. check: geopm
............................. check: py-pydv
............................. check: py-matplotlib
............................ check: py-prettytable
............................. direct: py-prettytable -> {'py-mg-rast-tools', 'py-abipy'}
............................. check: py-mg-rast-tools
............................. check: py-abipy
............................ check: py-pyfits
............................. direct: py-pyfits -> {'py-pywcs'}
............................. check: py-pywcs
............................ check: py-pyglet
............................ check: py-pybtex
............................. direct: py-pybtex -> {'py-pybtex-docutils', 'py-sphinxcontrib-bibtex'}
............................. check: py-pybtex-docutils
.............................. direct: py-pybtex-docutils -> {'py-sphinxcontrib-bibtex'}
.............................. check: py-sphinxcontrib-bibtex
............................. check: py-sphinxcontrib-bibtex
............................ check: py-kmodes
............................ check: py-gevent
............................ check: py-jinja2-time
............................ check: py-pyopenssl
............................ check: py-xmlrunner
............................ check: py-pybind11
............................ check: py-astroid
............................ check: fenics
............................ check: py-defusedxml
............................. direct: py-defusedxml -> {'py-nbconvert'}
............................. check: py-nbconvert
............................ check: py-current
............................. direct: py-current -> set()
............................ check: py-pybigwig
............................ check: py-maestrowf
............................ check: py-magic
............................. direct: py-magic -> set()
............................ check: py-astropy-helpers
............................. direct: py-astropy-helpers -> {'py-fits-tools'}
............................. check: py-fits-tools
............................ check: py-latexcodec
............................. direct: py-latexcodec -> {'py-pybtex', 'py-sphinxcontrib-bibtex'}
............................. check: py-pybtex
............................. check: py-sphinxcontrib-bibtex
............................ check: py-virtualenv
............................. direct: py-virtualenv -> {'py-pre-commit', 'py-tox', 'py-virtualenvwrapper', 'py-pbr', 'py-jupyterlab', 'py-mypy'}
............................. check: py-pre-commit
............................. check: py-tox
............................. check: py-virtualenvwrapper
............................. check: py-pbr
............................. check: py-jupyterlab
............................. check: py-mypy
............................ check: py-pep8-naming
............................ check: py-jedi
............................ check: py-basis-set-exchange
............................ check: py-deeptools
............................ check: py-asserts
............................. direct: py-asserts -> {'py-htmlgen'}
............................. check: py-htmlgen
............................ check: py-lark-parser
............................. direct: py-lark-parser -> {'charliecloud'}
............................. check: charliecloud
............................ check: py-dxchange
............................ check: py-metasv
............................ check: py-cdsapi
............................ check: py-jupyter-core
............................ check: py-nose-cov
............................ check: py-neobolt
............................. direct: py-neobolt -> {'py-py2neo'}
............................. check: py-py2neo
............................ check: py-zxcvbn
............................. direct: py-zxcvbn -> set()
............................ check: py-apscheduler
............................ check: py-sphinxcontrib-jsmath
............................. direct: py-sphinxcontrib-jsmath -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-twine
............................ check: py-json5
............................. direct: py-json5 -> {'py-jupyterlab-server'}
............................. check: py-jupyterlab-server
............................ check: py-pyrosar
............................ check: py-avro-json-serializer
............................ check: py-fsspec
............................. direct: py-fsspec -> {'py-dask'}
............................. check: py-dask
............................ check: py-markdown
............................. direct: py-markdown -> {'py-multiqc', 'py-tensorboard'}
............................. check: py-multiqc
............................. check: py-tensorboard
............................ check: py-pyscaf
............................. direct: py-pyscaf -> {'redundans'}
............................. check: redundans
.............................. direct: redundans -> set()
............................ check: py-h5sh
............................ check: py-spacy-models-en-core-web-sm
............................ check: py-hepdata-validator
............................ check: py-raven
............................ check: py-opt-einsum
............................ check: py-keras-preprocessing
............................. direct: py-keras-preprocessing -> {'py-keras', 'py-tensorflow'}
............................. check: py-keras
............................. check: py-tensorflow
............................ check: py-scp
............................ check: py-traits
............................. direct: py-traits -> {'py-envisage', 'py-traitsui', 'py-pyface', 'py-mayavi'}
............................. check: py-envisage
............................. check: py-traitsui
............................. check: py-pyface
............................. check: py-mayavi
............................ check: py-cartopy
............................ check: py-methylcode
............................ check: py-storm
............................. direct: py-storm -> set()
............................ check: py-requests-futures
............................ check: py-spdlog
............................ check: py-unicycler
............................ check: py-pyodbc
............................. direct: py-pyodbc -> set()
............................ check: kitty
............................ check: py-tempora
............................ check: py-py
............................ check: py-setuptools-git
............................ check: py-ics
............................ check: py-pytest-httpbin
............................ check: py-monty
............................. direct: py-monty -> {'py-pymatgen'}
............................. check: py-pymatgen
............................ check: py-arrow
............................ check: py-alabaster
............................ check: py-backports-shutil-get-terminal-size
............................. direct: py-backports-shutil-get-terminal-size -> {'py-ipython'}
............................. check: py-ipython
............................ check: py-pytimeparse
............................. direct: py-pytimeparse -> {'py-agate'}
............................. check: py-agate
............................ check: py-bleach
............................ check: py-apipkg
............................ check: py-natsort
............................. direct: py-natsort -> {'geopm'}
............................. check: geopm
............................ check: py-sh
............................. direct: py-sh -> set()
............................ check: py-oauthlib
............................ check: py-notebook
............................ check: py-jupyterhub
............................ check: py-blessed
............................. direct: py-blessed -> set()
............................ check: py-partd
............................ check: py-slepc4py
............................ check: py-onnx
............................ check: mercurial
............................ check: py-py-cpuinfo
............................. direct: py-py-cpuinfo -> set()
............................ check: py-illumina-utils
............................ check: py-pycparser
............................. direct: py-pycparser -> {'py-cffi'}
............................. check: py-cffi
............................ check: py-logilab-common
............................ check: py-redis
............................. direct: py-redis -> {'py-celery'}
............................. check: py-celery
............................ check: py-yt
............................ check: py-sortedcontainers
............................. direct: py-sortedcontainers -> {'py-intervaltree', 'py-astropy', 'py-hypothesis', 'py-distributed'}
............................. check: py-intervaltree
............................. check: py-astropy
............................. check: py-hypothesis
............................. check: py-distributed
............................ check: py-lxml
............................ check: py-mypy
............................ check: py-atomicwrites
............................. direct: py-atomicwrites -> {'py-pytest'}
............................. check: py-pytest
............................ check: py-mechanize
............................ check: py-fparser
............................ check: py-pep8
............................. direct: py-pep8 -> {'py-pytest-pep8'}
............................. check: py-pytest-pep8
............................ check: py-llvmlite
............................ check: py-freezegun
............................ check: py-pathspec
............................. direct: py-pathspec -> set()
............................ check: py-setuptools-rust
............................ check: py-dill
............................. direct: py-dill -> {'py-ppft', 'py-multiprocess', 'py-dask', 'py-petastorm', 'py-pathos'}
............................. check: py-ppft
............................. check: py-multiprocess
............................. check: py-dask
............................. check: py-petastorm
............................. check: py-pathos
............................ check: py-xdot
............................ check: py-syned
............................ check: py-stsci-distutils
............................ check: py-librosa
............................ check: lbann
............................ check: py-ecos
............................ check: py-poster
............................. direct: py-poster -> {'py-geeadd', 'py-mg-rast-tools'}
............................. check: py-geeadd
............................. check: py-mg-rast-tools
............................ check: py-pyinstrument
............................ check: py-zarr
............................ check: py-vcf-kit
............................ check: py-pypeflow
............................ check: py-bintrees
............................. direct: py-bintrees -> {'py-astropy'}
............................. check: py-astropy
............................ check: py-python-igraph
............................. direct: py-python-igraph -> {'transabyss'}
............................. check: transabyss
............................ check: py-pymeeus
............................. direct: py-pymeeus -> {'py-convertdate'}
............................. check: py-convertdate
............................ check: py-werkzeug
............................ check: py-pymatgen
............................ check: py-typing
............................. direct: py-typing -> {'py-onnx', 'py-chainer', 'py-mypy-extensions', 'py-htmlgen', 'py-typing-extensions', 'py-importlib-resources', 'py-sphinx', 'py-flake8', 'py-torch'}
............................. check: py-onnx
............................. check: py-chainer
............................. check: py-mypy-extensions
............................. check: py-htmlgen
............................. check: py-typing-extensions
............................. check: py-importlib-resources
............................. check: py-sphinx
............................. check: py-flake8
............................. check: py-torch
............................ check: py-plac
............................ check: py-macholib
............................. direct: py-macholib -> {'py-machotools'}
............................. check: py-machotools
.............................. direct: py-machotools -> set()
............................ check: py-h5py
............................ check: py-tetoolkit
............................ check: py-altgraph
............................. direct: py-altgraph -> {'py-macholib'}
............................. check: py-macholib
............................ check: py-amqp
............................. direct: py-amqp -> {'py-kombu'}
............................. check: py-kombu
............................ check: py-geeup
............................ check: py-requests-mock
............................. direct: py-requests-mock -> {'py-requests-oauthlib'}
............................. check: py-requests-oauthlib
............................ check: py-jdcal
............................. direct: py-jdcal -> {'py-openpyxl'}
............................. check: py-openpyxl
............................ check: py-soundfile
............................ check: py-spefile
............................ check: py-radical-utils
............................ check: py-send2trash
............................. direct: py-send2trash -> {'py-notebook'}
............................. check: py-notebook
............................ check: py-spykeutils
............................ check: py-utils
............................. direct: py-utils -> set()
............................ check: py-macs2
............................ check: py-geoalchemy2
............................ check: py-guiqwt
............................ check: py-py4j
............................ check: scons
............................. direct: scons -> {'metabat', 'cantera', 'kahip', 'sgpp', 'serf'}
............................. check: metabat
.............................. direct: metabat -> set()
............................. check: cantera
............................. check: kahip
............................. check: sgpp
............................. check: serf
.............................. direct: serf -> {'subversion'}
.............................. check: subversion
............................ check: py-imagesize
............................. direct: py-imagesize -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-pathos
............................ check: py-machotools
............................ check: py-weave
............................ check: py-docutils-stubs
............................. direct: py-docutils-stubs -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-tabulate
............................. direct: py-tabulate -> {'aws-parallelcluster', 'py-merlin', 'py-vcf-kit', 'py-abipy', 'py-maestrowf', 'py-pymatgen'}
............................. check: aws-parallelcluster
............................. check: py-merlin
............................. check: py-vcf-kit
............................. check: py-abipy
............................. check: py-maestrowf
............................. check: py-pymatgen
............................ check: py-graphviz
............................ check: py-flask-compress
............................ check: py-jupyterlab
............................ check: py-xenv
............................. direct: py-xenv -> {'gaudi'}
............................. check: gaudi
............................ check: py-emcee
............................ check: py-python-utils
............................ check: py-spatialist
............................ check: py-sphinxcontrib-websupport
............................. direct: py-sphinxcontrib-websupport -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-tensorboard-plugin-wit
............................ check: py-cinema-lib
............................ check: py-cffi
............................ check: py-seaborn
............................ check: py-mpi4py
............................ check: py-oauth2client
............................ check: py-pyelftools
............................. direct: py-pyelftools -> {'flit'}
............................. check: flit
.............................. direct: flit -> set()
............................ check: py-importlib-resources
............................ check: py-wradlib
............................ check: py-idna
............................. direct: py-idna -> {'py-requests', 'py-urllib3', 'py-cryptography'}
............................. check: py-requests
............................. check: py-urllib3
............................. check: py-cryptography
............................ check: py-ujson
............................. direct: py-ujson -> set()
............................ check: py-apache-libcloud
............................. direct: py-apache-libcloud -> {'py-saga-python'}
............................. check: py-saga-python
............................ check: py-scikit-build
............................ check: py-svgpathtools
............................ check: py-testtools
............................. direct: py-testtools -> {'py-pbr', 'py-testrepository', 'py-python-subunit', 'py-traceback2'}
............................. check: py-pbr
............................. check: py-testrepository
............................. check: py-python-subunit
............................. check: py-traceback2
............................ check: py-psyclone
............................ check: py-preshed
............................ check: py-rope
............................. direct: py-rope -> {'py-spyder'}
............................. check: py-spyder
............................ check: py-s3transfer
............................ check: py-mayavi
............................ check: py-spglib
............................ check: py-sphinxcontrib-serializinghtml
............................. direct: py-sphinxcontrib-serializinghtml -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-louie
............................. direct: py-louie -> set()
............................ check: py-toml
............................. direct: py-toml -> {'flit', 'py-pre-commit', 'py-black', 'py-tox', 'py-setuptools-rust'}
............................. check: flit
............................. check: py-pre-commit
............................. check: py-black
............................. check: py-tox
............................. check: py-setuptools-rust
............................ check: py-python-mapnik
............................ check: py-numexpr3
............................ check: py-pomegranate
............................ check: py-python-levenshtein
............................. direct: py-python-levenshtein -> {'py-illumina-utils'}
............................. check: py-illumina-utils
............................ check: py-virtualenvwrapper
............................ check: py-asgiref
............................. direct: py-asgiref -> {'py-django'}
............................. check: py-django
............................ check: py-abipy
............................ check: py-descartes
............................ check: sumo
............................ check: py-pycuda
............................ check: py-clustershell
............................. direct: py-clustershell -> {'py-hpcbench'}
............................. check: py-hpcbench
............................ check: py-kombu
............................ check: py-umalqurra
............................. direct: py-umalqurra -> {'py-dateparser'}
............................. check: py-dateparser
............................ check: py-srsly
............................ check: py-shroud
............................. direct: py-shroud -> {'axom'}
............................. check: axom
............................ check: py-rasterio
............................ check: py-itsdangerous
............................. direct: py-itsdangerous -> {'py-flask', 'py-httpbin'}
............................. check: py-flask
............................. check: py-httpbin
............................ check: py-ratelim
............................ check: py-binwalk
............................. direct: py-binwalk -> {'rose'}
............................. check: rose
............................ check: sgpp
............................ check: py-traceback2
............................ check: py-gensim
............................ check: py-lmfit
............................ check: geopm
............................ check: py-pathlib2
............................ check: py-readme-renderer
............................ check: py-tomopy
............................ check: py-path-py
............................. direct: py-path-py -> set()
............................ check: py-sqlparse
............................. direct: py-sqlparse -> {'py-django'}
............................. check: py-django
............................ check: py-fypp
............................. direct: py-fypp -> {'dbcsr'}
............................. check: dbcsr
............................ check: gaudi
............................ check: py-filemagic
............................ check: py-jaraco-functools
............................ check: py-boltons
............................. direct: py-boltons -> set()
............................ check: py-fits-tools
............................ check: py-affine
............................. direct: py-affine -> {'py-rasterio'}
............................. check: py-rasterio
............................ check: py-torch-nvidia-apex
............................ check: py-sfepy
............................ check: py-py2bit
............................. direct: py-py2bit -> {'py-deeptools'}
............................. check: py-deeptools
............................ check: py-widgetsnbextension
............................ check: py-hpcbench
............................ check: py-xopen
............................. direct: py-xopen -> {'py-cutadapt', 'py-dnaio', 'py-whatshap'}
............................. check: py-cutadapt
............................. check: py-dnaio
............................. check: py-whatshap
............................ check: py-pyjwt
............................ check: kmergenie
............................ check: py-cookiecutter
............................ check: py-coverage
............................. direct: py-coverage -> {'py-nose2', 'py-cov-core', 'py-dateparser', 'py-nosexcover', 'py-pkginfo', 'py-pbr', 'py-codecov', 'py-tqdm', 'py-zope-interface', 'py-attrs', 'py-pytest-cov'}
............................. check: py-nose2
............................. check: py-cov-core
.............................. direct: py-cov-core -> {'py-nose2', 'py-nose-cov'}
.............................. check: py-nose2
.............................. check: py-nose-cov
............................. check: py-dateparser
............................. check: py-nosexcover
............................. check: py-pkginfo
............................. check: py-pbr
............................. check: py-codecov
............................. check: py-tqdm
............................. check: py-zope-interface
............................. check: py-attrs
............................. check: py-pytest-cov
............................ check: py-xlwt
............................. direct: py-xlwt -> set()
............................ check: py-scikit-image
............................ check: py-editdistance
............................. direct: py-editdistance -> {'py-wub', 'py-pylint'}
............................. check: py-wub
............................. check: py-pylint
............................ check: py-pyepsg
............................ check: py-configparser
............................. direct: py-configparser -> {'aws-parallelcluster', 'py-entrypoints', 'py-importlib-metadata', 'lbann', 'py-flake8', 'py-pylint'}
............................. check: aws-parallelcluster
............................. check: py-entrypoints
.............................. direct: py-entrypoints -> {'py-nbconvert', 'py-jupyterhub', 'py-flake8'}
.............................. check: py-nbconvert
.............................. check: py-jupyterhub
.............................. check: py-flake8
............................. check: py-importlib-metadata
............................. check: lbann
............................. check: py-flake8
............................. check: py-pylint
............................ check: py-saga-python
............................ check: py-bcbio-gff
............................ check: py-spacy
............................ check: py-zc-lockfile
............................. direct: py-zc-lockfile -> {'py-cherrypy'}
............................. check: py-cherrypy
............................ check: py-cloudpickle
............................. direct: py-cloudpickle -> {'py-distributed', 'py-dask', 'py-horovod', 'py-scikit-image'}
............................. check: py-distributed
............................. check: py-dask
............................. check: py-horovod
............................. check: py-scikit-image
............................ check: py-advancedhtmlparser
............................. direct: py-advancedhtmlparser -> {'py-biomine'}
............................. check: py-biomine
............................ check: py-celery
............................ check: py-pytest-xdist
............................ check: py-async-generator
............................. direct: py-async-generator -> {'py-jupyterhub'}
............................. check: py-jupyterhub
............................ check: py-pluggy
............................ check: py-zope-event
............................. direct: py-zope-event -> {'py-zope-interface'}
............................. check: py-zope-interface
............................ check: py-asteval
............................ check: py-catalogue
............................ check: py-sncosmo
............................ check: py-flake8-import-order
............................ check: py-owslib
............................ check: py-cfgv
............................. direct: py-cfgv -> {'py-pre-commit'}
............................. check: py-pre-commit
............................ check: py-asciitree
............................. direct: py-asciitree -> {'py-zarr'}
............................. check: py-zarr
............................ check: py-python-rapidjson
............................. direct: py-python-rapidjson -> set()
............................ check: py-horovod
............................ check: py-pydeps
............................ check: py-mccabe
............................ check: py-chai
............................. direct: py-chai -> {'py-arrow'}
............................. check: py-arrow
............................ check: py-docutils
............................. direct: py-docutils -> {'py-pynn', 'mercurial', 'py-docutils-stubs', 'py-readme-renderer', 'opensubdiv', 'py-m2r', 'py-sphinx', 'py-botocore', 'py-pybtex-docutils', 'awscli', 'py-breathe', 'py-restview'}
............................. check: py-pynn
............................. check: mercurial
............................. check: py-docutils-stubs
............................. check: py-readme-renderer
............................. check: opensubdiv
............................. check: py-m2r
.............................. direct: py-m2r -> {'lbann'}
.............................. check: lbann
............................. check: py-sphinx
............................. check: py-botocore
............................. check: py-pybtex-docutils
............................. check: awscli
............................. check: py-breathe
............................. check: py-restview
............................ check: py-mlxtend
............................ check: py-pydotplus
............................ check: py-pygresql
............................. direct: py-pygresql -> set()
............................ check: py-absl-py
............................ check: py-isodate
............................. direct: py-isodate -> {'py-agate'}
............................. check: py-agate
............................ check: py-pythonqwt
............................. direct: py-pythonqwt -> {'py-guiqwt'}
............................. check: py-guiqwt
............................ check: py-vsc-install
............................. direct: py-vsc-install -> {'py-easybuild-framework'}
............................. check: py-easybuild-framework
............................ check: py-testrepository
............................ check: py-earthengine-api
............................ check: py-lscsoft-glue
............................ check: py-portend
............................ check: py-rnacocktail
............................ check: py-cachetools
............................. direct: py-cachetools -> {'py-google-auth'}
............................. check: py-google-auth
............................ check: py-cogent
............................ check: py-basemap
............................ check: py-torchvision
............................ check: py-hypothesis
............................ check: py-awesome-slugify
............................ check: py-pywcs
............................ check: py-tables
............................ check: py-multiqc
............................ check: py-pyside
............................ check: py-junit-xml
............................. direct: py-junit-xml -> set()
............................ check: py-ecdsa
............................. direct: py-ecdsa -> set()
............................ check: py-csvkit
............................ check: py-python-jenkins
............................ check: py-queryablelist
............................. direct: py-queryablelist -> {'py-advancedhtmlparser'}
............................. check: py-advancedhtmlparser
............................ check: py-soupsieve
............................ check: py-setproctitle
............................. direct: py-setproctitle -> set()
............................ check: py-pykwalify
............................ check: py-astropy
............................ check: py-parsedatetime
............................. direct: py-parsedatetime -> {'py-agate'}
............................. check: py-agate
............................ check: py-gdbgui
............................ check: py-numpydoc
............................. direct: py-numpydoc -> {'py-deeptools', 'py-elephant', 'py-spyder'}
............................. check: py-deeptools
............................. check: py-elephant
............................. check: py-spyder
............................ check: py-requests-oauthlib
............................ check: py-parso
............................ check: py-tqdm
............................ check: py-atropos
............................ check: py-regex
............................. direct: py-regex -> {'py-dateparser', 'py-umi-tools', 'py-transformers', 'py-nltk', 'py-awesome-slugify', 'py-sacremoses', 'py-tatsu'}
............................. check: py-dateparser
............................. check: py-umi-tools
............................. check: py-transformers
............................. check: py-nltk
............................. check: py-awesome-slugify
............................. check: py-sacremoses
............................. check: py-tatsu
............................ check: py-fastaindex
............................. direct: py-fastaindex -> {'redundans', 'py-pyscaf'}
............................. check: redundans
............................. check: py-pyscaf
............................ check: py-pymorph
............................. direct: py-pymorph -> set()
............................ check: py-html2text
............................. direct: py-html2text -> {'py-abipy'}
............................. check: py-abipy
............................ check: py-numpy
............................ check: py-jpype1
............................ check: py-pycmd
............................ check: py-py2neo
............................ check: py-futures
............................. direct: py-futures -> {'py-pre-commit', 'py-s3transfer', 'py-tensorboard', 'py-bokeh', 'py-rasterio', 'py-distributed', 'py-isort', 'py-pyarrow', 'py-requests-futures', 'py-tornado', 'py-grpcio', 'py-google-api-core', 'py-cnvkit', 'py-petastorm', 'py-python-swiftclient'}
............................. check: py-pre-commit
............................. check: py-s3transfer
............................. check: py-tensorboard
............................. check: py-bokeh
............................. check: py-rasterio
............................. check: py-distributed
............................. check: py-isort
............................. check: py-pyarrow
............................. check: py-requests-futures
............................. check: py-tornado
............................. check: py-grpcio
............................. check: py-google-api-core
............................. check: py-cnvkit
............................. check: py-petastorm
............................. check: py-python-swiftclient
............................ check: py-bcrypt
............................ check: py-edffile
............................ check: py-sonlib
............................. direct: py-sonlib -> {'hal'}
............................. check: hal
............................ check: py-traitsui
............................ check: py-dataclasses
............................. direct: py-dataclasses -> {'py-transformers', 'py-tatsu'}
............................. check: py-transformers
............................. check: py-tatsu
............................ check: py-mock
............................ check: py-clint
............................ check: py-pygdal
............................ check: py-pbr
............................ check: py-pid
............................. direct: py-pid -> {'py-coilmq'}
............................. check: py-coilmq
............................ check: py-python-ldap
............................ check: py-pillow
............................ check: py-stdlib-list
............................. direct: py-stdlib-list -> {'py-pydeps'}
............................. check: py-pydeps
............................ check: py-vcversioner
............................. direct: py-vcversioner -> {'py-jsonschema'}
............................. check: py-jsonschema
............................ check: py-pytest-mypy
............................ check: py-wcwidth
............................. direct: py-wcwidth -> {'py-pytest', 'py-prompt-toolkit', 'py-blessed'}
............................. check: py-pytest
............................. check: py-prompt-toolkit
............................. check: py-blessed
............................ check: py-babel
............................ check: py-cymem
............................ check: py-dask
............................ check: py-humanfriendly
............................. direct: py-humanfriendly -> {'py-coloredlogs'}
............................. check: py-coloredlogs
.............................. direct: py-coloredlogs -> {'py-cyvcf2', 'py-merlin'}
.............................. check: py-cyvcf2
.............................. check: py-merlin
............................ check: py-joblib
............................. direct: py-joblib -> {'py-kmodes', 'py-pomegranate', 'py-scikit-learn', 'py-sacremoses', 'py-librosa'}
............................. check: py-kmodes
............................. check: py-pomegranate
............................. check: py-scikit-learn
............................. check: py-sacremoses
............................. check: py-librosa
............................ check: py-pylint
............................ check: nghttp2
............................ check: py-dbfread
............................. direct: py-dbfread -> {'py-agate-dbf'}
............................. check: py-agate-dbf
............................ check: py-pyserial
............................. direct: py-pyserial -> set()
............................ check: py-qtpy
............................ check: snakemake
............................ check: py-scs
............................ check: py-guidata
............................ check: py-profilehooks
............................. direct: py-profilehooks -> set()
............................ check: py-luigi
............................ check: py-bitarray
............................. direct: py-bitarray -> set()
............................ check: py-coloredlogs
............................ check: py-pyfasta
............................ check: py-mistune
............................. direct: py-mistune -> {'py-nbconvert', 'py-m2r'}
............................. check: py-nbconvert
............................. check: py-m2r
............................ check: py-biom-format
............................ check: py-pre-commit
............................ check: py-cov-core
............................ check: py-moltemplate
............................. direct: py-moltemplate -> set()
............................ check: py-sphinxcontrib-qthelp
............................. direct: py-sphinxcontrib-qthelp -> {'py-sphinx'}
............................. check: py-sphinx
............................ check: py-tblib
............................. direct: py-tblib -> {'py-distributed', 'py-spatialist'}
............................. check: py-distributed
............................. check: py-spatialist
............................ check: py-backports-functools-lru-cache
............................ check: py-pyproj
............................ check: py-transformers
............................ check: py-torchsummary
............................ check: py-monotonic
............................. direct: py-monotonic -> {'py-humanfriendly', 'py-fasteners'}
............................. check: py-humanfriendly
............................. check: py-fasteners
............................ check: py-aenum
............................. direct: py-aenum -> {'py-pyproj'}
............................. check: py-pyproj
............................ check: py-alembic
............................ check: py-pytest-runner
............................ check: py-geopandas
............................ check: py-svgwrite
............................. direct: py-svgwrite -> {'py-svgpathtools'}
............................. check: py-svgpathtools
............................ check: py-uritemplate
............................. direct: py-uritemplate -> {'py-google-api-python-client'}
............................. check: py-google-api-python-client
............................ check: py-agate-excel
............................ check: py-django
............................ check: py-portalocker
............................. direct: py-portalocker -> {'py-gluoncv'}
............................. check: py-gluoncv
............................ check: py-chainer
............................ check: py-google-resumable-media
............................. direct: py-google-resumable-media -> {'py-google-cloud-storage'}
............................. check: py-google-cloud-storage
............................ check: py-jsonschema
............................ check: py-semantic-version
............................. direct: py-semantic-version -> {'py-asdf', 'py-setuptools-rust'}
............................. check: py-asdf
............................. check: py-setuptools-rust
............................ check: py-autopep8
............................ check: py-pyflakes
............................. direct: py-pyflakes -> {'py-spyder', 'py-flake8'}
............................. check: py-spyder
............................. check: py-flake8
............................ check: py-python-engineio
............................. direct: py-python-engineio -> {'py-python-socketio'}
............................. check: py-python-socketio
............................ check: py-counter
............................. direct: py-counter -> {'py-pybtex'}
............................. check: py-pybtex
............................ check: py-args
............................ check: py-cherrypy
............................ check: py-google-auth-httplib2
............................ check: py-python-slugify
............................ check: py-pybedtools
............................ check: py-lazy-object-proxy
............................. direct: py-lazy-object-proxy -> {'py-astroid'}
............................. check: py-astroid
............................ check: py-antlr4-python3-runtime
............................. direct: py-antlr4-python3-runtime -> {'py-cf-units'}
............................. check: py-cf-units
............................ check: py-cached-property
............................. direct: py-cached-property -> {'py-hpcbench', 'py-h5py', 'py-merlin'}
............................. check: py-hpcbench
............................. check: py-h5py
............................. check: py-merlin
............................ check: py-libensemble
............................ check: py-cyvcf2
............................ check: py-python-editor
............................. direct: py-python-editor -> {'py-alembic'}
............................. check: py-alembic
............................ check: py-crispresso
............................ check: py-pyugrid
............................ check: py-boto3
............................ check: py-genshi
............................. direct: py-genshi -> set()
............................ check: py-jsonpatch
............................ check: py-beautifulsoup4
............................ check: py-pybtex-docutils
............................ check: py-ruamel-yaml
............................. direct: py-ruamel-yaml -> {'py-pykwalify', 'py-dateparser'}
............................. check: py-pykwalify
............................. check: py-dateparser
............................ check: py-cairocffi
............................ check: py-whatshap
............................ check: py-iminuit
............................ check: py-tap-py
............................ check: py-linecache2
............................ check: py-protobuf
............................ check: py-tifffile
............................ check: py-pytest
............................ check: py-m2r
............................ check: meson
............................. direct: meson -> {'glib', 'libnotify', 'atk', 'julea', 'gdk-pixbuf', 'at-spi2-core', 'su2', 'libepoxy', 'at-spi2-atk'}
............................. check: glib
.............................. direct: glib -> {'libnotify', 'harfbuzz', 'workrave', 'libvips', 'sublime-text', 'libpeas', 'gmt', 'qt', 'cairo', 'gts', 'slurm', 'libexif', 'qemu', 'ufo-core', 'dbus', 'shared-mime-info', 'compiz', 'glibmm', 'exonerate', 'gtkplus', 'libsecret', 'gdk-pixbuf', 'pango', 'julea', 'py-pygobject', 'gtksourceview', 'json-glib', 'intel-gpu-tools', 'enchant', 'r', 'librsvg', 'atk', 'graphviz', 'audacious', 'openscenegraph', 'py-pygtk', 'poppler', 'exonerate-gff3', 'wireshark', 'gmime', 'gconf', 'gobject-introspection', 'at-spi2-core', 'libcroco', 'mc'}
.............................. check: libnotify
.............................. check: harfbuzz
.............................. check: workrave
.............................. check: libvips
.............................. check: sublime-text
.............................. check: libpeas
.............................. check: gmt
.............................. check: qt
.............................. check: cairo
.............................. check: gts
............................... direct: gts -> {'graphviz'}
............................... check: graphviz
.............................. check: slurm
.............................. check: libexif
............................... direct: libexif -> {'feh'}
............................... check: feh
.............................. check: qemu
............................... direct: qemu -> set()
.............................. check: ufo-core
.............................. check: dbus
............................... direct: dbus -> {'cube', 'kitty', 'qt', 'at-spi2-core'}
............................... check: cube
............................... check: kitty
............................... check: qt
............................... check: at-spi2-core
.............................. check: shared-mime-info
............................... direct: shared-mime-info -> {'gdk-pixbuf', 'gtkplus'}
............................... check: gdk-pixbuf
............................... check: gtkplus
.............................. check: compiz
............................... direct: compiz -> set()
.............................. check: glibmm
............................... direct: glibmm -> {'workrave', 'pangomm', 'gtkmm'}
............................... check: workrave
............................... check: pangomm
............................... check: gtkmm
.............................. check: exonerate
............................... direct: exonerate -> {'hybpiper', 'prank', 'maker'}
............................... check: hybpiper
............................... check: prank
............................... check: maker
.............................. check: gtkplus
.............................. check: libsecret
.............................. check: gdk-pixbuf
.............................. check: pango
.............................. check: julea
.............................. check: py-pygobject
.............................. check: gtksourceview
.............................. check: json-glib
............................... direct: json-glib -> {'ufo-core'}
............................... check: ufo-core
.............................. check: intel-gpu-tools
.............................. check: enchant
............................... direct: enchant -> set()
.............................. check: r
.............................. check: librsvg
.............................. check: atk
.............................. check: graphviz
.............................. check: audacious
............................... direct: audacious -> set()
.............................. check: openscenegraph
.............................. check: py-pygtk
.............................. check: poppler
.............................. check: exonerate-gff3
............................... direct: exonerate-gff3 -> set()
.............................. check: wireshark
.............................. check: gmime
............................... direct: gmime -> {'notmuch'}
............................... check: notmuch
................................ direct: notmuch -> set()
.............................. check: gconf
............................... direct: gconf -> {'grandr', 'compiz'}
............................... check: grandr
............................... check: compiz
.............................. check: gobject-introspection
.............................. check: at-spi2-core
.............................. check: libcroco
............................... direct: libcroco -> {'librsvg'}
............................... check: librsvg
.............................. check: mc
............................. check: libnotify
............................. check: atk
............................. check: julea
............................. check: gdk-pixbuf
............................. check: at-spi2-core
............................. check: su2
.............................. direct: su2 -> set()
............................. check: libepoxy
.............................. direct: libepoxy -> {'xorg-server', 'gtkplus'}
.............................. check: xorg-server
............................... direct: xorg-server -> {'rgb', 'xorg-gtest'}
............................... check: rgb
................................ direct: rgb -> set()
............................... check: xorg-gtest
................................ direct: xorg-gtest -> set()
.............................. check: gtkplus
............................. check: at-spi2-atk
............................ check: py-shiboken
............................. direct: py-shiboken -> set()
............................ check: py-unittest2
............................ check: py-spectra
............................ check: phyluce
............................ check: py-wheel
............................ check: py-zc-buildout
............................. direct: py-zc-buildout -> set()
............................ check: py-oset
............................. direct: py-oset -> {'py-sphinxcontrib-bibtex'}
............................. check: py-sphinxcontrib-bibtex
............................ check: py-pyeda
............................. direct: py-pyeda -> set()
............................ check: py-mg-rast-tools
............................ check: py-quast
............................ check: py-python-socketio
............................ check: py-weblogo
............................ check: py-tornado
............................ check: py-doxypypy
............................. direct: py-doxypypy -> set()
............................ check: py-keras-applications
............................. direct: py-keras-applications -> {'py-keras', 'py-tensorflow'}
............................. check: py-keras
............................. check: py-tensorflow
............................ check: py-ipaddress
............................. direct: py-ipaddress -> {'aws-parallelcluster', 'py-cryptography', 'py-urllib3', 'py-notebook', 'py-psutil'}
............................. check: aws-parallelcluster
............................. check: py-cryptography
............................. check: py-urllib3
............................. check: py-notebook
............................. check: py-psutil
............................ check: py-grequests
............................ check: py-pyminifier
............................. direct: py-pyminifier -> set()
............................ check: py-python-dateutil
............................ check: py-restview
............................ check: py-opppy
............................ check: py-sentencepiece
............................ check: py-deprecation
............................ check: py-murmurhash
............................ check: py-networkx
............................ check: py-pytest-mock
............................ check: py-pytest-cov
............................ check: py-pygdbmi
............................. direct: py-pygdbmi -> {'py-gdbgui'}
............................. check: py-gdbgui
............................ check: py-vine
............................. direct: py-vine -> {'py-amqp', 'py-celery'}
............................. check: py-amqp
............................. check: py-celery
............................ check: py-vsc-base
............................. direct: py-vsc-base -> {'py-easybuild-framework'}
............................. check: py-easybuild-framework
............................ check: py-exodus-bundler
............................. direct: py-exodus-bundler -> set()
............................ check: py-pudb
............................ check: py-pysmartdl
............................. direct: py-pysmartdl -> {'py-geeup'}
............................. check: py-geeup
............................ check: py-dxfile
............................ check: py-flexx
............................ check: py-pyarrow
............................ check: py-google-cloud-core
............................ check: py-nodeenv
............................. direct: py-nodeenv -> {'py-pre-commit'}
............................. check: py-pre-commit
............................ check: py-convertdate
............................ check: py-asn1crypto
............................. direct: py-asn1crypto -> {'py-cryptography'}
............................. check: py-cryptography
............................ check: py-blis
............................ check: py-shapely
............................ check: py-nose2
............................ check: py-fiona
............................ check: py-bx-python
............................ check: py-pydot2
............................. direct: py-pydot2 -> set()
............................ check: py-imageio
............................ check: py-more-itertools
............................. direct: py-more-itertools -> {'py-pytest', 'py-cheroot', 'py-jaraco-functools', 'py-cherrypy', 'py-zipp'}
............................. check: py-pytest
............................. check: py-cheroot
............................. check: py-jaraco-functools
............................. check: py-cherrypy
............................. check: py-zipp
............................ check: awscli
............................ check: py-identify
............................. direct: py-identify -> {'py-pre-commit'}
............................. check: py-pre-commit
............................ check: py-cf-units
............................ check: py-pyparsing
............................. direct: py-pyparsing -> {'py-psyclone', 'partitionfinder', 'py-pydotplus', 'py-pydot2', 'py-packaging', 'py-periodictable', 'py-svgwrite', 'py-goatools', 'py-matplotlib', 'py-snuggs', 'openmolcas', 'py-brian2', 'py-pydot'}
............................. check: py-psyclone
............................. check: partitionfinder
............................. check: py-pydotplus
............................. check: py-pydot2
............................. check: py-packaging
............................. check: py-periodictable
............................. check: py-svgwrite
............................. check: py-goatools
............................. check: py-matplotlib
............................. check: py-snuggs
............................. check: openmolcas
............................. check: py-brian2
............................. check: py-pydot
............................ check: py-brian2
............................ check: py-ont-fast5-api
............................ check: py-htseq
............................ check: py-python-magic
............................. direct: py-python-magic -> {'py-hpcbench', 'py-s3cmd'}
............................. check: py-hpcbench
............................. check: py-s3cmd
............................ check: py-flye
............................. direct: py-flye -> set()
............................ check: py-zict
............................ check: py-hpccm
............................ check: py-wub
............................ check: py-netcdf4
........................... check: py-arviz
........................... check: py-sphinx
........................... check: py-petastorm
........................... check: py-scikit-build
......................... check: py-pyinstrument
......................... check: py-python-utils
......................... check: py-wub
......................... check: py-rpy2
........................ check: py-symfit
........................ check: py-tensorflow-estimator
....................... check: py-traceback2
..................... check: py-symfit
..................... check: py-pytest-check-links
..................... check: py-python-swiftclient
..................... check: py-git-review
.................... check: py-sphinxautomodapi
.................... check: py-qtconsole
.................... check: gmt
.................... check: fenics
.................... check: kitty
.................... check: py-pyside2
.................... check: cmake
.................... check: py-breathe
.................... check: py-wand
.................... check: py-pyside
.................... check: py-sphinxcontrib-issuetracker
.................... check: charliecloud
.................... check: py-spyder
.................... check: py-sphinxcontrib-programoutput
.................... check: py-elephant
.................... check: vigra
.................... check: seqan
.................... check: axom
.................... check: fio
.................... check: flibcpp
.................... check: portcullis
.................... check: py-numpydoc
.................... check: py-shiboken
.................... check: ascent
.................... check: py-brian2
.................... check: py-pydotplus
.................... check: py-graphviz
.................... check: py-pythonqwt
.................... check: variorum
.................... check: conduit
.................... check: libnetworkit
................... check: py-pytest-mypy
.................. check: uriparser
.................. check: faodel
.................. check: ibmisc
................. check: sailfish
................. check: mstk
................. check: muparser
.................. direct: muparser -> {'dealii'}
.................. check: dealii
................. check: kvtree
................. check: nsimd
.................. direct: nsimd -> set()
................. check: pnmpi
................. check: kokkos
................. check: openpmd-api
................. check: clamr
................. check: asdf-cxx
................. check: votca-tools
................. check: conduit
................. check: spath
................. check: arpack-ng
................. check: openkim-models
................. check: gccxml
.................. direct: gccxml -> set()
................. check: miniqmc
................. check: gplates
................. check: diamond
.................. direct: diamond -> {'braker'}
.................. check: braker
................. check: asagi
................. check: superlu-dist
................. check: ghost
................. check: gl2ps
................. check: suite-sparse
................. check: pumi
................. check: ravel
................. check: butterflypack
................. check: aspcud
................. check: dakota
................. check: tfel
.................. direct: tfel -> {'mgis'}
.................. check: mgis
................. check: portage
................. check: branson
................. check: cmocka
.................. direct: cmocka -> set()
................. check: py-shiboken
................. check: flatbuffers
.................. direct: flatbuffers -> {'arrow'}
.................. check: arrow
................. check: hyphy
................. check: cxxopts
.................. direct: cxxopts -> set()
................. check: delphes
................. check: kcov
................. check: automaded
................. check: lighttpd
.................. direct: lighttpd -> {'trinotate'}
.................. check: trinotate
................. check: msgpack-c
................. check: typhon
................. check: netlib-scalapack
................. check: leveldb
................. check: nanopb
................. check: netlib-lapack
.................. direct: netlib-lapack -> {'elpa', 'hiop', 'bohrium', 'nektar', 'ceres-solver', 'gmt', 'casacore', 'cardioid', 'quinoa', 'hypre', 'strumpack', 'xhmm', 'moab', 'sirius', 'blaspp', 'openmx', 'openfast', 'lammps', 'cosma', 'hydrogen', 'shtools', 'bart', 'aspa', 'harminv', 'meep', 'opium', 'openmolcas', 'elsd', 'grass', 'py-numpy', 'root', 'trilinos', 'dbcsr', 'kokkos-kernels', 'libcint', 'cminpack', 'phist', 'abinit', 'py-cvxopt', 'atompaw', 'frontistr', 'qmcpack', 'dealii', 'mxnet', 'py-gpaw', 'elmerfem', 'py-scipy', 'pagmo', 'gslib', 'armadillo', 'mumps', 'superlu-mt', 'libflame', 'sundials', 'lazyten', 'elemental', 'qrupdate', 'yambo', 'octopus', 'steps', 'ermod', 'psi4', 'py-theano', 'thornado-mini', 'cantera', 'hpl', 'linsys-v', 'arpack-ng', 'miniqmc', 'mfem', 'librom', 'exasp2', 'gmsh', 'cp2k', 'aoflagger', 'ipopt', 'latte', 'superlu-dist', 'petsc', 'wannier90', 'py-torch', 'amp', 'ghost', 'coevp', 'scs', 'planck-likelihood', 'cbench', 'elsdc', 'ntpoly', 'suite-sparse', 'opencv', 'butterflypack', 'sw4lite', 'plumed', 'dakota', 'siesta', 'portage', 'kaldi', 'nekcem', 'globalarrays', 'draco', 'elk', 'qbox', 'tasmanian', 'netlib-scalapack', 'chombo', 'bml', 'netlib-lapack', 'plink', 'caffe', 'plasma', 'esmf', 'dhpmm-f', 'coinhsl', 'dsdp', 'jags', 'elsi', 'plink-ng', 'magma', 'octave', 'akantu', 'r', 'nwchem', 'superlu', 'cblas', 'quantum-espresso', 'gsl'}
.................. check: elpa
.................. check: hiop
.................. check: bohrium
.................. check: nektar
.................. check: ceres-solver
.................. check: gmt
.................. check: casacore
.................. check: cardioid
.................. check: quinoa
.................. check: hypre
.................. check: strumpack
.................. check: xhmm
.................. check: moab
.................. check: sirius
.................. check: blaspp
.................. check: openmx
.................. check: openfast
.................. check: lammps
.................. check: cosma
.................. check: hydrogen
.................. check: shtools
.................. check: bart
.................. check: aspa
.................. check: harminv
.................. check: meep
.................. check: opium
.................. check: openmolcas
.................. check: elsd
.................. check: grass
.................. check: py-numpy
.................. check: root
.................. check: trilinos
.................. check: dbcsr
.................. check: kokkos-kernels
.................. check: libcint
.................. check: cminpack
.................. check: phist
.................. check: abinit
.................. check: py-cvxopt
.................. check: atompaw
................... direct: atompaw -> set()
.................. check: frontistr
.................. check: qmcpack
.................. check: dealii
.................. check: mxnet
.................. check: py-gpaw
.................. check: elmerfem
.................. check: py-scipy
.................. check: pagmo
.................. check: gslib
.................. check: armadillo
.................. check: mumps
.................. check: superlu-mt
................... direct: superlu-mt -> {'gridlab-d', 'sundials'}
................... check: gridlab-d
................... check: sundials
.................. check: libflame
................... direct: libflame -> {'elpa', 'hiop', 'nektar', 'ceres-solver', 'gmt', 'casacore', 'cardioid', 'hypre', 'strumpack', 'xhmm', 'moab', 'sirius', 'openmx', 'openfast', 'lammps', 'hydrogen', 'shtools', 'bart', 'aspa', 'harminv', 'meep', 'opium', 'openmolcas', 'elsd', 'grass', 'py-numpy', 'trilinos', 'dbcsr', 'kokkos-kernels', 'phist', 'abinit', 'py-cvxopt', 'atompaw', 'qmcpack', 'dealii', 'py-gpaw', 'elmerfem', 'py-scipy', 'armadillo', 'mumps', 'sundials', 'lazyten', 'elemental', 'qrupdate', 'yambo', 'octopus', 'steps', 'psi4', 'thornado-mini', 'cantera', 'linsys-v', 'arpack-ng', 'miniqmc', 'mfem', 'librom', 'exasp2', 'gmsh', 'cp2k', 'aoflagger', 'ipopt', 'latte', 'superlu-dist', 'petsc', 'wannier90', 'py-torch', 'amp', 'scs', 'coevp', 'planck-likelihood', 'cbench', 'elsdc', 'suite-sparse', 'butterflypack', 'sw4lite', 'plumed', 'siesta', 'portage', 'nekcem', 'globalarrays', 'draco', 'elk', 'tasmanian', 'netlib-scalapack', 'chombo', 'bml', 'plasma', 'esmf', 'dhpmm-f', 'dsdp', 'jags', 'elsi', 'plink-ng', 'magma', 'octave', 'akantu', 'r', 'nwchem', 'quantum-espresso'}
................... check: elpa
................... check: hiop
................... check: nektar
................... check: ceres-solver
................... check: gmt
................... check: casacore
................... check: cardioid
................... check: hypre
................... check: strumpack
................... check: xhmm
................... check: moab
................... check: sirius
................... check: openmx
................... check: openfast
................... check: lammps
................... check: hydrogen
................... check: shtools
................... check: bart
................... check: aspa
................... check: harminv
................... check: meep
................... check: opium
................... check: openmolcas
................... check: elsd
................... check: grass
................... check: py-numpy
................... check: trilinos
................... check: dbcsr
................... check: kokkos-kernels
................... check: phist
................... check: abinit
................... check: py-cvxopt
................... check: atompaw
................... check: qmcpack
................... check: dealii
................... check: py-gpaw
................... check: elmerfem
................... check: py-scipy
................... check: armadillo
................... check: mumps
................... check: sundials
................... check: lazyten
................... check: elemental
................... check: qrupdate
.................... direct: qrupdate -> {'octave'}
.................... check: octave
................... check: yambo
................... check: octopus
................... check: steps
................... check: psi4
................... check: thornado-mini
................... check: cantera
................... check: linsys-v
................... check: arpack-ng
................... check: miniqmc
................... check: mfem
................... check: librom
................... check: exasp2
................... check: gmsh
................... check: cp2k
................... check: aoflagger
................... check: ipopt
................... check: latte
................... check: superlu-dist
................... check: petsc
................... check: wannier90
................... check: py-torch
................... check: amp
................... check: scs
.................... direct: scs -> set()
................... check: coevp
................... check: planck-likelihood
................... check: cbench
................... check: elsdc
.................... direct: elsdc -> set()
................... check: suite-sparse
................... check: butterflypack
................... check: sw4lite
................... check: plumed
................... check: siesta
................... check: portage
................... check: nekcem
................... check: globalarrays
................... check: draco
................... check: elk
................... check: tasmanian
................... check: netlib-scalapack
................... check: chombo
................... check: bml
................... check: plasma
.................... direct: plasma -> {'xsdk'}
.................... check: xsdk
................... check: esmf
................... check: dhpmm-f
.................... direct: dhpmm-f -> set()
................... check: dsdp
.................... direct: dsdp -> {'py-cvxopt'}
.................... check: py-cvxopt
................... check: jags
.................... direct: jags -> {'r-rjags'}
.................... check: r-rjags
................... check: elsi
................... check: plink-ng
.................... direct: plink-ng -> set()
................... check: magma
.................... direct: magma -> {'amgx', 'libceed', 'xsdk', 'tasmanian', 'sirius', 'ceed', 'py-torch'}
.................... check: amgx
.................... check: libceed
..................... direct: libceed -> {'ceed', 'mfem'}
..................... check: ceed
..................... check: mfem
.................... check: xsdk
.................... check: tasmanian
.................... check: sirius
.................... check: ceed
.................... check: py-torch
................... check: octave
................... check: akantu
................... check: r
................... check: nwchem
................... check: quantum-espresso
.................. check: sundials
.................. check: lazyten
.................. check: elemental
.................. check: qrupdate
.................. check: yambo
.................. check: octopus
.................. check: steps
.................. check: ermod
.................. check: psi4
.................. check: py-theano
.................. check: thornado-mini
.................. check: cantera
.................. check: hpl
.................. check: linsys-v
.................. check: arpack-ng
.................. check: miniqmc
.................. check: mfem
.................. check: librom
.................. check: exasp2
.................. check: gmsh
.................. check: cp2k
.................. check: aoflagger
.................. check: ipopt
.................. check: latte
.................. check: superlu-dist
.................. check: petsc
.................. check: wannier90
.................. check: py-torch
.................. check: amp
.................. check: ghost
.................. check: coevp
.................. check: scs
.................. check: planck-likelihood
.................. check: cbench
.................. check: elsdc
.................. check: ntpoly
.................. check: suite-sparse
.................. check: opencv
.................. check: butterflypack
.................. check: sw4lite
.................. check: plumed
.................. check: dakota
.................. check: siesta
.................. check: portage
.................. check: kaldi
.................. check: nekcem
.................. check: globalarrays
.................. check: draco
.................. check: elk
.................. check: qbox
.................. check: tasmanian
.................. check: netlib-scalapack
.................. check: chombo
.................. check: bml
.................. check: netlib-lapack
.................. check: plink
................... direct: plink -> {'vegas2'}
................... check: vegas2
.................. check: caffe
.................. check: plasma
.................. check: esmf
.................. check: dhpmm-f
.................. check: coinhsl
................... direct: coinhsl -> {'ipopt'}
................... check: ipopt
.................. check: dsdp
.................. check: jags
.................. check: elsi
.................. check: plink-ng
.................. check: magma
.................. check: octave
.................. check: akantu
.................. check: r
.................. check: nwchem
.................. check: superlu
.................. check: cblas
................... direct: cblas -> {'plink-ng', 'kokkos-kernels'}
................... check: plink-ng
................... check: kokkos-kernels
.................. check: quantum-espresso
.................. check: gsl
................... direct: gsl -> {'nco', 'r-diversitree', 'chgcentre', 'dealii', 'andi', 'votca-ctp', 'ea-utils', 'pagmo', 'gdl', 'fgsl', 'ape', 'relax', 'qgis', 'viennarna', 'pism', 'unuran', 'bcftools', 'sirius', 'flecsph', 'hmmer', 'preseq', 'ampliconnoise', 'plumed', 'pktools', 'unblur', 'octopus', 'meep', 'pnfft', 'nest', 'augustus', 'py-faststructure', 'msmc', 'root', 'draco', 'votca-tools', 'r-dirichletmultinomial', 'r-gsl', 'dysco', 'gaudi', 'py-cvxopt'}
................... check: nco
................... check: r-diversitree
................... check: chgcentre
................... check: dealii
................... check: andi
................... check: votca-ctp
................... check: ea-utils
................... check: pagmo
................... check: gdl
................... check: fgsl
.................... direct: fgsl -> set()
................... check: ape
.................... direct: ape -> set()
................... check: relax
................... check: qgis
................... check: viennarna
.................... direct: viennarna -> {'tcoffee', 'cleaveland4', 'shortstack', 'mirdeep2'}
.................... check: tcoffee
.................... check: cleaveland4
.................... check: shortstack
.................... check: mirdeep2
................... check: pism
................... check: unuran
.................... direct: unuran -> {'root'}
.................... check: root
................... check: bcftools
................... check: sirius
................... check: flecsph
................... check: hmmer
................... check: preseq
................... check: ampliconnoise
................... check: plumed
................... check: pktools
................... check: unblur
................... check: octopus
................... check: meep
................... check: pnfft
................... check: nest
................... check: augustus
................... check: py-faststructure
................... check: msmc
................... check: root
................... check: draco
................... check: votca-tools
................... check: r-dirichletmultinomial
................... check: r-gsl
................... check: dysco
................... check: gaudi
................... check: py-cvxopt
................. check: vdt
.................. direct: vdt -> {'root'}
.................. check: root
................. check: mbedtls
.................. direct: mbedtls -> {'hpctoolkit'}
.................. check: hpctoolkit
................. check: vtk-h
................. check: abseil-cpp
.................. direct: abseil-cpp -> {'grpc'}
.................. check: grpc
................. check: kealib
................. check: vecgeom
................. check: piranha
.................. direct: piranha -> {'symengine'}
.................. check: symengine
................. check: xsdktrilinos
................. check: geant4
................. check: plasma
................. check: py-pyarrow
................. check: libbson
.................. direct: libbson -> {'mongo-c-driver', 'julea'}
.................. check: mongo-c-driver
.................. check: julea
................. check: pfunit
................. check: nanomsg
.................. direct: nanomsg -> set()
................. check: libevpath
................. check: libspatialindex
.................. direct: libspatialindex -> {'qgis', 'py-rtree'}
.................. check: qgis
.................. check: py-rtree
................. check: py-blosc
................. check: cpprestsdk
.................. direct: cpprestsdk -> set()
................. check: parquet-cpp
................. check: libmo-unpack
.................. direct: libmo-unpack -> {'py-mo-pack'}
.................. check: py-mo-pack
................. check: gotcha
.................. direct: gotcha -> {'cbtf-krell', 'unifyfs', 'caliper', 'hpctoolkit', 'timemory'}
.................. check: cbtf-krell
.................. check: unifyfs
.................. check: caliper
.................. check: hpctoolkit
.................. check: timemory
................. check: libemos
................. check: qnnpack
.................. direct: qnnpack -> {'py-torch'}
.................. check: py-torch
................. check: hacckernels
.................. direct: hacckernels -> set()
................. check: magma
................. check: davix
.................. direct: davix -> {'root'}
.................. check: root
................. check: archer
................. check: cgal
.................. direct: cgal -> {'openfoam', 'gplates', 'memsurfer', 'sfcgal'}
.................. check: openfoam
.................. check: gplates
.................. check: memsurfer
.................. check: sfcgal
................. check: paraview
................. check: gtkorvo-atl
................. check: of-catalyst
................. check: uncrustify
.................. direct: uncrustify -> {'axom'}
.................. check: axom
................. check: acts
................. check: cpp-httplib
.................. direct: cpp-httplib -> set()
................. check: legion
................. check: typhonio
................. check: multiverso
................. check: ecp-viz-sdk
................ check: py-pyqt4
................. direct: py-pyqt4 -> {'py-qtpy', 'py-traitsui', 'py-tuiview', 'py-pythonqwt', 'qscintilla', 'qgis', 'py-guidata', 'py-matplotlib', 'py-pyface'}
................. check: py-qtpy
................. check: py-traitsui
................. check: py-tuiview
................. check: py-pythonqwt
................. check: qscintilla
................. check: qgis
................. check: py-guidata
................. check: py-matplotlib
................. check: py-pyface
................ check: qtkeychain
................ check: qca
................ check: silo
................ check: py-pyside
................ check: qjson
................ check: py-spyder
................ check: texstudio
................ check: geant4
................ check: openimageio
................ check: qscintilla
................ check: qgis
................ check: opencv
................ check: vtk
................ check: libfive
................ check: ravel
................ check: qt-creator
................. direct: qt-creator -> set()
................ check: cbtf-argonavis-gui
................ check: memaxes
................ check: openspeedshop
................ check: qtgraph
................ check: magics
................ check: sqlitebrowser
................ check: py-shiboken
................ check: gnuplot
................ check: mrtrix3
................ check: octave
................ check: py-pyqt5
................ check: ecflow
................ check: cgal
................ check: graphviz
................ check: paraview
................ check: audacious
................ check: openscenegraph
................ check: tulip
................ check: poppler
................ check: root
................ check: draco
................ check: qwtpolar
................ check: visit
................ check: wireshark
................ check: kdiff3
................. direct: kdiff3 -> set()
................ check: gplates
............... check: gl2ps
............... check: catalyst
............... check: geant4
............... check: vtk
............... check: libepoxy
............... check: jasper
............... check: r-rgl
............... check: opendx
............... check: glvis
............... check: glew
............... check: hwloc
............... check: paraview
............... check: grass
............... check: opensubdiv
............... check: root
............... check: ethminer
............... check: rodinia
............... check: vesta
............... check: mesa-glu
................ direct: mesa-glu -> {'r-rgl', 'fsl', 'root', 'glvis', 'rodinia', 'ftgl', 'mrtrix3', 'virtualgl', 'gplates', 'freeglut'}
................ check: r-rgl
................ check: fsl
................. direct: fsl -> set()
................ check: root
................ check: glvis
................ check: rodinia
................ check: ftgl
................ check: mrtrix3
................ check: virtualgl
................ check: gplates
................ check: freeglut
............... check: freeglut
.............. check: hipsycl
.............. check: f18
.............. check: flecsi
.............. check: ldc-bootstrap
.............. check: oclint
.............. check: cquery
.............. check: iwyu
.............. check: rtags
.............. check: sst-macro
.............. check: py-pyside2
.............. check: octave
.............. check: ldc
.............. check: archer
............. check: hpx
............. check: geopm
............. check: llvm-flang
............. check: funhpc
............. check: kokkos-legacy
............. check: variorum
............. check: lbann
............. check: flux-core
............ check: cloverleaf
............ check: papyrus
............ check: geopm
............ check: elk
............ check: dtcmp
............ check: swiftsim
............ check: tasmanian
............ check: timemory
............ check: redset
............ check: snap
............ check: nalu
............ check: nek5000
............ check: h5cpp
............ check: flann
............ check: mpix-launch-swift
............ check: ompss
............ check: er
............ check: miniamr
............ check: p3dfft3
............ check: tealeaf
............ check: elsi
............ check: damselfly
............ check: py-horovod
............ check: zoltan
............ check: sst-core
............ check: mdtest
............ check: visit
............ check: henson
............ check: caliper
............ check: foam-extend
............ check: pflotran
............ check: jali
............ check: nekbone
............ check: hiop
............ check: nektar
............ check: picsarlite
............ check: libnbc
............ check: libhio
............ check: netgauge
............ check: iq-tree
............ check: pfft
............ check: revbayes
............ check: moab
............ check: openfdtd
............ check: openmx
............ check: openfast
............ check: lammps
............ check: cosma
............ check: amrex
............ check: ppopen-math-mp
............ check: libquo
............ check: helics
............ check: swfft
............ check: amrvis
............ check: gromacs
............ check: maker
............ check: isaac
............ check: h5hut
............ check: raxml
............ check: vpic
............ check: filo
............ check: p4est
............ check: abinit
............ check: frontistr
............ check: rankstr
............ check: npb
............ check: gasnet
............ check: diy
............ check: netgen
............ check: pidx
............ check: xdmf3
............ check: parmgridgen
............ check: mumps
............ check: shengbte
............ check: everytrace
............ check: mstk
............ check: kvtree
............ check: turbine
............ check: ppopen-appl-fvm
............ check: pnmpi
............ check: openpmd-api
............ check: amg
............ check: clamr
............ check: py-python-meep
............ check: conduit
............ check: spath
............ check: arpack-ng
............ check: miniqmc
............ check: cp2k
............ check: librom
............ check: asagi
............ check: superlu-dist
............ check: petsc
............ check: eem
............ check: swap-assembler
............. direct: swap-assembler -> set()
............ check: ghost
............ check: coevp
............ check: py-libensemble
............ check: codes
............ check: pumi
............ check: neuron
............ check: butterflypack
............ check: darshan-runtime
............ check: tycho2
............ check: plumed
............ check: dakota
............ check: portage
............ check: comd
............ check: branson
............ check: r-rmpi
............ check: unifyfs
............ check: minigmg
............ check: automaded
............ check: typhon
............ check: h5part
............ check: netlib-scalapack
............ check: chombo
............ check: vtk-h
............ check: silo
............ check: xsdktrilinos
............ check: esmf
............ check: fastmath
............ check: pfunit
............ check: savanna
............ check: xios
............ check: graph500
............ check: cgm
............ check: paraview
............ check: examinimd
............ check: nwchem
............ check: quantum-espresso
............ check: typhonio
............ check: multiverso
........... check: libfabric
........... check: charmpp
.......... check: neo4j
........... direct: neo4j -> set()
.......... check: sqoop
........... direct: sqoop -> set()
.......... check: kinesis
.......... check: canal
.......... check: interproscan
.......... check: accumulo
........... direct: accumulo -> set()
.......... check: zookeeper-benchmark
........... direct: zookeeper-benchmark -> set()
......... check: antlr
.......... direct: antlr -> {'nco'}
.......... check: nco
......... check: pagit
.......... direct: pagit -> set()
......... check: sbml
......... check: jackcess
.......... direct: jackcess -> {'gdal'}
.......... check: gdal
......... check: gradle
.......... direct: gradle -> set()
......... check: hdf
......... check: mixcr
.......... direct: mixcr -> set()
......... check: nextflow
.......... direct: nextflow -> set()
......... check: py-pipits
......... check: sqoop
......... check: vardictjava
.......... direct: vardictjava -> set()
......... check: bbmap
.......... direct: bbmap -> set()
......... check: cromwell
.......... direct: cromwell -> set()
......... check: jblob
.......... direct: jblob -> set()
......... check: flume
.......... direct: flume -> set()
......... check: lucene
.......... direct: lucene -> set()
......... check: r-xlconnect
......... check: accumulo
......... check: bref3
.......... direct: bref3 -> set()
......... check: py-crispresso
......... check: opencv
......... check: igvtools
.......... direct: igvtools -> set()
......... check: beast1
......... check: commons-logging
.......... direct: commons-logging -> {'jackcess'}
.......... check: jackcess
......... check: sumo
......... check: r-xlsx
......... check: graylog2-server
.......... direct: graylog2-server -> set()
......... check: tfel
......... check: ucx
......... check: fastqc
.......... direct: fastqc -> {'trinity', 'trimgalore'}
.......... check: trinity
.......... check: trimgalore
......... check: r-xlconnectjars
......... check: commons-lang
.......... direct: commons-lang -> {'jackcess'}
.......... check: jackcess
......... check: storm
.......... direct: storm -> set()
......... check: minced
.......... direct: minced -> set()
......... check: fseq
.......... direct: fseq -> set()
......... check: py-quast
......... check: kafka
.......... direct: kafka -> set()
......... check: spark
......... check: prism
.......... direct: prism -> set()
......... check: rdp-classifier
.......... direct: rdp-classifier -> {'py-pipits'}
.......... check: py-pipits
......... check: tajo
.......... direct: tajo -> set()
......... check: commons-lang3
.......... direct: commons-lang3 -> set()
......... check: beagle
.......... direct: beagle -> set()
......... check: beast-tracer
......... check: claw
......... check: r-seurat
......... check: rose
......... check: gatk
......... check: hpcviewer
.......... direct: hpcviewer -> set()
......... check: tassel
.......... direct: tassel -> set()
......... check: cromwell-womtool
.......... direct: cromwell-womtool -> set()
......... check: varscan
.......... direct: varscan -> set()
......... check: blast2go
......... check: haploview
.......... direct: haploview -> set()
......... check: scala
.......... direct: scala -> set()
......... check: beast2
.......... direct: beast2 -> set()
......... check: cassandra
.......... direct: cassandra -> set()
......... check: octave
......... check: r
......... check: r-rjava
......... check: testdfsio
......... check: elasticsearch
.......... direct: elasticsearch -> set()
........ check: kraken2
......... direct: kraken2 -> set()
...... check: openmpi
...... check: hpx
...... check: petsc
...... check: wget
...... check: lz4
....... direct: lz4 -> {'adios', 'mariadb', 'libarchive', 'rocksdb', 'c-blosc2', 'root', 'subversion', 'slurm', 'vtk', 'jhpcn-df', 'flux-core', 'c-blosc'}
....... check: adios
....... check: mariadb
....... check: libarchive
........ direct: libarchive -> {'cmake', 'mpifileutils', 'singularity-legacy', 'pixz'}
........ check: cmake
........ check: mpifileutils
........ check: singularity-legacy
......... direct: singularity-legacy -> set()
........ check: pixz
......... direct: pixz -> set()
....... check: rocksdb
....... check: c-blosc2
....... check: root
....... check: subversion
....... check: slurm
....... check: vtk
....... check: jhpcn-df
....... check: flux-core
....... check: c-blosc
...... check: flux-core
...... check: argobots
....... direct: argobots -> {'sollve', 'bolt', 'margo'}
....... check: sollve
....... check: bolt
....... check: margo
..... check: ember
..... check: alquimia
..... check: picsar
..... check: flecsi
..... check: pism
..... check: py-tensorflow
..... check: mrbayes
..... check: ppopen-math-vis
..... check: pmlib
..... check: ross
..... check: cram
..... check: veloc
..... check: akantu
..... check: openfoam
..... check: hpx
..... check: minivite
..... check: funhpc
..... check: grackle
..... check: hpctoolkit
..... check: damaris
..... check: adept-utils
..... check: elpa
..... check: py-espressopp
..... check: intel-mpi-benchmarks
..... check: cloverleaf3d
..... check: ebms
..... check: xbraid
..... check: panda
..... check: sirius
..... check: ampliconnoise
..... check: netcdf-fortran
..... check: bookleaf-cpp
..... check: muster
..... check: albany
..... check: aluminum
..... check: aspa
..... check: mrcpp
..... check: ascent
..... check: minighost
..... check: kokkos-nvcc-wrapper
..... check: dbcsr
..... check: converge
..... check: vtk-m
..... check: phist
..... check: qmcpack
..... check: minitri
..... check: lulesh
..... check: minife
..... check: pagmo
..... check: opencoarrays
..... check: gslib
..... check: megadock
..... check: vtk
..... check: flecsph
..... check: hpccg
..... check: elemental
..... check: everytrace-example
..... check: lwgrp
..... check: adlbx
..... check: pbmpi
..... check: yambo
..... check: hoomd-blue
..... check: molcas
..... check: tau
..... check: vpfft
..... check: openstf
..... check: dftfe
..... check: boxlib
..... check: mpileaks
..... check: mfem
..... check: sosflow
..... check: minimd
..... check: spfft
..... check: ppopen-appl-fem
..... check: wannier90
..... check: amp
..... check: ior
..... check: cbench
..... check: ntpoly
..... check: ppopen-appl-fdm-at
..... check: sst-macro
..... check: dataspaces
..... check: parsplice
..... check: lwm2
..... check: camellia
..... check: mpe2
..... check: macsio
..... check: upcxx
..... check: ppopen-appl-bem
..... check: scorep
..... check: cabana
..... check: scalasca
..... check: globalarrays
..... check: simul
..... check: qbox
..... check: mercury
..... check: med
..... check: bml
..... check: nccl-tests
..... check: fenics
..... check: amber
..... check: liggghts
..... check: hmmer
..... check: hdf5
..... check: athena
..... check: precice
..... check: slate
..... check: quicksilver
..... check: exabayes
..... check: extrae
..... check: ray
..... check: meshkit
..... check: cardioid
..... check: hypre
..... check: eq-r
..... check: regcm
..... check: axom
..... check: seacas
..... check: ccs-qcd
..... check: meep
..... check: raft
..... check: omega-h
..... check: textparser
..... check: adiak
..... check: lbann
..... check: callpath
..... check: adios2
..... check: amgx
..... check: minixyce
..... check: py-gpaw
..... check: osu-micro-benchmarks
..... check: nalu-wind
..... check: py-h5py
..... check: ppopen-appl-amr-fdm
..... check: py-espresso
..... check: fast-global-file-status
..... check: sundials
..... check: simplemoc
..... check: steps
..... check: shuffile
..... check: channelflow
..... check: ffr
..... check: cbtf-krell
..... check: tracer
..... check: netcdf-c
..... check: thornado-mini
..... check: gmsh
..... check: cntk
..... check: erne
...... direct: erne -> set()
..... check: meme
..... check: py-mpi4py
..... check: amg2013
..... check: ppopen-appl-dem-util
..... check: mpip
..... check: relion
..... check: infernal
..... check: camx
..... check: vampirtrace
..... check: opencv
..... check: chatterbug
..... check: sgpp
..... check: nekcem
..... check: hwloc
..... check: cloverleaf
..... check: papyrus
..... check: geopm
..... check: elk
..... check: dtcmp
..... check: swiftsim
..... check: tasmanian
..... check: timemory
..... check: redset
..... check: snap
..... check: nalu
..... check: nek5000
..... check: h5cpp
..... check: flann
..... check: mpix-launch-swift
..... check: ompss
..... check: er
..... check: miniamr
..... check: p3dfft3
..... check: tealeaf
..... check: elsi
..... check: damselfly
..... check: py-horovod
..... check: zoltan
..... check: sst-core
..... check: mdtest
..... check: visit
..... check: henson
..... check: caliper
..... check: foam-extend
..... check: pflotran
..... check: jali
..... check: nekbone
..... check: hiop
..... check: migrate
...... direct: migrate -> set()
..... check: nektar
..... check: picsarlite
..... check: libnbc
..... check: libhio
..... check: netgauge
..... check: iq-tree
..... check: pfft
..... check: revbayes
..... check: moab
..... check: openfdtd
..... check: openmx
..... check: openfast
..... check: lammps
..... check: cosma
..... check: amrex
..... check: ppopen-math-mp
..... check: libquo
..... check: helics
..... check: swfft
..... check: amrvis
..... check: gromacs
..... check: maker
..... check: isaac
..... check: h5hut
..... check: raxml
..... check: vpic
..... check: filo
..... check: p4est
..... check: abinit
..... check: frontistr
..... check: rankstr
..... check: npb
..... check: gasnet
..... check: diy
..... check: netgen
..... check: pidx
..... check: xdmf3
..... check: parmgridgen
..... check: mumps
..... check: shengbte
..... check: everytrace
..... check: mstk
..... check: kvtree
..... check: turbine
..... check: ppopen-appl-fvm
..... check: pnmpi
..... check: openpmd-api
..... check: amg
..... check: clamr
..... check: py-python-meep
..... check: conduit
..... check: spath
..... check: arpack-ng
..... check: miniqmc
..... check: cp2k
..... check: librom
..... check: asagi
..... check: superlu-dist
..... check: petsc
..... check: eem
..... check: ghost
..... check: coevp
..... check: py-libensemble
..... check: codes
..... check: pumi
..... check: neuron
..... check: butterflypack
..... check: darshan-runtime
..... check: tycho2
..... check: plumed
..... check: dakota
..... check: portage
..... check: comd
..... check: branson
..... check: r-rmpi
..... check: unifyfs
..... check: minigmg
..... check: automaded
..... check: typhon
..... check: h5part
..... check: netlib-scalapack
..... check: chombo
..... check: vtk-h
..... check: silo
..... check: xsdktrilinos
..... check: esmf
..... check: fastmath
..... check: pfunit
..... check: savanna
..... check: xios
..... check: graph500
..... check: cgm
..... check: paraview
..... check: examinimd
..... check: nwchem
..... check: quantum-espresso
..... check: typhonio
..... check: multiverso
.... check: mpich
.... check: charmpp
.... check: mvapich2
.... check: libyogrt
..... direct: libyogrt -> {'scr'}
..... check: scr
.. check: iq-tree
.. check: thrift
.. check: folly
.. check: revbayes
.. check: cleverleaf
.. check: gource
.. check: ibmisc
.. check: cbtf-argonavis-gui
.. check: dd4hep
.. check: muster
.. check: paraver
.. check: abyss
.. check: helics
.. check: modern-wheel
.. check: faodel
.. check: isaac
.. check: sympol
.. check: mapnik
.. check: mofem-cephas
.. check: templight-tools
.. check: hipsycl
.. check: trilinos
.. check: dysco
.. check: sfcgal
.. check: yaml-cpp
.. check: snap-korf
... direct: snap-korf -> {'maker'}
... check: maker
.. check: metall
.. check: flux-sched
.. check: isaac-server
.. check: stat
.. check: symengine
.. check: mira
... direct: mira -> set()
.. check: autodock-vina
... direct: autodock-vina -> set()
.. check: qmcpack
.. check: arrow
.. check: flecsale
.. check: paradiseo
.. check: dealii
.. check: polymake
... direct: polymake -> set()
.. check: xdmf3
.. check: simgrid
.. check: casper
... direct: casper -> {'mefit'}
... check: mefit
.. check: pagmo
.. check: nix
.. check: openimageio
.. check: liblas
.. check: py-espresso
.. check: meraculous
.. check: libkml
.. check: source-highlight
... direct: source-highlight -> {'gdb'}
... check: gdb
.... direct: gdb -> {'py-cython', 'py-gdbgui', 'ddd', 'gaudi', 'rr'}
.... check: py-cython
.... check: py-gdbgui
.... check: ddd
..... direct: ddd -> set()
.... check: gaudi
.... check: rr
.. check: vigra
.. check: hisea
... direct: hisea -> set()
.. check: bcl2fastq2
.. check: vtk
.. check: flecsph
.. check: libmesh
.. check: highfive
.. check: seqan
.. check: clfft
.. check: dimemas
... direct: dimemas -> set()
.. check: sailfish
.. check: launchmon
... direct: launchmon -> {'stat', 'spindle'}
... check: stat
... check: spindle
.... direct: spindle -> set()
.. check: channelflow
.. check: psi4
.. check: ecflow
.. check: augustus
.. check: denovogear
.. check: cbtf-krell
.. check: cantera
.. check: votca-tools
.. check: openbabel
.. check: ns-3-dev
.. check: cbtf
.. check: gplates
.. check: aoflagger
.. check: cntk
.. check: erne
.. check: librom
.. check: range-v3
.. check: samrai
.. check: chill
.. check: imp
.. check: mothur
... direct: mothur -> set()
.. check: kea
.. check: amp
.. check: metabat
.. check: shark
.. check: py-python-mapnik
.. check: r-phantompeakqualtools
.. check: aspcud
.. check: parsplice
.. check: masurca
... direct: masurca -> set()
.. check: py-pycuda
.. check: dakota
.. check: tfel
.. check: portage
.. check: branson
.. check: percept
.. check: spot
... direct: spot -> set()
.. check: fairlogger
.. check: sgpp
.. check: tophat
.. check: mgis
.. check: py-quast
.. check: delly2
.. check: fsl
.. check: automaded
.. check: ethminer
.. check: gaudi
.. check: mercury
.. check: assimp
.. check: adol-c
... direct: adol-c -> {'mofem-cephas', 'dealii'}
... check: mofem-cephas
... check: dealii
.. check: rose
.. check: votca-csg-tutorials
.. check: fenics
.. check: caffe
.. check: gearshifft
.. check: flann
.. check: hyperscan
.. check: blasr
.. check: votca-csgapps
.. check: piranha
.. check: valgrind
.. check: geant4
.. check: mallocmc
.. check: flecsi
.. check: openspeedshop-utils
.. check: apex
.. check: libfive
.. check: scallop
.. check: heaptrack
.. check: cpprestsdk
.. check: mrnet
... direct: mrnet -> {'cbtf-argonavis-gui', 'cbtf-krell', 'openspeedshop', 'cbtf-lanl', 'stat', 'cbtf-argonavis', 'openspeedshop-utils', 'fast-global-file-status', 'cbtf'}
... check: cbtf-argonavis-gui
... check: cbtf-krell
... check: openspeedshop
... check: cbtf-lanl
... check: stat
... check: cbtf-argonavis
... check: openspeedshop-utils
... check: fast-global-file-status
... check: cbtf
.. check: openspeedshop
.. check: express
.. check: pktools
.. check: parquet-cpp
.. check: xios
.. check: cbtf-argonavis
.. check: votca-csg
.. check: magics
.. check: manta
.. check: precice
.. check: wt
.. check: veloc
.. check: libint
... direct: libint -> {'cp2k'}
... check: cp2k
.. check: akantu
.. check: ncbi-toolkit
.. check: pbbam
.. check: cgal
.. check: openfoam
.. check: dyninst
.. check: strelka
.. check: hpx
.. check: biobloom
.. check: acts
.. check: extrae
.. check: hpctoolkit
.. check: multiverso
.. check: jali
.. check: damaris
.. check: adept-utils
. check: py-nestle
. check: py-wxmplot
. check: sgpp
. check: mrtrix3
. check: py-cnvkit
. check: py-dgl
. check: py-checkm-genome
. check: py-opentuner
. check: py-gensim
. check: py-mg-rast-tools
. check: py-biopython
. check: geopm
. check: py-lmfit
. check: py-tomopy
. check: py-weblogo
. check: py-asdf
. check: py-numcodecs
. check: cmor
. check: py-xarray
. check: tasmanian
. check: timemory
. check: mefit
. check: py-fits-tools
. check: py-pyfits
. check: py-sfepy
. check: openmm
. check: py-hpcbench
. check: py-kmodes
. check: py-opppy
. check: py-mdanalysis
. check: py-gnuplot
.. direct: py-gnuplot -> set()
. check: fenics
. check: py-pytools
. check: py-scikit-image
. check: py-tensorboardx
. check: py-pybigwig
. check: cosmomc
. check: caffe
. check: grib-api
. check: flann
. check: py-tensorboard
. check: py-spacy
. check: gdl
. check: py-modred
. check: catalyst
. check: py-pyarrow
. check: pism
. check: py-colorpy
. check: redundans
. check: rmats
.. direct: rmats -> set()
. check: py-tensorflow
. check: py-blis
. check: py-deeptools
. check: py-blosc
. check: py-dxchange
. check: py-pymol
. check: py-pynn
. check: py-shapely
. check: py-sncosmo
. check: py-quantities
.. direct: py-quantities -> {'py-spykeutils', 'py-pynn', 'py-elephant', 'py-neo'}
.. check: py-spykeutils
.. check: py-pynn
.. check: py-elephant
.. check: py-neo
. check: py-tuiview
. check: py-bx-python
. check: magics
. check: py-thirdorder
. check: py-imageio
. check: py-mmcv
. check: precice
. check: py-cf-units
. check: py-horovod
. check: akantu
. check: py-brian2
. check: py-pyrosar
. check: hic-pro
. check: paraview
. check: py-htseq
. check: py-mlxtend
. check: py-ont-fast5-api
. check: py-cclib
.. direct: py-cclib -> set()
. check: py-torchtext
. check: py-h5sh
. check: py-pythonqwt
. check: py-lscsoft-glue
. check: py-opt-einsum
. check: py-cvxpy
. check: py-wub
. check: py-netcdf4
 check: root
 check: trilinos
 check: dbcsr
 check: kokkos-kernels
 check: libcint
 check: cminpack
 check: phist
 check: abinit
 check: py-cvxopt
 check: atompaw
 check: frontistr
 check: qmcpack
 check: dealii
 check: mxnet
 check: py-gpaw
 check: elmerfem
 check: py-scipy
 check: pagmo
 check: gslib
 check: armadillo
 check: mumps
 check: superlu-mt
 check: libflame
 check: sundials
 check: lazyten
 check: elemental
 check: qrupdate
 check: yambo
 check: octopus
 check: steps
 check: ermod
 check: psi4
 check: py-theano
 check: thornado-mini
 check: cantera
 check: hpl
 check: linsys-v
 check: arpack-ng
 check: miniqmc
 check: mfem
 check: librom
 check: exasp2
 check: gmsh
 check: cp2k
 check: aoflagger
 check: ipopt
 check: latte
 check: superlu-dist
 check: petsc
 check: wannier90
 check: py-torch
 check: amp
 check: ghost
 check: coevp
 check: scs
 check: planck-likelihood
 check: cbench
 check: elsdc
 check: ntpoly
 check: suite-sparse
 check: opencv
 check: butterflypack
 check: sw4lite
 check: plumed
 check: dakota
 check: siesta
 check: portage
 check: kaldi
 check: nekcem
 check: globalarrays
 check: draco
 check: elk
 check: qbox
 check: tasmanian
 check: netlib-scalapack
 check: chombo
 check: bml
 check: netlib-lapack
 check: csdp
. direct: csdp -> set()
 check: clapack
. direct: clapack -> {'phast', 'kokkos-kernels'}
. check: phast
.. direct: phast -> set()
. check: kokkos-kernels
 check: plink
 check: caffe
 check: plasma
 check: esmf
 check: dhpmm-f
 check: coinhsl
 check: dsdp
 check: jags
 check: elsi
 check: plink-ng
 check: magma
 check: octave
 check: akantu
 check: r
 check: nwchem
 check: superlu
 check: cblas
 check: quantum-espresso
 check: gsl
MeshSimAdvanced
abinit
abseil-cpp
abyss
accfft
accumulo
acts
adept-utils
adiak
adios
adios2
adlbx
adol-c
aegean
akantu
albany
alquimia
aluminum
ambari
amber
amg
amg2013
amgx
amp
ampliconnoise
amrex
amrvis
andi
angsd
ant
antlr
ants
aoflagger
aom
ape
aperture-photometry
apex
arborx
archer
argobots
aria2
armadillo
arpack-ng
arrow
asagi
ascent
asdf-cxx
aspa
aspcud
aspect
assimp
astral
at-spi2-atk
at-spi2-core
athena
atk
atompaw
audacious
augustus
autodock-vina
automaded
aws-parallelcluster
awscli
axl
axom
bam-readcount
bamtools
barrnap
bart
bat
bbmap
bcftools
bcl2fastq2
beagle
bear
beast-tracer
beast1
beast2
benchmark
bertini
biobloom
biopieces
bismark
blaspp
blasr
blasr-libcpp
blast-plus
blast2go
blaze
bml
bohrium
boinc-client
bolt
bookleaf-cpp
boost
bowtie
bowtie2
boxlib
bpp-core
bpp-phyl
bpp-seq
bpp-suite
braker
branson
breakdancer
bref3
breseq
bridger
brotli
brpc
bsseeker2
bumpversion
busco
butter
butterflypack
c-ares
c-blosc
c-blosc2
cabana
caffe
cairo
cairomm
caliper
callpath
camellia
camx
canal
candle-benchmarks
cantera
canu
capstone
cardioid
casacore
cask
casper
cassandra
catalyst
catch2
cbench
cblas
cbtf
cbtf-argonavis
cbtf-argonavis-gui
cbtf-krell
cbtf-lanl
ccfits
ccs-qcd
cdo
ceed
cereal
ceres-solver
cfitsio
cgal
cgm
cgns
channelflow
charliecloud
charmpp
chatterbug
chgcentre
chill
chombo
circos
cistem
citcoms
clamr
clapack
clara
claw
cleaveland4
cleverleaf
clfft
clhep
clingo
cloverleaf
cloverleaf3d
cmake
cminpack
cmocka
cmor
cnmem
cnpy
cntk
cnvnator
codec2
codes
coevp
cohmm
coinhsl
comd
commons-lang
commons-lang3
commons-logging
compiz
conduit
converge
cosbench
cosma
cosmomc
cosp2
cotter
cp2k
cpp-httplib
cppad
cppcheck
cppgsl
cpprestsdk
cppzmq
cpu-features
cpuinfo
cquery
cram
cromwell
cromwell-womtool
csdp
ctffind
ctre
cube
cuda-memtest
curl
cxxopts
dakota
damaris
damselfly
darshan-runtime
dataspaces
davix
dbcsr
dbus
dcmtk
dd4hep
ddd
dealii
dealii-parameter-gui
delly2
delphes
denovogear
dftfe
dhpmm-f
dia
diamond
dihydrogen
dimemas
direnv
discovardenovo
dislin
diy
dmd
dmlc-core
double-conversion
doxygen
draco
drill
dsdp
dtcmp
dyninst
dysco
ea-utils
eagle
easybuild
ebms
eccodes
ecflow
ecp-io-sdk
ecp-proxy-apps
ecp-viz-sdk
eem
eigen
elasticsearch
elemental
elk
elmerfem
elpa
elsd
elsdc
elsi
emacs
ember
emboss
enchant
eq-r
er
ermod
erne
esmf
etcd
ethminer
etsf-io
everytrace
everytrace-example
exa
exabayes
examinimd
exampm
exasp2
exiv2
exodusii
exonerate
exonerate-gff3
express
extrae
eztrace
f18
fabtests
fairlogger
falcon
fann
faodel
fast-global-file-status
fastmath
fastq-screen
fastqc
fd-find
feh
fenics
ferret
ffmpeg
ffr
fftw
fgsl
figtree
filo
find-circ
fio
fish
flang
flann
flatbuffers
flatcc
flecsale
flecsi
flecsph
flibcpp
flink
flit
flume
flux-core
flux-sched
fmt
foam-extend
folly
fox
fpocket
freebayes
freeglut
frontistr
fseq
fsl
fstrack
ftgl
funhpc
fzf
gasnet
gatk
gaudi
gccxml
gconf
gdal
gdb
gdk-pixbuf
gdl
geant4
gearshifft
genomefinisher
genometools
geode
geopm
gflags
ghost
ghostscript
ginkgo
git
git-fat-git
git-imerge
git-lfs
gitconddb
gl2ps
glew
glfw
glib
glibmm
glm
globalarrays
glog
gloo
glvis
gmime
gmodel
gmsh
gmt
gmtsar
gnuplot
go
go-bootstrap
go-md2man
gobject-introspection
googletest
gotcha
gource
gpdb
gplates
grackle
gradle
grandr
graph500
graphicsmagick
graphite2
graphlib
graphviz
grass
graylog2-server
grib-api
gridlab-d
grnboost
groff
gromacs
grpc
gsl
gslib
gtk-doc
gtkmm
gtkorvo-atl
gtkorvo-cercs-env
gtkorvo-dill
gtkplus
gtksourceview
gts
guacamole-server
guidance
gunrock
h5cpp
h5hut
h5part
h5utils
h5z-zfp
hacckernels
hadoop
hal
halc
haploview
harfbuzz
harminv
hbase
hc
hcol
hdf
hdf5
hdf5-blosc
healpix-cxx
heaptrack
heffte
helics
henson
hepmc
hic-pro
highfive
hiop
hipsycl
hisea
hive
hmmer
homer
hoomd-blue
hpccg
hpctoolkit
hpcviewer
hpgmg
hpl
hpx
hpx5
htslib
httpie
httpress
hub
hugo
hwloc
hybpiper
hydrogen
hyperscan
hyphy
hypre
i3
ibmisc
icedtea
icet
iegenlib
ignite
igv
igvtools
imagemagick
imlib2
imp
infernal
intel-gpu-tools
intel-llvm
intel-mpi-benchmarks
intel-tbb
interproscan
ioapi
ior
ipopt
iq-tree
isaac
isaac-server
itsx
iwyu
jackcess
jafka
jags
jali
jansson
jasper
jblob
jchronoss
jhpcn-df
jmol
json-fortran
json-glib
jsoncpp
jube
julea
julia
kafka
kahip
kaiju
kaldi
kallisto
kcov
kdiff3
kea
kealib
kentutils
kim-api
kinesis
kitty
kmergenie
kokkos
kokkos-kernels
kokkos-kernels-legacy
kokkos-legacy
kokkos-nvcc-wrapper
kraken2
krims
kripke
kubernetes
kvtree
ladot
laghos
lammps
lapackpp
laszip
latte
launchmon
lazyten
lbann
lcms
ldc
ldc-bootstrap
legion
lemon
leveldb
libaec
libarchive
libbeagle
libbson
libcanberra
libceed
libcint
libcircle
libcroco
libdap4
libdivsufsort
libemos
libepoxy
libevpath
libexif
libfabric
libffs
libfive
libflame
libgd
libgeotiff
libgit2
libgpuarray
libhio
libibumad
libical
libint
libjpeg-turbo
libkml
liblas
libmesh
libmmtf-cpp
libmng
libmo-unpack
libnbc
libnetworkit
libnotify
libnrm
libpeas
libpmemobj-cpp
libpsl
libquo
librom
librsvg
libseccomp
libsecret
libsharp
libspatialindex
libsplash
libssh
libssh2
libtiff
libtlx
libtree
libvdwxc
libvips
libwebsockets
libyogrt
liggghts
lighttpd
linsys-v
llvm
llvm-flang
llvm-openmp
llvm-openmp-ompt
lmod
log4cplus
lordec
lua-luafilesystem
lua-mpack
lucene
lulesh
lumpy-sv
lwgrp
lwm2
lz4
macsio
mad-numdiff
magics
magma
maker
mallocmc
maloc
man-db
manta
mapnik
mapserver
margo
mariadb
mariadb-c-client
masurca
matio
maven
mbedtls
mc
mdtest
med
meep
mefit
megadock
memaxes
meme
memsurfer
meraculous
mercurial
mercury
mesa
mesa-glu
meshkit
meson
mesquite
metabat
metall
metis
mfem
mgis
microbiomeutil
migrate
minced
mindthegap
miniaero
miniamr
minife
minigan
minighost
minigmg
minimap2
minimd
miniqmc
minisign
minismac2d
minitri
minivite
minixyce
mira
mirdeep2
mitos
mixcr
mmg
moab
modern-wheel
modylas
mofem-cephas
mofem-fracture-module
mofem-minimal-surface-equation
mofem-users-modules
molcas
mongo-c-driver
mongo-cxx-driver
mono
mosh
mothur
motif
motioncor2
mpark-variant
mpe2
mpi-bash
mpich
mpifileutils
mpilander
mpileaks
mpip
mpix-launch-swift
mrbayes
mrbench
mrcpp
mrnet
mrtrix3
mscgen
msgpack-c
msmc
mstk
mt-metis
multiverso
mummer
mumps
muparser
muster
mutationpp
mvapich2
mxnet
mysql
nalu
nalu-wind
namd
nanoflann
nanomsg
nanopb
ncbi-toolkit
nccl
nccl-tests
nccmp
ncl
nco
ncview
nek5000
nekbone
nekcem
nektar
nektools
neo4j
neovim
nest
netcdf-c
netcdf-cxx
netcdf-cxx4
netcdf-fortran
netgauge
netgen
netlib-lapack
netlib-scalapack
neuron
nextflow
nfft
nfs-ganesha
nghttp2
ngmlr
nix
nlohmann-json
nlopt
nnvm
notmuch
npb
nrm
ns-3-dev
nsimd
ntirpc
ntpoly
numamma
numap
nut
nwchem
oce
oclint
octave
octave-optim
octave-splines
octave-struct
octopus
of-catalyst
of-precice
omega-h
ompss
ompt-openmp
onednn
ont-albacore
openbabel
opencascade
opencoarrays
opencv
opendx
openfast
openfdtd
openfoam
openfoam-org
openimageio
openjpeg
openkim-models
openmc
openmm
openmolcas
openmpi
openmx
openpbs
openpmd-api
openscenegraph
openslide
openspeedshop
openspeedshop-utils
openstf
opensubdiv
opentsdb
opium
optional-lite
orca
orthofiller
orthofinder
orthomcl
osqp
osu-micro-benchmarks
p3dfft3
p4est
pacbio-dextractor
packmol
pagit
pagmo
panda
pandoc
pango
pangomm
papyrus
paradiseo
parallel-netcdf
paraver
paraview
parmetis
parmgridgen
parquet-cpp
parsplice
partitionfinder
pasta
pbbam
pbmpi
pbsuite
pdf2svg
pegtl
pennant
percept
perfstubs
perl-cairo
perl-dbd-mysql
perl-gd
perl-gdgraph
perl-gdtextutil
petsc
pexsi
pfft
pflotran
pfunit
pgdspider
pgmath
phantompeakqualtools
phast
phasta
phist
phylobayesmpi
phyluce
picard
picsar
picsarlite
pidx
pilon
pindel
piranha
pism
pixz
pktools
planck-likelihood
plasma
platypus
plink
plink-ng
plplot
plumed
pmix
pmlib
pnfft
pngwriter
pnmpi
pocl
podio
polymake
poppler
poppler-data
portage
portcullis
powerapi
ppopen-appl-amr-fdm
ppopen-appl-bem
ppopen-appl-bem-at
ppopen-appl-dem-util
ppopen-appl-fdm
ppopen-appl-fdm-at
ppopen-appl-fem
ppopen-appl-fvm
ppopen-math-mp
ppopen-math-vis
prank
precice
preseq
prinseq-lite
prism
prmon
prokka
protobuf
protobuf-c
prrte
pruners-ninja
ps-lite
psi4
pslib
pugixml
pumi
py-abipy
py-absl-py
py-addict
py-adios
py-advancedhtmlparser
py-aenum
py-affine
py-agate
py-agate-dbf
py-agate-excel
py-agate-sql
py-alabaster
py-alembic
py-altgraph
py-amqp
py-ansible
py-antlr4-python3-runtime
py-apache-libcloud
py-apipkg
py-apptools
py-apscheduler
py-argcomplete
py-argparse
py-args
py-arrow
py-arviz
py-asciitree
py-asdf
py-ase
py-asgiref
py-asn1crypto
py-aspy-yaml
py-asserts
py-asteval
py-astor
py-astpretty
py-astroid
py-astropy
py-astropy-helpers
py-astunparse
py-async-generator
py-atomicwrites
py-atropos
py-attrs
py-audioread
py-autopep8
py-auxlib
py-avro
py-avro-json-serializer
py-awesome-slugify
py-babel
py-backports-abc
py-backports-functools-lru-cache
py-backports-shutil-get-terminal-size
py-backports-tempfile
py-backports-weakref
py-basemap
py-basis-set-exchange
py-bcbio-gff
py-bcrypt
py-beautifulsoup4
py-billiard
py-binaryornot
py-bintrees
py-binwalk
py-biom-format
py-biomine
py-biopandas
py-biopython
py-bitarray
py-black
py-bleach
py-blessed
py-blessings
py-blinker
py-blis
py-blosc
py-bokeh
py-boltons
py-boto3
py-botocore
py-bottleneck
py-breakseq2
py-breathe
py-brian
py-brian2
py-brotlipy
py-bsddb3
py-bx-python
py-cached-property
py-cachetools
py-cairocffi
py-cartopy
py-catalogue
py-cclib
py-cdat-lite
py-cdo
py-cdsapi
py-celery
py-certifi
py-certipy
py-cf-units
py-cffi
py-cfgv
py-cftime
py-chai
py-chainer
py-chardet
py-checkm-genome
py-cheroot
py-cherrypy
py-cinema-lib
py-click
py-click-plugins
py-cligj
py-clint
py-clipboard
py-cloudpickle
py-clustershell
py-cmake-format
py-cnvkit
py-codecov
py-cogent
py-coilmq
py-colorama
py-coloredlogs
py-colorlog
py-colormath
py-colorpy
py-configparser
py-contextlib2
py-convertdate
py-cookiecutter
py-counter
py-cov-core
py-coverage
py-crispresso
py-crossmap
py-cryptography
py-csvkit
py-current
py-cutadapt
py-cvxopt
py-cvxpy
py-cycler
py-cymem
py-cython
py-cyvcf2
py-d2to1
py-dask
py-dataclasses
py-dateparser
py-dbfread
py-decorator
py-deeptools
py-deeptoolsintervals
py-defusedxml
py-dendropy
py-deprecated
py-deprecation
py-descartes
py-dgl
py-dill
py-diskcache
py-distributed
py-django
py-dlcpar
py-dnaio
py-docopt
py-docutils
py-docutils-stubs
py-doxypypy
py-dryscrape
py-dxchange
py-dxfile
py-earthengine-api
py-easybuild-easyblocks
py-easybuild-easyconfigs
py-easybuild-framework
py-ecdsa
py-ecos
py-edffile
py-editdistance
py-elasticsearch
py-elephant
py-emcee
py-entrypoints
py-enum34
py-envisage
py-espresso
py-espressopp
py-et-xmlfile
py-eventlet
py-execnet
py-exodus-bundler
py-extras
py-fastaindex
py-fastcache
py-fastcluster
py-fasteners
py-faststructure
py-filemagic
py-fiona
py-fiscalyear
py-fits-tools
py-fixtures
py-flake8
py-flake8-import-order
py-flake8-polyfill
py-flask
py-flask-compress
py-flask-socketio
py-flexx
py-flye
py-fn-py
py-fparser
py-freezegun
py-fsspec
py-funcsigs
py-fusepy
py-future
py-futures
py-fypp
py-gast
py-gcovr
py-gdbgui
py-gdc-client
py-gee-asset-manager
py-geeadd
py-geeup
py-genshi
py-gensim
py-geoalchemy2
py-geopandas
py-gevent
py-gf256
py-git-review
py-gluoncv
py-gnuplot
py-goatools
py-google-api-core
py-google-api-python-client
py-google-auth
py-google-auth-httplib2
py-google-auth-oauthlib
py-google-cloud-core
py-google-cloud-storage
py-google-pasta
py-google-resumable-media
py-googleapis-common-protos
py-gpaw
py-graphviz
py-grequests
py-griddataformats
py-grpcio
py-guidata
py-guiqwt
py-h5glance
py-h5py
py-h5sh
py-hacking
py-hatchet
py-hdfs
py-heapdict
py-hepdata-validator
py-horovod
py-hpcbench
py-hpccm
py-html2text
py-html5lib
py-htmlgen
py-htseq
py-httpbin
py-httplib2
py-humanfriendly
py-humanize
py-hvac
py-hypothesis
py-ics
py-identify
py-idna
py-illumina-utils
py-imageio
py-imagesize
py-iminuit
py-importlib-metadata
py-importlib-resources
py-intervaltree
py-invoke
py-ipaddress
py-ipdb
py-ipykernel
py-ipython
py-ipywidgets
py-irpf90
py-isodate
py-isort
py-itsdangerous
py-jaraco-functools
py-jdcal
py-jedi
py-jellyfish
py-jinja2
py-jinja2-time
py-jmespath
py-joblib
py-jplephem
py-jprops
py-jpype1
py-json5
py-jsonpatch
py-jsonpointer
py-jsonschema
py-junit-xml
py-jupyter
py-jupyter-client
py-jupyter-console
py-jupyter-core
py-jupyterhub
py-jupyterlab
py-jupyterlab-server
py-keras
py-keras-applications
py-keras-preprocessing
py-kitchen
py-kiwisolver
py-kmodes
py-kombu
py-lark-parser
py-latexcodec
py-lazy
py-lazy-object-proxy
py-lazy-property
py-lazyarray
py-leather
py-libconf
py-libensemble
py-librosa
py-ligo-segments
py-line-profiler
py-linecache2
py-lit
py-llvmlite
py-lmfit
py-localcider
py-lockfile
py-logilab-common
py-louie
py-lru-dict
py-lscsoft-glue
py-luigi
py-lxml
py-lzstring
py-m2r
py-macholib
py-machotools
py-macs2
py-maestrowf
py-magic
py-mako
py-markdown
py-markupsafe
py-matplotlib
py-mayavi
py-mccabe
py-mdanalysis
py-mechanize
py-memory-profiler
py-merlin
py-metasv
py-methylcode
py-mg-rast-tools
py-misopy
py-mistune
py-mlxtend
py-mmcv
py-mo-pack
py-mock
py-modred
py-moltemplate
py-monotonic
py-monty
py-more-itertools
py-mpi4py
py-mpld3
py-msgpack
py-multiprocess
py-multiqc
py-munch
py-murmurhash
py-myhdl
py-mypy
py-mypy-extensions
py-mysql-connector-python
py-mysqlclient
py-mysqldb1
py-natsort
py-nbconvert
py-nbformat
py-nc-time-axis
py-neo
py-neobolt
py-neotime
py-nestle
py-netcdf4
py-netifaces
py-networkx
py-nltk
py-nodeenv
py-nose
py-nose-cov
py-nose2
py-nosexcover
py-notebook
py-numba
py-numcodecs
py-numexpr
py-numexpr3
py-numpy
py-numpydoc
py-oauth2client
py-oauthlib
py-onnx
py-ont-fast5-api
py-openpmd-validator
py-openpyxl
py-openslide-python
py-opentuner
py-opppy
py-opt-einsum
py-oset
py-osqp
py-overpy
py-owslib
py-packaging
py-palettable
py-pamela
py-pandas
py-parameterized
py-paramiko
py-parse
py-parsedatetime
py-parso
py-partd
py-path-py
py-pathlib2
py-pathos
py-pathspec
py-patsy
py-pauvre
py-pbr
py-pep8
py-pep8-naming
py-performance
py-periodictable
py-petastorm
py-petsc4py
py-phonopy
py-pickleshare
py-picrust
py-pid
py-pillow
py-pint
py-pip
py-pipits
py-pispino
py-pkgconfig
py-pkginfo
py-plac
py-plotly
py-pluggy
py-pomegranate
py-portalocker
py-portend
py-poster
py-pox
py-poyo
py-ppft
py-pre-commit
py-preshed
py-prettytable
py-profilehooks
py-progress
py-progressbar2
py-projectq
py-prometheus-client
py-prompt-toolkit
py-protobuf
py-psutil
py-psyclone
py-psycopg2
py-pudb
py-py
py-py-cpuinfo
py-py2bit
py-py2cairo
py-py2neo
py-py4j
py-pyani
py-pyarrow
py-pyasn1
py-pyasn1-modules
py-pybedtools
py-pybigwig
py-pybind11
py-pybtex
py-pybtex-docutils
py-pycairo
py-pycares
py-pycbc
py-pycifrw
py-pycmd
py-pycodestyle
py-pycparser
py-pycuda
py-pycurl
py-pydeps
py-pydispatcher
py-pydot
py-pydot2
py-pydotplus
py-pydv
py-pyeda
py-pyelftools
py-pyepsg
py-pyface
py-pyfaidx
py-pyfasta
py-pyfftw
py-pyfits
py-pyflakes
py-pygdal
py-pygdbmi
py-pygit2
py-pyglet
py-pygments
py-pygobject
py-pygpu
py-pygresql
py-pygtk
py-pyinstrument
py-pyinstrument-cext
py-pyjwt
py-pykml
py-pykwalify
py-pyliblzma
py-pylint
py-pymatgen
py-pymc3
py-pymeeus
py-pyminifier
py-pymol
py-pymongo
py-pymorph
py-pymysql
py-pynio
py-pynn
py-pyodbc
py-pyomo
py-pyopenssl
py-pypar
py-pyparsing
py-pypeflow
py-pyperclip
py-pyperf
py-pyprof2html
py-pyproj
py-pyqi
py-pyqt4
py-pyqt5
py-pyquaternion
py-pyrad
py-pyrosar
py-pyrsistent
py-pysam
py-pyscaf
py-pyserial
py-pyshp
py-pyside
py-pyside2
py-pysmartdl
py-pysocks
py-pyspark
py-pytailf
py-pytest
py-pytest-cache
py-pytest-check-links
py-pytest-cov
py-pytest-flake8
py-pytest-forked
py-pytest-httpbin
py-pytest-isort
py-pytest-mock
py-pytest-mypy
py-pytest-pep8
py-pytest-runner
py-pytest-xdist
py-python-daemon
py-python-dateutil
py-python-editor
py-python-engineio
py-python-gitlab
py-python-igraph
py-python-jenkins
py-python-ldap
py-python-levenshtein
py-python-magic
py-python-mapnik
py-python-meep
py-python-memcached
py-python-oauth2
py-python-rapidjson
py-python-slugify
py-python-socketio
py-python-subunit
py-python-swiftclient
py-python-utils
py-pythonqwt
py-pytimeparse
py-pytools
py-pytz
py-pyudev
py-pyugrid
py-pyutilib
py-pyvcf
py-pywavelets
py-pywcs
py-pyzmq
py-qtawesome
py-qtconsole
py-qtpy
py-quantities
py-quast
py-queryablelist
py-radical-utils
py-rasterio
py-ratelim
py-raven
py-readme-renderer
py-redis
py-regex
py-repoze-lru
py-requests
py-requests-futures
py-requests-mock
py-requests-oauthlib
py-requests-toolbelt
py-resampy
py-restview
py-retrying
py-rnacocktail
py-rope
py-rpy2
py-rsa
py-rseqc
py-rtree
py-ruamel-yaml
py-s3cmd
py-s3transfer
py-sacremoses
py-saga-python
py-scandir
py-scientificpython
py-scikit-build
py-scikit-image
py-scikit-learn
py-scikit-optimize
py-scipy
py-scoop
py-scp
py-scs
py-seaborn
py-selenium
py-semantic-version
py-semver
py-send2trash
py-sentencepiece
py-setproctitle
py-setuptools
py-setuptools-git
py-setuptools-rust
py-setuptools-scm
py-setuptools-scm-git-archive
py-sfepy
py-sh
py-shapely
py-shiboken
py-shroud
py-simplegeneric
py-simplejson
py-singledispatch
py-slepc4py
py-slurm-pipeline
py-smart-open
py-sncosmo
py-snowballstemmer
py-snuggs
py-sonlib
py-sortedcontainers
py-soundfile
py-soupsieve
py-spacy
py-spacy-models-en-core-web-sm
py-spatialist
py-spatialite
py-spdlog
py-spectra
py-spefile
py-spglib
py-sphinx
py-sphinx-bootstrap-theme
py-sphinx-rtd-theme
py-sphinxautomodapi
py-sphinxcontrib-applehelp
py-sphinxcontrib-bibtex
py-sphinxcontrib-devhelp
py-sphinxcontrib-htmlhelp
py-sphinxcontrib-issuetracker
py-sphinxcontrib-jsmath
py-sphinxcontrib-programoutput
py-sphinxcontrib-qthelp
py-sphinxcontrib-serializinghtml
py-sphinxcontrib-websupport
py-spyder
py-spykeutils
py-sqlalchemy
py-sqlparse
py-srsly
py-statsmodels
py-stdlib-list
py-stestr
py-stevedore
py-storm
py-stratify
py-stsci-distutils
py-subprocess32
py-subrosa
py-svgpathtools
py-svgwrite
py-symengine
py-symfit
py-syned
py-tables
py-tabulate
py-tap-py
py-tatsu
py-tblib
py-tempora
py-tensorboard
py-tensorboard-plugin-wit
py-tensorboardx
py-tensorflow
py-tensorflow-estimator
py-terminado
py-testinfra
py-testrepository
py-testresources
py-testscenarios
py-testtools
py-tetoolkit
py-text-unidecode
py-texttable
py-tfdlpack
py-theano
py-thinc
py-thirdorder
py-tifffile
py-tokenizers
py-toml
py-tomopy
py-toolz
py-torch
py-torch-nvidia-apex
py-torchaudio
py-torchsummary
py-torchtext
py-torchvision
py-tornado
py-tox
py-tqdm
py-traceback2
py-traitlets
py-traits
py-traitsui
py-transformers
py-tuiview
py-twine
py-twisted
py-typed-ast
py-typesentry
py-typing
py-typing-extensions
py-tzlocal
py-ujson
py-umalqurra
py-umi-tools
py-unicycler
py-unidecode
py-unittest2
py-unittest2py3k
py-uritemplate
py-urllib3
py-urwid
py-us
py-usgs
py-utils
py-uwsgi
py-vcf-kit
py-vcversioner
py-vermin
py-versioneer
py-vine
py-virtualenv
py-virtualenv-clone
py-virtualenvwrapper
py-voluptuous
py-vsc-base
py-vsc-install
py-wand
py-warlock
py-wasabi
py-wcsaxes
py-wcwidth
py-weave
py-webencodings
py-weblogo
py-werkzeug
py-whatshap
py-wheel
py-whichcraft
py-widgetsnbextension
py-wradlib
py-wub
py-wxmplot
py-wxpython
py-xarray
py-xattr
py-xdot
py-xenv
py-xlwt
py-xmlrunner
py-xmltodict
py-xopen
py-xvfbwrapper
py-yahmm
py-yajl
py-yamlreader
py-yapf
py-youtube-dl
py-yt
py-ytopt
py-zarr
py-zc-buildout
py-zc-lockfile
py-zict
py-zipp
py-zope-event
py-zope-interface
py-zxcvbn
pythia6
qbox
qca
qemu
qgis
qhull
qjson
qmcpack
qmd-progress
qnnpack
qorts
qrupdate
qscintilla
qt
qt-creator
qtgraph
qthreads
qtkeychain
quantum-espresso
quicksilver
quinoa
qwt
qwtpolar
r
r-a4
r-a4base
r-a4classif
r-a4core
r-a4preproc
r-a4reporting
r-abadata
r-abaenrichment
r-abind
r-absseq
r-acde
r-acepack
r-acgh
r-acme
r-ada
r-adabag
r-ade4
r-adegenet
r-adgoftest
r-adsplit
r-aer
r-affxparser
r-affy
r-affycomp
r-affycompatible
r-affycontam
r-affycoretools
r-affydata
r-affyexpress
r-affyilm
r-affyio
r-affypdnn
r-affyplm
r-affyqcreport
r-affyrnadegradation
r-agdex
r-agilp
r-agimicrorna
r-aims
r-aldex2
r-allelicimbalance
r-alpine
r-als
r-alsace
r-altcdfenvs
r-amap
r-ampliqueso
r-analysispageserver
r-anaquin
r-aneufinder
r-aneufinderdata
r-animation
r-annaffy
r-annotate
r-annotationdbi
r-annotationfilter
r-annotationforge
r-annotationhub
r-aod
r-ape
r-argparse
r-aroma-light
r-askpass
r-assertthat
r-backports
r-bamsignals
r-base64
r-base64enc
r-bayesm
r-bbmisc
r-beachmat
r-beanplot
r-beeswarm
r-bfast
r-bfastspatial
r-bglr
r-bh
r-biasedurn
r-bibtex
r-bindr
r-bindrcpp
r-bio3d
r-biobase
r-biocfilecache
r-biocgenerics
r-biocinstaller
r-biocmanager
r-biocneighbors
r-biocparallel
r-biocsingular
r-biocstyle
r-biom-utils
r-biomart
r-biomartr
r-biomformat
r-biostrings
r-biovizbase
r-bit
r-bit64
r-bitops
r-blob
r-blockmodeling
r-bmp
r-bookdown
r-boot
r-boruta
r-brew
r-broom
r-bsgenome
r-bsgenome-hsapiens-ucsc-hg19
r-bumphunter
r-c50
r-cairo
r-callr
r-car
r-caracas
r-cardata
r-caret
r-caroline
r-category
r-catools
r-cdcfluview
r-cellranger
r-checkmate
r-checkpoint
r-chemometrics
r-chron
r-circlize
r-class
r-classint
r-cli
r-clipr
r-clisymbols
r-clue
r-cluster
r-clustergeneration
r-clusterprofiler
r-cner
r-coda
r-codetools
r-codex
r-coin
r-colorspace
r-combinat
r-commonmark
r-complexheatmap
r-compositions
r-condop
r-construct
r-convevol
r-copula
r-corhmm
r-corpcor
r-corrplot
r-covr
r-cowplot
r-crayon
r-crosstalk
r-crul
r-ctc
r-cubature
r-cubist
r-curl
r-dada2
r-data-table
r-dbi
r-dbplyr
r-debugme
r-decipher
r-delayedarray
r-delayedmatrixstats
r-deldir
r-dendextend
r-deoptim
r-deoptimr
r-desc
r-deseq
r-deseq2
r-desolve
r-devtools
r-diagrammer
r-dicekriging
r-dichromat
r-diffusionmap
r-digest
r-diptest
r-dirichletmultinomial
r-dismo
r-diversitree
r-dnacopy
r-do-db
r-domc
r-doparallel
r-dorng
r-dose
r-dosnow
r-dotcall64
r-downloader
r-dplyr
r-dqrng
r-dt
r-dtw
r-dygraphs
r-dynamictreecut
r-e1071
r-earth
r-ecp
r-edger
r-ellipse
r-ellipsis
r-emmli
r-energy
r-enrichplot
r-ensembldb
r-envstats
r-ergm
r-europepmc
r-evaluate
r-evd
r-exactextractr
r-exomecopy
r-exomedepth
r-expint
r-expm
r-factoextra
r-factominer
r-fansi
r-farver
r-fastcluster
r-fastmatch
r-fdb-infiniummethylation-hg18
r-fdb-infiniummethylation-hg19
r-ff
r-fftwtools
r-fgsea
r-fields
r-filehash
r-findpython
r-fit-models
r-fitdistrplus
r-flashclust
r-flexclust
r-flexmix
r-fnn
r-forcats
r-foreach
r-forecast
r-foreign
r-formatr
r-formula
r-fpc
r-fracdiff
r-fs
r-futile-logger
r-futile-options
r-future
r-future-apply
r-gamlss
r-gamlss-data
r-gamlss-dist
r-gbm
r-gbrd
r-gcrma
r-gdalutils
r-gdata
r-gdsfmt
r-geiger
r-genefilter
r-genelendatabase
r-genemeta
r-geneplotter
r-generics
r-genetics
r-genie3
r-genomeinfodb
r-genomeinfodbdata
r-genomicalignments
r-genomicfeatures
r-genomicranges
r-gensa
r-geomorph
r-geonames
r-geoquery
r-geor
r-geosphere
r-getopt
r-getoptlong
r-ggally
r-ggbeeswarm
r-ggbio
r-ggdendro
r-ggforce
r-ggjoy
r-ggmap
r-ggplot2
r-ggplotify
r-ggpubr
r-ggraph
r-ggrepel
r-ggridges
r-ggsci
r-ggsignif
r-ggvis
r-gh
r-gistr
r-git2r
r-glimma
r-glmnet
r-globaloptions
r-globals
r-glue
r-gmodels
r-gmp
r-go-db
r-goftest
r-gofuncr
r-googlevis
r-goplot
r-gosemsim
r-goseq
r-gostats
r-gower
r-gplots
r-graph
r-graphlayouts
r-grbase
r-gridbase
r-gridextra
r-gridgraphics
r-gsa
r-gsalib
r-gseabase
r-gsl
r-gss
r-gstat
r-gsubfn
r-gtable
r-gtools
r-gtrellis
r-gviz
r-gwmodel
r-haven
r-hdf5array
r-hdf5r
r-hexbin
r-highr
r-hmisc
r-hms
r-hoardr
r-htmltable
r-htmltools
r-htmlwidgets
r-httpcode
r-httpuv
r-httr
r-hwriter
r-hypergraph
r-ica
r-igraph
r-illuminahumanmethylation450kanno-ilmn12-hg19
r-illuminaio
r-imager
r-impute
r-influencer
r-ini
r-inline
r-interactivedisplaybase
r-intervals
r-inum
r-ipred
r-iranges
r-irdisplay
r-irkernel
r-irlba
r-isdparser
r-iso
r-iterators
r-janitor
r-jaspar2018
r-jomo
r-jpeg
r-jsonlite
r-kegg-db
r-kegggraph
r-keggrest
r-kernlab
r-kernsmooth
r-kknn
r-knitr
r-ks
r-labeling
r-lambda-r
r-laplacesdemon
r-lars
r-later
r-lattice
r-latticeextra
r-lava
r-lazyeval
r-ldheatmap
r-leaflet
r-leaps
r-learnbayes
r-leiden
r-lfe
r-lhs
r-libcoin
r-limma
r-listenv
r-lme4
r-lmtest
r-locfit
r-log4r
r-loo
r-lpsolve
r-lsei
r-lubridate
r-lumi
r-magic
r-magick
r-magrittr
r-makecdfenv
r-maldiquant
r-manipulatewidget
r-mapplots
r-mapproj
r-maps
r-maptools
r-markdown
r-mass
r-matlab
r-matr
r-matrix
r-matrixmodels
r-matrixstats
r-mclust
r-mcmcglmm
r-mco
r-mda
r-memoise
r-mergemaid
r-metap
r-methylumi
r-mgcv
r-mgraster
r-mice
r-mime
r-minfi
r-miniui
r-minqa
r-misc3d
r-mitml
r-mitools
r-mixtools
r-mlbench
r-mlinterfaces
r-mlr
r-mlrmbo
r-mmwrweek
r-mnormt
r-modelmetrics
r-modelr
r-modeltools
r-mpm
r-msnbase
r-multcomp
r-multicool
r-multitaper
r-multtest
r-munsell
r-mvtnorm
r-mzid
r-mzr
r-nanotime
r-ncbit
r-ncdf4
r-network
r-networkd3
r-nimble
r-nleqslv
r-nlme
r-nloptr
r-nmf
r-nmof
r-nnet
r-nnls
r-nor1mix
r-nortest
r-np
r-npsurv
r-numderiv
r-oligoclasses
r-openssl
r-openxlsx
r-optparse
r-ordinal
r-org-hs-eg-db
r-organismdbi
r-packrat
r-pacman
r-paleotree
r-pamr
r-pan
r-parallelmap
r-paramhelpers
r-party
r-partykit
r-pathview
r-pbapply
r-pbdzmq
r-pbkrtest
r-pcamethods
r-pcapp
r-permute
r-pfam-db
r-phangorn
r-phantompeakqualtools
r-philentropy
r-phyloseq
r-phylostratr
r-phytools
r-picante
r-pillar
r-pkgbuild
r-pkgconfig
r-pkgload
r-pkgmaker
r-plogr
r-plot3d
r-plotly
r-plotmo
r-plotrix
r-pls
r-plyr
r-pmcmr
r-png
r-polspline
r-polyclip
r-polynom
r-popgenome
r-popvar
r-powerlaw
r-prabclus
r-praise
r-preprocesscore
r-prettyunits
r-processx
r-prodlim
r-progress
r-proj
r-proj4
r-promises
r-protgenerics
r-proto
r-proxy
r-pryr
r-ps
r-pscbs
r-pspline
r-psych
r-ptw
r-purrr
r-qtl
r-quadprog
r-quantmod
r-quantreg
r-quantro
r-qvalue
r-r-cache
r-r-methodss3
r-r-oo
r-r-utils
r-r6
r-randomfields
r-randomfieldsutils
r-randomforest
r-randomglm
r-ranger
r-rann
r-rappdirs
r-raster
r-rbgl
r-rbokeh
r-rcmdcheck
r-rcolorbrewer
r-rcpp
r-rcppannoy
r-rcpparmadillo
r-rcppblaze
r-rcppcctz
r-rcppcnpy
r-rcppeigen
r-rcpphnsw
r-rcppparallel
r-rcppprogress
r-rcurl
r-rda
r-rdpack
r-readbitmap
r-readr
r-readxl
r-recipes
r-registry
r-rematch
r-rematch2
r-remotes
r-reordercluster
r-reportingtools
r-repr
r-reprex
r-reshape
r-reshape2
r-reticulate
r-rex
r-rferns
r-rgdal
r-rgenoud
r-rgeos
r-rgexf
r-rgl
r-rgooglemaps
r-rgraphviz
r-rhdf5
r-rhdf5lib
r-rhmmer
r-rhtslib
r-rinside
r-rio
r-rjags
r-rjava
r-rjson
r-rjsonio
r-rlang
r-rmariadb
r-rmarkdown
r-rminer
r-rmpfr
r-rmpi
r-rms
r-rmutil
r-rmysql
r-rnaseqmap
r-rngtools
r-rnoaa
r-robust
r-robustbase
r-roc
r-rocr
r-rodbc
r-rook
r-rots
r-roxygen2
r-rpart
r-rpart-plot
r-rpostgresql
r-rprojroot
r-rrblup
r-rrcov
r-rrpp
r-rsamtools
r-rsnns
r-rsolnp
r-rspectra
r-rsqlite
r-rstan
r-rstantools
r-rstudioapi
r-rsvd
r-rtracklayer
r-rtsne
r-runit
r-rvcheck
r-rversions
r-rvest
r-rzmq
r-s4vectors
r-samr
r-sandwich
r-scales
r-scater
r-scatterplot3d
r-scrime
r-sctransform
r-sdmtools
r-segmented
r-selectr
r-seqinr
r-seqlogo
r-sessioninfo
r-seurat
r-sf
r-sfsmisc
r-shape
r-shiny
r-shinydashboard
r-shinyfiles
r-shortread
r-siggenes
r-simpleaffy
r-singlecellexperiment
r-sitmo
r-sm
r-smoof
r-sn
r-snakecase
r-snow
r-snowfall
r-snprelate
r-snpstats
r-som
r-somaticsignatures
r-sourcetools
r-sp
r-spacetime
r-spam
r-sparsem
r-spatial
r-spatialeco
r-spatialpack
r-spatialreg
r-spatstat
r-spatstat-data
r-spatstat-utils
r-spdata
r-spdep
r-speedglm
r-spem
r-splancs
r-splitstackshape
r-sqldf
r-squarem
r-squash
r-sseq
r-stabledist
r-stanheaders
r-stargazer
r-statmod
r-statnet-common
r-stringi
r-stringr
r-strucchange
r-subplex
r-summarizedexperiment
r-suppdists
r-survey
r-survival
r-sva
r-sys
r-tarifx
r-taxizedb
r-tclust
r-teachingdemos
r-tensor
r-tensora
r-testit
r-testthat
r-tfbstools
r-tfmpvalue
r-th-data
r-threejs
r-tibble
r-tidycensus
r-tidygraph
r-tidyr
r-tidyselect
r-tidyverse
r-tiff
r-tigris
r-timedate
r-tinytex
r-tmixclust
r-topgo
r-triebeard
r-trimcluster
r-truncdist
r-truncnorm
r-trust
r-tseries
r-tsne
r-ttr
r-tweenr
r-txdb-hsapiens-ucsc-hg18-knowngene
r-txdb-hsapiens-ucsc-hg19-knowngene
r-tximport
r-ucminf
r-udunits2
r-units
r-upsetr
r-urca
r-urltools
r-usethis
r-utf8
r-uuid
r-uwot
r-variantannotation
r-varselrf
r-vcd
r-vctrs
r-vegan
r-vfs
r-vgam
r-vioplot
r-vipor
r-viridis
r-viridislite
r-visnetwork
r-vsn
r-watermelon
r-webshot
r-wgcna
r-whisker
r-withr
r-xde
r-xfun
r-xgboost
r-xlconnect
r-xlconnectjars
r-xlsx
r-xlsxjars
r-xmapbridge
r-xml
r-xml2
r-xnomial
r-xopen
r-xtable
r-xts
r-xvector
r-yaimpute
r-yaml
r-yapsa
r-yaqcaffy
r-yarn
r-zeallot
r-zip
r-zlibbioc
r-zoo
racon
raft
raja
range-v3
rankstr
rapidjson
ravel
raxml
ray
rclone
rdma-core
rdp-classifier
rdptools
redset
redundans
regcm
relax
relion
remhos
rempi
repeatmasker
repeatmodeler
revbayes
revocap-coupler
revocap-refiner
rgb
ripgrep
rmats
rmlab
rocketmq
rocksdb
rockstar
rodinia
root
rose
ross
rr
rsem
rtags
ruby-gnuplot
ruby-svn2git
rust
rust-bindgen
saga-gis
sailfish
salmon
sambamba
samrai
samtools
sas
satsuma2
savanna
sbml
sbt
scala
scalasca
scallop
scons
scorec-core
scorep
scotch
scr
scs
sdl2
sdl2-image
sdsl-lite
seacas
sensei
sentencepiece
seqan
serf
sfcgal
sga
sgpp
shared-mime-info
shark
shengbte
shiny-server
shoremap
shortbred
shortstack
shtools
shuffile
siesta
signalp
silo
simgrid
simplemoc
simul
simulationio
singularity
singularity-legacy
sirius
skilion-onedrive
skopeo
slate
sleef
slepc
slurm
snakemake
snap
snap-korf
snappy
snbone
sniffles
snphylo
sollve
somatic-sniper
sortmerna
sosflow
source-highlight
spades
span-lite
spark
sparta
spath
spdlog
spfft
spglib
sph2pipe
spindle
spiral
spot
sqlitebrowser
sqoop
ssht
sst-core
sst-elements
sst-macro
sst-transports
stat
stc
steps
stinger
storm
strelka
string-view-lite
stringtie
strumpack
su2
sublime-text
subversion
suite-sparse
sumo
sundials
superlu
superlu-dist
superlu-mt
supernova
sw4lite
swap-assembler
swfft
swftools
swiftsim
swipl
symengine
sympol
sz
tajo
talass
targetp
task
taskd
tasmanian
tassel
tau
tcoffee
tealeaf
templight
templight-tools
tensorflow-serving-client
testdfsio
tethex
texlive
texstudio
textparser
tfel
the-platinum-searcher
thornado-mini
thrift
timemory
tinker
tinyxml
tinyxml2
tioga
tixi
tophat
tppred
tracer
transabyss
transposome
treesub
trident
trilinos
trilinos-catalyst-ioss-adapter
trimgalore
trimmomatic
trinity
trinotate
trnascan-se
tulip
turbine
tycho2
typhon
typhonio
uchardet
ucx
ufo-core
ufo-filters
umap
umoci
umpire
unblur
uncrustify
unifyfs
unittest-cpp
unqlite
unuran
upcxx
uqtk
uriparser
utf8proc
uvw
valgrind
vampirtrace
vardictjava
variorum
varscan
vc
vcsh
vdt
veccore
vecgeom
vegas2
veloc
vesta
viennarna
vigra
virtualgl
visit
vizglow
votca-csg
votca-csg-tutorials
votca-csgapps
votca-ctp
votca-tools
votca-xtp
vpfft
vpic
vtk
vtk-h
vtk-m
wannier90
warpx
wcslib
wget
wireshark
workrave
wt
wxpropgrid
wxwidgets
xbraid
xcfun
xdmf3
xeus
xgboost
xhmm
xios
xorg-gtest
xorg-server
xrootd
xsbench
xsdk
xsdk-examples
xsdktrilinos
xsimd
xssp
xtensor
xtensor-python
xtl
yajl
yambo
yaml-cpp
z3
zfp
zoltan
zookeeper-benchmark
