#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"

### BLAS-libs ###
BLASlibs="atlas
amdblis
openblas
blis
py-blis
eigen
essl
intel-mkl
netlib-xblas
netlib-lapack
netlib-scalapack
veclibfort
libxsmm
cuda"
# accelerate (missing)
# acml (missing)
# amp (false positive)
# cublas (missing)
# nvblas (missing)
# clblas (missing)
# clblast (missing)
# gotoblas (missing)
# mlib (false positive)
# keisan (missing)
# pdlib (missing)
# scsl (missing)
# blaspp (just lang. interface)
# cblas (just lang. interface)
# r-rcppeigen (just lang. interface)
# clapack (just lang. interface)
# lapackpp (just lang. interface)

### BLAS-like ###
#BLASlike="armadillo
#elemental
#gsl
#fgsl
#r-gsl
#libflame
#magma
#plasma" (all link to BLAS -> covered by 1. degree dep)
# acl (false positive)
# hasem (missing)
# lama (missing)
# mir (missing)
# mtl4 (missing)
# ejml (missing)
# eispack (missing)
# librsb (for sparse -> ignore)

if ! [ -f $ROOTDIR/dep/spack/share/spack/setup-env.sh ];
then
	echo "ERR: spack not found in dep/ as expected!"
	exit
fi

source $ROOTDIR/dep/spack/share/spack/setup-env.sh
rm -f ./blaslibs_*_degree.txt ./_b_*_d

# dep.dist. 0
for lib in ${BLASlibs};
do
	echo ${lib} >> ./_b_0_d
done
sort -u ./_b_0_d > ./blaslibs_0_degree.txt

# dep.dist. 1
for lib in `cat ./blaslibs_0_degree.txt`;
do
	# spack dependents includes optional dependents as well
	for dep in `spack dependents ${lib} | /usr/bin/grep -v 'No dependents'`;
	do
		if ! /usr/bin/grep -q "${dep}" ./_b_1_d ;
		then
			echo ${dep} >> ./_b_1_d
		fi
	done
done
sort -u ./_b_1_d > ./blaslibs_1_degree.txt

# dep.dist. 2
for lib in `cat ./blaslibs_1_degree.txt`;
do
	for dep in `spack dependents ${lib} | /usr/bin/grep -v 'No dependents'`;
	do
		if ! /usr/bin/grep -q "${dep}" ./_b_2_d ;
		then
			echo ${dep} >> ./_b_2_d
		fi
	done
done
sort -u ./_b_2_d > ./blaslibs_2_degree.txt

# dep.dist. 3
for lib in `cat ./blaslibs_2_degree.txt`;
do
	for dep in `spack dependents ${lib} | /usr/bin/grep -v 'No dependents'`;
	do
		if ! /usr/bin/grep -q "${dep}" ./_b_3_d ;
		then
			echo ${dep} >> ./_b_3_d
		fi
	done
done
sort -u ./_b_3_d > ./blaslibs_3_degree.txt

for lib in ${BLASlibs};
do
	spack dependents -t ${lib} | /usr/bin/grep -v 'No dependents' >> ./_b_all_d
done
sort -u ./_b_all_d > ./blaslibs_all_degree.txt
rm -f ./_b_*_d
