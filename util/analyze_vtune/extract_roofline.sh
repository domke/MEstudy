#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"

# Make sure you have installed Intel advisor.
source $ROOTDIR/dep/intel-advisor/advisor_2020/advixe-vars.sh

SPEC_LOG_ROOT="$ROOTDIR/log/`hostname -s`/profrun"

ARCHS=(cpu omp)
for arch in ${ARCHS[@]}; do
	echo arch=${arch}
	LOGDIR=${SPEC_LOG_ROOT}/spec_${arch}
	for app in `ls ${LOGDIR} | /usr/bin/grep \\.vtune$`; do
		echo app=${app}
		rm -rf ${LOGDIR}/${app}/pp; mkdir -p ${LOGDIR}/${app}/pp
		advixe-cl \
			--report=roofline \
			--project-dir=${LOGDIR}/${app} \
			--data-type=mixed \
			--no-with-stack \
			--memory-level=L1_L2_L3_DRAM \
			--memory-operation-type=all \
			--threading-model=openmp \
			--report-output=${LOGDIR}/${app}/pp/${app}.html >/dev/null 2>&1
		$ROOTDIR/util/analyze_vtune/html2csv.py ${LOGDIR}/${app}/pp/
	done
done

ARCHS=(mpi)
for arch in ${ARCHS[@]}; do
	echo arch=${arch}
	LOGDIR=${SPEC_LOG_ROOT}/spec_${arch}
	for app in `ls ${LOGDIR} | /usr/bin/grep \\.vtune$`; do
		echo app=${app}
		rm -rf ${LOGDIR}/${app}/pp; mkdir -p ${LOGDIR}/${app}/pp
		ranks=`ls ${LOGDIR}/${app} | /usr/bin/grep rank\\. | wc -l`
		for R in `seq 0 $((ranks-1))`; do
			echo rank=${R}
			advixe-cl \
				--report=roofline \
				--project-dir=${LOGDIR}/${app} \
				--data-type=mixed \
				--no-with-stack \
				--memory-level=L1_L2_L3_DRAM \
				--memory-operation-type=all \
				--report-output=${LOGDIR}/${app}/pp/rank${R}.${app}.html \
				--mpi-rank=${R} >/dev/null 2>&1
		done
		$ROOTDIR/util/analyze_vtune/html2csv.py ${LOGDIR}/${app}/pp/
	done
done

$ROOTDIR/util/analyze_vtune/csv2fnlist.sh > ${SPEC_LOG_ROOT}/../spec_vtune_tagged.log

