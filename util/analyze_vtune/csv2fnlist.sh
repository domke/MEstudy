#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"

SPEC_LOG_ROOT="$ROOTDIR/log/`hostname -s`/profrun"
ARCHS=(cpu omp mpi)
for arch in ${ARCHS[@]}; do
	echo arch=${arch}
	LOGDIR="${SPEC_LOG_ROOT}/spec_${arch}"
	for app in `ls ${LOGDIR} | /usr/bin/grep \\.vtune$`; do
		echo app=${app}
		LOG="${LOGDIR}/${app}/pp/${app}.csv"
		SEARCHDIR="$ROOTDIR/SPEC_${arch^^}/benchspec/${arch^^}*/${app%.*}/build/build_*vtune.*/"
		for FILE in `/usr/bin/grep -E '^[[:digit:]]+' $LOG | cut -d ',' -f2 | sort -u`; do
			if [ -z "$FILE" ]; then continue; fi
			LINE=`echo $FILE | cut -d ':' -f2`
			FILEPATH=`echo $FILE | cut -d ':' -f1`
			FILEPATHS=`find $SEARCHDIR -type f -name $FILEPATH`
			if [ -z "$FILEPATHS" ]; then
				#only a few stl headers show up like stl_algo.h -> ignore
				continue
			fi
			for FILEPATH in $FILEPATHS; do
				echo -n -e ":b# | bd# | view `echo $FILEPATH | sed -e "s#$ROOTDIR#\\\$ROOTDIR#"` | $LINE\n"
			done
		done
	done
done
