#!/usr/bin/env python3

import json
import math
import os
import re
import sys

import pandas as pd

LOG_DIR = sys.argv[1]

files = [x for x in os.listdir(LOG_DIR) if re.compile(r'\.html$').search(x)]
#files=[sys.argv[1]]
for f in files:
    appName = re.compile(r'(?:rank\d+\.)?(.*)\.html').match(f).group(1)
    print('html2csv for app={}'.format(appName))

    with open(os.path.join(LOG_DIR, f), 'r') as fp:
        lines = fp.read().strip().split('\n')

    df, programTotalElapsedTime = None, None
    for l in lines:
        # All the data points SEEM to be in the "loops" array.
        m = re.compile(r'"loops"\s*:\s*(.*)\s*,\s*"allRoofs"').search(l)
        k = re.compile(r'"programTotal"\s*:\s*(.*)\s*,\s*"loopTypes').search(l)
        if m is None and k is None:
            continue

        if k is not None:
            assert(df is None)
            programTotalElapsedTime = \
                float(
                    json.loads(re.compile(r'(.*)\s*,\s*"roofs"').search(
                        k.group(1)).group(1))['selfElapsedTime'])
            if m is None:
                continue

        def cleanUpLoopEntry(l):
            return {
                'locat': l['locat'] if 'locat' in l.keys() else '',
                'name': l['name'],
                'opPerByte': math.pow(10, l['x']),
                'gigaOps': math.pow(10, l['y']),
                'selfElapsedTime': l['selfElapsedTime'],
                'totalElapsedTime': l['totalElapsedTime'],
                'hasChild': 'children' in l.keys() and len(l['children']) > 0,
            }

        loops = json.loads(m.group(1))
        if len(loops) == 0:
            continue

        assert(programTotalElapsedTime is not None)
        loops = [cleanUpLoopEntry(x) for x in loops]
        df = pd.DataFrame(loops)
        # 7.09: vtune reported (int) compute bound region on Xeon E5-2650 v4
        df = df[df['opPerByte'] >= 7.09]
        df = df.sort_values('opPerByte', ascending=False)
        # lets only focus on loops/fn with Point Weight (PtW) >= 1%
        idx = []
        for index, row in df.iterrows():
            if float(row['selfElapsedTime']) / programTotalElapsedTime < .01:
                idx.append(index)
        if len(idx):
            df = df.drop(index=idx)
        df.reset_index(inplace=True, drop=True)
        break

    if df is None:
        sys.stderr.write('WRN: No roofline data found in {}\n'.format(f))
        # Create an empty table instead.
        df = pd.DataFrame()

    with open(os.path.join(LOG_DIR, '{}.csv'.format(appName)), 'a') as csv_f:
        df.to_csv(csv_f)
