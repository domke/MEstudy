#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"

LOG_ROOT="$ROOTDIR/log/`hostname -s`/profrun"
BMS=(amg babelstream2gb babelstream14gb comd ffb ffvc hpcg hpl laghos miniamr minife minitri modylas mvmc nekbone ngsa nicam ntchem qcd sw4lite swfft xsbench)
for bm in ${BMS[@]}; do
	app=${bm}.log
	echo $app
	LOG="${LOG_ROOT}/${app}.vtune/pp/${app}.csv"
	SEARCHDIR=$(find $ROOTDIR -mindepth 1 -maxdepth 1 -iname "`echo ${bm} | cut -c1-11`*")
	for FILE in `/usr/bin/grep -E '^[[:digit:]]+' $LOG | cut -d ',' -f2 | sort -u`; do
		if [ -z "$FILE" ]; then continue; fi
		LINE=`echo $FILE | cut -d ':' -f2`
		FILEPATH=`echo $FILE | cut -d ':' -f1`
		FILEPATHS=`find $SEARCHDIR -type f -name $FILEPATH`
		if [ -z "$FILEPATHS" ]; then
			#only a few stl headers show up like stl_algo.h -> ignore
			continue
		fi
		for FILEPATH in $FILEPATHS; do
			echo -n -e ":b# | bd# | view `echo $FILEPATH | sed -e "s#$ROOTDIR#\\\$ROOTDIR/#"` | $LINE\n"
		done
	done
done
