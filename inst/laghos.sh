#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="Laghos"
VERSION="9a074521257434e0b9acff9e59ff10e3e881bc32"
if [ ! -f $ROOTDIR/$BM/laghos ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	if [ ! -f ./hypre-2.10.0b/src/hypre/lib/libHYPRE.a ]; then
		wget https://computation.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods/download/hypre-2.10.0b.tar.gz
		tar xzf hypre-2.10.0b.tar.gz
		cd ./hypre-2.10.0b/src
		SCOREP_WRAPPER=off ./configure --disable-fortran -with-openmp CC="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicc" CFLAGS="-O3 -ipo -xHost" CXX="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicxx" CXXFLAGS="-O3 -ipo -xHost" F77="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpif77" FFLAGS="-O3 -ipo -xHost" --with-blas-libs=mkl --with-blas-lib-dirs="$MKLROOT/lib" --with-lapack-libs=mkl --with-lapack-lib-dirs="$MKLROOT/lib"
		sed -i -e 's/ -openmp/ -fopenmp/g' ./config/Makefile.config
		make -j
		cd $ROOTDIR/$BM/
	fi
	if [ ! -f ./metis-4.0.3/graphchk ]; then
		wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/OLD/metis-4.0.3.tar.gz
		tar xzf metis-4.0.3.tar.gz
		cd ./metis-4.0.3/
		patch -p1 < $ROOTDIR/patches/*1-metis4-*.patch
		sed -i -e 's/^CC = cc/CC = scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp icc/g' -e 's/OPTFLAGS = -O2\s*$/OPTFLAGS = -O2 -ipo -xHost/g' -e 's#^LDOPTIONS = #LDOPTIONS = -L${I_MPI_ROOT}/intel64/lib/release -lmpi#' ./Makefile.in
		make
		cd $ROOTDIR/$BM/
	fi
	if [ ! -f $ROOTDIR/dep/mfem/libmfem.a ]; then
		cd $ROOTDIR/dep/mfem/
		git checkout laghos-v1.0
		git apply --check $ROOTDIR/patches/*1-mfem*.patch
		if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-mfem*.patch; fi
		sed -i -e 's/MK) config MF/MK) config MFEM_USE_LAPACK=YES MF/' ./makefile
		MFEM_CXX="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiicpc" make parallel -j
		# remove libs which are covered by mkl
		sed -i -e 's/ -llapack -lblas//' ./config/config.mk
		cd $ROOTDIR/$BM/
	fi
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	# some scorep issues with wrong region exit
	# -> take gemm time and adjust for mini drift betw main kernel and outer loop
	sed -i -e 's#^SCOREP_RECORDING_#//SCOREP_RECORDING_#' ./laghos_solver.cpp
	sed -i '451i double mkrts, mkrte;\nmkrts = MPI_Wtime();' ./laghos.cpp
	sed -i '569i mkrte = MPI_Wtime();\nif (mpi.Root()) printf("Walltime of the outer loop: %.6lf sec\\n", mkrte - mkrts);' ./laghos.cpp
	make
	cd $ROOTDIR
fi

