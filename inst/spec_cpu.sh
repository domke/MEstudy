#!/bin/bash

function dump_cpu_config {
cat <<EOF > $1
default:
   action               = validate
   bench_post_setup     = sync
   command_add_redirect = 1
   flagsurl1            = http://www.spec.org/cpu2017/flags/Intel-ic19.0u1-official-linux64.2019-07-09.xml
   iterations           = 1
   line_width           = 1020
   log_line_width       = 1020
   makeflags            = -j 32
   mean_anyway          = 1
   output_format        = txt
   preenv               = 1
   reportable           = 0
   tune                 = peak
   label                = %{COMP}

   preENV_OMP_STACKSIZE=256M
   preENV_OMP_DYNAMIC=false
   preENV_OMP_NESTED=false
   preENV_KMP_AFFINITY=balanced
   preENV_KMP_BLOCKTIME=infinite
   preENV_KMP_LIBRARY=turnaround
#   preENV_KMP_PLACE_THREADS=1T
   preENV_KMP_SCHEDULE=static,balanced
   preENV_KMP_STACKSIZE=256M
   preENV_I_MPI_HBW_POLICY=hbw_preferred
   preENV_SCOREP_TOTAL_MEMORY=4089446400
   preENV_SCOREP_ENABLE_PROFILING=true
   preENV_SCOREP_ENABLE_TRACING=false
%ifdef %{RESDIR}
   preENV_SCOREP_EXPERIMENT_DIRECTORY=%{RESDIR}
%if '%{COMP}' eq 'intel'
%ifdef %{FILTER}
   preENV_SCOREP_FILTERING_FILE=%{FILTER}
%else
%error please define FILTER for launch
%endif
%endif
%else
%error please define RESDIR for launch
%endif

intrate,fprate:
   copies               = 1   # EDIT to change number of copies (see above)
%if '%{COMP}' eq 'gnu' || '%{COMP}' eq 'intel'
   submit               = ulimit -n 4096; ulimit -s unlimited; numactl -C \$SPECCOPYNUM \$command
%else
   submit               = ulimit -n 4096; ulimit -s unlimited; advixe-cl -q -collect survey -data-limit=2048 -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command; advixe-cl -q -collect tripcounts -flop -enable-cache-simulation -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command
%endif

intspeed,fpspeed:
%if '%{COMP}' eq 'vtune'
   submit               = ulimit -n 4096; ulimit -s unlimited; advixe-cl -q -collect survey -data-limit=2048 -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command; advixe-cl -q -collect tripcounts -flop -enable-cache-simulation -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command
%endif

default:
%if '%{COMP}' eq 'gnu'
   CC                   = scorep-gcc -m64 -std=c11
   CXX                  = scorep-g++ -m64
   FC                   = scorep-gfortran -m64
   OPT_ROOT             = -O3 -march=native -funroll-loops -ffast-math -ftree-vectorize
%elif '%{COMP}' eq 'intel'
   CC                   = scorep-icc -m64 -std=c11
   CXX                  = scorep-icpc -m64
   FC                   = scorep-ifort -m64
   OPT_ROOT             = -O3 -xHOST -no-prec-div -qopt-mem-layout-trans=4 -qopt-prefetch
   EXTRA_FOPTIMIZE      = -nostandard-realloc-lhs
%elif '%{COMP}' eq 'vtune'
   CC                   = icc -m64 -std=c11
   CXX                  = icpc -m64
   FC                   = ifort -m64
   OPT_ROOT             = -O3 -g -xHOST -no-prec-div -qopt-mem-layout-trans=4 -qopt-prefetch
   EXTRA_FOPTIMIZE      = -nostandard-realloc-lhs
%else
%error wrong or unsupported COMP variable specified
%endif
   CC_VERSION_OPTION    = --version
   CXX_VERSION_OPTION   = --version
   FC_VERSION_OPTION    = --version
   COPTIMIZE            = \$(OPT_ROOT)
   CXXOPTIMIZE          = \$(OPT_ROOT)
   FOPTIMIZE            = \$(OPT_ROOT)

%if '%{COMP}' eq 'gnu'
intspeed:
   EXTRA_COPTIMIZE      = -fopenmp -DSPEC_OPENMP

fpspeed:
   EXTRA_OPTIMIZE       = -fopenmp -DSPEC_OPENMP
%else
intspeed:
   EXTRA_COPTIMIZE      = -qopenmp -DSPEC_OPENMP

fpspeed:
   EXTRA_OPTIMIZE       = -qopenmp -DSPEC_OPENMP
%endif

intspeed,fpspeed:
   PORTABILITY          = -DSPEC_LP64

500.perlbench_r,600.perlbench_s:
   CPORTABILITY         = -DSPEC_LINUX_X64

502.gcc_r,602.gcc_s:
%if '%{COMP}' eq 'gnu'
   CPORTABILITY         = -fgnu89-inline
%endif

521.wrf_r,621.wrf_s:
   CPORTABILITY         = -DSPEC_CASE_FLAG
%if '%{COMP}' eq 'gnu'
   FPORTABILITY         = -fconvert=big-endian
%else
   FPORTABILITY         = -convert big_endian
%endif

523.xalancbmk_r,623.xalancbmk_s:
   CXXPORTABILITY       = -DSPEC_LINUX

526.blender_r:
   CPORTABILITY         = -DSPEC_LINUX -funsigned-char

527.cam4_r,627.cam4_s:
   CPORTABILITY         = -DSPEC_CASE_FLAG

628.pop2_s:
   CPORTABILITY         = -DSPEC_CASE_FLAG
%if '%{COMP}' eq 'gnu'
   FPORTABILITY         = -fconvert=big-endian
%else
   FPORTABILITY         = -convert big_endian -assume byterecl
%endif
EOF
}

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load gcc@8.4.0
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1`

BM="SPEC_CPU"
if [ ! -f $ROOTDIR/$BM/bin/runcpu ]; then
        if [ ! -f $ROOTDIR/dep/cpu2017-1.1.0.iso ]; then
                echo -e "ERR: cannot find cpu2017-1.1.0.iso under dep/; please fix it!"
                exit 1
        fi
        cd $ROOTDIR/dep/
        mkdir -p /tmp/mnt_$BM
        fuseiso ./cpu2017-1.1.0.iso /tmp/mnt_$BM
        cd /tmp/mnt_$BM/
        mkdir -p $ROOTDIR/$BM/
        ./install.sh -f -d $ROOTDIR/$BM/ -u linux-x86_64
        cd -
        fusermount -u /tmp/mnt_$BM
        cd $ROOTDIR/$BM/
        #patch -p1 < $ROOTDIR/patches/*1-${BM}*.patch
        dump_cpu_config $ROOTDIR/$BM/config/matrixengine.cfg
	bash -c "source ./shrc; runcpu --config=matrixengine.cfg --action=scrub --define COMP=gnu --define RESDIR=0 --define FILTER=0 intspeed fpspeed intrate fprate"
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp' runcpu --config=matrixengine.cfg --action=build --define COMP=gnu --define RESDIR=0 --define FILTER=0 intspeed"
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp' runcpu --config=matrixengine.cfg --action=build --define COMP=gnu --define RESDIR=0 --define FILTER=0 fpspeed"
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp' runcpu --config=matrixengine.cfg --action=build --define COMP=gnu --define RESDIR=0 --define FILTER=0 508.namd_r 510.parest_r 511.povray_r 526.blender_r 527.cam4_r"
	# special snowflakes had some dummy mpi in it confusing scorep
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp --mpp=none' runcpu --config=matrixengine.cfg --action=build --define COMP=gnu --define RESDIR=0 --define FILTER=0 628.pop2_s 649.fotonik3d_s"
	spack unload --all
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp' runcpu --config=matrixengine.cfg --action=build --define COMP=intel --define RESDIR=0 --define FILTER=0 600.perlbench_s"
	spack unload --all
	bash -c "source ./shrc; runcpu --config=matrixengine.cfg --action=build --define COMP=vtune --define RESDIR=0 --define FILTER=0 intspeed"
	bash -c "source ./shrc; runcpu --config=matrixengine.cfg --action=build --define COMP=vtune --define RESDIR=0 --define FILTER=0 fpspeed"
	bash -c "source ./shrc; runcpu --config=matrixengine.cfg --action=build --define COMP=vtune --define RESDIR=0 --define FILTER=0 508.namd_r 510.parest_r 511.povray_r 526.blender_r 527.cam4_r"
        cd $ROOTDIR
fi
