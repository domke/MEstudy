#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="FFVC"
VERSION="890a3f9bb3a5cf358504063a1751383b7d46f86d"
if [ ! -f $ROOTDIR/$BM/bin/ffvc_mini ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	cd $ROOTDIR/$BM/src
	rm make_setting; ln -s make_setting.intel make_setting
	#unsure, but vtune doesnt like to run w/o this
	#sed -i -e 's/-O3 -ipo -xHost/-O2 -g/g' ./make_setting
	sed -i -e 's/^CXX\s*= mpicxx/CXX = scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiicpc/' -e 's/^F90\s*= mpif90/F90 = scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiifort/' ./make_setting
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	make
	cd $ROOTDIR
fi

