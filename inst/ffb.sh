#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
# ffb with scorep/intel runs into some mysterious error; giving up on this branch; doesnt matter for perf. since no blas is found anyhow
spack load gcc@8.4.0
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1` 2> /dev/null

if [ ! -f $ROOTDIR/dep/REVOCAP_Refiner-1.1.01.tgz ]; then
	echo "ERR: Cannot find REVOCAP_Refiner-1.1.01.tgz"
	echo "Please download from: http://www.ciss.iis.u-tokyo.ac.jp/dl/index.php?pScdownload_6 and place REVOCAP_Refiner-1.1.01.tgz in ./dep subfolder"
	exit
fi

BM="FFB"
VERSION="e273244b65c7d340cc101ae596a55301359024dd"
if [ ! -f $ROOTDIR/$BM/bin/les3x.mpi ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	cd $ROOTDIR/$BM/src
	if [ ! -f ./metis-5.1.0/bin/graphchk ]; then
		wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
		tar xzf metis-5.1.0.tar.gz
		cd ./metis-5.1.0/
		patch -p1 < $ROOTDIR/patches/*1-metis5-*.patch
		#only for vtune: sed -i -e 's/-DCMAKE_C_COMPILER=$(cc)/-DCMAKE_C_COMPILER=$(cc) -DCMAKE_C_FLAGS=-g/' ./Makefile
		SCOREP_WRAPPER=off make config cc=scorep-gcc prefix=`pwd`
		make SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--verbose --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp" SCOREP_WRAPPER_COMPILER_FLAGS="-L${I_MPI_ROOT}/intel64/lib/release -lmpi" V=1
		make install
		cd $ROOTDIR/$BM/src
	fi
	if [ ! -f ./REVOCAP_Refiner-1.1.01/lib/x86_64-linux-intel/libRcapRefiner.a ]; then
		tar xzf $ROOTDIR/dep/REVOCAP_Refiner-1.1.01.tgz
		cd ./REVOCAP_Refiner-1.1.01
		rm ./MakefileConfig.in; ln -s ./MakefileConfig.LinuxCluster ./MakefileConfig.in
		sed -i '/#include <sstream>/a #include <stdlib.h>' ./RevocapIO/kmbHecmwIO_V3.cpp
		sed -i -e 's/^CC = gcc/CC = scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp gcc/' -e 's/^CXX = g++/CXX = scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp g++/' -e 's/^F90 = gfortran/F90 = scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp gfortran/' -e 's#= $(FFLAGS)#= $(FFLAGS) -L${I_MPI_ROOT}/intel64/lib/release -lmpi#' ./MakefileConfig.in
		#only for vtune: sed -i -e 's/ -Wall/ -g -Wall/g' ./MakefileConfig.in
		sed -i -e 's#-o $(BINDIR)/$(ARCH)/rcapPreFitting#-o $(BINDIR)/$(ARCH)/rcapPreFitting -L${I_MPI_ROOT}/intel64/lib/release -lmpi#' ./Refiner/Makefile
		make
		ln -s ./Refiner include
		cd $ROOTDIR/$BM/src
	fi
	sed -i -e 's/^DEFINE += -DNO_METIS/#DEFINE += -DNO_METIS/g' -e "s#\$(HOME)/opt_intel/metis5#$ROOTDIR/$BM/src/metis-5.1.0#g" ./make_setting
	sed -i -e 's/^DEFINE += -DNO_REFINER/#DEFINE += -DNO_REFINER/g' -e "s#\$(HOME)/opt_intel/REVOCAP_Refiner#$ROOTDIR/$BM/src/REVOCAP_Refiner-1.1.01#g" -e "s#REFINER)/lib #REFINER)/lib/x86_64-linux #" ./make_setting
	sed -i -e 's/^CC = mpicc/CC = scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicc/' -e 's/^FC = mpif90/FC = scorep --user --nocompiler --thread=none --libwrap=mklcpp mpif90/' -e 's/-ipo -xHost -mcmodel=large -shared-intel/-march=native/' ./make_setting
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	#only for vtune: sed -i -e 's/-march=native/-g -march=native/g' ./make_setting
	make
	cd $ROOTDIR
fi

