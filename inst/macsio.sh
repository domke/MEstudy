#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="MACSio"
VERSION1="e8bece99bfa5eab9355549bb587ee36aec9d6c67"
VERSION2="30008dc17cd5f787dedd303c51367bb5a8885271"
if [ ! -f $ROOTDIR/$BM/macsio/macsio ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION1}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	if [ ! -f $ROOTDIR/dep/json-cwx/lib/libjson-cwx.a ]; then
		cd $ROOTDIR/dep/json-cwx/
		git checkout -b precision ${VERSION2}
		cd $ROOTDIR/dep/json-cwx/json-cwx
		./autogen.sh
		SCOREP_WRAPPER=off ./configure --prefix=`pwd`/../ CC=scorep-icc CFLAGS="-O2 -ipo -xHost"
		make SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--user --nocompiler --thread=none --mpp=none --libwrap=mklcpp" SCOREP_WRAPPER_COMPILER_FLAGS="-L${I_MPI_ROOT}/intel64/lib/release -lmpi"
		make install
		cd $ROOTDIR/$BM/
	fi
	if [ ! -f $ROOTDIR/dep/silo-4.10.2/bin/silofile ]; then
		cd $ROOTDIR/dep/
		wget https://wci.llnl.gov/content/assets/docs/simulation/computer-codes/silo/silo-4.10.2/silo-4.10.2.tar.gz
		tar xzf silo-4.10.2.tar.gz
		cd silo-4.10.2/
		SCOREP_WRAPPER=off ./configure --prefix=`pwd` CC=scorep-icc CFLAGS="-O2 -ipo -xHost" CXX=scorep-icpc CXXFLAGS="-O2 -ipo -xHost" FC=scorep-ifort FCFLAGS="-O2 -ipo -xHost" F77=ifort FFLAGS="-O2 -ipo -xHost"
		make SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--user --nocompiler --thread=none --mpp=none --libwrap=mklcpp" SCOREP_WRAPPER_COMPILER_FLAGS="-L${I_MPI_ROOT}/intel64/lib/release -lmpi"
		make install
		cd $ROOTDIR/$BM/
	fi
	rm -rf build; mkdir -p build; cd build
	SCOREP_WRAPPER=off cmake -DCMAKE_C_COMPILER=`which scorep-mpicc` -DCMAKE_C_FLAGS="-O3 -ipo -xHost -I${ADVISOR_2018_DIR}/include" -DCMAKE_CXX_COMPILER=`which scorep-mpicxx` -DCMAKE_CXX_FLAGS="-O3 -ipo -xHost -I${ADVISOR_2018_DIR}/include" -DCMAKE_INSTALL_PREFIX=../ -DWITH_JSON-CWX_PREFIX=../../dep/json-cwx -DWITH_SILO_PREFIX=../../dep/silo-4.10.2 ..
	sed -i -e "s# -lpthread # -lpthread -L${ADVISOR_2018_DIR}/lib64 -littnotify #" ./macsio/CMakeFiles/macsio.dir/link.txt
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r ../ | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	make SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--user --nocompiler --thread=none --libwrap=mklcpp"
	make install
	cd $ROOTDIR
fi

