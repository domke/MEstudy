#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="MiniFE"
VERSION="daeddf3bfaf3b521a932245fad9871336b53c166"
if [ ! -f $ROOTDIR/$BM/mkl/src/miniFE.x ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	for SUB in mkl openmp-opt openmp-opt-knl ; do
		cd $ROOTDIR/$BM/$SUB/src
		sed -i -e 's/^CXX=mpiicpc/CXX=scorep-mpicxx/' -e 's/^CC=mpiicc/CC=scorep-mpicc/' ./Makefile
		#only for vtune: sed -i -e 's/-ipo -xHost/-g -ipo -xHost/g' ./Makefile
		make SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--verbose --user --nocompiler --thread=none --libwrap=mklcpp" V=1
	done
	cd $ROOTDIR
fi

