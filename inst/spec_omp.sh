#!/bin/bash

function dump_omp_config {
cat <<EOF > $1
action         = validate
delay          = 1
iterations     = 1
output_format  = text
teeout         = no
teerunout      = yes
tune           = peak
verbose        = 99
env_vars       = 1
makeflags      = -j 32
ext            = %{COMP}

%if '%{COMP}' eq 'gnu'
submit         = ulimit -n 4096; ulimit -s unlimited; \$command
BOPTS          = -O3 -fopenmp -march=native -funroll-loops -ffast-math -ftree-vectorize
%elif '%{COMP}' eq 'intel'
submit         = ulimit -n 4096; ulimit -s unlimited; \$command
BOPTS          = -O3 -qopenmp -xHOST -no-prec_div -fp-model fast=2 -fma
%elif '%{COMP}' eq 'vtune'
submit         = ulimit -n 4096; ulimit -s unlimited; advixe-cl -q -collect survey -data-limit=2048 -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command; advixe-cl -q -collect tripcounts -flop -enable-cache-simulation -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR} -- \$command
BOPTS          = -O3 -g -qopenmp -xHOST -no-prec_div -fp-model fast=2 -fma
%else
%error wrong or unsupported COMP variable specified
%endif

preENV_OMP_STACKSIZE=256M
preENV_OMP_DYNAMIC=false
preENV_OMP_NESTED=false
preENV_KMP_AFFINITY=balanced
preENV_KMP_BLOCKTIME=infinite
preENV_KMP_LIBRARY=turnaround
#preENV_KMP_PLACE_THREADS=1T
preENV_KMP_SCHEDULE=static,balanced
preENV_KMP_STACKSIZE=256M
preENV_I_MPI_HBW_POLICY=hbw_preferred
preENV_SCOREP_TOTAL_MEMORY=4089446400
preENV_SCOREP_ENABLE_PROFILING=true
preENV_SCOREP_ENABLE_TRACING=false
%ifdef %{RESDIR}
preENV_SCOREP_EXPERIMENT_DIRECTORY=%{RESDIR}
%if '%{COMP}' eq 'intel'
%ifdef %{FILTER}
preENV_SCOREP_FILTERING_FILE=%{FILTER}
%else
%error please define FILTER for launch
%endif
%endif
%else
%error please define RESDIR for launch
%endif

default=default=default=default:
%if '%{COMP}' eq 'gnu'
sw_compiler    = GNU Compilers
FC             = scorep-gfortran
CC             = scorep-gcc
CXX            = scorep-g++
%elif '%{COMP}' eq 'intel'
sw_compiler    = Intel Compiler Suite
FC             = scorep-ifort
CC             = scorep-icc
CXX            = scorep-icpc
%elif '%{COMP}' eq 'vtune'
sw_compiler    = Intel Compiler Suite
FC             = ifort
CC             = icc
CXX            = icpc
%endif
FOPTIMIZE      = \${BOPTS}
COPTIMIZE      = \${BOPTS}
CXXOPTIMIZE    = \${BOPTS}

350.md=default=default=default:
%if '%{COMP}' eq 'gnu'
FPORTABILITY   = -ffree-form -fno-range-check
%else
FPORTABILITY   = -free
%endif

357.bt331=default=default=default:
FPORTABILITY   = -mcmodel=medium

359.botsspar=default=default=default:
%if '%{COMP}' eq 'intel'
strict_rundir_verify = 0
%endif

363.swim=default=default=default:
FPORTABILITY   = -mcmodel=medium

367.imagick=default=default=default:
CPORTABILITY   = -std=c99
EOF
}

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load gcc@8.4.0
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1`

BM="SPEC_OMP"
if [ ! -f $ROOTDIR/$BM/bin/runspec ]; then
	if [ ! -f $ROOTDIR/dep/omp2012-1.1.iso ]; then
		echo -e "ERR: cannot find omp2012-1.1.iso under dep/; please fix it!"
		exit 1
	fi
	cd $ROOTDIR/dep/
	mkdir -p /tmp/mnt_$BM
	fuseiso ./omp2012-1.1.iso /tmp/mnt_$BM
	cd /tmp/mnt_$BM/
	mkdir -p $ROOTDIR/$BM/
	./install.sh -f -d $ROOTDIR/$BM/ -u linux-suse10-amd64
	cd -
	fusermount -u /tmp/mnt_$BM
	cd $ROOTDIR/$BM/
	# no patch for v1.1 on https://www.spec.org/omp2012/src.alt/
	dump_omp_config $ROOTDIR/$BM/config/matrixengine.cfg
	bash -c "source ./shrc; runspec --config=matrixengine.cfg --action=scrub --define COMP=gnu --define RESDIR=0 --define FILTER=0 gross"
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=ompmklcpp' runspec --config=matrixengine.cfg --action=build --size=train --define COMP=gnu --define RESDIR=0 --define FILTER=0 gross"
	spack unload --all
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=omp --noopenmp --libwrap=ompmklcpp' runspec --config=matrixengine.cfg --action=build --size=train --define COMP=intel --define RESDIR=0 --define FILTER=0 357.bt331"
	### something is broken here -> neg. numbers -> confirmed error in cube tools -> ignore percentates and analyze timings which should be correct
	cd $ROOTDIR/$BM/benchspec/OMP2012/359.botsspar/src; patch -p1 < $ROOTDIR/patches/*1-${BM}*.patch; cd -
	bash -c "source ./shrc; SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --nocompiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=omp --libwrap=ompmklcpp' runspec --config=matrixengine.cfg --action=build --size=train --define COMP=intel --define RESDIR=0 --define FILTER=0 359.botsspar"
	cd $ROOTDIR/$BM/benchspec/OMP2012/359.botsspar/src; patch -R -p1 < $ROOTDIR/patches/*1-${BM}*.patch; cd -
	spack unload --all
	bash -c "source ./shrc; runspec --config=matrixengine.cfg --action=build --size=train --define COMP=vtune --define RESDIR=0 --define FILTER=0 gross"
	cd $ROOTDIR
fi
