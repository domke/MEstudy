#!/bin/bash

function dump_mpi_config {
cat <<EOF > $1
action               = validate
delay                = 1
iterations           = 1
output_format        = text
tune                 = peak
verbose              = 99
env_vars             = 1
makeflags            = -j 32
ext                  =%{COMP}

%ifdef %{RANKS}
ranks                = %{RANKS}
%else
ranks                = 64
%endif

%if '%{COMP}' eq 'gnu'
BOPTS                = -O3 -march=native -funroll-loops -ffast-math -ftree-vectorize
%elif '%{COMP}' eq 'intel'
BOPTS                = -O3 -xHOST -no-prec_div -fp-model fast=2 -fma
%elif '%{COMP}' eq 'vtune'
BOPTS                = -O3 -g -xHOST -no-prec_div -fp-model fast=2 -fma
%else
%error wrong or unsupported COMP variable specified
%endif

%if '%{COMP}' eq 'gnu' || '%{COMP}' eq 'intel'
FC                   = scorep-mpif90
CC                   = scorep-mpicc
CXX                  = scorep-mpicxx
%elif '%{COMP}' eq 'vtune'
FC                   = mpiifort
CC                   = mpiicc
CXX                  = mpiicpc
%endif

default=default=default=default:
COPTIMIZE            = \${BOPTS}
FOPTIMIZE            = \${BOPTS}
CXXOPTIMIZE          = \${BOPTS}
use_submit_for_speed = 1
%ifdef %{RESDIR}
%if '%{COMP}' eq 'gnu'
submit               = ulimit -n 4096; ulimit -s unlimited; mpiexec -genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=true -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=%{RESDIR} -genv OMP_NUM_THREADS=1 -host \`hostname\` -np \$ranks \$command
%elif '%{COMP}' eq 'intel'
%ifdef %{FILTER}
submit               = ulimit -n 4096; ulimit -s unlimited; mpiexec -genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=true -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=%{RESDIR} -genv SCOREP_FILTERING_FILE=%{FILTER} -genv OMP_NUM_THREADS=1 -host \`hostname\` -np \$ranks \$command
%else
%error please define FILTER for launch
%endif
%elif '%{COMP}' eq 'vtune'
submit               = ulimit -n 4096; ulimit -s unlimited; mpiexec -genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -genv OMP_NUM_THREADS=1 -host \`hostname\` -np \$ranks -gtool "advixe-cl -q -collect survey -data-limit=2048 -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR}:0-\$((\$ranks-1))" \$command; mpiexec -genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -genv OMP_NUM_THREADS=1 -host \`hostname\` -np \$ranks -gtool "advixe-cl -q -collect tripcounts -flop -enable-cache-simulation -project-dir %{RESDIR} --search-dir src:rp=%{SRCDIR}:0-\$((\$ranks-1))" \$command
%endif
%else
%error please define RESDIR for launch
%endif

121.pop2=default=default=default:
CPORTABILITY         = -DSPEC_MPI_CASE_FLAG

126.lammps=default=default=default:
CXXPORTABILITY       = -DMPICH_IGNORE_CXX_SEEK

127.wrf2=default=default=default:
%if '%{COMP}' eq 'vtune'
CPORTABILITY         = -DSPEC_MPI_CASE_FLAG -DSPEC_MPI_LINUX -no-gcc
%else
CPORTABILITY         = -DSPEC_MPI_CASE_FLAG -DSPEC_MPI_LINUX
%endif
strict_rundir_verify = 0

129.tera_tf=default=default=default:
%if '%{SIZE}' eq 'medium'
srcalt               = add_rank_support
notes_base_129       = src.alt used: 129.tera_tf->add_rank_support
%endif

130.socorro=default=default=default:
%if '%{COMP}' eq 'gnu' || '%{COMP}' eq 'vtune'
notes_base_130       = src.alt used: 130.socorro->nullify_ptrs
srcalt               = nullify_ptrs
# nullify_ptrs and scorep patches merged, so no need in 'intel' case
%endif
%if '%{COMP}' eq 'vtune'
FPORTABILITY         = -assume nostd_intent_in
%endif
%if '%{COMP}' eq 'intel'
FPORTABILITY         = -assume nostd_intent_in -fpp
strict_rundir_verify = 0
%endif

143.dleslie=default=default=default:
srcalt               = integer_overflow
notes_base_000       = src.alt used: 143.dleslie->integer_overflow
EOF
}

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load gcc@8.4.0
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1`

BM="SPEC_MPI"
if [ ! -f $ROOTDIR/$BM/bin/runspec ]; then
	if [ ! -f $ROOTDIR/dep/mpi2007-2.0.1.iso ]; then
		echo -e "ERR: cannot find mpi2007-2.0.1.iso under dep/; please fix it!"
		exit 1
	fi
	cd $ROOTDIR/dep/
	mkdir -p /tmp/mnt_$BM
	fuseiso ./mpi2007-2.0.1.iso /tmp/mnt_$BM
	cd /tmp/mnt_$BM/
	mkdir -p $ROOTDIR/$BM/
	./install.sh -f -d $ROOTDIR/$BM/ -u linux-suse101-AMD64
	cd -
	fusermount -u /tmp/mnt_$BM
	cd $ROOTDIR/$BM/
	wget https://www.spec.org/mpi2007/src.alt/mpi2007-dleslie-integer_overflow-160224.tar.bz2
	wget https://www.spec.org/mpi2007/src.alt/mpi2007-socorro-nullify_ptr-1202022.tar.bz2
	wget https://www.spec.org/mpi2007/src.alt/mpi2007-tera_tf-add_rank_support-150610.tar.bz2
	tar xjf ./mpi2007-dleslie-integer_overflow-160224.tar.bz2
	tar xjf ./mpi2007-socorro-nullify_ptr-1202022.tar.bz2
	tar xjf ./mpi2007-tera_tf-add_rank_support-150610.tar.bz2
	#patch -p1 < $ROOTDIR/patches/*1-${BM}*.patch
	dump_mpi_config $ROOTDIR/$BM/config/matrixengine.cfg
	sed -i -e '/= iargc_/i #ifdef __GNUC__\n  xargc = _gfortran_iargc()+1;\n#else' -e '/= iargc_/a #endif' ./benchspec/MPI2007/127.wrf2/src/rsl_mpi_compat.c
	bash -c "source ./shrc; runspec --config=matrixengine.cfg --action=scrub --define COMP=gnu --define RESDIR=0 --define FILTER=0 medium; runspec --config=matrixengine.cfg --action=scrub --define COMP=gnu --define RESDIR=0 --define FILTER=0 large"
	bash -c "source ./shrc; I_MPI_CC=gcc I_MPI_CXX=g++ I_MPI_F77=gfortran I_MPI_F90=gfortran SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=mklcpp' runspec --config=matrixengine.cfg --action=build --size=mtrain --define COMP=gnu --define RESDIR=0 --define FILTER=0 medium"
	bash -c "source ./shrc; I_MPI_CC=gcc I_MPI_CXX=g++ I_MPI_F77=gfortran I_MPI_F90=gfortran SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --instrument-filter=$ROOTDIR/dep/blas.filter --thread=none --libwrap=mklcpp' runspec --config=matrixengine.cfg --action=build --size=ltrain --define COMP=gnu --define RESDIR=0 --define FILTER=0 large"
	# some don't run and segfault with gnu
	spack unload --all
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`
	cd $ROOTDIR/$BM/benchspec/MPI2007/130.socorro/src; patch -p1 < $ROOTDIR/patches/*1-${BM}*.patch; cd -
	bash -c "source ./shrc; I_MPI_CC=icc I_MPI_CXX=icpc I_MPI_F77=ifort I_MPI_F90=ifort SCOREP_WRAPPER_INSTRUMENTER_FLAGS='--verbose --user --compiler --thread=none --libwrap=mklcpp' runspec --config=matrixengine.cfg --action=build --size=mtrain --define COMP=intel --define RESDIR=0 --define FILTER=0 115.fds4 129.tera_tf 130.socorro"
	cd $ROOTDIR/$BM/benchspec/MPI2007/130.socorro/src; patch -R -p1 < $ROOTDIR/patches/*1-${BM}*.patch; cd -
	bash -c "source ./shrc; I_MPI_CC=icc I_MPI_CXX=icpc I_MPI_F77=ifort I_MPI_F90=ifort runspec --config=matrixengine.cfg --action=build --size=mtrain --define COMP=vtune --define RESDIR=0 --define FILTER=0 medium"
	bash -c "source ./shrc; I_MPI_CC=icc I_MPI_CXX=icpc I_MPI_F77=ifort I_MPI_F90=ifort runspec --config=matrixengine.cfg --action=build --size=ltrain --define COMP=vtune --define RESDIR=0 --define FILTER=0 large"
	cd $ROOTDIR
fi
