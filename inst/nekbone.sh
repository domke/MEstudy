#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="Nekbone"
VERSION="d681db43f45f5437e5258b3663b5a92c078cfb57"
if [ ! -f $ROOTDIR/$BM/test/nek_mgrid/nekbone ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	#only for vtune, ignore *1- in this case: git apply --check $ROOTDIR/patches/*2-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	cd $ROOTDIR/$BM/test/nek_mgrid
	sed -i -e 's/lp = 10)/lp = 576)/' -e 's/lelt=100/lelt=1024/' SIZE
	sed -i -e 's/^F77="mpif77"/F77="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiifort"/' -e 's/^CC="mpicc"/CC="scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicc"/' ./makenek
	#only for vtune: sed -i -e 's/-ipo -xHost/-g -ipo -xHost/g' ./makenek
	#only for vtune: sed -i -e 's/-ipo -xHost/-g -ipo -xHost/g' ../../src/makefile.template
	./makenek NotUsedCasename $ROOTDIR/$BM/src
	cd $ROOTDIR
fi

