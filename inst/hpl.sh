#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="HPL"
if [ ! -f $ROOTDIR/$BM/bin/Linux_Intel64/xhpl ]; then
	if [ ! -f $ROOTDIR/hpl-2.2.tar.gz ]; then
		wget http://www.netlib.org/benchmark/hpl/hpl-2.2.tar.gz
	fi
	mkdir -p $ROOTDIR/$BM/
	cd $ROOTDIR/$BM/
	tar xzf ../hpl-2.2.tar.gz -C $ROOTDIR/$BM --strip-components 1
	patch -p1 < $ROOTDIR/patches/*1-${BM}*.patch
	sed -i -e "s#\$(HOME)/hpl#$ROOTDIR/$BM#g" ./Make.Linux_Intel64
	sed -i -e 's/^CC.*= mpiicc/CC = scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicc/g' ./Make.Linux_Intel64
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	make arch=Linux_Intel64
	cd $ROOTDIR
fi

