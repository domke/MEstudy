#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="NTChem"
VERSION="fcafcc4fec195d8a81c19affd1a3b83f7bab4285"
if [ ! -f $ROOTDIR/$BM/bin/rimp2.exe ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	TOP_DIR=`pwd`
	TYPE=intel
	#only for vtune: sed -i -e 's/-ipo -xHost/-g -ipo -xHost/g' ./config/linux64_mpif90_omp_intel_proto.makeconfig.in
	./config_mine
	mkdir -p $ROOTDIR/$BM/bin
	sed -i -e 's/= mpif77/= scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiifort/' -e 's/= mpif90/= scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiifort/' -e 's/= icc/= scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp icc/' -e 's/= icpc/= scorep --user --nocompiler --thread=none --mpp=none --libwrap=mklcpp icpc/' ./config/makeconfig
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	make CC=scorep-mpicc CXX=scorep-mpicxx F77C=scorep-mpif90 F90C=scorep-mpif90 SCOREP_WRAPPER_INSTRUMENTER_FLAGS="--user --nocompiler --thread=none --libwrap=mklcpp"
	cd $ROOTDIR
fi

