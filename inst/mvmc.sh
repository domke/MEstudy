#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

BM="MVMC"
VERSION="7c58766b180ccb1035e4c220208b64ace3c49cf2"
if [ ! -f $ROOTDIR/$BM/src/vmc.out ] || [ "x`ls -s $ROOTDIR/$BM/src/vmc.out | awk '{print $1}'`" = "x0" ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	cd $ROOTDIR/$BM/src
	sed -i -e "s#L/usr/local/intel/composer_xe_2013/mkl#L$MKLROOT#g" -e 's/= mpif90/= scorep --user --nocompiler --thread=omp --libwrap=mklcpp mpif90/' -e 's/= mpicc/= scorep --user --nocompiler --thread=omp --libwrap=mklcpp mpicc/' ./Makefile_intel
	sed -i -e 's/= ifort/= scorep --user --nocompiler --thread=omp --mpp=none --libwrap=mklcpp ifort/' ./pfapack/Makefile_intel
	sed -i -e 's/= icc/= scorep --user --nocompiler --thread=omp --mpp=none --libwrap=mklcpp icc/' ./sfmt/Makefile_intel
	make intel
	cd $ROOTDIR
fi

