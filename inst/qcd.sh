#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

source $ROOTDIR/conf/qcd.sh

BM="QCD"
VERSION="07277047b170529caa5fcd164afd814e70286ce4"
if [ ! -f $ROOTDIR/$BM/src/ccs_qcd_solver_bench_class2_111 ]; then
	cd $ROOTDIR/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	for FILE in `/usr/bin/grep 'STOPSDE\|STARTSDE' -r | cut -d':' -f1 | sort -u`; do sed -i -e '/^\s*STARTSDE/a SCOREP_RECORDING_ON();' -e '/^\s*STOPSDE/i SCOREP_RECORDING_OFF();' -e '/\s*include.*ittnotify.h/i #include <scorep/SCOREP_User.h>' $FILE; done
	cd $ROOTDIR/$BM/src
	for TEST in $TESTCONF; do
		PX="`echo $TEST | cut -d '|' -f3`"
		PY="`echo $TEST | cut -d '|' -f4`"
		PZ="`echo $TEST | cut -d '|' -f5`"
		sed -i -e 's/= mpif90/= scorep --user --nocompiler --thread=none --libwrap=mklcpp mpiifort/' -e 's/= mpicc/= scorep --user --nocompiler --thread=none --libwrap=mklcpp mpicc/' ./make.ifort.inc
		sed -i -e 's/E) -c $</E) -c $< -o $(<:.F90=.o)/' ./Makefile
		sed -i -e 's/FFLAGS) -c $</FFLAGS) -c $< -o $(<:.F90=.o)/' ./ma_prof/src/Makefile
		if [ ! -f $ROOTDIR/$BM/src/ccs_qcd_solver_bench_class2_${PX}${PY}${PZ} ]; then
			make clean >> /dev/null 2>&1
			make MAKE_INC=make.ifort.inc CLASS=2 PX=${PX} PY=${PY} PZ=${PZ}
			mv $ROOTDIR/$BM/src/ccs_qcd_solver_bench_class2 $ROOTDIR/$BM/src/ccs_qcd_solver_bench_class2_${PX}${PY}${PZ}
		fi
	done
	cd $ROOTDIR
fi

