#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
export I_MPI_CC=icc
export I_MPI_CXX=icpc
export I_MPI_F77=ifort
export I_MPI_F90=ifort
alias ar=`which xiar`
alias ld=`which xild`
export ADVISOR_2018_DIR=${ADVISOR_2019_DIR}

echo -e '\nATTENTION!!! This script will ask for sudo/root access\nNote: MSR changes are not persistent.\n      Installing in NFS will not work when mounted with nosuid flag.\n\n(10s time to abort and manually install)\n'
sleep 10

echo -e '\nInstalling known dependencies'
if [[ -f /etc/redhat-release ]];then
	sudo yum -y install cmake autoconf automake libtool cpupowerutils gcc libstdc++ gcc-c++ gcc-gfortran bzip2 patch zlib-devel ncurses-devel kernel-devel bc gawk coreutils grep perf fuseiso
	# vim screen
	# for Intel Parallel XE: gtk2 gtk3 pango xorg-x11-server-Xorg
else
	echo -e '\nNote: untested linux distro; please install -- cmake autoconf automake libtool cpupowerutils gcc libstdc++ gcc-c++ gcc-gfortran bzip2 patch zlib-devel ncurses-devel kernel-devel bc gawk coreutils grep perf fuseiso -- yourself;\n      everything hereafter may break, no guarantees ;-)'
	sleep 10
fi

echo -e '\nUpdating ldconfig'
echo $LD_LIBRARY_PATH | sed -e 's/:/\n/g' > /dev/shm/precision.conf
sudo mv /dev/shm/precision.conf /etc/ld.so.conf.d/
sudo ldconfig

echo -e '\nFix problem with Vtune getting stuck it seems'
ssh -o StrictHostKeyChecking=no `hostname` echo ''

echo -e '\nDisabling NMI watchdog'
sudo sh -c "echo 0 > /proc/sys/kernel/nmi_watchdog"
sudo sh -c "echo 'kernel.nmi_watchdog=0' >> /etc/sysctl.conf"

echo -e '\nRemoving broken Intel VTune Sampling Drivers and use perf'
cd ${VTUNE_AMPLIFIER_2018_DIR}/sepdk/src
sudo ./rmmod-sep -R
sudo ./boot-script --uninstall
sudo sh -c 'echo 0 > /proc/sys/kernel/perf_event_paranoid'
#sudo ./insmod-sep -r -g `whoami` -pu
#sudo ./boot-script --install -g `whoami` -pu

echo -e '\nInstalling Likwid'
BM="likwid"
VERSION="4e1a04eed1371f82d04eb9c1d1d739706a633b4a"
if [ ! -f $ROOTDIR/dep/$BM/likwid-setFrequencies ]; then
	cd $ROOTDIR/dep/$BM/
	git checkout -b precision ${VERSION}
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	sed -i -e "s# /usr/local\#NO# $ROOTDIR/dep/$BM\#/usr/local\#NO#" ./config.mk
	make
	sudo make install
	for x in `ls bin/likwid-*`; do if [ -x $x ]; then sudo setcap cap_sys_admin,cap_sys_rawio+ep $x; fi; done
	cat /proc/cmdline | grep 'intel_pstate=disable'  > /dev/null
	# vim /etc/default/grub
	# grub2-mkconfig -o /boot/grub2/grub.cfg
	# grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
	# reboot
	if [ ! "x$?" = "x0" ]; then echo -e 'Note: for likwid to work, please add 'intel_pstate=disable' to kernel parameter and reboot; Afterwards, run this script again'; fi
	echo -e "Please execute:\n  export PATH=$ROOTDIR/dep/$BM/bin:\$PATH\n  export LD_LIBRARY_PATH=$ROOTDIR/dep/$BM/lib:\$LD_LIBRARY_PATH"
	cd $ROOTDIR
fi

# enable support for MSR and MSR-safe counters from user space
echo -e '\nInstalling MSR/MSR-Safe'
BM="msr-safe"
VERSION="b5bdf8b200db5a0bfa7e9ba2aadb85159f72c697"
WL="`printf 'wl_%.2x%x\n' $(lscpu | awk '/CPU family:/{print $3}') $(lscpu | awk '/Model:/{print $2}')`"
if [ ! -f $ROOTDIR/dep/$BM/msr-safe.ko ] || [ ! -r /dev/cpu/0/msr ]; then
	cd $ROOTDIR/dep/$BM/
	git checkout -b precision ${VERSION}
	make
	# use KNL whitelist for KNM nodes
	if [ ! -e ./whitelists/wl_0685 ]; then cp ./whitelists/wl_0657 ./whitelists/wl_0685; fi
	sudo insmod msr-safe.ko
	sudo chmod go+rw /dev/cpu/msr_whitelist
	sudo cat ./whitelists/${WL} > /dev/cpu/msr_whitelist
	sudo touch /sys/firmware/acpi/tables/MCFG
	sudo chmod go+rw /sys/firmware/acpi/tables/MCFG
	sudo chmod go+rw /dev/cpu/*/msr
	sudo chmod go+rw /dev/cpu/*/msr_safe
	sudo chmod go+rw /proc/bus/pci/*/*.*
	sudo chmod go+rw /dev/mem
	cd $ROOTDIR
fi

echo -e '\nInit Intel Advisor 2020'
BM="advisor"
VERSION="_2020_update2"
if [ ! -f $ROOTDIR/dep/$BM/ ]; then
	if [ ! -f $ROOTDIR/dep/${BM}${VERSION}.tar.gz ]; then
		echo -e "ERR: cannot find ${BM}${VERSION}.tar.gz under dep/; please fix it!"
		exit 1
	fi
	cd $ROOTDIR/dep/
	tar xzf ./advisor_2020_update2.tar.gz
	cd $ROOTDIR/dep/${BM}${VERSION}
cat <<EOF > ./my-silent.cfg
ACCEPT_EULA=accept
CONTINUE_WITH_OPTIONAL_ERROR=yes
PSET_INSTALL_DIR=$ROOTDIR/dep/intel-${BM}
CONTINUE_WITH_INSTALLDIR_OVERWRITE=yes
COMPONENTS=DEFAULTS
PSET_MODE=install
CLUSTER_INSTALL_AUTOMOUNT=yes
INTEL_SW_IMPROVEMENT_PROGRAM_CONSENT=no
SIGNING_ENABLED=yes
EOF
	./install.sh --cli-mode --install_dir $ROOTDIR/dep/intel-${BM} --silent ./my-silent.cfg --accept_eula
	# installer has tendency to install in ~/intel if it existed
	if [ ! -f $ROOTDIR/dep/intel-${BM}/advisor_2020/advixe-vars.sh ]; then
		if [ -f $HOME/intel/advisor_2020/advixe-vars.sh ]; then
			cd $ROOTDIR/dep; ln -s $HOME/intel intel-${BM}
		else
			echo "ERR: problem with advisor installation"
		fi
	fi
	cd $ROOTDIR
fi

echo -e '\nInit spack'
BM="spack"
VERSION="96fa6f0c1be4ab55ec6ba7cd5af059e1ed95351f"
cd $ROOTDIR/dep/$BM/
if [[ "`git branch`" = *"develop"* ]]; then
	git checkout -b precision ${VERSION}
	sed -i -e 's/# build_jobs: 16/build_jobs: 32/' etc/spack/defaults/config.yaml
	source $ROOTDIR/dep/spack/share/spack/setup-env.sh
	spack compiler find
	# check system compiler and install one we like
	if [[ "`gcc --version | /usr/bin/grep 'gcc (GCC)' | cut -d ' ' -f3`" = "8.4.0" ]]; then
		spack install gcc@8.4.0
		spack load gcc@8.4.0
		spack compiler find
	fi
fi
cd $ROOTDIR

echo -e '\nInstalling scorep via spack'
BM="scorep"
VERSION="unwind=True"
SPACK_COMP_VERS="gcc@8.4.0"
if [ ! -n "$(find $ROOTDIR/dep/spack/opt/spack/linux-*/gcc-8.4.0/ -name 'scorep-libwrap-init' -print -quit)" ]; then
	source $ROOTDIR/dep/spack/share/spack/setup-env.sh
	spack load gcc@8.4.0
	git apply --check $ROOTDIR/patches/*1-${BM}*.patch
	if [ "x$?" = "x0" ]; then git am --ignore-whitespace < $ROOTDIR/patches/*1-${BM}*.patch; fi
	spack install llvm@10.0.0%${SPACK_COMP_VERS} gold=False
	sed -i -e "/variant('mpi/i \    variant('compiler', default='gcc', values=('gcc', 'intel'), description='Set the compiler suite to use')" -e "s/cname = /cname = spec.variants['compiler'].value #/" $ROOTDIR/dep/spack/var/spack/repos/builtin/packages/scorep/package.py
	spack install ${BM}@6.0%${SPACK_COMP_VERS} ${VERSION} ^intel-mpi ^llvm@10.0.0/`spack find -l llvm | /usr/bin/grep llvm | cut -d ' ' -f1`
	spack install ${BM}@6.0%${SPACK_COMP_VERS} compiler=intel ${VERSION} ^intel-mpi ^llvm@10.0.0/`spack find -l llvm | /usr/bin/grep llvm | cut -d ' ' -f1`
	# need some 'fake' gcc versions for ngsa BWA tool, luckily no problems with compiler flags in this case
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`
	scorep-wrapper --create gcc; scorep-wrapper --create g++
	spack unload scorep
	### create libwrapping for mkl
	#https://software.intel.com/content/www/us/en/develop/documentation/mkl-windows-developer-guide/top/appendix-a-intel-math-kernel-library-language-interfaces-support/include-files.html
	#https://software.intel.com/content/www/us/en/develop/articles/intel-mkl-link-line-advisor.html
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1`
	cd $ROOTDIR/dep/
	### wrapper for MPI/OMP-based apps
	LIBN=mklcpp
	scorep-libwrap-init --name ${LIBN} -x c++ --cppflags "-m64 -I${MKLROOT}/include -I`pwd`/wrap_${LIBN}" --ldflags "-L${MKLROOT}/lib/intel64" --libs "-Wl,--no-as-needed -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -lgomp -lpthread -lm -ldl" --update wrap_${LIBN}
	cd wrap_${LIBN}/
	sed -i -e 's/= g++/= mpiicpc/' ./Makefile
	cp `find ${MKLROOT}/include -name mkl_blas.h` mkl_blas_underscore.h
	sed -i -e 's/\s*(/_(/' -e 's/_MKL_BLAS_H_/_MKL_BLAS_UNDERSCORE_H_/' ./mkl_blas_underscore.h
	sed -i -e '/^#endif/i #include<mkl_blas_underscore.h>\n\/\/#include<mkl_blas.h>\n\/\/#include<mkl_trans.h>\n\/\/#include<mkl_cblas.h>\n#include<mkl_lapack.h>\n\/\/#include<mkl_lapacke.h>\n\/\/#include<mkl_pblas.h>\n\/\/#include<mkl_scalapack.h>' ./libwrap.h
	I_MPI_CC=gcc I_MPI_CXX=g++ make V=1         #intel compiler crashes for unknown reason
	/usr/bin/grep -P '\-wrap [A-Z0-9_]+_$' ./${LIBN}.wrap | cut -d ' ' -f2 | sed -e 's/$/*/' >> ./${LIBN}.filter
	echo -e 'mkl_cblas_jit_create_dgemm_*\nmkl_cblas_jit_create_sgemm_*' >> ./${LIBN}.filter
	sed -i -e 's/SCOREP_REGION_NAMES_END/EXCLUDE/' ./${LIBN}.filter
	echo SCOREP_REGION_NAMES_END >> ./${LIBN}.filter
	sed -i -e 's#^//##' ./libwrap.h
	make clean
	I_MPI_CC=gcc I_MPI_CXX=g++ make install V=1 #intel compiler crashes for unknown reason
	scorep-info libwrap-summary
	cd $ROOTDIR/dep/; mv wrap_${LIBN}/ wrap_${LIBN}_gcc
	### wrapper for OMP-based apps
	LIBN=ompmklcpp
	scorep-libwrap-init --name ${LIBN} -x c++ --cppflags "-m64 -I${MKLROOT}/include -I`pwd`/wrap_${LIBN}" --ldflags "-L${MKLROOT}/lib/intel64" --libs "-Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl" --update wrap_${LIBN}
	cd wrap_${LIBN}/
	cp `find ${MKLROOT}/include -name mkl_blas.h` mkl_blas_underscore.h
	sed -i -e 's/\s*(/_(/' -e 's/_MKL_BLAS_H_/_MKL_BLAS_UNDERSCORE_H_/' ./mkl_blas_underscore.h
	sed -i -e '/^#endif/i #include<mkl_blas_underscore.h>\n\/\/#include<mkl_blas.h>\n\/\/#include<mkl_trans.h>\n\/\/#include<mkl_cblas.h>\n#include<mkl_lapack.h>\n\/\/#include<mkl_lapacke.h>' ./libwrap.h
	make V=1
	/usr/bin/grep -P '\-wrap [A-Z0-9_]+_$' ./${LIBN}.wrap | cut -d ' ' -f2 | sed -e 's/$/*/' >> ./${LIBN}.filter
	echo -e 'mkl_cblas_jit_create_dgemm_*\nmkl_cblas_jit_create_sgemm_*' >> ./${LIBN}.filter
	sed -i -e 's/SCOREP_REGION_NAMES_END/EXCLUDE/' ./${LIBN}.filter
	echo SCOREP_REGION_NAMES_END >> ./${LIBN}.filter
	sed -i -e 's#^//##' ./libwrap.h
	make clean
	make install V=1
	scorep-info libwrap-summary
	cd $ROOTDIR/dep/; mv wrap_${LIBN}/ wrap_${LIBN}_gcc
	### just a filter file in case of self-coded fn
	LIBN=tmp
	scorep-libwrap-init --name ${LIBN} -x c++ --cppflags "-m64 -I${MKLROOT}/include -I`pwd`/wrap_${LIBN}" --ldflags "-L${MKLROOT}/lib/intel64" --libs "-Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl" --update wrap_${LIBN}
	cd wrap_${LIBN}/
	cp `find ${MKLROOT}/include -name mkl_blas.h` mkl_blas_underscore.h
	sed -i -e 's/\s*(/_(/' -e 's/_MKL_BLAS_H_/_MKL_BLAS_UNDERSCORE_H_/' ./mkl_blas_underscore.h
	sed -i -e '/^#endif/i #include<mkl_blas_underscore.h>\n#include<mkl_blas.h>\n#include<mkl_trans.h>\n#include<mkl_cblas.h>\n#include<mkl_lapack.h>\n#include<mkl_lapacke.h>' ./libwrap.h
	make V=1
	echo -n -e "SCOREP_REGION_NAMES_BEGIN\n\tEXCLUDE *\n\tINCLUDE " > $ROOTDIR/dep/blas.filter
	/usr/bin/grep -P '\-wrap [a-zA-Z0-9_]' ./${LIBN}.wrap | cut -d ' ' -f2 | sed -e 's/_$//' | sort -u | sed -e 's/^/\t\t*/' -e 's/$/*/' >> $ROOTDIR/dep/blas.filter
	sed -i -e 's/E \t\t/E /' $ROOTDIR/dep/blas.filter
	echo -n -e "\t\t*matmul*\n\t\t*mult_su3_an*\n\t\t*mult_su3_na*\n\t\t*mult_su3_nn*\nSCOREP_REGION_NAMES_END" >> $ROOTDIR/dep/blas.filter
	cd $ROOTDIR/dep/; rm -rf ./wrap_${LIBN}
	spack unload scorep
	#
	spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`
	cd $ROOTDIR/dep/
	### wrapper for MPI/OMP-based apps
	LIBN=mklcpp
	scorep-libwrap-init --name ${LIBN} -x c++ --cppflags "-m64 -I${MKLROOT}/include -I`pwd`/wrap_${LIBN}" --ldflags "-L${MKLROOT}/lib/intel64" --libs "-Wl,--no-as-needed -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -liomp5 -lpthread -lm -ldl" --update wrap_${LIBN}
	cd wrap_${LIBN}/
	sed -i -e 's/= icpc/= mpiicpc/' ./Makefile
	cp `find ${MKLROOT}/include -name mkl_blas.h` mkl_blas_underscore.h
	sed -i -e 's/\s*(/_(/' -e 's/_MKL_BLAS_H_/_MKL_BLAS_UNDERSCORE_H_/' ./mkl_blas_underscore.h
	sed -i -e '/^#endif/i #include<mkl_blas_underscore.h>\n\/\/#include<mkl_blas.h>\n\/\/#include<mkl_trans.h>\n\/\/#include<mkl_cblas.h>\n#include<mkl_lapack.h>\n\/\/#include<mkl_lapacke.h>\n\/\/#include<mkl_pblas.h>\n\/\/#include<mkl_scalapack.h>' ./libwrap.h
	I_MPI_CC=gcc I_MPI_CXX=g++ make V=1         #intel compiler crashes for unknown reason
	/usr/bin/grep -P '\-wrap [A-Z0-9_]+_$' ./${LIBN}.wrap | cut -d ' ' -f2 | sed -e 's/$/*/' >> ./${LIBN}.filter
	echo -e 'mkl_cblas_jit_create_dgemm_*\nmkl_cblas_jit_create_sgemm_*' >> ./${LIBN}.filter
	sed -i -e 's/SCOREP_REGION_NAMES_END/EXCLUDE/' ./${LIBN}.filter
	echo SCOREP_REGION_NAMES_END >> ./${LIBN}.filter
	sed -i -e 's#^//##' ./libwrap.h
	make clean
	I_MPI_CC=gcc I_MPI_CXX=g++ make install V=1 #intel compiler crashes for unknown reason
	scorep-info libwrap-summary
	cd $ROOTDIR/dep/; mv wrap_${LIBN}/ wrap_${LIBN}_intel
	### wrapper for OMP-based apps
	LIBN=ompmklcpp
	scorep-libwrap-init --name ${LIBN} -x c++ --cppflags "-m64 -I${MKLROOT}/include -I`pwd`/wrap_${LIBN}" --ldflags "-L${MKLROOT}/lib/intel64" --libs "-Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl" --update wrap_${LIBN}
	cd wrap_${LIBN}/
	sed -i -e 's/= icpc/?= icpc/' ./Makefile
	cp `find ${MKLROOT}/include -name mkl_blas.h` mkl_blas_underscore.h
	sed -i -e 's/\s*(/_(/' -e 's/_MKL_BLAS_H_/_MKL_BLAS_UNDERSCORE_H_/' ./mkl_blas_underscore.h
	sed -i -e '/^#endif/i #include<mkl_blas_underscore.h>\n\/\/#include<mkl_blas.h>\n\/\/#include<mkl_trans.h>\n\/\/#include<mkl_cblas.h>\n#include<mkl_lapack.h>\n\/\/#include<mkl_lapacke.h>' ./libwrap.h
	SCOREP_CC=g++ make V=1
	/usr/bin/grep -P '\-wrap [A-Z0-9_]+_$' ./${LIBN}.wrap | cut -d ' ' -f2 | sed -e 's/$/*/' >> ./${LIBN}.filter
	echo -e 'mkl_cblas_jit_create_dgemm_*\nmkl_cblas_jit_create_sgemm_*' >> ./${LIBN}.filter
	sed -i -e 's/SCOREP_REGION_NAMES_END/EXCLUDE/' ./${LIBN}.filter
	echo SCOREP_REGION_NAMES_END >> ./${LIBN}.filter
	sed -i -e 's#^//##' ./libwrap.h
	make clean
	SCOREP_CC=g++ make install V=1
	scorep-info libwrap-summary
	cd $ROOTDIR/dep/; mv wrap_${LIBN}/ wrap_${LIBN}_intel
fi

echo -e "\nIn case of reboot, run again:
sudo sh -c 'echo 0 > /proc/sys/kernel/perf_event_paranoid'
sudo sh -c 'echo 0 > /proc/sys/kernel/nmi_watchdog'
cd $ROOTDIR/dep/msr-safe
sudo insmod msr-safe.ko
sudo chmod go+rw /dev/cpu/msr_whitelist
sudo cat ./whitelists/${WL} > /dev/cpu/msr_whitelist
sudo touch /sys/firmware/acpi/tables/MCFG
sudo chmod go+rw /sys/firmware/acpi/tables/MCFG
sudo chmod go+rw /dev/cpu/*/msr
sudo chmod go+rw /dev/cpu/*/msr_safe
sudo chmod go+rw /proc/bus/pci/*/*.*
sudo chmod go+rw /dev/mem
sudo tuned-adm off
sleep 5
sudo tuned-adm profile latency-performance
tuned-adm verify
export PATH=$ROOTDIR/dep/likwid/bin:\$PATH
export LD_LIBRARY_PATH=$ROOTDIR/dep/likwid/lib:\$LD_LIBRARY_PATH
#Xeon: likwid-setFrequencies -g performance --freq 2.2 --turbo 1 --umin 2.7 --umax 2.7
#KNL:  likwid-setFrequencies -g performance --freq 1.301 --turbo 1
#KNM:  likwid-setFrequencies -g performance --freq 1.501 --turbo 1
likwid-setFrequencies -p"
