# Matrix Engine Study
* The 'master' branch contains the measurement infrastructure for results presented in Fig.3
  and 4 and Table III of the publication.
* Branch 'gemm-power' contains stuff for Table II and branch 'tensorcores' contains stuff for
  Fig.2 and Table IV (contact A. Drozd for more details).
* For results shown in Table VIII contact D. Mukunoki.


## Dependencies (not included due to licensing; build/run scripts usually warn about it)
* Intel Compilers and VTune
* modylas-mini (optional benchmark; skip if not reproducing entire study)
* REVOCAP_Refiner (needed for FFB; optional benchmark; skip if not reproducing entire study)
* SPEC CPU/OMP/MPI (optional benchmark; skip if not reproducing entire study)


## Cloning the repo
```
git clone --recurse-submodules https://gitlab.com/domke/MEstudy.git
```


## Preparation of the environment
```
cd ./MEstudy/
vim ./conf/intel.cfg	# configure correct path to Intel's Parallel Studio XE
vim ./conf/host.cfg	# specify hostnames and CPU freqs (replace kiev,etc.)
./inst/_init.sh		# follow instructions if printed (eps. freq. settings)
```
(Note: root access and setcap will be required; setcap usually does not work
on remote file systems, so the repo should be cloned onto a local disk)


## Examplified installation of one proxy-app
```
./inst/amg.sh
```


## Examplified modification of proxy-app configurations
* allows to specify binary names, changes in input settings, number of benchmark
  trials, maximum running time of the benchmark, which performance tools is
  executed, and MPI/OMP sweep configurations per host system, etc
```
vim ./conf/amg.sh
```


## Running one of the proxy-apps (e.g. AMG)
```
vim ./conf/amg.sh	# replace BESTCONF or modify RUNSCOREP/RUNVTUNE if desired
./run/amg/prof.sh	# use performance tools to extract raw metrics
```
* all logs (results+raw data) will be placed in ./log/\<hostname\>/\<profrun\>/\<proxy\>


## Author's previously colleted results for reference
* untar the files with our raw data
```
cd ./MEstudy/
tar xjf paper/data/kiev0.tar.bz2	# Broadwell
```
* afterwards, raw data will be in ./log/kiev0


## Additional notes:
* this repo does not include 3rd party and proprietary libraries
* if a dependency is not met, the scripts usually point it out and ask the
  user to install the missing lib/tool/etc.
* the license (see ./LICENSE) applies ONLY to all source code, logs, etc.
  written/produced by the authors of this repo (for everything else: please,
  refer to individual licenses of the submodules (proxy-apps) and other
  libraries/dependencies after pulling those)


## Citing our MatrixEngine paper
Publications re-using (parts of) this repo and/or citing our work should refer to:

>J. Domke, E. Vatai, A. Drozd, P. Chen, Y. Oyama, L. Zhang, S. Salaria, D. Mukunoki, A. Podobas, M. Wahib, S. Matsuoka, "Matrix Engines for High Performance Computing: A Paragon of Performance or Grasping at Straws?," in Proceedings of the 35th IEEE International Parallel & Distributed Processing Symposium (IPDPS), (Portland, Oregon, USA), IEEE Computer Society, May 2021.

Bibtex Entry:

```bibtex
@inproceedings{domke_matrix_2021,
	address = {Portland, Oregon, USA},
	title = {Matrix {Engines} for {High} {Performance} {Computing}: {A} {Paragon} of {Performance} or {Grasping} at {Straws}?},
	booktitle = {2021 {IEEE} {International} {Parallel} and {Distributed} {Processing} {Symposium}, {IPDPS} 2021, {Portland}, {Oregon}, {USA}, {May} 17-21, 2021},
	author = {Domke, Jens and Vatai, Emil and Drozd, Aleksandr and Peng, Chen and Oyama, Yosuke and Zhang, Lingqi and Salaria, Shweta and Mukunoki, Daichi and Podobas, Artur and Wahib, Mohamed and Matsuoka, Satoshi},
	month = may,
	year = {2021},
	pages = {10},
}
```

## Development Team
Authored and maintained by Jens Domke ([@contact](http://domke.gitlab.io/#contact)) with help from E. Vatai, and others.
