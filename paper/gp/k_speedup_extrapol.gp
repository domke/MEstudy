set terminal svg size 400,500 dynamic enhanced fname 'Times' fsize 28 butt dashlength 1.0
set output "../figures/k_speedup_extrapol.svg"

set auto x
set style data histogram
#set style histogram rowstacked gap 1
set style histogram rowstacked title textcolor lt -1
set style data histograms
#set style fill solid border -1
set style fill   solid 1.00 border lt -1
set boxwidth 0.8
#set key Left reverse box
#set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key outside above center vertical Left reverse maxrow 4
set key samplen .3 spacing .7 width -1 height 0
set key font ",20"
set xlabel "Speedup through ME" offset -1,1
set xtic font ",24" rotate by 0 scale 0 left offset -.5,.4
set xrange [.5:7.5]
set yrange [0:100]
set ylabel "Node hours spent [%]" offset 2.5,0
set grid
set datafile missing "-"
set label "8" at 7,-8.7 center font ",32" rotate by 90
set label "8" at 7,-8.7 center font ",32" rotate by -90
set label "..." at 6,-4 center font ",38"

plot \
	"../data/k_speedup_extrapol.data" using  2:xtic(1) title 'NICAM', \
	"" using  3 title 'NTChem', \
	"" using  4 title 'mVMC', \
	"" using  5 title 'MatSc', \
	"" using  6 title 'NGSA', \
	"" using  7 title 'other' fs pattern 1 fs solid 0.25

