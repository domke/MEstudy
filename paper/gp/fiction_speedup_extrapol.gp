set terminal svg size 400,500 dynamic enhanced fname 'Times' fsize 28 butt dashlength 1.0
set output "../figures/fiction_speedup_extrapol.svg"

set auto x
set style data histogram
#set style histogram rowstacked gap 1
set style histogram rowstacked title textcolor lt -1
set style data histograms
#set style fill solid border -1
set style fill pattern 1 border lt -1
set boxwidth 0.8
#set key Left reverse box
#set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key outside above center vertical Left reverse maxrow 5
set key samplen .3 spacing .7 width -3 height 0
set key font ",18"
set xlabel "Speedup through ME" offset 1,1 font ",25"
set xtic font ",23" rotate by 0 scale 0 left offset -.5,.4
set xrange [.5:7.5]
set yrange [0:100]
set y2label "Node hours spent [%]" offset -3,0 font ",25"
set y2tics font ",25"
set noytics
set y2range [0:100]
set grid
set datafile missing "-"
set label "8" at 7,-10 center font ",32" rotate by 90
set label "8" at 7,-10 center font ",32" rotate by -90
set label "..." at 6,-6 center font ",28"

plot \
	"../data/fiction_speedup_extrapol.data" using  2:xtic(1) title 'BERT', \
	"" using  3 title 'NGSA', \
	"" using  4 title 'socorro', \
	"" using  5 title 'Laghos', \
	"" using  6 title 'WRF', \
	"" using  7 title 'bt331', \
	"" using  8 title 'NTChem', \
	"" using  9 title 'milc', \
	"" using  10 title 'botsspar' fs pattern 1 fs solid 0.25

