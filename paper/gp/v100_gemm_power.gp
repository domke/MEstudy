set terminal svg size 1000,450 dynamic enhanced fname 'Times' fsize 28 butt dashlength 1.0
set output "../figures/v100_gemm_power.svg"

set grid nopolar
set grid noxtics nomxtics ytics nomytics noztics nomztics nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

set key right bottom opaque vertical Left reverse noenhanced autotitles columnhead box width -1

set auto x
set xtics border in scale 1,0.5 nomirror norotate offset character 0, 0, 0 autojustify font ",24" offset 0,.3
set xrange [0:3176]

set auto y
set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autojustify
set yrange [0:400]
set ytics 100

set datafile missing '-'

set xlabel "Runtime [in ms]" offset 0,1
set ylabel "Power [in W]" offset 2.5,0

set arrow from 0,300 to 3176,300 nohead lc rgb 'black' dt 2 lw 3
set label "TDP" at 3000,320 center font ",28"

plot '../data/v100_gemm_power.data' \
	   u 3 w line lw 4 lc rgb '#d95f02' title 'DGEMM', \
	'' u 2 w line lw 4 lc rgb '#1b9e77' title 'SGEMM', \
	'' u 4 w line lw 4 lc rgb '#7570b3' title 'HGEMM (TC)'
