set terminal svg size 2600,600 dynamic enhanced fname 'Times' fsize 28 butt dashlength 1.0
set output "../figures/hpc_blas_usage.svg"

set auto x
set style data histogram
#set style histogram rowstacked gap 1
set style histogram rowstacked title textcolor lt -1
set style data histograms
#set style fill solid border -1
set style fill   solid 1.00 border lt -1
set boxwidth 0.8
#set key Left reverse box
set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key outside above center vertical Left reverse maxrow 1
set key invert samplen 2 spacing 1 width 0 height 0
set xtic font ",23" rotate by -45 scale 0 left offset -.5,0
set yrange [0:100]
set ylabel "BLAS/(Sca)LAPACK usage [%]" offset 2.5,0
set grid
set datafile missing "-"
set label "TOP500" at 0,-40 center font ",26"
set label "ECP" at 8,-40 center font ",26"
set label "RIKEN" at 19,-40 center font ",26"
set label "SPEC CPU" at 36,-40 center font ",26"
set label "SPEC OMP" at 56,-40 center font ",26"
set label "SPEC MPI" at 73,-40 center font ",26"

set arrow from 2,-45 to 2,-30, graph 1 nohead
set arrow from 14,-45 to 14,-30, graph 1 nohead
set arrow from 23,-45 to 23,-30, graph 1 nohead
set arrow from 48,-45 to 48,-30, graph 1 nohead
set arrow from 63,-45 to 63,-30, graph 1 nohead

plot \
	"../data/blas.data" using  4:xtic(2) title 'GEMM', \
	"" using  3 title 'BLAS', \
	"" using  5 title '(Sca)LAPACK', \
	"" using  6 title 'Other' fs pattern 1 fs solid 0.25

