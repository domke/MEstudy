set terminal svg size 310,500 dynamic enhanced fname 'Times' fsize 28 butt dashlength 1.0
set output "../figures/anl_speedup_extrapol.svg"

set auto x
set style data histogram
#set style histogram rowstacked gap 1
set style histogram rowstacked title textcolor lt -1
set style data histograms
#set style fill solid border -1
set style fill   solid 1.00 border lt -1
set boxwidth 0.8
#set key Left reverse box
#set key outside right top vertical Left reverse noenhanced autotitle columnhead nobox
set key outside above center vertical Left reverse maxrow 4
set key samplen .3 spacing .7 width -1 height 0
set key font ",20"
set xlabel "Speedup through ME" offset 0,1
set xtic font ",24" rotate by 0 scale 0 left offset -.5,.4
set xrange [.5:7.5]
set yrange [0:100]
#set ylabel "Node hours spent [%]" offset 2.5,0
set noytics
unset ytics
set grid
set datafile missing "-"
set label "8" at 7,-9 center font ",32" rotate by 90
set label "8" at 7,-9 center font ",32" rotate by -90
set label "..." at 6,-5 center font ",28"

plot \
	"../data/anl_speedup_extrapol.data" using  2:xtic(1) title 'miniAMR', \
	"" using  3 title 'CoMD', \
	"" using  4 title 'Laghos', \
	"" using  5 title 'Nekbone', \
	"" using  6 title 'miniTri', \
	"" using  7 title 'AMG', \
	"" using  8 title 'other' fs pattern 1 fs solid 0.25

