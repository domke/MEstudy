\section{Matrix Engines from a Hardware Perspective}\label{sec:hardware} % page 2-3
%\memo{@wahib/artur: need 1-2 into sentences; and maybe quick section outline; see sec.III as example}

\begin{table*}[tbp]
  \caption{Overview of existing and emerging general-purpose and AI architectures that leverage matrix engines to accelerate computations (\textbf{f16} = IEEE-754 16-bit or BFloat16, \textbf{f32} = IEEE-754 single prec., \textbf{f64} = IEEE-754 double prec.; \textbf{GF} = \unit{Gflop/s}; INT4/8 support omitted\big)}
    \centering\scriptsize
    \begin{tabularx}{\textwidth}{ |c|X|r|r|c|r|r|r|c| }
    \hline \hC
    \tH{\small Type} & \tH{\small System}  & \tH{\small Tech.} & \tH{\small Die size} & \tH{\small ME size} & \tH{\small \unit{Tflop/s} (f16)} & \tH{\small \unit{Tflop/s} (f32)} & \tH{\small \unit{Tflop/s} (f64)} & \tH{\small Support}  \\ \hline
    \multirow{4}{*}{\rotatebox[origin=c]{90}{General}}
     &  Intel Sapphire Rapids\footnotemark & \unit[10]{nm} & \multicolumn{1}{c|}{|} & 16x32 & \multicolumn{1}{c|}{|}  & \multicolumn{1}{c|}{|} & \multicolumn{1}{c|}{|} &  f16\\  \cline{2-9}
     &\cC  IBM Power10~\footnotemark        &\cC  \unit[7]{nm}   &\cC \unit[602]{mm\TSS{2}} &\cC  4x4     &\cC 16.4~~(\unit[27.2]{GF/mm\TSS{2}})  &\cC 8.2~(\unit[13.6]{GF/mm\TSS{2}})   &\cC 4.1~~(\unit[6.8]{GF/mm\TSS{2}}) &\cC f16, f32, f64 \\ \cline{2-9}
     &  NVIDIA Tesla V100     & \unit[12]{nm}   & \unit[815]{mm\TSS{2}} & 4x4x4    & 125.0~(\unit[153.4]{GF/mm\TSS{2}})  & 15.7~(\unit[19.3]{GF/mm\TSS{2}})  & 7.8~~(\unit[9.6]{GF/mm\TSS{2}}) & f16 \\ \cline{2-9}
     &\cC  NVIDIA Tesla A100\footnotemark     &\cC  \unit[7]{nm}   &\cC \unit[826]{mm\TSS{2}} &\cC 4x4x4    &\cC 312.0~(\unit[377.7]{GF/mm\TSS{2}})  &\cC 19.5~(\unit[23.6]{GF/mm\TSS{2}}) &\cC 19.5~(\unit[23.6]{GF/mm\TSS{2}}) &\cC f16, f32, f64 \\ \hline\hline
    \multirow{4}{*}{\rotatebox[origin=c]{90}{AI}}
     &  Google TPUv2          & \unit[20]{nm}   & \multicolumn{1}{c|}{|} & 128x128  & 45.0~~(|)   & \multicolumn{1}{c|}{|} & \multicolumn{1}{c|}{|} & f16 \\ \cline{2-9}
     &\cC  Google TPUv3          &\cC \unit[16]{nm}   & \multicolumn{1}{c|}{\cC |} &\cC 128x128  &\cC 90.0~~(|)    & \multicolumn{1}{c|}{\cC |} & \multicolumn{1}{c|}{\cC |} &\cC f16 \\ \cline{2-9}
     &  Habana Labs Gaudi     & \unit[16]{nm}   & \unit[500]{mm\TSS{2}} & Shared   & \multicolumn{1}{c|}{|} & \multicolumn{1}{c|}{|} & \multicolumn{1}{c|}{|} & f16, f32 \\ \cline{2-9}
     &\cC  Huawei Ascend 910\footnotemark     &\cC  \unit[7]{nm} &\cC \unit[1228]{mm\TSS{2}} &\cC 16x16x16 &\cC 256.0~(\unit[208.5]{GF/mm\TSS{2}})   & \multicolumn{1}{c|}{\cC |} & \multicolumn{1}{c|}{\cC |} &\cC f16 \\ \hline
    \end{tabularx}
    \label{tab:overview}
    \vspace{-4ex}
\end{table*} % leave above \section to avoid move to page 3
%Performance is estimated by assuming a 20 cores system running at \unit[2]{GHz}, and Systolic Array size estimated based on information in the 'Intel Architecture Instruction Set Extensions Programming Reference'.}
\addtocounter{footnote}{-3}\footnotetext{AMX listed for completeness (16x64 ME for INT8); performance unknown.}
\addtocounter{footnote}{+1}\footnotetext{Performance is calculated assuming 16 SMT8 cores running at \unit[4]{GHz}.}
\addtocounter{footnote}{+1}\footnotetext{The A100 also offers a unique hybrid 19-bit TF32 format combining a 10-bit mantissa of IEEE-754 half-precision and 8-bit exponent of BFloat16, yielding up to \unit[156]{Tflop/s} with TCs.}
\addtocounter{footnote}{+1}\footnotetext{Die size includes the Nimbus co-accelerator and four HBM2 stacks.}

In this section, we describe historical and current developments of ME adoption by vendors. The section is structured as follows: first, we discuss the historical lead up to MEs, then elaborate on current trends in hardware, and finally, we highlight the HPC community's motivation for leveraging MEs.


\subsection{Matrix Engines Lineage}\label{ssec:MElinage}
%The inclusion of MEs, from a chronological point of view, is logical. 
Early processors were scalar: executing a single operation on pairs of operands one at a time, possibly using Out-of-Order (OoO) and superscalar execution to improve performance via increased Instruction Level Parallelism (ILP)~\cite{wall1991limits}.
As time moved on, Moore's law facilitated the inclusion of more complex BLAS-1 (Vector-Vector) operations in hardware through the use of Single Instruction Multiple Data (SIMD)~\cite{duncan1990survey} units.
These additions, refined in systems such as ARM A64FX's Scalable Vector Extensions (SVE)~\cite{yoshida2018fujitsu}, Intel's AVX512~\cite{cornea2015intel}, or abstracted away through threading in NVIDIA's CUDA~\cite{kirk2007nvidia}, did increase the performance of processors at the cost of a small area of silicon; area that was available due to Moore's law.
Moving (or upgrading) these SIMD units to support GEMM operations is only the next natural step, with one small caveat: Moore's law is approaching its end~\cite{theis2017end}. The obvious question becomes: \textit{Are MEs really what we should be spending our silicon on, given that Moore's law is about to die out?}

\subsection{Matrix Engines: State-of-the-Art Performance and Trends}
We compile Table~\ref{tab:overview} to highlight some well known commercial general-purpose systems or AI accelerators that already exist or are about to be released (for the plethora of academic AI accelerators, we refer interested readers to surveys on the subject~\cite{reuther2019survey,podobas2020survey,wang2018survey}). All these new systems contain some form of ME integrated into the architecture.  From the general-purpose side, both the upcoming Intel Sapphire Rapids and the IBM Power10 will be augmented with MEs. According to public documents, IBM Power10~\cite{starke_ibms_2020} will be the more general CPU of the two, with support for a large variety of numerical representations (Int[4$|$8$|$16], FP[16$|$32$|$64]), while the Intel's AMX unit will focus more on AI applications (bfloat16), based on the available information in the programming reference~\cite{intelamx}. IBM Power10 features a hybrid model, which means that it accumulates into a wider representation than what it multiplies in; the only exception is for FP64, where it both multiplies and accumulates into the same representation.

From the GPU side, both NVIDIA~V100~\cite{choquette2018volta} and A100 feature MEs, called Tensor Cores, abbreviated TCs hereafter. The MEs in the V100 are hybrid, meaning that they accumulate into a wider numerical representation (in V100's case: FP32) than what it multiplies with (FP16). This limitation was overcome in A100, which supports up to double-precision (FP64) in its MEs.
From a performance perspective, the GPU-based systems---in particular the new A100---are grossly outperforming the other systems in both a higher peak performance (\unit{Tflop/s}) and a higher compute density (\unitfrac{Gflop/s~}{~mm\textsuperscript{2}}), where the IBM Power10 only reaches~18\% of the compute-density of an NVIDIA~V100. The NVIDIA~A100 is reported to reach up to \unit[312]{Tflop/s} of half-precision (FP16) performance.

The available information for AI-accelerators is more sparse and most architectures primarily report peak performance. Furthermore, most focus exclusively on bfloat16 (same as Intel Sapphire Rapids), and overall show a higher performance over general-purpose CPUs. The Habana Labs Gaudi~\cite{habana_labs_ltd_whitepaper_2019} architecture uses a shared ME unit accessible to all the cores, but most of the architectural details (including performance) are undisclosed. The highest performance and compute density of the AI accelerators delivers the Huawei's Ascend 910~\cite{liao2019davinci}, which reaches \unit[256]{Tflop/s} of raw FP16 performance, leading to \unitfrac[208]{Gflop/s~}{~mm\textsuperscript{2}} of compute density---nearly an order of magnitude~(7.7x) more than IBM~Power10 and but still only~55\% of the NVIDIA~A100's peak performance.

To summarize: modern architectures are moving towards integration of MEs into the core fabric.
Specializing the silicon towards acceleration of GEMM can be worthwhile, and can yield substantial increase in performance and compute densities (with orders of magnitude better performance see e.g., NVIDIA A100 vs.~P100 in FP16; the latter with \unit[18.7]{Tflop/s} peak).
This could, however, come at a cost and a potential loss in generality, which may be counter-productive from a HPC perspective.
%To summarize: modern architectures are moving towards integration of MEs into the core fabric. From the above discussion, it is clear that specializing the silicon towards acceleration of matrix multiplication can be worthwhile, and can yield substantial increase in performance and compute densities (with up to $\approx$7x improved compute density). This could, however, come at a cost and a potential loss in generality, which may be counter-productive from a HPC perspective.

\subsection{Matrix Engines: Motivation from HPC Side}\label{ssec:motivation-hpc}
The consideration for inclusion of MEs into future systems can be motivated by improvements in performance and energy efficiency by such a transition. Energy-consumption is one of the two limiting factors~\cite{shalf_exascale_2011}
(the other is cost) when building supercomputers, and any architectural choices that reduce energy (without sacrificing performance) will directly contribute to better systems. Furthermore, we can measure the impact of past architectural design-choices on energy consumption; choices that are similar to those we are looking at today. 

Consider, for example, exercising the Intel Xeon E5-2650v4 processor, which supports both scalar and vector instructions (AVX + AVX2) with a GEMM operation.
The energy-efficiency (in \unitfrac{flop~}{~J}) of this GEMM operation differs between the scalar and vectorized version and yields an average~2.3x observed increase in energy-efficiency, in favor of the vectorized version.
To measure this, we use OpenBLAS~\cite{openblas}, compiled with and without AVX support. %\footnote{\texttt{NO\_AVX}, \texttt{NO\_AVX2} and \texttt{NO\_AVX512} flags in the Makefile}.
OpenBLAS GEMM calls (for both single and double precision) are invoked with square matrices of size $n=5000$, and we repeat the call 30 times; resulting in a total of $2\cdot n^3 =$~\unit[7.5]{Tflop}. %not \unit[7.5]{Tflop/s}
The energy consumption is measured using Intel Performance Counter Monitor (PCM) and we list the results in Table~\ref{tbl:cpupower}. 
The similar results for both DGEMM and SGEMM suggests that this increase in efficiency is agnostic to the numerical precision for this particular experiment.
\begin{comment}
\memo{The following part is about A64FX where I couldn't make things work proerly. I'll need a review of it, I'm not sure it sould be included. --Emil}
We attempted similar experiments on A64FX with Scalable Vector Extension (SVE) \cite{armsve} \memo{is this the right citation? or should it be \cite{yoshida2018fujitsu}? --Emil}.
OpenBLAS did not have an SVE option/compiler flag which can be turned on or off, the performance difference between Fujitsu's BLAS implementation (SSL2) which do have a SVE and a NOSVE version had an unreasonable difference in performance (probably because the NOSVE version lacks other optimiztaions other then turning of SVE), so neither of these approaches was suitable for doing this experiment.
As a last resort, we investigated the power-efficiency of a GEMM-like triple nested loop on A64FX (which had poor performance both is \unitfrac{flop}{s} as well as \unitfrac{flop}{J}, incomparable to sophisticated BLAS implementations), but impact of enabling SVE resulted in a 2.23x improvement in power-efficiency (on average of single and double precision), which is similar to the value we got from the experiment on Intel Xeon.
\end{comment}

\begin{table}[tbp]
    \caption{Energy-eff. of Vector Extensions on a Intel Xeon CPU}
    \centering\scriptsize
    \begin{tabularx}{\columnwidth}{|c|c|r|R|}
    \hline \hC
    % \multirow{2}{*}{Matrix Size} & \multicolumn{2}{c|}{\bf{GPU}}          \\
    % & runtime (s) & Tflops              & runtime (s)            \\
    \tH{\small Precision} & \tH{\small Vector extension} & \tH{\small Walltime} & \tH{\small Energy-efficiency} \\\hline
    \multirow{2}{*}{DGEMM}
        & |         & \unit[34.22]{s}       & \unit[1.23]{Gflop/J} \\ \cline{2-4} %1.28 Gflops/watt
        &\cC AVX2   &\cC \unit[12.49]{s}    &\cC \unit[2.92]{Gflop/J} \\% 3.11 Gflops/watt
    \hline
    \multirow{2}{*}{SGEMM}
        & |         & \unit[16.79]{s}       & \unit[2.65]{Gflop/J} \\ \cline{2-4} %2.83 Gflops/watt
        &\cC AVX2   &\cC \unit[6.36]{s}     &\cC \unit[5.92]{Gflop/J} \\ \hline %6.64 Gflops/watt
    \end{tabularx}
    \label{tbl:cpupower}
    \vspace{-4ex}
\end{table}

Another example compares the energy efficiency of modern GPUs (in this case, an NVIDIA V100), which contains mixed-precision MEs. Executing a GEMM operation on such a processor, with and without using the built-in MEs, will lead to the observable performance and power consumption which we show in Figure~\ref{fig:power-gpu-tensorcore}. 
%We can observe that the accelerator's power consumption is agnostic to whether the MEs are used or not (Figure \memo{X}).
We observe that the accelerator's power can be greatly reduced using TCs. As Figure~\ref{fig:power-gpu-tensorcore} shows, we measure with square matrices of size $n=16384$ using TCs at half precision: see HGEMM (with TC), and GPU cores at single and double precision (see SGEMM \& DGEMM). Two important points to notea are: a) SGEMM and DGEMM draw power close to the TDP (\unit[300]{W}), and b) SGEMM or DGEMM cannot run concurrently with HGEMM. This indicates that SGEMM and DGEMM are already running at the highest possible performance (under TDP constraints), without any compromise due to resources occupied by the TCs.
%Note that SGEMM runs at power close to TDP (\unit[300]{W}), while DGEMM runs at \unit[270]{W}. 
%The energy used by the GPU is \unit[17]{J}, \unit[169]{J}, and \unit[304]{J}, respectively.
%\memo{(b) need some summary sentence for what we learn from this part}
%We generally show the energy-efficiency here, the more evaluation on GPUs will be reported at Section~\ref{sec:emulating-high-precision}. => not relevant to let reader jump that far
%\memo{@Chen: calculate the integral in Figure X}.

Finally, with Moore's law ending, adding architecture support is no longer free, but comes at the expense of removing something else, and there is a large number of other architectural optimization that could yield better performance and/or energy efficiency for the same amount of silicon, e.g., improved caches, more aggressive OoO, more cores, etc. Hence, to reason whether spending silicon on MEs is a good architectural direction to pursue for HPC, we first must answer a more critical question: \textit{To what extent do HPC applications and DL workloads actually invoke GEMM-like operations?}


\begin{figure}[tbp]
    \centering
    \includegraphics[width=.8\columnwidth]{figures/v100_gemm_power}\vspace{1ex}
    \caption{Power consumption evaluation of GPU cores and TCs on a single Tesla V100 GPU using squared matrices of size $n=16384$; collected using NVML API calls (via nvmlDeviceGetPowerUsage).}
    \label{fig:power-gpu-tensorcore}
    \vspace{-2ex}
\end{figure}

%\memo{MOTIVATION} Argue that HPC has historically been an adopter of solutions to fit the high-performance requirements (eg, commodity CPUs to replace vector machines in the 1990s, and GPUs designed originally for gaming to replace HPC-CPUs in 2000s). Therefore, HPC could very well be an adopter of specialized matrix engines if there is a practical benefit worth paying the cost of matrix engines in HPC applications. That is the primary motivation of this study; can we quantify the added benefit of using specialized matrix engines for workloads that would otherwise be using wider vectors? And assuming matrix engines would nonetheless prevail, can non dense matrix HPC applications adapt to use the matrix engines (even if at lower efficiency), for example A100 Tensor cores support sparse matrix operations.
%\todo[inline]{@Alex: could we highlight the multiple claims of orders of magnitude perf improvement. For instance Volta GPUs were advertised to be 10x in terms of flops, and TPUs advertise ~30x performance per watt(!) over GPUs (https://cloud.google.com/blog/products/gcp/an-in-depth-look-at-googles-first-tensor-processing-unit-tpu)}


%\memo{|||what to do with this part...}
%\input{11_eval_energy}
%\memo{...what to do with this part|||}
