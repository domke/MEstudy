\subsection{HPC Proxy-Applications and SPEC Benchmarks}\label{ssec:hpcapps}
In contrast to the previous section, here we are in looking into more traditional supercomputing and HPC applications.

\subsubsection{Application Overview and Input Selection}
%\todo[inline]{@Jens: proxy apps / ecp}
%\todo[inline]{@Jens: SPEC at al.}
%\todo[inline]{@Jens: spec cpu 2017 V1.1}
%\todo[inline]{@Jens: spec omp 2012 V1.1}
%\todo[inline]{@Jens: spec mpi 2007 V2.0.1}
We select benchmarks from six different sets which represent
typical workloads and are commonly used by HPC centers and vendors for architecture comparisons and hardware procurement:
\begin{itemize}[leftmargin=2.5mm]
    \item TOP500 Benchmarks~\cite{strohmaier_top500_2018}: The HPC community
    utilizes High Performance Linpack (HPL) and High Performance Conjugate Gradients (HPCG) for a world-wide performance ranking of supercomputers and HPC systems.
    \item Exascale Computing Project (ECP) Proxy Applications~\cite{exascale_computing_project_ecp_2018}: The ECP released with version 1.0 a set of~12 workloads, which are used for procurement by HPC centers in the USA. We investigate eleven of them in our study\footnote{We exclude CANDLE, since Section~\ref{ssec:ml} covers AI workloads extensively.}.
    \item RIKEN CCS' Fiber Miniapp Suite~\cite{riken_aics_fiber_2015}: The eight proxy applications developed by RIKEN CCS represent the priority areas of the Japanese government and were used in the procurement of Supercomputer Fugaku~\cite{matsuoka_a64fx_2019}.
    \item SPEC Benchmarks~\cite{standard_performance_evaluation_corporation_specs_2020}: While the SPEC corporation hosts benchmarks for numerous areas of interest, such as clouds or accelerators, we focus on SPEC CPU 2017~V1.1 (24 benchmarks; contains OpenMP-accelerated versions), and the two sets derived from HPC workloads, namely SPEC OMP 2012~V1.1 (14) and SPEC MPI 2007~V2.0.1 (18).
\end{itemize}
The detailed list of all 77 HPC benchmarks, i.e., individual names per set, as well as an overview of the scientific domain they are representing, is available
in Table~\ref{tab:appsoverview}.

Our selection of inputs and configurations for the individual benchmarks of the TOP500,
ECP, and RIKEN Fiber sets remains unchanged from our previous publication on
double-precision floating-point unit utilization, and therefore readers may
consult Sec.~II (B) of~\cite{domke_double-precision_2019} for further details.

SPEC benchmarks generally provide three different input sizes (ordered by short
to long runtime): \texttt{test}, \texttt{train}, and \texttt{ref}(erence). The
difference between those three options is usually the problem size, e.g., time
steps, grid size, \#atoms, etc., with a few exceptions as stated in the SPEC
documentation (yet we expect no major changes in compute patterns). Hence, we
select the \texttt{train} input configuration for all our SPEC measurements.
For SPEC MPI \texttt{mtrain} is preferred over \texttt{ltrain} whenever possible.
Furthermore, we setup SPEC to perform \texttt{peak} runs (not \texttt{base})
which allows the usage of OpenMP threads for SPEC CPU benchmarks.


\subsubsection{Measurement Methodology}\label{ssec:swmethod}
From our prior work~\cite{domke_double-precision_2019}, we know that TOP500,
ECP, and RIKEN's benchmarks are highly optimized proxies for the represented applications (with the sole exception of Laghos), which utilize high performance
math libraries, such as Intel's MKL whenever suitable. To identify the dense,
GEMM-like operation which could be accelerated by a ME, we employ the
profiling functionality of the Score-P performance analysis
tool~\cite{knupfer_score-p_2012}. We create a Score-P library wrapper for all
functions found in the header files for dense-matrix compute libraries of MKL, i.e.,
(C)BLAS, PBLAS, BLAS-like Extension Transposition Routines, (C)LAPACK, and
ScaLAPACK.

Furthermore, since the applications can exhibit considerable amounts
of initialization and post-processing phases, we identify these phases
and exclude them from profiling via Score-P API calls. Additionally, we search
the source codes for function names indicating GEMM operations or Fortran's
\texttt{matmul} intrinsic, and instrument them to be included in our application profiles for the TOP500, ECP and RIKEN's set.

Unfortunately, SPEC benchmarks are implemented without external dependencies
for portability and ease of use reasons. Hence, we are not able to rely
on our Score-P library wrapper for these three SPEC sets. Our workaround involves
a three step approach---besides searching the source codes for relevant
function names as above---before collecting the necessary metrics: first, we
utilize the Intel Advisor tool to perform a Roofline analysis and extract
all source code regions which are tagged as ``compute intensive'', meaning
functions and loops with sufficient Arithmetic Intensity, i.e., flop/byte
ratio~$\geq$~7 for our CPU testbed (cf.~System 1 in Section~\ref{ssec:hwforcpu}).
This list was further pruned by requiring a Point Weight
(PtW)~$\geq$~1, i.e., the self-elapse time of this region is at least~1\%
of the total elapse time of the benchmark. The next step consists of
\textit{two independent, manual code inspection of all~598 location}
to determine their compute pattern (majority vote; with third check if needed),
and instrument them to be included in our application
profiles when they perform a GEMM-like operation. We identify and
instrument~14 source code locations\footnote{Similarly, we identified numerous hand-written GEMM kernels in Nekbone.}.
And lastly, we rely on Score-P's automatic
compiler instrumentation and function filter ability---filtering the
same function names we collected for the MKL wrapper above---to profile
source code regions where the benchmark programmer replaced external
library calls with hand-written functions. For SPEC benchmarks, we omit
the exclusion of initialization and post-processing phases, since these
are negligible\footnote{According to SPEC's rules for benchmark submission:
95\% of runtime must be compute-bound and spend within the benchmark's
own source code.}.

For TOP500, ECP and RIKEN's benchmarks, we compile and execute the tests
equivalently (except for FFB\footnote{Using combination of Score-P and
Intel Compiler for FFB results in erroneous intermediate results and early
benchmark termination.}) to our previous work (i.e., using the Intel compiler suite,
compiler flags as listed in Sec.~III~(A), and a combination of MPI processes
and OpenMP threads as provided in Tab.~IV of~\cite{domke_double-precision_2019}).
These combinations yield the best performance for each application on our
benchmarking hardware.

In contrast, we primarily make use of the GNU compiler
suite for SPEC benchmarks\footnote{Only for GNU compilers, Score-P performs 
filtering of instrumented function at compile time, not at runtime,
reducing the profiling overhead.}, except for botsspar, bt331, fds4, perlbench,
and socorro, which require Intel's compilers to avoid runtime issues.
The main compiler optimizations are \texttt{-O3}, \texttt{-march=native} or
\texttt{-xHOST} (depending on the compiler suite), and additional flags for
vectorization, loop unrolling, and fast math\footnote{For compilation details
see our framework: \url{gitlab.com/domke/MEstudy}}. We run SPEC CPU and OMP
benchmarks consecutively and with~48 OpenMP threads to match the number of logical cores on our testbed. Similarly, all SPEC~MPI test are run
with~48 MPI processes matching the core count, because SPEC MPI benchmarks
are not OpenMP-parallelized. In the next section, we will provide the results
and analysis of our profiling measurements.


\subsubsection{Evaluation of Measurement Results}\label{ssec:hpceval} 
%
\begin{figure*}[tbp]
    \begin{center}
    \includegraphics[width=\textwidth]{figures/hpc_blas_usage}\vspace{1ex}
    \caption{GEMM, BLAS (non-GEMM functions), and (Sca)LAPACK utilization across 77 HPC benchmarks, see Section~\ref{ssec:hpceval} and  Table~\ref{tab:appsoverview} for details [Data for blender(R) missing due to unresolvable runtime errors; however, the source code contains GEMM function calls]}
    \label{fig:hpcgemm}
    \end{center}
    \vspace{-4ex}
\end{figure*}

The results of our measurements, as outlined in Section~\ref{ssec:swmethod},
to identify the GEMM usage per HPC (proxy-)application is visualized in
Figure~\ref{fig:hpcgemm}. We analyze the Score-P profiles for the runtime
spend in the profiled regions\footnote{Percentages
derived from ratio of \{region runtime\} to \{total application runtime minus
MPI\_Init/Finalize, and initialization/post-processing phases\}}.
Furthermore, we distinguish between, and show, four different compute regions
in the Figure: (1) GEMM operations, (2) BLAS operations (non-GEMM), i.e., all
BLAS Level[1$|$2$|$3] functions except for matrix-matrix multiplications,
(3) LAPACK and ScaLAPACK functions, and (4) all other code regions.
This distinction allows for an easy classification of code sections which can
be either directly accelerated~(1), or potentially indirectly accelerated~(2 \& 3),
or most probably not accelerated~(4) by MEs. To clarify, the
indirectly acceleration can result from use of GEMM operations within (Sca)LAPACK
routines or from mapping other BLAS function (such as GEMV) to MEs.

Unfortunately, as we can see from Figure~\ref{fig:hpcgemm}, the number of existing HPC
applications and workloads which can directly benefit from MEs is
rather sparse. Obviously, the most dominant example is High Performance Linpack.
In our test, HPL's compute kernel spends 76.81\% in GEMM operations and 0.14\%
in other BLAS function, and hence, executing the same test on a CPU with matrix
engine could substantially reduce the runtime of HPL. Other combinations of
benchmarks and chosen input parameters which perform GEMM are:
Laghos~(41.24\%), NTChem~(25.78\%), Nekbone~(4.58\%), SPEC OMP's botsspar~(18.9\%) and
bt331~(14.16\%), and SPEC MPI's milc \& dmilc~(40.16\% and 35.57\%, respectively) and
socorro benchmark~(9.52\%). We have instrumented GEMM regions in other
benchmarks as well, but our measurement indicate that
these regions are either dormant, or the type of input results in
alternative code paths, bypassing the GEMM operations.

Furthermore, we see that MiniFE, NTChem, mVMC, and socorro utilizes other BLAS 
functions during 9.38\%, 0.45\%, 16.41\%, and 0.99\% of their runtimes, respectively.
Upon closer inspection of the profiles, it is unlikely that the former two
benchmarks can benefit from MEs, because they utilize only BLAS Level 1 (i.e., 
vector-vector operations), while the other two also call BLAS Level 2 functions.
Looking at the (Sca)LAPACK dependencies and percentages of runtime in
Figure~\ref{fig:hpcgemm} yields similar results: mVMC spends noticeable amount
of runtime (14.35\%) in those libraries, while NTChem (0.95\%) and socorro's
(0.73\%) usage is negligible. Nevertheless, only these three out of all 77
could benefit from MEs if the used (Sca)LAPACK functions can be mapped to them.

Overall, we can state that only ten out of the 77 HPC benchmarks, which we
investigate at for this study, perform GEMM operation or outsource dense
linear algebra computations to libraries. Assuming an idealized equal
distribution of the node hours spend per our 77 benchmark on a supercomputer
results in an average runtime of 3.5\% spend in GEMM operations, or roughly
12 and a half days of a full year.
