\section{Matrix Engines from a Software Perspective}\label{sec:software} % page 4-7
In this section we describe, categorize, and analyze a broad set of
applications and benchmarks, from traditional HPC workloads
to modern Deep Learning, and analyze historical data from one supercomputer. The section is
structured as follows: first we perform a historical data and an offline
software dependency analysis, then conduct various measurements on AI and HPC
applications, and finally, for completeness, provide exact details about our
evaluation environments.

\subsection{Analysing Historical Data of the K Computer}\label{ssec:Klogs}
%\todo[inline]{@Jens: riken library calls data base (if we get the data from shoji)}
For the K Computer, RIKEN collected multiple
metrics of the executed application~\cite{yamamoto_k_2014} and the system,
such as power consumption and failure statistics. For parallel applications, launched via MPI, their database includes additional
information about the application binary's symbol table (through the Linux tool \texttt{nm}), but excludes data from linked shared libraries.

We analyze this data for a full year of K's operation (April~'18
to March~'19), during which the system executed $487,563$ scientific
applications distributed over 543 million node hours. Symbol table data is only
available for~96\% of these node hours, while the remainder can be attributed
to interactive jobs, non-parallel jobs, or jobs for which the collection was
intentionally disabled by the user. When searching the \texttt{nm} data for GEMM
functions, we can attribute~53.4\%, or $277,258,182$ node hours, to
applications which likely executed GEMM operations\footnote{Fujitsu's
compiler infrastructure defaults to selectively including individual functions
from their math kernel library, instead of linking the entire library.}.
However, the data does not reveal the exact node hours spent inside of GEMM routines. 
%So, in the absolute best case, MEs could have reduced over half of K's node hours.

To conclude, in the absolute best case, the inclusion of MEs into the K computer could (in theory) have halved the number of node hours without any loss in quality for the applications, but a significant reduction in energy consumption (and, possibly, repair-costs), or an increase science throughput of the machine.

\subsection{Software Dependency Analysis via Spack}\label{ssec:spack}
%\todo[inline]{@Jens: offline analysis with spack}
The analysis of the job history of the K computer is rather narrow, both in
terms of common HPC workloads found in other countries as well as in terms of
possible math libraries providing and utilizing GEMM kernels.
Hence, we broaden the scope by also analyzing the software/library dependencies in Spack~\cite{gamblin_spack_2015}, which is a package manager that targets supercomputers. Spack provides easy access to thousands of scientific software packages to users without the necessity of the administrator's installation and maintenance.

We identified all libraries, which provide dense linear algebra routines, among
the~4,371 packages currently supported by Spack, namely: AMD BLIS, Atlas, BLIS,
Eigen, ESSL, Intel MKL, Netlib's LAPACK, ScaLAPACK and XBLAS, OpenBLAS, CUDA (cuBLAS), py-blis, libxsmm,
and veclibfort. Hereafter, we refer to these packages as BLAS libraries
with dependency distance~of~0. Using Spack's ability to track and list dependencies,
we subsequently identify packages which directly depend on libraries of
dependency distance~0, i.e., a set of packages with dependency distance of~1, and
so on and so forth. While the number of packages with dependency distance~0 is
only~14, the number increases to~239 in the next step (excluding the set of
dependency distance~0), or~5.47\% of all Spack packages.

Table~\ref{tab:spackblas}
lists further dependency distances, with the last column adjusting for the fact
that Spack includes a large number of sub-packages for Python and~R, which we
merge under their parent packages. As we can see, \textit{51\% (or 70\% without sub-package
adjustment) of Spack's packages depend directly or indirectly on BLAS libraries}.
While the similarity of the percentage with Section~\ref{ssec:Klogs} is mere
coincidental, our findings show that in the best case, only about half the packages could benefit from MEs. Unfortunately, our analysis is oblivious of a more important metric: \textit{How much time of the execution is actually spent in these BLAS routines?} In order to reason around said question, we split our efforts into two paths: \textbf{(i)}~What is the impact of using MEs in Deep Learning workloads, which are known to contain a lot of GEMM operations?, and \textbf{(ii)}~What \textit{could} be the impact of using MEs in regular HPC applications whose GEMM usage today remains unquantified?

%the conclusion remains the same: In the absolute best case, only half of packages benefit from matrix engines.


\begin{table}[tbp]
    \caption{Dependency Analysis of Dense Linear Algebra Libraries for Spack
    Package Manager (w/ \& w/o Python and R sub-packages)}
    \centering\scriptsize
    \begin{tabularx}{\columnwidth}{|c|R|R|}
    \hline \hC
    \tH{Dependency Distance} & \tH{\# and \% of Packages} & \tH{excluding py-* \& R-*} \\ \hline
    % total w/o r-/py- is: 2548
    $0$             & 14~~~(0.32)~    & 14~~~(0.55)~    \\ \hline \rC
    $1$             & 239~~~(5.47)~   & 226~~~(8.87)~   \\ \hline
    $2$             & 762~~(17.43)~   & 541~~(21.23)~   \\ \hline \rC
    $3$             & 968~~(22.15)~   & 714~~(28.02)~   \\ \hline\hline
    $1$--$\infty$   & 3061~~(70.03)~  & 1311~~(51.45)~  \\ \hline
    \end{tabularx}
    \label{tab:spackblas}
    \vspace{-4ex}
\end{table}


\input{21_AIworkloads}


\begin{table*}[tbp]
    \caption{\label{table:APPS} Overview of (Proxy-) Applications used for this Study; `(R)' for SPEC CPU indicates lack of OpenMP parallelization}
    \centering\scriptsize
    \begin{tabularx}{\textwidth}{l V{3} X|l V{3} X|l V{3} X|l|}
    \hline \hC
    \tH{\small Set} & \tH{\small Name} & \tH{\small Sci. / Eng. / AI Domain} & \tH{\small Name} & \tH{\small Sci. / Eng. / AI Domain} & \tH{\small Name} & \tH{\small Sci. / Eng. / AI Domain} \\ \hline
    \multirow{4}{*}{Deep Learning}
        & BERT & Natural Language Processing & 
        DeepLabV3 & Image Segmentation 
        & GRU & Single Layer\\ \cline{2-7}
        &\cC Cosmoflow  &\cC Computational Cosmology&
        \cC SSD300 &\cC Object Detection  &
        \cC LSTM &\cC Single Layer\\ \cline{2-7}
        & VGG16 & Image Recognition & 
        NCF & Recommender Systems& 
        Conv2D & Single Layer\\ \cline{2-7}
        &\cC Resnet50 &\cC Image Recognition
        &\cC GEMM &\cC Single Layer
        &\cC Attention &\cC Single Layer\\
    \midrule
    \multirow{1}{*}{TOP500}
        %& HPL & Math/Computer Science & HPCG & Math/Computer Science & HPL-AI & Artificial Intelligence \\
        & HPL & Math/Computer Science & HPCG & Math/Computer Science & & \\
    \midrule
    \multirow{4}{*}{ECP}
        &\cC AMG &\cC Physics and Bioscience &\cC miniAMR &\cC Geoscience/Earthscience &\cC SW4lite &\cC Geoscience/Earthscience \\ \cline{2-7}
        & CoMD & Material Science/Engineering & miniFE & Physics & SWFFT & Physics \\ \cline{2-7}
        &\cC Laghos &\cC Physics &\cC miniTRI &\cC Math/Computer Science &\cC XSBench &\cC Physics \\ \cline{2-7}
        & MACSio & Math/Computer Science & Nekbone & Engineering (Mechanics, CFD) & & \\
    \midrule
    \multirow{3}{*}{RIKEN}
        &\cC FFB &\cC Engineering (Mechanics, CFD) &\cC mVMC &\cC Physics &\cC NTChem &\cC Chemistry \\ \cline{2-7}
        & FFVC & Engineering (Mechanics, CFD) & NGSA & Bioscience & QCD & Lattice QCD \\ \cline{2-7}
        &\cC MODYLAS &\cC Physics and Chemistry &\cC NICAM &\cC Geoscience/Earthscience &\cC &\cC \\
    \midrule
    \multirow{8}{*}{SPEC CPU}
        & blender(R) & Math/Computer Science & exchange2 & Artificial Intelligence & omnetpp & Math/Computer Science \\ \cline{2-7}
        &\cC cam4(R) &\cC Geoscience/Earthscience &\cC fotonik3d &\cC Physics &\cC perlbench &\cC Math/Computer Science \\ \cline{2-7}
        & namd(R) & Material Science/Engineering & gcc & Math/Computer Science & pop2 & Geoscience/Earthscience \\ \cline{2-7}
        &\cC parest(R) &\cC Bioscience &\cC imagick &\cC Math/Computer Science &\cC wrf &\cC Geoscience/Earthscience \\ \cline{2-7}
        & povray(R) & Math/Computer Science & lbm & Engineering (Mechanics, CFD) & roms & Geoscience/Earthscience \\ \cline{2-7}
        &\cC bwaves &\cC Physics &\cC leela &\cC Artificial Intelligence &\cC x264 &\cC Math/Computer Science \\ \cline{2-7}
        & cactuBSSN & Physics & mcf & Math/Computer Science & xalancbmk & Math/Computer Science \\ \cline{2-7}
        &\cC deepsjeng &\cC Artificial Intelligence &\cC nab &\cC Material Science/Engineering &\cC xz &\cC Math/Computer Science \\
    \midrule
    \multirow{5}{*}{SPEC OMP}
        & applu331 & Engineering (Mechanics, CFD) & fma3d & Physics & mgrid331 & Engineering (Mechanics, CFD) \\ \cline{2-7}
        &\cC botsalgn &\cC Bioscience &\cC ilbdc &\cC Engineering (Mechanics, CFD) &\cC nab &\cC Chemistry \\ \cline{2-7}
        & botsspar & Math/Computer Science & imagick & Math/Computer Science & smithwa & Bioscience \\ \cline{2-7}
        &\cC bt331 &\cC Engineering (Mechanics, CFD) &\cC kdtree &\cC Math/Computer Science &\cC swim &\cC Geoscience/Earthscience \\ \cline{2-7}
        & bwaves & Engineering (Mechanics, CFD) & md & Material Science/Engineering & & \\
    \midrule
    \multirow{5}{*}{SPEC MPI}
        &\cC $[$d$]$leslie3d &\cC Engineering (Mechanics, CFD) &\cC $[$l$]$GemsFDTD &\cC Physics &\cC socorro &\cC Material Science/Engineering \\ \cline{2-7}
        & $[$d$]$milc & Lattice QCD & lu & Engineering (Mechanics, CFD) & tachyon & Math/Computer Science \\ \cline{2-7}
        &\cC fds4 &\cC Engineering (Mechanics, CFD) &\cC $[$l$]$wrf2 &\cC Geoscience/Earthscience &\cC tera\_tf &\cC Geoscience/Earthscience \\ \cline{2-7}
        & GAPgeofem & Physics & pop2 & Geoscience/Earthscience & zeusmp2 & Engineering (Mechanics, CFD) \\ \cline{2-7}
        &\cC lammps &\cC Material Science/Engineering &\cC RAxML &\cC Bioscience &\cC &\cC \\ \hline
    \end{tabularx}
    \label{tab:appsoverview}
    \vspace{-3ex}
\end{table*}


\input{25_HPCworkloads}


\subsection{Details of Evaluation Environments for Reproducibility}\label{ssec:hwswlist}
%\memo{@wahib: can we move C. this to end of section III; it kinda breaks the flow betw B. and D.???}
\subsubsection{CPU-based Measurements}\label{ssec:hwforcpu}
\begin{comment}
\memo{originalpart1:}
For the CPU-based runs of HPC proxy-applications in Section~\ref{ssec:hpcapps},
we use the following hardware testbed: a dual-socket x86\_64 compute node with Intel Xeon CPUs and \unit[256]{GiB} main memory.

% The power efficiency evaluation in Section~\ref{sec:motivation-hpc} are performed on one single-socket ARM A64FX compute node, with \unit[32]{GiB} HBM2 memory, of the Supercomputer Fugaku. 
% Further details about the two hardware and operating system configurations are listed in Table~\ref{tab:hwconf}.

\memo{originalpart2:}
The power efficiency evaluation in Section~\ref{ssec:motivation-hpc} are performed on one dual-socket Intel Xeon.
Further details about the two hardware and operating system configurations are listed in Table~\ref{tab:hwconf}.

\memo{Merged:}
\end{comment}
For the CPU-based measurement of our HPC (proxy-)applications in Section~\ref{ssec:hpcapps}, and the power efficiency evaluation in Section~\ref{ssec:motivation-hpc} we use a dual-socket x86\_64 compute node with Intel Xeon CPUs.
The details of this system are listed in Table~\ref{tab:hwconf} under \textbf{System 1}.

\subsubsection{GPU-based Measurements}\label{ssec:hwforgpu}
%\memo{Chen's stuff Subsection~\ref{ssec:motivation-hpc} here}. 
For the DL energy-efficiency measurements in Section~\ref{ssec:ml} we use NVIDIA GPUs ranging from consumer models, i.e., GTX~1060 \& 1080~Ti and RTX~2070 \& 2080~Ti, up to the data center lineup of Tesla P100-PCIE and V100-SXM2 GPUs. Additionally, the section also includes results of a Intel Xeon Gold CPU (compute node of ABCI~\cite{national_institute_of_advanced_industrial_science_and_technology_aist_ai_2020}; see \textbf{System 2} in Table~\ref{tab:hwconf}) for comparisons. 
The analysis of TC utilization by different kernels, see Table~\ref{table:DL-TC-speedups}, as well as power evaluation of GPU cores in Section~\ref{ssec:motivation-hpc}, and the comparisons between native and emulated precision GEMMs in Section~\ref{sec:emulating-high-precision}, is done on a Tesla V100-SXM2 of ABCI.
%The analysis of TC utilization by different kernels is done on the Tesla V100-SXM2 of ABCI.
%Last but not least, for the power evaluation of GPU cores in Section~\ref{ssec:motivation-hpc}, and the comparisons between native and emulated precision GEMMs in Section~\ref{sec:emulating-high-precision}, we utilize a PCIe version of the Tesla V100.
%\memo{check:\textcolor{blue}{Daichi wrote: this is incorrect. The results on Table VIII in Section~\ref{sec:emulating-high-precision} were obtained using Tesla V100-SXM2-16GB. I'm confirming with Chen-san just in case.}}
% \memo{I commented out the little text and memo that was here (check the source, but I think it can/should be deleted). I guess Chen has to check this part and it is done. Or dunno which GPUs Chen used, maybe it should be split into two separate sentences.  Also enumerating all the GPUs here feels a be redundant.--Emil}

% This should probably be deleted. --Emil
% \memo{@alex/emil/Chen: Elaborate on used GPUs; check table and add more / remove if needed}
% "...various NVIDIA GPUs from gaming and data center lineup starting from GTX 1060 up to Tesla V100 16GB..."
% \memo{do we have to incl. xeon gold in table?}

\subsubsection{Auxiliary Software}\label{ssec:auxsw}
%Intel's Parallel Studio XE (version 2019; update 1)
%GNU Compiler Collection 8.4.0
%Spack 0.15.1 \cite{gamblin_spack_2015}
%Score-P 6.0 \cite{knupfer_score-p_2012}
%Intel Advisor Version 2020; Update 2~\footnote{Version 2019 (of Intel's P.S. XE) lacks needed feature for roofline analysis.}
%NVIDIA CUDA Toolkit 10.1 (10.0 for BLAS measurements on V100) and CUDA Deep Neural Network library (cuDNN) 7.6.5.
%\memo{pytorch 1.6 libraries
%cpu-based stuff integrated into fp64 eval %framework~\cite{domke_double-precision_2019}
%gpu-based stuff integrated into benchmarker~\cite{}}
We employ various auxiliary software packages to facilitate our measurements
on the different hardware architecture, such as compilers, Deep Neural Network
(DNN) libraries, and performance analysis frameworks. The main software
components are listed in Table~\ref{tab:swconf}.

\begin{table}[tbp]
    \caption{CPU-based Compute Nodes used for Measurements}
    \centering\scriptsize
    \begin{tabularx}{\columnwidth}{|X|l|l|}
    \hline \hC
    \tH{~}      & \tH{\small System 1~(\cref{ssec:motivation-hpc},~\cref{ssec:hpceval})}          & \tH{\small System 2~(\cref{sssec:mlperf:measurements})} \\ \hline
    Mainboard   & Supermicro X10DRG-Q           & Fujitsu Primergy-RX2540-M4 \\ \hline \rC
    CPU         & 2x Intel Xeon E5-2650v4       & Intel Xeon Gold 6148 \\ \hline
    \#Cores     & 24 (hyper-threading enabled)  & 20 (hyper-threading enabled) \\ \hline \rC
    Memory      & \unit[256]{GiB} DDR4 \unit[2400]{MHz} & \unit[32]{GiB} DDR4 \unit[2666]{MHz} \\ \hline
    OS          & CentOS Linux (v7.8.2003)      & CentOS Linux (v7.5.1804) \\ \hline \rC
    Kernel      & 3.10.0-1062                   & 3.10.0-862 \\ \hline
    \end{tabularx}
    \label{tab:hwconf}
    \vspace{-1ex}
\end{table}

\begin{table}[tbp]
    \caption{Auxiliary Software used for Measurements}
    \centering\scriptsize
    \begin{tabularx}{\columnwidth}{|r|l|r|l|}
    \hline \hC
    \tH{\small Package}    & \tH{\small Vers.}   & \tH{\small Package}  & \tH{\small Vers.} \\ \hline
    Intel Parallel Studio XE        & 2019; U1        & NVIDIA CUDA Toolkit           & 10.[1\&2]\\ \hline \rC
    Intel Advisor                   & 2020; U2\footnotemark & NVIDIA cuDNN & 7.6.5\\ \hline
    GNU Compiler Coll.         & 8.4.0                 & PyTorch ML Framework          & 7.6.5 \\ \hline \rC
    FUJITSU Software Suite          & 1.2.27b               & Score-P Analysis Frame.    & 6.0 \\ \hline
    Spack Package Manger            & 0.15.1                & Intel PCM tools   & 201710 \\ \hline
    \end{tabularx}
    \label{tab:swconf}
    \vspace{-3ex}
\end{table}
\footnotetext{Version 2019 (of Intel's P.S. XE) lacks needed feature for roofline analysis.}



\begin{comment}
\begin{table}[tbp]
    \caption{Auxiliary Software during Measurements}
    \centering\scriptsize
    \begin{tabularx}{\columnwidth}{|R|X|}
    \hline \hC
    \tH{\small Software Package} & \tH{\small Version} \\ \hline
    Intel Parallel Studio XE        & 2019; Update 1 \\ \hline \rC
    Intel Advisor                   & 2020; Update 2\footnote{Version 2019 (of Intel's P.S. XE) lacks needed feature for roofline analysis.} \\ \hline
    GNU Compiler Collection         & 8.4.0 \\ \hline \rC
    FUJITSU Software Suite     &\cC 1.2.27b \\ \hline
    NVIDIA CUDA Toolkit             & 10.1 / 10.2 \\ \hline \rC
    NVIDIA CUDA DNN (cuDNN)    &\cC 7.6.5 \\ \hline
    PyTorch ML Framework            & 7.6.5 \\ \hline \rC
    Score-P Analysis Framework &\cC 6.0 \\ \hline
    Spack Package Manger            & 0.15.1 \\ \hline
    \end{tabularx}
    \label{tab:swconf}
\end{table}
\begin{table}[tbp]
    \caption{\memo{refactor/compress for space} Overview of CPU Hardware Architectures, General-purpose Graphics Processing Units (GPU), and Auxiliary Software \memo{add column with ref to section using the HW}}
    \centering\scriptsize
    \begin{tabular}{|c|c|l|}
    \hline \hC
    \tH{\small Type}   & \tH{\small System}   & \tH{\small Hardware/OS Details} \\ \hline
    \multirow{12}{*}{\rotatebox[origin=c]{90}{CPU-based Arch.}}
        & \multirow{6}{*}{x86\_64}
                        &  Supermicro X10DRG-Q Mainboard \\ \cline{3-3}
            &           &\cC 2x Intel Xeon E5-2650v4 \\ \cline{3-3}
            &           & 24 cores (hyper-threading enabled) \\ \cline{3-3}
            &           &\cC \unit[256]{GiB} DDR4 \unit[2400]{MHz} RAM \\ \cline{3-3}
            &           & CentOS Linux (Release 7.8.2003) \\ \cline{3-3}
            &           &\cC Kernel version: 3.10.0-1062 \\ \cline{2-3}
% ABCI DUMP
% in[acc12262dj@es3 ~]$ ia # GO TO INTERACTIVE SESSION ON ABCA
% [acc12262dj@g0242 ~]$ cat /etc/redhat-release 
% CentOS Linux release 7.5.1804 (Core) 
% [acc12262dj@g0242 ~]$ uname -a
% Linux g0242.abci.local 3.10.0-862.el7.x86_64 #1 SMP Fri Apr 20 16:44:24 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
        & \multirow{6}{*}{x86\_64 \memo{ABCI here? --Emil}}
                        & Fujitsu Server Primergy-RX2540-M4 \\ \cline{3-3}
            &           &\cC Intel Xeon Gold 6148 2.4 GHz \\ \cline{3-3}
            &           & 20 Cores (hyper-threading enabled) \\ \cline{3-3}
            &           &\cC \unit[32]{GiB} DDR4 \unit[2666]{MHz} RAM \\ \cline{3-3}
            &           & CentOS Linux (Release 7.5.1804) \\ \cline{3-3}
            &           &\cC Kernel version: 3.10.0-862 \\ \hline\hline
%        & \multirow{6}{*}{A64FX}
%                        & Fujitsu FX1000 compute node \\ \cline{3-3}
%            &           &\cC A64FX \unit[2.2]{GHz} CPU \\ \cline{3-3}
%            &           & 48 compute (+4 auxiliary) core \\ \cline{3-3}
%            &           &\cC \unit[32]{GiB} HBM2 \unit[2400]{MHz} RAM \\ \cline{3-3}
%            &           & Red Hat Enterprise Linux 8.2 \\ \cline{3-3}
%            &           &\cC Kernel version: 4.18.0-193 \\ \hline\hline
    \multirow{6}{*}{\rotatebox[origin=c]{90}{GPUs}}
        & \multirow{6}{*}{NVIDIA}
                        & GTX 1060  \\ \cline{3-3}
            &           &\cC GTX 1080 Ti   \\ \cline{3-3}
            &           & RTX 2070  \\ \cline{3-3}
            &           &\cC RTX 2080 Ti   \\ \cline{3-3}
            &           & Tesla P100-PCIe   \\ \cline{3-3}
            &           &\cC Tesla V100-SXM2 \unit[16]{GiB} \\ \hline\hline \hC
    \tH{\small Type}   & \tH{\small Name}     & \tH{\small Version Details} \\ \hline
    \multirow{9}{*}{\rotatebox[origin=c]{90}{Auxiliary Software}}
        & Intel Parallel Studio XE      & 2019; Update 1 \\ \cline{2-3}
        &\cC Intel Advisor              &\cC 2020; Update 2\footnote{Version 2019 (of Intel's P.S. XE) lacks needed feature for roofline analysis.} \\ \cline{2-3}
        & GNU Compiler Collection       & 8.4.0 \\ \cline{2-3}
        &\cC FUJITSU Software Suite     &\cC 1.2.27b \\ \cline{2-3}
        & NVIDIA CUDA Toolkit           & 10.1 / 10.2 \\ \cline{2-3}
        &\cC NVIDIA CUDA DNN (cuDNN)    &\cC 7.6.5 \\ \cline{2-3}
        & PyTorch ML Framework          & 7.6.5 \\ \cline{2-3}
        &\cC Score-P Analysis Framework &\cC 6.0 \\ \cline{2-3}
        & Spack Package Manger          & 0.15.1 \\ \hline
    \end{tabular}
    \label{tab:hwswconf}
\end{table}
\end{comment}
