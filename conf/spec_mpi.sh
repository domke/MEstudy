#!/bin/bash

export APPDIR="./SPEC_MPI"
export BINARY="104.milc|gnu|mtrain
107.leslie3d|gnu|mtrain
113.GemsFDTD|gnu|mtrain
115.fds4|intel|mtrain
121.pop2|gnu|mtrain
122.tachyon|gnu|mtrain
125.RAxML|gnu|ltrain
126.lammps|gnu|mtrain
127.wrf2|gnu|mtrain
128.GAPgeofem|gnu|mtrain
129.tera_tf|intel|mtrain
130.socorro|intel|mtrain
132.zeusmp2|gnu|mtrain
137.lu|gnu|mtrain
142.dmilc|gnu|ltrain
143.dleslie|gnu|ltrain
145.lGemsFDTD|gnu|ltrain
147.l2wrf2|gnu|ltrain"
export INPUT=""
export NumRunsTEST=3
export NumRunsBEST=10
export MAXTIME="1m"
export RUNSCOREP="yes"
export RUNVTUNE="yes"

if [[ $HOSTNAME = *"${XEONHOST}"* ]]; then
	# on "normal" Xeon
	export TESTCONF="48"
	export BESTCONF="48"
elif [[ $HOSTNAME = *"${IKNLHOST}"* ]]; then
	# on one of the Phi (knl)
	export TESTCONF=""
	export BESTCONF=""
elif [[ $HOSTNAME = *"${IKNMHOST}"* ]]; then
	# on one of the Phi (knm)
	export TESTCONF=""
	export BESTCONF=""
else
	echo "Unsupported host"
	exit
fi
