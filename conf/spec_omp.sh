#!/bin/bash

export APPDIR="./SPEC_OMP"
export BINARY="350.md|gnu|train
351.bwaves|gnu|train
352.nab|gnu|train
357.bt331|intel|train
358.botsalgn|gnu|train
359.botsspar|intel|train
360.ilbdc|gnu|train
362.fma3d|gnu|train
363.swim|gnu|train
367.imagick|gnu|train
370.mgrid331|gnu|train
371.applu331|gnu|train
372.smithwa|gnu|train
376.kdtree|gnu|train"
export INPUT=""
export NumRunsTEST=3
export NumRunsBEST=10
export MAXTIME="1m"
export RUNSCOREP="yes"
export RUNVTUNE="yes"

if [[ $HOSTNAME = *"${XEONHOST}"* ]]; then
	# on "normal" Xeon
	export TESTCONF="48"
	export BESTCONF="48"
elif [[ $HOSTNAME = *"${IKNLHOST}"* ]]; then
	# on one of the Phi (knl)
	export TESTCONF=""
	export BESTCONF=""
elif [[ $HOSTNAME = *"${IKNMHOST}"* ]]; then
	# on one of the Phi (knm)
	export TESTCONF=""
	export BESTCONF=""
else
	echo "Unsupported host"
	exit
fi
