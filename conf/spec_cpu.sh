#!/bin/bash

export APPDIR="./SPEC_CPU"
export BINARY="508.namd_r|gnu|train
510.parest_r|gnu|train
511.povray_r|gnu|train
526.blender_r|gnu|train
527.cam4_r|gnu|train
600.perlbench_s|intel|train
602.gcc_s|gnu|train
603.bwaves_s|gnu|train
605.mcf_s|gnu|train
607.cactuBSSN_s|gnu|train
619.lbm_s|gnu|train
620.omnetpp_s|gnu|train
621.wrf_s|gnu|train
623.xalancbmk_s|gnu|train
625.x264_s|gnu|train
628.pop2_s|gnu|train
631.deepsjeng_s|gnu|train
638.imagick_s|gnu|train
641.leela_s|gnu|train
644.nab_s|gnu|train
648.exchange2_s|gnu|train
649.fotonik3d_s|gnu|train
654.roms_s|gnu|train
657.xz_s|gnu|train"
#526.blender_r|gnu|train	-> seg faults (regardl. compiler and flags)
#627.cam4_s|gnu|train		-> seg faults -> switch to 5*_r works
#628.pop2_s|gnu|train		-> no scorep output for unknow reason -> dummy MPI fuckery
#649.fotonik3d_s|gnu|train	-> no scorep output for unknow reason -> dummy MPI fuckery
#  but leave them in the loop because we have to run vtune too
export INPUT=""
export NumRunsTEST=3
export NumRunsBEST=10
export MAXTIME="1m"
export RUNSCOREP="yes"
export RUNVTUNE="yes"

if [[ $HOSTNAME = *"${XEONHOST}"* ]]; then
	# on "normal" Xeon
	export TESTCONF="48"
	export BESTCONF="48"
elif [[ $HOSTNAME = *"${IKNLHOST}"* ]]; then
	# on one of the Phi (knl)
	export TESTCONF=""
	export BESTCONF=""
elif [[ $HOSTNAME = *"${IKNMHOST}"* ]]; then
	# on one of the Phi (knm)
	export TESTCONF=""
	export BESTCONF=""
else
	echo "Unsupported host"
	exit
fi
