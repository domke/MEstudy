#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
ulimit -s unlimited
ulimit -n 4096
MPIEXECOPT="-genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -host `hostname`"

source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

source $ROOTDIR/dep/intel-advisor/advisor_2020/advixe-vars.sh
VTOP=" -q -collect survey -data-limit=2048 | -q -collect tripcounts -flop -enable-cache-simulation "
VTRO="--report=roofline --data-type=mixed --no-with-stack --memory-level=L1_L2_L3_DRAM --memory-operation-type=all"

# ============================ NICam ==========================================
source conf/nicam.sh
LOG="$ROOTDIR/log/`hostname -s`/profrun/nicam.log"
mkdir -p `dirname $LOG`
cd $APPDIR

# scale down #steps from 11 days to 1 day, and create input data set
sed -i -e 's/^LSMAX  = 0/LSMAX  = 72/'  ../../test.conf
make jobshell > /dev/null 2>&1
if [ -d "$INPUT" ] && [ "x" != "x$INPUT" ]; then cd $INPUT; else exit; fi
export FORT_FMT_RECL=400
ln -s ../../../../bin/nhm_driver .
ln -s ../../../../data/mnginfo/rl00-prc10.info .
ln -s ../../../../data/grid/vgrid/vgrid40_24000-600m.dat .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000000 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000001 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000002 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000003 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000004 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000005 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000006 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000007 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000008 .
ln -s ../../../../data/grid/boundary/gl05rl00pe10/boundary_GL05RL00.pe000009 .

for BEST in $BESTCONF; do
	NumMPI="`echo $BEST | cut -d '|' -f1`"
	NumOMP="`echo $BEST | cut -d '|' -f2`"
	if [ "x$RUNSCOREP" = "xyes" ]; then
		MPIEXECOPT="-genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=true -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
		echo "=== scorep run ===" >> $LOG 2>&1
		echo "mpiexec $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY" >> $LOG 2>&1
		mpiexec $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY >> $LOG 2>&1
		cat ./msg.pe00000 >> $LOG 2>&1
		echo "=== scorep summary ===" >> $LOG 2>&1
		scorep-score -r ${LOG}.scorep/profile.cubex >> $LOG 2>&1
		rm -rf ${LOG}.scorep/
	fi
	if [ "x$RUNVTUNE" = "xyes" ]; then
		mkdir -p ${LOG}.vtune
		MPIEXECOPT="-genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=false -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
		echo "=== vtune run for $BEST ===" >> $LOG 2>&1
		for VTOPNR in `seq 1 2`; do
			echo "mpiexec -gtool \"advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-\$((\$NumMPI-1))\" $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY" >> $LOG 2>&1
			timeout --kill-after=30s --signal=9 240m mpiexec -gtool "advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-$(($NumMPI-1))" $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY >> $LOG 2>&1
			cat ./msg.pe00000 >> $LOG 2>&1
			rm -rf ${LOG}.scorep/
		done
		rm -rf ${LOG}.vtune/pp; mkdir -p ${LOG}.vtune/pp
		for R in `seq 0 $((NumMPI-1))`; do
			echo post process rank=${R}
			advixe-cl $VTRO --mpi-rank=${R} --project-dir=${LOG}.vtune --report-output=${LOG}.vtune/pp/rank${R}.`basename ${LOG}`.html >/dev/null 2>&1
		done
		$ROOTDIR/util/analyze_vtune/html2csv.py ${LOG}.vtune/pp/
	fi
done

# clean up
cd $ROOTDIR
cd $APPDIR
if [ -d "$INPUT" ] && [ "x" != "x$INPUT" ]; then rm -rf $INPUT; fi

cd $ROOTDIR
