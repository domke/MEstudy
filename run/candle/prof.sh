#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
ulimit -s unlimited
ulimit -n 4096

source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

source $ROOTDIR/dep/intel-advisor/advisor_2020/advixe-vars.sh
VTOP=" -q -collect survey -data-limit=2048 | -q -collect tripcounts -flop -enable-cache-simulation "
VTRO="--report=roofline --data-type=mixed --no-with-stack --memory-level=L1_L2_L3_DRAM --memory-operation-type=all"

# ============================ CANDLE =========================================
source conf/candle.sh
source activate idp
LOG="$ROOTDIR/log/`hostname -s`/profrun/candle.log"
mkdir -p `dirname $LOG`
cd $APPDIR
for BEST in $BESTCONF; do
	for BINARY in $BINARYS; do
		NumMPI=1
		NumOMP=$BEST
		pushd "`find . -name $BINARY -exec dirname {} \;`"
		make libssc.so
		# check if data is hot or must be preloaded
		python ./p1b1.py
		if [ "x$RUNSCOREP" = "xyes" ]; then
			MPIEXECOPT="-genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=true -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
			echo "=== scorep run ===" >> $LOG 2>&1
			echo "OMP_NUM_THREADS=$NumOMP KMP_BLOCKTIME=30 KMP_SETTINGS=1 KMP_AFFINITY=\"granularity=fine,compact,1,0\" numactl --preferred 1 python $BINARY $INPUT" >> $LOG 2>&1
			export OMP_NUM_THREADS=$NumOMP; export KMP_BLOCKTIME=30; export KMP_SETTINGS=1; export KMP_AFFINITY="granularity=fine,compact,1,0";
			numactl --preferred 1 python $BINARY $INPUT >> $LOG 2>&1
			echo "=== scorep summary ===" >> $LOG 2>&1
			scorep-score -r ${LOG}.scorep/profile.cubex >> $LOG 2>&1
			rm -rf ${LOG}.scorep/
		fi
		if [ "x$RUNVTUNE" = "xyes" ]; then
			mkdir -p ${LOG}.vtune
			MPIEXECOPT="-genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=false -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
			echo "=== vtune run for $BEST ===" >> $LOG 2>&1
			for VTOPNR in `seq 1 2`; do
				echo "OMP_NUM_THREADS=$NumOMP KMP_BLOCKTIME=30 KMP_SETTINGS=1 KMP_AFFINITY=\"granularity=fine,compact,1,0\" numactl --preferred 1 advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-\$((\$NumMPI-1)) python $BINARY $INPUT" >> $LOG 2>&1
				export OMP_NUM_THREADS=$NumOMP; export KMP_BLOCKTIME=30; export KMP_SETTINGS=1; export KMP_AFFINITY="granularity=fine,compact,1,0";
				numactl --preferred 1 advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-\$((\$NumMPI-1)) python $BINARY $INPUT >> $LOG 2>&1
				rm -rf ${LOG}.scorep/
			done
			rm -rf ${LOG}.vtune/pp; mkdir -p ${LOG}.vtune/pp
			for R in `seq 0 $((NumMPI-1))`; do
				echo post process rank=${R}
				advixe-cl $VTRO --mpi-rank=${R} --project-dir=${LOG}.vtune --report-output=${LOG}.vtune/pp/rank${R}.`basename ${LOG}`.html >/dev/null 2>&1
			done
			$ROOTDIR/util/analyze_vtune/html2csv.py ${LOG}.vtune/pp/
		fi
		popd
	done
done
cd $ROOTDIR
