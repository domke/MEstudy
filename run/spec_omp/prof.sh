#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
ulimit -s unlimited
ulimit -n 4096
source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load gcc@8.4.0
GNUCCHASH="`spack find -lv scorep | /usr/bin/grep '=gcc' | cut -d ' ' -f1`"
INTELHASH="`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1`"

#MPIEXECOPT="-genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -host `hostname`"
SPECCMD="runspec --config=matrixengine.cfg --nobuild --action=run --noreportable"

# ============================ SPEC OMP =======================================
source conf/spec_omp.sh
LOGDIR="$ROOTDIR/log/`hostname -s`/profrun/spec_omp"
LOG="$LOGDIR/spec_omp.log"
mkdir -p `dirname $LOG`
cd $APPDIR
for BENCH in $BINARY; do
	BM="`echo $BENCH | cut -d '|' -f1`"
	COMP="`echo $BENCH | cut -d '|' -f2`"
	SIZE="`echo $BENCH | cut -d '|' -f3`"
	for NumOMP in $BESTCONF; do
		if [ "x$RUNSCOREP" = "xyes" ]; then
			START="`date +%s.%N`"
			echo "=== scorep run for $BENCH ===" >> $LOG 2>&1
			echo "$SPECCMD --size=$SIZE --threads=$NumOMP --define COMP=$COMP --define RESDIR=$LOGDIR/$BM --define FILTER=$ROOTDIR/dep/blas.filter $BM" >> $LOG 2>&1
			if [[ $COMP = *"gnu"* ]]; then HASH=$GNUCCHASH
			else                           HASH=$INTELHASH; fi
			bash -c "spack load scorep/$HASH; source ./shrc; $SPECCMD --size=$SIZE --threads=$NumOMP --define COMP=$COMP --define RESDIR=$LOGDIR/$BM --define FILTER=$ROOTDIR/dep/blas.filter $BM" >> $LOG 2>&1
			ENDED="`date +%s.%N`"
			echo "Total running time: `echo \"$ENDED - $START\" | bc -l`" >> $LOG 2>&1
			echo "=== scorep summary for $BENCH ===" >> $LOG 2>&1
			bash -c "spack load scorep/$HASH; scorep-score -r $LOGDIR/$BM/profile.cubex" >> $LOG 2>&1
			rm -rf $LOGDIR/$BM
			# some run multiple workloads internally
			for WL in `find $LOGDIR -maxdepth 1 -type d -iname "${BM}*" | /usr/bin/grep -v vtune`; do
				bash -c "spack load scorep/$HASH; scorep-score -r $WL/profile.cubex" >> $LOG 2>&1
				rm -rf $WL
			done
		fi
		if [ "x$RUNVTUNE" = "xyes" ]; then
			START="`date +%s.%N`"
			echo "=== vtune run for $BENCH ===" >> $LOG 2>&1
			bash -c "source $ROOTDIR/dep/intel-advisor/advisor_2020/advixe-vars.sh; source ./shrc; $SPECCMD --size=$SIZE --threads=$NumOMP --define COMP=vtune --define RESDIR=$LOGDIR/${BM}.vtune --define SRCDIR=$ROOTDIR/SPEC_OMP/benchspec/OMP2012/$BM/build/build_peak_vtune.0000 --define FILTER=0 $BM" >> $LOG 2>&1
			ENDED="`date +%s.%N`"
			echo "Total running time: `echo \"$ENDED - $START\" | bc -l`" >> $LOG 2>&1
		fi
	done
done
cd $ROOTDIR

