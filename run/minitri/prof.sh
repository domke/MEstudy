#!/bin/bash

ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../../"
cd $ROOTDIR

source $ROOTDIR/conf/host.cfg
source $ROOTDIR/conf/intel.cfg
source $INTEL_PACKAGE intel64 > /dev/null 2>&1
ulimit -s unlimited
ulimit -n 4096
MPIEXECOPT="-genv I_MPI_FABRICS=shm:ofi -genv FI_PROVIDER=sockets -genv I_MPI_HBW_POLICY=hbw_preferred -host `hostname`"

source $ROOTDIR/dep/spack/share/spack/setup-env.sh
spack load scorep/`spack find -lv scorep | /usr/bin/grep '=intel' | cut -d ' ' -f1` 2> /dev/null

source $ROOTDIR/dep/intel-advisor/advisor_2020/advixe-vars.sh
VTOP=" -q -collect survey -data-limit=2048 | -q -collect tripcounts -flop -enable-cache-simulation "
VTRO="--report=roofline --data-type=mixed --no-with-stack --memory-level=L1_L2_L3_DRAM --memory-operation-type=all"

# ============================ miniTri ========================================
source conf/minitri.sh
LOG="$ROOTDIR/log/`hostname -s`/profrun/minitri.log"
mkdir -p `dirname $LOG`
cd $APPDIR
for BEST in $BESTCONF; do
	NumMPI="`echo $BEST | cut -d '|' -f1`"
	NumOMP="`echo $BEST | cut -d '|' -f2`"
	if [ "x$NumMPI" != "x1" ]; then
		BINARY=$BINARYMPI
		INPUT=$INPUTMPI
	else
		BINARY=$BINARYOMP
		INPUT="`echo $INPUTOMP | sed -e \"s/OMPNT/$NumOMP/\"`"
	fi
	if [ "x$RUNSCOREP" = "xyes" ]; then
		MPIEXECOPT="-genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=true -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
		echo "=== scorep run ===" >> $LOG 2>&1
		echo "mpiexec $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY $INPUT" >> $LOG 2>&1
		mpiexec $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY $INPUT >> $LOG 2>&1
		echo "=== scorep summary ===" >> $LOG 2>&1
		scorep-score -r ${LOG}.scorep/profile.cubex >> $LOG 2>&1
		rm -rf ${LOG}.scorep/
	fi
	if [ "x$RUNVTUNE" = "xyes" ]; then
		mkdir -p ${LOG}.vtune
		MPIEXECOPT="-genv I_MPI_VAR_CHECK_SPELLING=0 -genv I_MPI_COMPATIBILITY=4 -genv SCOREP_TOTAL_MEMORY=4089446400 -genv SCOREP_ENABLE_PROFILING=false -genv SCOREP_ENABLE_TRACING=false -genv SCOREP_EXPERIMENT_DIRECTORY=${LOG}.scorep $MPIEXECOPT"
		echo "=== vtune run for $BEST ===" >> $LOG 2>&1
		for VTOPNR in `seq 1 2`; do
			echo "mpiexec -gtool \"advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-\$((\$NumMPI-1))\" $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY $INPUT" >> $LOG 2>&1
			timeout --kill-after=30s --signal=9 240m mpiexec -gtool "advixe-cl `echo $VTOP | cut -d'|' -f${VTOPNR}` -project-dir ${LOG}.vtune --search-dir src:rp=$ROOTDIR/${SRCDIR}:0-$(($NumMPI-1))" $MPIEXECOPT -genv OMP_NUM_THREADS=$NumOMP -n $NumMPI $BINARY $INPUT >> $LOG 2>&1
			rm -rf ${LOG}.scorep/
		done
		rm -rf ${LOG}.vtune/pp; mkdir -p ${LOG}.vtune/pp
		for R in `seq 0 $((NumMPI-1))`; do
			echo post process rank=${R}
			advixe-cl $VTRO --mpi-rank=${R} --project-dir=${LOG}.vtune --report-output=${LOG}.vtune/pp/rank${R}.`basename ${LOG}`.html >/dev/null 2>&1
		done
		$ROOTDIR/util/analyze_vtune/html2csv.py ${LOG}.vtune/pp/
	fi
done
cd $ROOTDIR
